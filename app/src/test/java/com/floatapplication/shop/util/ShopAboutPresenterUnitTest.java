package com.floatapplication.shop.util;

import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.R;
import com.floatapplication.TestApplication;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.shop.util.ShopAboutPresenter.OnShopAboutButtonClickListener;
import com.simplexorg.customviews.fragment.ZoomViewPagerFragment;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by william on 2018-06-06.
 *
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ShopAboutPresenterUnitTest {
    private ShopAboutPresenter presenter;
    @Mock private ZoomViewPagerFragment zoomViewPagerFragment;
    @Mock private ViewPager viewPager;
    @Mock private TabLayout tabDots;
    @Mock private ImageView ownerIcon;
    @Mock private TextView ownerName;
    @Mock private ImageView messageIcon;
    @Mock private ImageView gotoShopIcon;
    @Mock private TextView shopDescription;
    @Mock private Shop shop;
    private List<String> usernames;
    private OnShopAboutButtonClickListener listener;

    @Before
    public void setup() {
        initMocks(this);
        presenter = new ShopAboutPresenter();
        listener = mock(OnShopAboutButtonClickListener.class);
        presenter.setOnShopAboutButtonClickListener(listener);
        usernames = new ArrayList<>();
        usernames.add("Test");
        when(shop.getShopUsernames()).thenReturn(usernames);
        presenter.present(zoomViewPagerFragment, ownerIcon, ownerName,
                gotoShopIcon, messageIcon, shopDescription, shop);
    }

    @Test
    public void verifyCorrectInitialization() {
        verify(viewPager).setAdapter(any(PagerAdapter.class));
        verify(ownerIcon).setOnClickListener(presenter);
        verify(messageIcon).setOnClickListener(presenter);
        verify(shopDescription).setText(any(String.class));
        verify(ownerName).setText(StringUtils.capitalize(usernames.get(0)));
    }

    @Test
    public void onClickOwnerIcon_invokesOwnerIconCallback() {
        View view = mock(View.class);
        when(view.getId()).thenReturn(R.id.owner_icon);
        presenter.onClick(view);
        verify(listener).onOwnerIconClicked();
    }

    @Test
    public void onClickMessageIcon_invokesMessageIconCallback() {
        View view = mock(View.class);
        when(view.getId()).thenReturn(R.id.message_icon);
        presenter.onClick(view);
        verify(listener).onMessageIconClicked();
    }
}
