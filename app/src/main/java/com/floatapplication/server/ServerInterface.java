package com.floatapplication.server;

import com.floatapplication.server.MultipartRequest.DataPart;

import java.util.List;
import java.util.Map;

/**
 * Created by william on 2017/12/8.
 *
 * An interface that provides methods for interacting with the server.
 */

public interface ServerInterface {
    String HOST = "themapapp.win";
    String URL = "https://" + HOST;

    void asyncReq(int method, String path, Map<String, String> headers,
                  ServerCallback callback, ServerErrorCallback errorCallback);

    void asyncReq(int method, String path, Map<String, String> headers, String jsonBody,
                  ServerCallback callback, ServerErrorCallback errorCallback);

    /**
     * Makes an asynchronous binary file upload to the server.
     *
     * @param path the path to send request to.
     * @param headers the headers of the request, can be null.
     * @param data the binary data files to send to the server.
     * @param callback the callback to invoke on successful response received.
     * @param errorCallback the error callback to invoke on error response received.
     */
    void asyncFileUpload(String path, Map<String, String> headers, List<DataPart> data,
                      ServerCallback callback, ServerErrorCallback errorCallback);


}
