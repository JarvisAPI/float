package com.floatapplication.search;


import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.product.models.Product;
import com.floatapplication.search.SearchPageContract.OnItemsLoadedListener;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.util.Extractor;

import java.util.List;
import java.util.Map;

import static com.floatapplication.search.SearchQuery.LIMIT;
import static com.floatapplication.search.SearchQuery.OFFSET;

public class SearchPageModel implements SearchPageContract.Model<Product>,
        ServerCallback, ServerErrorCallback {
    private static final String TAG = SearchPageModel.class.getSimpleName();
    @VisibleForTesting
    int mOffset;
    @VisibleForTesting
    int mLimit;
    private Map<String, String> mQueries;
    private OnItemsLoadedListener<Product> mOnItemsLoadedListener;

    SearchPageModel() {
        mOffset = 0;
        mLimit = 20;
    }

    @Override
    public void setOnItemsLoadedListener(OnItemsLoadedListener<Product> loadedListener) {
        mOnItemsLoadedListener = loadedListener;
    }

    @Override
    public void loadMoreItems() {
        if (mQueries != null) {
            mQueries.put(OFFSET, String.valueOf(mOffset));
            FloatApplication.getHypermediaInterface()
                    .follow(Hypermedia.GET, HypermediaInterface.SEARCH_PRODUCTS,
                            null,
                            null,
                            mQueries,
                            this,
                            this);
        }
    }

    @Override
    public void reload(SearchQuery searchQuery) {
        mOffset = 0;
        mQueries = searchQuery.toQueryParams();
        mQueries.put(OFFSET, String.valueOf(mOffset));
        mQueries.put(LIMIT, String.valueOf(mLimit));
        FloatApplication.getHypermediaInterface()
                .follow(Hypermedia.GET, HypermediaInterface.SEARCH_PRODUCTS,
                        null,
                        null,
                        mQueries,
                        this,
                        this);
    }

    @Override
    public void onSuccessResponse(String response) {
        List<Product> products = Extractor.getInstance().extractProducts(response);
        mOffset += products.size();
        if (mOnItemsLoadedListener != null) {
            mOnItemsLoadedListener.onItemsLoaded(products);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Error: " + error.getMessage());
    }
}
