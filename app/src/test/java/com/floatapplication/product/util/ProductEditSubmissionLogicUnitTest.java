package com.floatapplication.product.util;

import android.content.Context;

import com.floatapplication.FloatApplication;
import com.floatapplication.TestApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.product.models.Product;
import com.floatapplication.product.models.ProductCreator;
import com.floatapplication.product.util.ProductEditSubmissionLogic.ProductEditSubmissionListener;
import com.floatapplication.product.util.ProductEditSubmissionStateMachine.Event;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.util.GlobalHandler;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by william on 2018-06-20.
 * Tests the logic to communicate with the server on editing a product.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ProductEditSubmissionLogicUnitTest {
    private ProductEditSubmissionLogic logic;
    @Mock private Context context;
    @Mock private ProductEditSubmissionStateMachine stateMachine;
    @Mock private GlobalHandler globalHandler;
    @Mock private HypermediaInterface hypermediaInterface;
    @Mock private Product product;
    @Mock private ProductCreator productCreator;
    @Mock private ProductEditSubmissionListener listener;
    private Set<String> imageDeleteUrlSet;
    private String swapMainImageUrl;
    private List<String> newImagePaths;
    private String newMainImagePath;

    private String productName;
    private String productPrice;
    private String productCategory;
    private String productDescription;

    @Before
    public void setup() {
        initMocks(this);
        imageDeleteUrlSet = new HashSet<>();
        newImagePaths = new ArrayList<>();
        setupProductMock();
        logic = new ProductEditSubmissionLogic(context, stateMachine);
        logic.setSubmissionListener(listener);
        verify(stateMachine).bind(logic, logic);
        GlobalHandler.setGlobalHandler(globalHandler);
        FloatApplication.setHypermediaInterface(hypermediaInterface);
    }

    private void setupProductMock() {
        productName = "name";
        productPrice = "10";
        productCategory = "food";
        productDescription = "desc";
        when(product.getProductName()).thenReturn(productName);
        when(product.getProductPrice()).thenReturn(productPrice);
        when(product.getProductCategory()).thenReturn(productCategory);
        when(product.getProductDescription()).thenReturn(productDescription);
    }

    @After
    public void clear() {
        FloatApplication.clear();
    }

    private void submit() {
        logic.submit(product, productCreator, imageDeleteUrlSet,
                swapMainImageUrl, newImagePaths, newMainImagePath);
    }

    @Test
    public void submitExecutes_correctSequence() {
        submit();
        verify(stateMachine).start();
    }

    @Test
    public void uploadProductDetails_followCorrectHyperLink() {
        submit();
        logic.uploadProductDetails();
        verify(hypermediaInterface).authFollow(eq(Hypermedia.PUT), any(Context.class), eq(HypermediaInterface.EDIT_PRODUCT), anyMapOf(String.class, String.class),
                anyString(), any(ServerCallback.class), any(ServerErrorCallback.class));
    }

    @Test
    public void uploadNewImages_uploadImagesProperly() {
        newImagePaths.add("0");
        newImagePaths.add("1");

        submit();
        logic.uploadNewImages();
        ArgumentCaptor<ServerCallback> captor = ArgumentCaptor.forClass(ServerCallback.class);
        verify(hypermediaInterface, times(newImagePaths.size())).uploadImage(any(Context.class), anyString(), anyString(),
                captor.capture(), any(ServerErrorCallback.class));
        ServerCallback callback = captor.getValue();
        callback.onSuccessResponse("once");
        callback.onSuccessResponse("twice");
        verify(stateMachine).run(Event.SUCCESS);
    }

    @Test
    public void uploadNewMainImage_uploadsImageProperly() {
        newMainImagePath = "new path";
        submit();

        logic.uploadNewMainImage();
        verify(hypermediaInterface).uploadImage(any(Context.class), anyString(), eq(newMainImagePath),
                any(ServerCallback.class), any(ServerErrorCallback.class));
    }

    @Test
    public void onSubmitFailed_shouldCallbackListener() {
        logic.onSubmitFailed();
        verify(listener).onSubmitFailure();
    }

    @Test
    public void onSubmitSuccess_shouldCallbackListener() {
        logic.onSubmitSuccess();
        verify(listener).onSubmitSuccess();
    }

    private void verifySwapMainImageJsonBody(String body, String new_main_image_file,
                                             String old_main_image_file) throws Exception {
        JSONObject jsonBody = new JSONObject(body);
        assertEquals(new_main_image_file, jsonBody.get("new_main_image_file"));
        assertEquals(old_main_image_file, jsonBody.get("old_main_image_file"));
    }

    @Test
    public void swapMainImageAutoSelected_shouldSendRequestCorrectly() throws Exception {
        List<String> imageFiles = new ArrayList<>();
        imageFiles.add("0");
        String productMainImageFile = "main-file";
        when(product.getProductImageFiles()).thenReturn(imageFiles);
        when(product.getProductMainImageFile()).thenReturn(productMainImageFile);
        submit();

        logic.swapMainImageAutoSelected();
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(hypermediaInterface).authFollow(eq(Hypermedia.PUT), any(Context.class), eq(HypermediaInterface.SWAP_PRODUCT_MAIN_IMAGE), anyMapOf(String.class, String.class),
                captor.capture(), any(ServerCallback.class), any(ServerErrorCallback.class));

        verifySwapMainImageJsonBody(captor.getValue(), imageFiles.get(0), productMainImageFile);
    }

    @Test
    public void swapMainImageUserSelected_shouldSendRequestCorrectly() throws Exception {
        String productMainImageFile = "main-file";
        when(product.getProductMainImageFile()).thenReturn(productMainImageFile);
        swapMainImageUrl = "url";
        String swapMainImageFile = "file";
        when(globalHandler.constructImageFile(swapMainImageUrl)).thenReturn(swapMainImageFile);
        submit();

        logic.swapMainImageUserSelected();
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(hypermediaInterface).authFollow(eq(Hypermedia.PUT), any(Context.class), eq(HypermediaInterface.SWAP_PRODUCT_MAIN_IMAGE), anyMapOf(String.class, String.class),
                captor.capture(), any(ServerCallback.class), any(ServerErrorCallback.class));

        verifySwapMainImageJsonBody(captor.getValue(), swapMainImageFile, productMainImageFile);
    }

    @Test
    public void deleteImages_deleteProductImagesCorrectly() throws Exception {
        imageDeleteUrlSet.add("image0");
        imageDeleteUrlSet.add("image1");
        String imageFileOnce = "file0";
        String imageFileTwice = "file1";
        Set<String> imageFileSet = new HashSet<>();
        imageFileSet.add(imageFileOnce);
        imageFileSet.add(imageFileTwice);
        when(globalHandler.constructImageFile("image0")).thenReturn(imageFileOnce);
        when(globalHandler.constructImageFile("image1")).thenReturn(imageFileTwice);
        submit();

        logic.deleteImages();
        ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
        verify(hypermediaInterface).authFollow(eq(Hypermedia.PUT), any(Context.class), eq(HypermediaInterface.DELETE_PRODUCT_IMAGES), anyMapOf(String.class, String.class),
                captor.capture(), any(ServerCallback.class), any(ServerErrorCallback.class));
        JSONObject jsonBody = new JSONObject(captor.getValue());
        JSONArray jsonArray = jsonBody.getJSONArray("image_files");
        assertEquals(imageDeleteUrlSet.size(), jsonArray.length());
        assertTrue(imageFileSet.contains(jsonArray.getString(0)));
        assertTrue(imageFileSet.contains(jsonArray.getString(1)));
    }

    // Setup the product creator mock such that it is the same as product.
    private void setupProductCreatorMockSame() {
        when(productCreator.getProductName()).thenReturn(productName);
        when(productCreator.getProductPrice()).thenReturn(productPrice);
        when(productCreator.getProductCategory()).thenReturn(productCategory);
        when(productCreator.getProductDescription()).thenReturn(productDescription);
    }

    private void setupProductCreatorMockDifferent() {
        when(productCreator.getProductName()).thenReturn("productCreator");
        when(productCreator.getProductPrice()).thenReturn("10");
        when(productCreator.getProductCategory()).thenReturn("creator");
        when(productCreator.getProductDescription()).thenReturn("desc");
    }

    @Test
    public void shouldUploadProductDetails_returnFalse_ifProductSameAsProductCreator() {
        setupProductCreatorMockSame();
        submit();

        assertFalse(logic.shouldUploadProductDetails());
    }

    @Test
    public void shouldUploadProductDetails_returnTrue_ifProductNotSameAsProductCreator() {
        setupProductCreatorMockDifferent();
        submit();

        assertTrue(logic.shouldUploadProductDetails());
    }

    @Test
    public void shouldUploadNewImagesIsCorrect() {
        submit();
        assertFalse(logic.shouldUploadNewImages());
        newImagePaths.add("0");

        submit();
        assertTrue(logic.shouldUploadNewImages());
    }

    @Test
    public void shouldChangeMainImageWithNewImageIsCorrect() {
        submit();
        assertFalse(logic.shouldChangeMainImageWithNewImage());

        newMainImagePath = "new-image-path";
        submit();
        assertTrue(logic.shouldChangeMainImageWithNewImage());
    }

    @Test
    public void shouldDeleteMainImageIsCorrect() {
        String mainImageUrl = "main-image-url";
        String mainImageFile = "main-image-file";
        imageDeleteUrlSet.add(mainImageUrl);
        when(globalHandler.constructImageFile(mainImageUrl)).thenReturn(mainImageFile);
        when(product.getProductMainImageFile()).thenReturn(mainImageFile);
        submit();
        assertTrue(logic.shouldDeleteMainImage());

        imageDeleteUrlSet.clear();
        submit();
        assertFalse(logic.shouldDeleteMainImage());
    }

    @Test
    public void shouldSwapMainImageIsCorrect() {
        String swapMainImageFile = "swap-main-image-file";
        String urlVal = "swap-main-image-url";
        when(globalHandler.constructImageFile(urlVal)).thenReturn(swapMainImageFile);

        when(product.getProductMainImageFile()).thenReturn("null");
        submit();
        assertFalse(logic.shouldSwapMainImage());

        swapMainImageUrl = urlVal;
        when(product.getProductMainImageFile()).thenReturn(swapMainImageFile);
        submit();
        assertFalse(logic.shouldSwapMainImage());

        swapMainImageUrl = urlVal;
        when(product.getProductMainImageFile()).thenReturn("main-image-file");
        submit();
        assertTrue(logic.shouldSwapMainImage());
    }

    @Test
    public void shouldDeleteImagesIsCorrect() {
        submit();
        assertFalse(logic.shouldDeleteImages());

        String imageUrl = "url";
        imageDeleteUrlSet.add(imageUrl);
        when(globalHandler.constructImageFile(imageUrl)).thenReturn("image-file");
        submit();
        assertTrue(logic.shouldDeleteImages());
    }

    @Test
    public void noImagesRemainIsCorrect() {
        submit();
        assertTrue(logic.noImagesRemain());

        List<String> imageFiles = new ArrayList<>();
        imageFiles.add("0");
        when(product.getProductImageFiles()).thenReturn(imageFiles);
        submit();
        assertFalse(logic.noImagesRemain());
    }
}

