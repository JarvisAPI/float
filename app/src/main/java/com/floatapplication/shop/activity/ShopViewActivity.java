package com.floatapplication.shop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.map.activity.MapMainActivity;
import com.floatapplication.messages.activity.MessagesActivity;
import com.floatapplication.product.activity.ProductActivity;
import com.floatapplication.product.models.Product;
import com.floatapplication.shop.adapter.ShopViewFragmentPagerAdapter;
import com.floatapplication.shop.fragment.ShopAboutFragment;
import com.floatapplication.shop.fragment.ShopProductsFragment;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.shop.util.ShopAboutPresenter.OnShopAboutButtonClickListener;
import com.floatapplication.shop.util.ShopViewProductPresenter.OnProductClickListener;
import com.floatapplication.user.activity.LoginActivity;
import com.floatapplication.user.activity.UserActivity;
import com.floatapplication.user.models.User;
import com.floatapplication.user.models.User.NamePair;
import com.floatapplication.util.TransitionHandler;
import com.floatapplication.util.Util;
import com.simplexorg.mapfragment.model.GeoPoint;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * The shop that is passed in should have the following things set:
 * - Shop name
 * - Shop description
 * - Shop Id
 */
public class ShopViewActivity extends AppCompatActivity
        implements
        OnShopAboutButtonClickListener,
        OnPageChangeListener,
        OnProductClickListener {
    private static final String TAG = ShopViewActivity.class.getSimpleName();

    public interface ShopProductsOptionMenuListener {
        void onReorderItemsClicked();
    }

    private Shop mShop;
    private TransitionHandler mTransitionHandler;
    private ShopProductsFragment mShopProductsFragment;

    private ViewPager mViewPager;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_view);
        ButterKnife.bind(this);
        mShop = getIntent().getParcelableExtra("shop");
        setupActionBar();
        mTransitionHandler = new TransitionHandler();
        setupViewPager();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTransitionHandler.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        final int SHOP_ABOUT = 0;
        final int SHOP_PRODUCTS = 1;

        switch (mViewPager.getCurrentItem()) {
            case SHOP_PRODUCTS:
                getMenuInflater().inflate(R.menu.activity_shop_view_products, menu);
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_reorder:
                mShopProductsFragment.onReorderItemsClicked();
        }
        return true;
    }

    private void setupActionBar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(mShop.getShopName());
        }
    }

    private void setupViewPager() {
        mViewPager = findViewById(R.id.view_pager);
        mViewPager.addOnPageChangeListener(this);
        ShopViewFragmentPagerAdapter adapter = new ShopViewFragmentPagerAdapter(getFragments(), getSupportFragmentManager());
        mViewPager.setAdapter(adapter);
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(mViewPager);
    }

    private List<Fragment> getFragments() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(makeShopAboutFragment());
        fragments.add(makeShopProductsFragment());
        return fragments;
    }

    private Fragment makeShopProductsFragment() {
        mShopProductsFragment = new ShopProductsFragment();
        Bundle args = new Bundle();
        args.putString("title", getString(R.string.fragment_shop_products_title));
        args.putParcelable("shop", mShop);
        mShopProductsFragment.setArguments(args);
        return mShopProductsFragment;
    }

    private Fragment makeShopAboutFragment() {
        ShopAboutFragment shopAboutFragment = new ShopAboutFragment();
        Bundle args = new Bundle();
        args.putString("title", getString(R.string.fragment_shop_about_title));
        args.putParcelable("shop", mShop);
        shopAboutFragment.setArguments(args);
        return shopAboutFragment;
    }

    @Override
    public void onOwnerIconClicked() {
        if(!mShop.getShopUsernames().isEmpty()) {
            NamePair namePair = mShop.getShopUsernames().get(0);
            Intent data = new Intent();
            data.putExtra(User.USERNAME, namePair.username);
            data.putExtra(User.NAME, namePair.name);
            mTransitionHandler.transition(getApplicationContext(), UserActivity.class, data);
        }
    }

    @Override
    public void onMessageIconClicked() {
        if(!mShop.getShopUsernames().isEmpty() && FloatApplication.getUserSessionInterface().isLoggedIn()) {
            Intent data = new Intent();
            data.putExtra(getString(R.string.contact), mShop.getShopId());
            data.putExtra(getString(R.string.chat_username), FloatApplication.getUserSessionInterface().getUsername());
            mTransitionHandler.transition(this, MessagesActivity.class, data);
        } else if(!FloatApplication.getUserSessionInterface().isLoggedIn()){
            mTransitionHandler.transition(this, LoginActivity.class);
        }
    }

    @Override
    public void onGotoShopLocationIconClicked() {
        Intent data = new Intent();
        data.setAction(MapMainActivity.ACTION_GO_TO_LOCATION);
        GeoPoint location = mShop.getShopLocation();
        if (location != null) {
            data.putExtra("location", location);
            mTransitionHandler.transition(this, MapMainActivity.class, data);
        } else {
            Toast.makeText(this, R.string.error_occurred, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onProductClick(Product product, View itemView) {
        Intent data = new Intent();
        data.putExtra(Util.EXTRA_PRODUCT, product);
        product.setProductShopId(mShop.getShopId());
        data.putExtra(Util.EXTRA_TRANSITION, ViewCompat.getTransitionName(itemView));
        data.putExtra(ProductActivity.INCLUDE_HOME_ICON, false);
        mTransitionHandler.sharedElementTransition(ShopViewActivity.this, ProductActivity.class,
                data, itemView);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        invalidateOptionsMenu();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
