package com.floatapplication.messages.server_connection;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.messages.message_database.ChatUserCache;
import com.floatapplication.messages.message_database.InboxDatabaseHelper;
import com.floatapplication.messages.message_database.MessageDatabaseHelper;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.models.User;
import com.floatapplication.notification.MessageReceiver;
import com.floatapplication.server.ServerInterface;

import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.SmackException.NotConnectedException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat2.Chat;
import org.jivesoftware.smack.chat2.ChatManager;
import org.jivesoftware.smack.chat2.IncomingChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smack.util.TLSUtils;
import org.jivesoftware.smackx.carbons.CarbonCopyReceivedListener;
import org.jivesoftware.smackx.carbons.CarbonManager;
import org.jivesoftware.smackx.carbons.packet.CarbonExtension;
import org.jxmpp.jid.EntityBareJid;
import org.jxmpp.jid.Jid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import de.measite.minidns.DNSClient;
import de.measite.minidns.dnsserverlookup.AndroidUsingExec;


/**
 * The class that attempts to connect with the XMPP chat server
 */
public class ChatConnection implements ConnectionListener , MessageSyncManager.onMessageSyncCompleteListener{


    private static final String TAG = "ChatConnection";
    private final Context mContext;
    private XMPPTCPConnection mConnection;
    private MessageDatabaseHelper mMessageDatabase;
    private InboxDatabaseHelper mInboxDatabase;
    private ArchiveMessageManager mArchiveMessageManager;
    private ChatConnection.ConnectionState sConnectionState;
    private ChatConnection.DialogLoadState sDialogLoadState;
    private ChatUserCache mChatUserCache;
    private ArrayList<CustomMessage> lastMessages;
    private boolean isBindedWithDialogActivity;
    private String currentContactView;
    private String mUsername;
    private String mPassword;
    private MessageSyncManager msm;

    private final String[] ciphers =
            new String[] {"TLS_RSA_WITH_AES_128_GCM_SHA256",
                    "TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256" , "TLS_RSA_WITH_AES_256_GCM_SHA384" ,
                    "TLS_EMPTY_RENEGOTIATION_INFO_SCSV" , "TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA",
                    "TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA" , "TLS_RSA_WITH_AES_256_CBC_SHA",
                    "TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305_SHA256" , "TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256" ,
                    "TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA" , "TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256",
                    "TLS_RSA_WITH_AES_128_CBC_SHA", "TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384",
                    "TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384", "TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA"};


    public enum ConnectionState {
        CONNECTED, AUTHENTICATED, CONNECTING, DISCONNECTED
    }

    public enum DialogLoadState {
        LOADING, LOADED
    }

    public ChatConnection(Context context) {
        Log.d(TAG, "init ChatConnection");
        Log.d(TAG, "ChatConnection Constructor called.");
        isBindedWithDialogActivity = false;
        currentContactView = "";
        mContext = context;
        mChatUserCache = FloatApplication.getFactoryInterface().getChatUserCache();
    }

    public void initXMPPConnection(String jid) throws InterruptedException, XMPPException, SmackException, IOException {
        mUsername = jid;
        mMessageDatabase = FloatApplication.getFactoryInterface().getMessageDatabase(mUsername);
        mInboxDatabase = FloatApplication.getFactoryInterface().getInboxDatabase(mUsername);
        mPassword = FloatApplication.getFactoryInterface().getMessageUserDatabase().getChatToken(jid);
        connect();
    }

    private void connect() throws IOException, XMPPException, SmackException, InterruptedException {
        ArrayList<CustomMessage> lastMessages = mInboxDatabase.getLastMessages();
        msm = FloatApplication.getFactoryInterface().createMessageSyncManager(lastMessages, this);
        msm.fetchServerDialogs();
        XMPPTCPConnectionConfiguration.Builder conf = XMPPTCPConnectionConfiguration.builder()
                .setXmppDomain(ServerInterface.HOST)
                .setHost(ServerInterface.HOST)
                .setSendPresence(true)
                .setPort(5222)
                .setEnabledSSLCiphers(ciphers)
                .setEnabledSSLProtocols(new String[]{TLSUtils.PROTO_TLSV1_2})
                .setResource(FirebaseInstanceId.getInstance().getId())
                .setSecurityMode(ConnectionConfiguration.SecurityMode.required)
                .setCompressionEnabled(true);

        SSLContext SSLContext = getSSLSocketFactory(mContext);
        if(SSLContext != null){
            conf.setCustomSSLContext(SSLContext);
        }

        Log.d(TAG, "Username : " + mUsername);
        Log.d(TAG, "Password : " + mPassword);
        Log.d(TAG, "Server : " + ServerInterface.HOST);
        mConnection = new XMPPTCPConnection(conf.build());
        mConnection.setUseStreamManagement(true);
        mConnection.setUseStreamManagementResumption(true);
        XMPPTCPConnection.setUseStreamManagementResumptionDefault(true);
        XMPPTCPConnection.setUseStreamManagementDefault(true);
        mConnection.addConnectionListener(this);
        initIncomingMessageListener();
        try {
            Log.d(TAG, "Calling connect() ");
            mConnection.connect();
            Log.d(TAG, " login() Called ");
            mConnection.login(mUsername.toLowerCase(), mPassword);
        } catch (Exception e) {
            e.printStackTrace();
        }
        initCarbonMessageManager();
        initReconnectionManager();
    }

    private static SSLContext getSSLSocketFactory(Context context) {
        try(InputStream caInput =
                    context.getResources().openRawResource(R.raw.xmpp_cert)) {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            Certificate ca = cf.generateCertificate(caInput);

            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);
            return sslContext;
        } catch (IOException | CertificateException | KeyStoreException |
                NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void initReconnectionManager() {
        ReconnectionManager reconnectionManager = ReconnectionManager.getInstanceFor(mConnection);
        reconnectionManager.setReconnectionPolicy(ReconnectionManager.ReconnectionPolicy.FIXED_DELAY);
        reconnectionManager.setFixedDelay(3);
        ReconnectionManager.setEnabledPerDefault(true);
        reconnectionManager.enableAutomaticReconnection();
    }


    private void initCarbonMessageManager() throws InterruptedException, XMPPException, SmackException {
        CarbonManager c = CarbonManager.getInstanceFor(mConnection);
        c.enableCarbons();
        c.addCarbonCopyReceivedListener(new CarbonCopyReceivedListener() {
            @Override
            public void onCarbonCopyReceived(CarbonExtension.Direction direction, Message carbonCopy, Message wrappingMessage) {
                if(carbonCopy.getBody() == null){return;}
                Log.d(TAG, "CarbonMessage.toString() :" + carbonCopy.toXML().toString());
                Log.d(TAG, "CarbonMessage.getBody() :" + carbonCopy.getBody());
                Log.d(TAG, "CarbonMessage.getFrom() :" + carbonCopy.getFrom());
                Log.d(TAG, "CarbonMessage.getFrom() :" + carbonCopy.toString());
                String contactId = User.formatJid((carbonCopy.getFrom().toString()));
                ArrayList<String> username = new ArrayList<>();
                username.add(contactId);
                mChatUserCache.updateCachedUsers(username);
                User user;
                ChatConnectionService.MessageType messageType;

                if (contactId.equals(mUsername)) {
                    messageType = ChatConnectionService.MessageType.SEND;

                    user = new User(carbonCopy.getTo().toString());
                } else {
                    messageType = ChatConnectionService.MessageType.RECEIVE;
                    user = new User(carbonCopy.getFrom().toString());
                }

                CustomMessage msg = new CustomMessage(carbonCopy, user, messageType);
                mInboxDatabase.addData(msg);
                mMessageDatabase.addData(msg);

                Intent intent = new Intent(ChatConnectionService.INCOMING_MESSAGE);
                intent.setPackage(mContext.getPackageName());
                intent.putExtra("msg", msg);
                mContext.sendBroadcast(intent);
            }
        });
    }

    private void initIncomingMessageListener() {
        ChatManager.getInstanceFor(mConnection).addIncomingListener(new IncomingChatMessageListener() {
            @Override
            public void newIncomingMessage(EntityBareJid messageFrom, Message message, Chat chat) {
                if(message.getBody() == null) {return;}
                Log.d(TAG, "message.toString() :" + message.toXML().toString());
                Log.d(TAG, "message.getBody() :" + message.getBody());
                Log.d(TAG, "message.getFrom() :" + message.getFrom());
                Log.d(TAG, "message.getTO() :" + message.getTo());
                ArrayList<String> username = new ArrayList<>();
                username.add(message.getFrom().toString());
                mChatUserCache.updateCachedUsers(username);
                User user = new User(messageFrom.toString());
                CustomMessage msg = new CustomMessage(message, user, ChatConnectionService.MessageType.RECEIVE);
                if(!msg.isDelayed()){
                    mInboxDatabase.addData(msg);
                }
                mMessageDatabase.addData(msg);
                Intent intent = new Intent(ChatConnectionService.INCOMING_MESSAGE);
                intent.setPackage(mContext.getPackageName());
                intent.putExtra("msg",msg);
                if(!msg.isDelayed() && !isBindedWithDialogActivity
                        && !currentContactView.equalsIgnoreCase(msg.getUser().getId())){
                    MessageReceiver.requestNotification(mContext, User.formatJid(message.getTo().toString()), msg);
                }
                mContext.sendBroadcast(intent);
            }
        });
    }

    void setIsBindedWithDialogActivity(boolean isBinded) {
        Log.d(TAG, "setting binded with dialog Activity to:" + Boolean.toString(isBinded));
        isBindedWithDialogActivity = isBinded;
    }

    void setCurrentContactView(String contact) {
        Log.d(TAG, "setting current dialog view contact: " + contact);
        currentContactView = contact;
    }

    public CustomMessage sendMessage(String body, String toJid) {
        String receiverJid = toJid + "@" + ServerInterface.HOST;
        Log.d(TAG, "Sending message to :" + receiverJid);
        Jid jid = null;
        try {
            jid = JidCreate.from(receiverJid);
        } catch (XmppStringprepException e) {
            e.printStackTrace();
        }
        if (jid == null) {
            return null;
        }

        ChatManager chatManager = ChatManager.getInstanceFor(mConnection);
        Message message = new Message(jid, Message.Type.chat);
        message.setBody(body);

        Chat chat = chatManager.chatWith(jid.asBareJid().asEntityBareJidIfPossible());

        try {
            chat.send(message);
            Log.d(TAG, message.toXML().toString() + receiverJid);
            User user = new User(message.getTo().toString());
            CustomMessage msg = new CustomMessage(message, user, ChatConnectionService.MessageType.SEND);
            mMessageDatabase.addData(msg);
            mInboxDatabase.addData(msg);
            return msg;

        } catch (NotConnectedException | InterruptedException e) {
            e.printStackTrace();
            try {
                connect();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            return null;
        }
    }

    void disconnect() {
        sConnectionState = ConnectionState.DISCONNECTED;
        Log.d(TAG, "Disconnecting from server " + ServerInterface.HOST);
        if (mConnection != null) {
            mConnection.disconnect();
        }
        mConnection = null;
    }

    public ConnectionState getState() {
        if (sConnectionState == null) {
            return ConnectionState.DISCONNECTED;
        }
        return sConnectionState;
    }

    public ChatConnection.DialogLoadState getsDialogLoadState(){
        if (sDialogLoadState == null){
            return DialogLoadState.LOADING;
        }
        return sDialogLoadState;
    }

    @Override
    public void connected(XMPPConnection connection) {
        sConnectionState = ConnectionState.CONNECTED;
        Log.d(TAG, "Connected Successfully");
        broadcastConnectionStateChange();
    }

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {
        mArchiveMessageManager = FloatApplication.getFactoryInterface().createArchiveMessageManager(mConnection, mUsername);
        msm.setArchiveMessageManager(mArchiveMessageManager);
        Log.d(TAG, "authenticated");
        sConnectionState = ConnectionState.AUTHENTICATED;
        broadcastConnectionStateChange();
        if(!resumed) {
            broadcastDialogStateChange();
            try {
                msm.syncDialogs();
            } catch (Exception e) {
                Log.d(TAG, "dialogs sync failed");
                e.printStackTrace();
            }
        }
        else{
            sDialogLoadState = DialogLoadState.LOADED;
        }
    }

    @Override
    public void onMessageSyncComplete() {
        Log.d(TAG, "dialogs sync successful");
        sDialogLoadState = DialogLoadState.LOADED;
        broadcastDialogStateChange();
    }

    @Override
    public void connectionClosed() {
        sConnectionState = ChatConnection.ConnectionState.DISCONNECTED;
        sDialogLoadState = DialogLoadState.LOADING;
        broadcastDialogStateChange();
        Log.d(TAG, "Connectionclosed()");
        broadcastConnectionStateChange();
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        sConnectionState = ConnectionState.DISCONNECTED;
        Log.d(TAG, "ConnectionClosedOnError, error " + e.toString());
        broadcastConnectionStateChange();

    }

    @Override
    public void reconnectingIn(int seconds) {
        sConnectionState = ConnectionState.CONNECTING;
        Log.d(TAG, "ReconnectingIn() ");

    }

    @Override
    public void reconnectionSuccessful() {
        sConnectionState = ConnectionState.CONNECTED;
        broadcastConnectionStateChange();
        Log.d(TAG, "ReconnectionSuccessful()");
    }

    @Override
    public void reconnectionFailed(Exception e) {
        sConnectionState = ConnectionState.DISCONNECTED;
        Log.d(TAG, "ReconnectionFailed()");
    }

    private void broadcastDialogStateChange() {
        Intent intent = new Intent(ChatConnectionService.DIALOG_LOAD_STATE_CHAGE);
        mContext.sendBroadcast(intent);
    }

    private void broadcastConnectionStateChange() {
        Intent intent = new Intent(ChatConnectionService.CONNECTION_STATE_CHANGE);
        mContext.sendBroadcast(intent);
    }

    public ArchiveMessageManager getArchiveMessageManager(){
        return mArchiveMessageManager;
    }

    public String getUsername() {return mUsername;}
}
