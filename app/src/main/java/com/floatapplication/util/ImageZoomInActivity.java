package com.floatapplication.util;

import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.transition.Fade;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.user.util.UserSessionInterface;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by jamescho on 2018-06-07.
 *
 */

public class ImageZoomInActivity extends AppCompatActivity {

    private String imageUrl;

    @BindView(R.id.zoom_in_main_image)
    ImageView mMainImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("TAG","Oncreate");
        setContentView(R.layout.activity_image_zoom_in);
        ButterKnife.bind(this);
        imageUrl = getIntent().getStringExtra("image_url");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Fade fade = new Fade();
            fade.excludeTarget(android.R.id.statusBarBackground, true);
            fade.excludeTarget(android.R.id.navigationBarBackground, true);
            getWindow().setEnterTransition(fade);
            getWindow().setExitTransition(fade);
        }
        loadImage();
    }

    private void loadImage(){
        FloatApplication.getUserSessionInterface()
                .getUserAccessToken(getApplicationContext(), new UserSessionInterface.TokenCallback() {
                    @Override
                    public void onTokenObtained(String token) {
                        handleGlideTokenCallback(mMainImage,imageUrl,token);
                    }
                    @Override
                    public void onTokenError(String errorMsg) {
                        Log.d("TAG",errorMsg);
                    }
                });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void handleGlideTokenCallback(ImageView imageView, String url, String token){
        if(mMainImage == null)
            Log.d("tag", "mainimage is null!");
        Log.d("tag", url);
        Log.d("tag", token);

        LazyHeaders.Builder builder = new LazyHeaders.Builder()
                .addHeader("Authorization", "Bearer " + token);
        GlideUrl glideUrl = null;
        if (url != null && !url.equals(""))
            glideUrl = new GlideUrl(url, builder.build());
        GlideApp.with(ImageZoomInActivity.this)
                .load(glideUrl)
                .placeholder(R.drawable.default_no_image)
                .into(imageView);
    }


}
