package com.floatapplication.util;

import android.support.annotation.VisibleForTesting;

import java.util.HashMap;
import java.util.Map;

public class IFactory {
    private static IFactory mIFactory;

    private IFactory() {

    }

    public <T, E> Map<T, E> createMap() {
        return new HashMap<>();
    }

    public <T> T create(Class<T> clazz) {
        try {
            return clazz.newInstance();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static IFactory getInstance() {
        if (mIFactory == null) {
            mIFactory = new IFactory();
        }
        return mIFactory;
    }

    @VisibleForTesting
    public static void setIFactory(IFactory iFactory) {
        mIFactory = iFactory;
    }
}
