package com.floatapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.floatapplication.config.ConfigStore;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.map.activity.MapMainActivity;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.user.activity.LoginActivity;
import com.floatapplication.user.util.UserHandler;
import com.floatapplication.util.PhotoHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Try to obtain user tokens before starting main activity_shop_creation.
 */

public class SplashActivity extends AppCompatActivity
        implements ServerCallback, ServerErrorCallback {
    private static final String TAG = SplashActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PhotoHandler.clearImagePaths(this);
        FloatApplication.getHypermediaInterface()
                .init(null,
                        (String response) -> {
                            getConfigFromServer();
                            startMainActivity();
                        }, (VolleyError error) -> {
                            Log.e(TAG, "Volley error message: " + error.getMessage());
                            error.printStackTrace();
                            Toast.makeText(getApplicationContext(), "No Connection", Toast.LENGTH_SHORT).show();
                            startMainActivity();
                        });
    }

    private void getConfigFromServer() {
        FloatApplication.getHypermediaInterface()
                .follow(Hypermedia.GET, Hypermedia.DEFAULT_CONFIG, null, null,
                        (String response) -> {
                            ConfigStore.getInstance().setConfigurations(getApplicationContext(), response);
                        },
                        (VolleyError error) -> {
                            Log.e(TAG, "Volley error message: " + error.getMessage());
                            error.printStackTrace();
                        });
    }

    private void startMainActivity() {
        Intent intent;
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
        String refresh_token = sharedPref.getString(getString(R.string.refresh_token), null);
        String access_token = sharedPref.getString(getString(R.string.access_token), null);
        if (refresh_token == null || access_token == null) {
            boolean skipLogin = sharedPref.getBoolean(getString(R.string.skip_login), false);
            if (skipLogin) {
                intent = new Intent(this, MapMainActivity.class);
            } else {
                intent = new Intent(this, LoginActivity.class);
            }
        } else {
            Log.d(TAG, "Refreshing token on app start up");
            refreshToken(access_token, refresh_token);
            return;
        }
        startActivity(intent);
        finish();
    }

    private void refreshToken(String access_token, String refresh_token) {
        FloatApplication.getUserSessionInterface().setRefreshToken(refresh_token);
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + access_token);
        try {
            String jsonBody = new JSONObject().put("refresh_token", refresh_token).toString();
            FloatApplication.getHypermediaInterface()
                    .follow(Hypermedia.POST, HypermediaInterface.LOGIN, null, headers, jsonBody,
                            this, this);
        } catch (JSONException e) {
            e.printStackTrace();
            finish();
        }
    }

    @Override
    public void onSuccessResponse(String response) {
        UserHandler.refreshToken(this, response, FloatApplication.getUserSessionInterface());
        Intent intent = new Intent(this, MapMainActivity.class);
        FloatApplication.getMessageFeatureHandlerInterface().syncChatUserDatabase();
        FloatApplication.getMessageFeatureHandlerInterface().initXMPPConnection();
        startActivity(intent);
        finish();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.e(TAG, "Unable to refresh token");
        Log.e(TAG, "Volley error: " + error.getMessage());
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }
}
