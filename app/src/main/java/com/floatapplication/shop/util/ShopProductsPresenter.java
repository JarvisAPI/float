package com.floatapplication.shop.util;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

import com.floatapplication.config.ConfigStore;
import com.floatapplication.product.adapter.ProductAdapter.OnBottomReachedListener;
import com.floatapplication.product.models.Product;
import com.floatapplication.shop.adapter.ShopViewAdapter;
import com.floatapplication.shop.util.ShopProductLoader.OnProductLoadedListener;
import com.floatapplication.shop.util.ShopViewProductPresenter.OnProductClickListener;
import com.floatapplication.util.Util;

import java.util.List;

/**
 * Created by william on 2018-06-06.
 * Presents the products in the shop products fragment.
 */

public class ShopProductsPresenter
        implements
        OnBottomReachedListener,
        OnProductLoadedListener {
    private ShopViewAdapter mShopViewAdapter;
    private OnProductClickListener mOnProductClickListener;
    private ShopProductLoader mShopProductLoader;
    private StaggeredGridLayoutManager mLayoutManager;
    private Context mContext;


    public void present(RecyclerView recyclerView, ShopViewAdapter adapter, ShopProductLoader shopProductLoader) {
        mContext = recyclerView.getContext();
        mShopViewAdapter = adapter;
        setupRecyclerView(recyclerView);
        mShopViewAdapter.getPresenter().setOnProductClickListener(mOnProductClickListener);
        mShopProductLoader = shopProductLoader;
        setupShopProductLoader();
    }

    public void setOnProductClickedListener(OnProductClickListener listener) {
        mOnProductClickListener = listener;
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        int spanCount = ConfigStore.getInstance().getProductColumnSpanCount(mContext);
        mLayoutManager = new StaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mShopViewAdapter);
    }


    private void setupShopProductLoader() {
        mShopProductLoader.setListener(this);
        mShopViewAdapter.setOnBottomReachedListener(this);
        mShopProductLoader.getShopProducts();
    }

    @Override
    public void onBottomReached() {
        mShopProductLoader.getShopProducts();
    }

    @Override
    public void onProductLoaded(List<Product> products) {
        mShopViewAdapter.addShopItems(products);
    }


    public void onReorderItemsClicked() {
        Util.getInstance().reorderItems(mContext, mLayoutManager);
    }
}
