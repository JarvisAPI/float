package com.floatapplication.shop.fragment;

import com.floatapplication.TestApplication;
import com.floatapplication.shop.activity.ShopCreationActivity;
import com.simplexorg.mapfragment.marker.BaseMarker;

import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

/**
 * Created by william on 2018-05-11.
 * Tests setting shop location.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ShopSetLocationFragmentUnitTest {
    private ShopCreationActivity activity;
    private ShopSetLocationFragment fragment;
    private BaseMarker mapInterface;
    private BaseMarker markerInterface;

//    @Before
//    public void setup() {
//        markerAnimationFactoryInterface = mock(MarkerAnimationFactoryInterface.class);
//        when(markerAnimationFactoryInterface.createMarkerAnimation(anyLong(), any(MarkerInterface.class), any(Handler.class),
//                any(LatLng.class), any(LatLng.class))).thenReturn(mock(MarkerAnimator.class));
//        mapInterface = mock(MapInterface.class);
//        when(mapInterface.getCameraZoomLevel()).thenReturn(MapMarkerDisplay.MARKER_DISPLAY_ZOOM_LEVEL);
//        when(mapInterface.projectFromScreenLocation(any(Point.class))).thenReturn(new LatLng(0, 0));
//        markerInterface = mock(MarkerInterface.class);
//        when(markerInterface.getPosition()).thenReturn(new LatLng(0, 0));
//        when(mapInterface.addMarker(any(MarkerOptions.class))).thenReturn(markerInterface);
//        FloatApplication.setMarkerAnimationFactoryInterface(markerAnimationFactoryInterface);
//        activity = Robolectric.buildActivity(ShopCreationActivity.class)
//                .create()
//                .start()
//                .resume()
//                .get();
//        fragment = new ShopSetLocationFragment();
//        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
//        transaction.add(fragment, null);
//        transaction.commit();
//    }
//
//    @After
//    public void cleanup() {
//        FloatApplication.clear();
//    }
//
//
//    @Test
//    public void addMarkerToMap_andSetMarker() {
//        Callback testCallback = mock(Callback.class);
//        fragment.setConfirmCallback(testCallback);
//        GeoPoint testPos = new GeoPoint(1, 1);
//        when(markerInterface.getPosition()).thenReturn(testPos);
//        fragment.onResumeFragment(null);
//        View view = fragment.getView();
//        assertNotNull(view);
//
//        view.findViewById(R.id.fab_done).performClick();
//        verify(testCallback).callback();
//        assertEquals(testPos, fragment.getSetShopLocation());
//    }
//
//    @Test
//    public void onResumeFragment_addMarkerShouldOnlyBeCalledOnce() {
//        // Shop location's icon marker shouldn't be shown if it isn't confirmed by the user yet.
//        Bundle data = new Bundle();
//        data.putInt("icon_res", 100);
//        fragment.initializeMapInterface(mapInterface);
//        fragment.onResumeFragment(data);
//        ArgumentCaptor<MarkerOptions> argumentCaptor = ArgumentCaptor.forClass(MarkerOptions.class);
//        verify(mapInterface).addMarker(argumentCaptor.capture());
//        assertNotEquals("Shop Location", argumentCaptor.getValue().getTitle());
//    }
//
//    @Test
//    public void markerIconShouldBeSet_onSetShopIconClick() {
//        Bundle data = new Bundle();
//        data.putInt("icon_res", 100);
//        fragment.initializeMapInterface(mapInterface);
//        fragment.onResumeFragment(data);
//        View view = fragment.getView();
//        assertNotNull(view);
//        FloatingActionButton dropMarkerButton = view.findViewById(R.id.fab_drop);
//        dropMarkerButton.performClick();
//        FloatingActionButton doneButton = view.findViewById(R.id.fab_done);
//        doneButton.performClick();
//        verify(markerInterface).setIcon(intThat(Matchers.not(0)));
//    }
}
