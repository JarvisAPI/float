package com.floatapplication.product.util;

import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Created by william on 2018-06-05.
 *
 */

public class ProductShopQueryQueue {
    private static final String TAG = ProductShopQueryQueue.class.getSimpleName();
    public interface ProductShopQuery {
        void execute();
    }
    static final int MAX_QUERIES = 5; // Max number of product requests to shops at a given instance.
    private List<ProductShopQueryState> mQueryStates;
    private Queue<ProductShopQuery> mQueue;

    private ProductShopQueryFactory mFactory;
    private int mCurrentQueryIndex;

    public ProductShopQueryQueue() {
        mCurrentQueryIndex = 0;
        mQueryStates = new ArrayList<>();
        mQueue = new LinkedList<>();
    }

    /**
     * Add shop to be queried.
     * @param shopId the shop id.
     * @param limit the max number of products to get per query.
     */
    void addShopToQuery(String shopId, int limit) {
        mQueryStates.add(new ProductShopQueryState(shopId, limit));
    }

    /**
     * Remove a shop from the query states.
     * @param shopId the shop id to match in the query states.
     */
    void removeShopFromQuery(@NonNull String shopId) {
        for (int i = 0; i < mQueryStates.size(); i++) {
            if (shopId.equals(mQueryStates.get(i).getShopId())) {
                mQueryStates.remove(i);
                return;
            }
        }
    }

    void loadProductFromShops() {
        if (mQueryStates.size() > 0) {
            int numQueries = Math.min(MAX_QUERIES, mQueryStates.size());
            mCurrentQueryIndex %= mQueryStates.size();
            for (int i = 0; i < numQueries; i++) {
                int idx = (i + mCurrentQueryIndex) % mQueryStates.size();
                enqueueProductQuery(mQueryStates.get(idx));
            }
            mCurrentQueryIndex += numQueries;
            execute();
        }
    }

    private void execute() {
        while (!mQueue.isEmpty()) {
            mQueue.poll().execute();
        }
    }

    private void enqueueProductQuery(final ProductShopQueryState queryState) {
        Log.d(TAG, "enqueueProductQuery");
        mQueue.add(mFactory.create(queryState.getShopId(), queryState.getNextQueryOffset(), queryState.getLimit()));
    }

    public int size() {
        return mQueryStates.size();
    }

    void setProductShopQueryFactory(ProductShopQueryFactory factory) {
        mFactory = factory;
    }
}
