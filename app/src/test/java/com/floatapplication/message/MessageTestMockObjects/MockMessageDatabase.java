package com.floatapplication.message.MessageTestMockObjects;

import com.floatapplication.messages.models.CustomMessage;

import junit.framework.Assert;

import java.util.HashMap;
import java.util.List;

/**
 * Created by jamescho on 2018-06-15.
 */

public class MockMessageDatabase {
    private HashMap<String,List<CustomMessage>> db;

    public MockMessageDatabase(){
        this.db = new HashMap<>();
    }


    public void insertData(List<CustomMessage> messages){
        if(messages.isEmpty()){
            Assert.fail("message list should not be empty something went wrong");
        }
        if(!db.containsKey(messages.get(0).getUser().getId())) {
            db.put(messages.get(0).getUser().getId(), messages);
        }
        else{
            List<CustomMessage> conversation = db.get(messages.get(0).getUser().getId());
            for(CustomMessage msg: messages){
                if(!messages.contains(msg))
                    conversation.add(msg);
            }
            db.put(messages.get(0).getUser().getId(), messages);
        }
    }

    public List<CustomMessage> getConversation(String jid){
        return db.get(jid);
    }

    public boolean conversationExists(String jid){
        return db.containsKey(jid) && !db.get(jid).isEmpty();
    }

}
