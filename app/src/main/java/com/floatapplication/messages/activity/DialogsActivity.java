package com.floatapplication.messages.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.SplashActivity;
import com.floatapplication.messages.chatkit.commons.ImageLoader;
import com.floatapplication.messages.chatkit.dialogs.DialogsList;
import com.floatapplication.messages.chatkit.dialogs.DialogsListAdapter;
import com.floatapplication.messages.chatkit.dialogs.DialogsListAdapter.OnDialogLongClickListener;
import com.floatapplication.messages.message_database.InboxDatabaseHelper;
import com.floatapplication.messages.models.Dialog;
import com.floatapplication.messages.server_connection.ChatConnection;
import com.floatapplication.messages.server_connection.ChatConnectionService;
import com.floatapplication.util.GlideApp;

import java.util.ArrayList;
import java.util.List;

public class DialogsActivity extends AppCompatActivity
        implements DialogsListAdapter.OnDialogClickListener<Dialog>,
        OnDialogLongClickListener<Dialog> {

    private static final String TAG = DialogsActivity.class.getSimpleName();
    private DialogsList mDialogsList;
    protected List<Dialog> mDialogList;
    private String mUsername;
    private InboxDatabaseHelper mInboxDataBase;
    protected DialogsListAdapter<Dialog> mDialogsAdapter;
    private ChatConnectionService mChatConnectionService;
    private boolean mShouldUnbindService;

    private BroadcastReceiver mDialogLoadStateBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkDialogStateAndDisplayDialogs();
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreate()");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_default_dialogs);
        mDialogsList = findViewById(R.id.dialogsList);
        mUsername = getIntent().getStringExtra("username");
        setupBroadCastDialogListUpdate();
        registerStateBroadcastReceiver();
        initAdapter();
        setupActionBar();
    }

    protected ImageLoader imageLoader = (imageView, url) -> GlideApp.with(DialogsActivity.this)
            .load(url)
            .into(imageView);

    private ServiceConnection mChatConnectionServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mChatConnectionService = ((ChatConnectionService.ChatConnectionServiceBinder) iBinder).getService();
            if(!FloatApplication.getUserSessionInterface().isLoggedIn()){
                startActivity(new Intent(getApplicationContext(), SplashActivity.class));
                finish();
                return;
            }
            if(!mChatConnectionService.getState(mUsername).equals(ChatConnection.ConnectionState.AUTHENTICATED)){
                mChatConnectionService.reconnect(mUsername);
            }
            mInboxDataBase = FloatApplication.getFactoryInterface().getInboxDatabase(mUsername);
            mChatConnectionService.setIsBindedWithDialogActivity(mUsername,true);
            checkDialogStateAndDisplayDialogs();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mChatConnectionService = null;
        }
    };

    private void checkDialogStateAndDisplayDialogs(){
        ProgressBar progressBar = findViewById(R.id.progressBar);
        if (mChatConnectionService != null && mChatConnectionService.getLoadState(mUsername).equals(ChatConnection.DialogLoadState.LOADING) && mUsername != null) {
            Log.d(TAG,"dialog loading hiding dialog list");
            mDialogsAdapter.setItems(new ArrayList<>());
            mDialogsAdapter.notifyDataSetChanged();
            progressBar.setVisibility(View.VISIBLE);
        } else{
            Log.d(TAG,"dialog loaded showing dialog list");
            progressBar.setVisibility(View.INVISIBLE);
            updateDialogList();
        }
    }

    private BroadcastReceiver mListPreviewUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, "create mListPreviewUpdateReceiver");
            updateDialogList();
        }
    };

    private void bindService() {
        if (bindService(new Intent(DialogsActivity.this, ChatConnectionService.class),
                mChatConnectionServiceConnection, Context.BIND_AUTO_CREATE)) {
            Log.d(TAG, "bindService()");
            mShouldUnbindService = true;
        } else {
            Log.d(TAG, "Unable to request ChatConnectionService for binding");
        }
    }

    private void unbindService() {
        Log.d(TAG, "unbindService()");
        if (mShouldUnbindService) {
            mChatConnectionService.setIsBindedWithDialogActivity(mUsername,false);
            unbindService(mChatConnectionServiceConnection);
            mShouldUnbindService = false;
        }
    }

    private void registerStateBroadcastReceiver(){
        IntentFilter filter = new IntentFilter(ChatConnectionService.DIALOG_LOAD_STATE_CHAGE);
        registerReceiver(mDialogLoadStateBroadcastReceiver, filter);
    }

    @Override
    public void onDialogClick(Dialog dialog) {
        Log.d(TAG, "onDialogClick()");
        Intent intent = new Intent(this, MessagesActivity.class);
        intent.putExtra(getString(R.string.chat_username), mUsername);
        intent.putExtra(getString(R.string.contact), dialog.getUsers().get(0).getId());
        startActivity(intent);
    }

    private void initAdapter() {
        Log.d(TAG, "initAdapter()");
        mDialogList = new ArrayList<>();
        mDialogsAdapter = new DialogsListAdapter<>(imageLoader);
        mDialogsAdapter.setItems(mDialogList);
        mDialogsAdapter.setOnDialogClickListener(this);
        mDialogsAdapter.setOnDialogLongClickListener(this);
        mDialogsList.setAdapter(mDialogsAdapter);
    }

    protected void updateDialogList() {
        Log.d(TAG, "updateDialogList()");
        mDialogsAdapter.setItems(mInboxDataBase.getDialogs());
    }

    private void setupBroadCastDialogListUpdate() {
        Log.d(TAG, "setupBroadCastDialogListUpdate()");
        IntentFilter filter = new IntentFilter();
        filter.addAction(ChatConnectionService.INCOMING_MESSAGE);
        registerReceiver(mListPreviewUpdateReceiver, filter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }

    @Override
    protected void onResume() {
        Log.d(TAG, "onResume()");
        super.onResume();
        FloatApplication.getMessageFeatureHandlerInterface().initXMPPConnection();
        bindService();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy()");
        super.onDestroy();
        if(mInboxDataBase != null){
            mInboxDataBase.close();
        }
        unregisterReceiver(mListPreviewUpdateReceiver);
        unregisterReceiver(mDialogLoadStateBroadcastReceiver);
    }

    @Override
    protected void onPause() {
        Log.d(TAG, "onPause()");
        super.onPause();
        unbindService();
    }

    @Override
    public void onDialogLongClick(Dialog dialog) {
    }

    private void setupActionBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Messages");
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}
