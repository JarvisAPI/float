package com.floatapplication.messages.server_connection;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.messages.message_database.MessageUserDatabaseHelper;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * This class is initialized and accessed through the FloatApplication
 *
 */

public class MessageFeatureHandler implements MessageFeatureHandlerInterface {
    private final String TAG = "MessageFeatureHandler";
    public interface OnChatUserDatabaseSyncComplete {
        void onSyncComplete();
    }

    private Context mContext;
    private MessageUserDatabaseHelper mMessageUserDatabaseHelper;

    public MessageFeatureHandler(Context context){
        mContext = context;
        mMessageUserDatabaseHelper = new MessageUserDatabaseHelper(context);
    }

    public void addChatUser(String jid, String chatToken){
        mMessageUserDatabaseHelper.addUser(jid,chatToken);
    }


    public void syncChatUserDatabase(){
        Map<String,String> vMap = new HashMap<>();
        vMap.put("username",FloatApplication.getUserSessionInterface().getUsername());
        updateNotificationToken(FloatApplication.getUserSessionInterface().getUsername());
        FloatApplication.getHypermediaInterface().follow(Hypermedia.GET,
                HypermediaInterface.USER_SHOPS, vMap, null, new ServerCallback() {
            @Override
            public void onSuccessResponse(String response) {
                try {
                    JSONObject res = new JSONObject(response);
                    getChatTokens(res.getJSONArray("shops"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new ServerErrorCallback() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });
    }

    private void getChatTokens(JSONArray shops) throws JSONException {
        for(int i = 0 ; i < shops.length(); i++){
            requestShopChatToken(shops.getJSONObject(i).getString("shop_id"));
        }
    }

    private void requestShopChatToken(final String shopId) {
        Map<String, String> queries = new HashMap<>();
        queries.put("shop_id", shopId);
        Log.d(TAG,"getting chatToken for shop: " + shopId);
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.GET,
                mContext, HypermediaInterface.GET_CHAT_TOKEN, queries, new ServerCallback() {
                    @Override
                    public void onSuccessResponse(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            Log.d(TAG,"adding shop token for: " + shopId);
                            updateNotificationToken(shopId);
                            Log.d(TAG,"shop token: " + res.getString("chat_token"));
                            mMessageUserDatabaseHelper.addUser(shopId, res.getString("chat_token"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new ServerErrorCallback() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
    }

    @Override
    public void initXMPPConnection() {
        if(!isChatConnectionRunning(ChatConnectionService.class)) {
            Log.d(TAG,"Chat connection service does not running starting service");
            startXMPPConnection();
        }
    }

    private void startXMPPConnection(){
        Intent intent = new Intent(mContext, ChatConnectionService.class);
        mContext.startService(intent);
    }

    private void updateNotificationToken(String jid){
        Map<String, String> variables = new HashMap<>();
        Map<String, String> queries = new HashMap<>();
        if(!FloatApplication.getUserSessionInterface().getUsername().equals(jid)){
            Log.d(TAG,"adding shop_id query for: " + jid);
            queries.put("shop_id", jid);
        }
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG,"setting notification token for: " + jid);
        Log.d(TAG, "NOTE TOKEN:" + token);
        variables.put(ChatConnectionService.JSON_NOTIFICATION_TOKEN_KEY, token);
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.POST,
                mContext, Hypermedia.UPDATE_CHAT_NOTIFICATION_TOKEN, variables, queries, null,
                new ServerCallback() {
                    @Override
                    public void onSuccessResponse(String response) {
                        // nothing needs to be done
                    }
                },
                new ServerErrorCallback() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
    }

    private boolean isChatConnectionRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) mContext.getSystemService(Context.ACTIVITY_SERVICE);
        if(manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void flushNotificationToken(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d(TAG,"deleting token");
                    FirebaseInstanceId.getInstance().deleteInstanceId();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
