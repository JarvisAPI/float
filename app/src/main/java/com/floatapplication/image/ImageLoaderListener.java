package com.floatapplication.image;

/**
 * Created by william on 2018-12-28.
 * Listener for the image loading progress.
 */

public interface ImageLoaderListener {
    /**
     * Called when the image is successfully loaded.
     */
    void onImageLoaded();

    /**
     * Called when there is an error when loading the image.
     */
    void onImageLoadingError();
}
