package com.floatapplication.message.message_database;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.floatapplication.messages.message_database.MessageDatabaseHelper;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.models.User;
import com.floatapplication.messages.server_connection.ChatConnectionService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.jxmpp.stringprep.XmppStringprepException;

import static junit.framework.Assert.assertNotNull;

/**
 * Created by jamescho on 2018-04-30.
 *
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class MessageDatabaseHelperTest {
    private MessageDatabaseHelper mMessageDatabaseHelper;

    @Before
    public void setUp(){
        mMessageDatabaseHelper = new MessageDatabaseHelper(InstrumentationRegistry.getTargetContext(), "");
    }

    @After
    public void finish() {
        mMessageDatabaseHelper.close();
    }

    @Test
    public void testPreConditions() {
        assertNotNull(mMessageDatabaseHelper);
    }

    @Before
    public void testInsertUser1Data() throws XmppStringprepException {
        org.jivesoftware.smack.packet.Message m1 = new org.jivesoftware.smack.packet.Message();
        String b1 = "text1";
        m1.setBody(b1);
        User u1 = new User("test");

        org.jivesoftware.smack.packet.Message m2 = new org.jivesoftware.smack.packet.Message();
        String b2 = "text2";
        m1.setBody(b1);
        User u2 = new User("test");

        org.jivesoftware.smack.packet.Message m3 = new org.jivesoftware.smack.packet.Message();
        String b3 = "hi this is a test";
        m1.setBody(b1);
        User u3 = new User("test");

        org.jivesoftware.smack.packet.Message m4 = new org.jivesoftware.smack.packet.Message();
        String b4 = "hi this is a test";
        m1.setBody(b1);
        User u4 = new User("test");

        org.jivesoftware.smack.packet.Message m5 = new org.jivesoftware.smack.packet.Message();
        String b5 = "hi this is a test";
        m1.setBody(b1);
        User u5 = new User("test");

        org.jivesoftware.smack.packet.Message m6 = new org.jivesoftware.smack.packet.Message();
        String b6 = "hi this is a test";
        m1.setBody(b1);
        User u6 = new User("test");

        org.jivesoftware.smack.packet.Message m7 = new org.jivesoftware.smack.packet.Message();
        String b7 = "hi this is a test";
        m1.setBody(b1);
        User u7 = new User("test");

        org.jivesoftware.smack.packet.Message m8 = new org.jivesoftware.smack.packet.Message();
        String b8 = "hi this is a test";
        m1.setBody(b1);
        User u8 = new User("test");

        org.jivesoftware.smack.packet.Message m9 = new org.jivesoftware.smack.packet.Message();
        String b9 = "hi this is a test";
        m1.setBody(b1);
        User u9 = new User("test");

        org.jivesoftware.smack.packet.Message m10 = new org.jivesoftware.smack.packet.Message();
        String b10 = "hi this is a test";
        m1.setBody(b1);
        User u10 = new User("test");


        CustomMessage am1 = new CustomMessage(m1, u1, ChatConnectionService.MessageType.SEND);
        CustomMessage am2 = new CustomMessage(m2, u2, ChatConnectionService.MessageType.RECEIVE);
        CustomMessage am3 = new CustomMessage(m3, u3, ChatConnectionService.MessageType.RECEIVE);
        CustomMessage am4 = new CustomMessage(m4, u4, ChatConnectionService.MessageType.RECEIVE);
        CustomMessage am5 = new CustomMessage(m5, u5, ChatConnectionService.MessageType.SEND);
        CustomMessage am6 = new CustomMessage(m6, u6, ChatConnectionService.MessageType.RECEIVE);
        CustomMessage am7 = new CustomMessage(m7, u7, ChatConnectionService.MessageType.RECEIVE);
        CustomMessage am8 = new CustomMessage(m8, u8, ChatConnectionService.MessageType.SEND);
        CustomMessage am9 = new CustomMessage(m9, u9, ChatConnectionService.MessageType.RECEIVE);
        CustomMessage am10 = new CustomMessage(m10, u10, ChatConnectionService.MessageType.SEND);
    }
}