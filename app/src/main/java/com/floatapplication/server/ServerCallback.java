package com.floatapplication.server;

/**
 * Created by william on 2017/12/8.
 *
 */

public interface ServerCallback {
    void onSuccessResponse(String response);
}
