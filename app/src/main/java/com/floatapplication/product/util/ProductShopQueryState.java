package com.floatapplication.product.util;

/**
 * Created by william on 2018-05-28.
 * This class stores the querying state that we are in for a particular shop.
 * For example querying the shop with id '0' currently on offset 0 and limit 10.
 */
class ProductShopQueryState {
    private String mShopId;
    private int mOffset;
    private int mLimit;

    ProductShopQueryState(String shopId, int limit) {
        mShopId = shopId;
        mOffset = 0;
        mLimit = limit;
    }

    String getShopId() {
        return mShopId;
    }

    int getNextQueryOffset() {
        int offset = mOffset;
        mOffset += mLimit;
        return offset;
    }

    int getLimit() {
        return mLimit;
    }
}
