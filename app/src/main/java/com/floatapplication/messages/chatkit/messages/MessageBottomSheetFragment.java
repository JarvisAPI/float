package com.floatapplication.messages.chatkit.messages;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.floatapplication.R;
import com.floatapplication.util.GlobalHandler;
import com.floatapplication.util.PhotoHandler;

import java.util.ArrayList;
import java.util.List;

import static android.app.Activity.RESULT_OK;

public class MessageBottomSheetFragment extends BottomSheetDialogFragment {
    public interface OnPhotosObtainedListener {
        void onPhotosObtained(List<String> imagesEncodedList);
    }
    public static final String TAG = MessageBottomSheetFragment.class.getSimpleName();
    public static final int REQUEST_IMAGE_GALLERY = 11;
    public static final int REQUEST_READ_EXTERNAL_PERMISSION = 12;

    private View mRootView;
    PhotoHandler mPhotoHandler;
    List<String> imagesEncodedList;
    private OnPhotosObtainedListener mOnPhotosObtainedListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = getActivity();
        if (activity instanceof OnPhotosObtainedListener) {
            mOnPhotosObtainedListener = (OnPhotosObtainedListener) activity;
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mPhotoHandler = new PhotoHandler();
        mRootView = inflater.inflate(R.layout.fragment_bottom_sheet_send_photo, container, false);
        setTakePhotoTab();
        setAddPhotoTab();
        setAddCancelTab();
        return mRootView;
    }

    private void setAddPhotoTab() {
        mRootView.findViewById(R.id.get_photo_tab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity() != null) {
                    if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {
                        Log.d(TAG, "Requesting permission");
                        requestPermission();
                    } else {
                        Log.d(TAG, "dispatchUploadImageIntent()");
                        dispatchUploadImageIntent();
                    }
                }
            }
        });
    }

    private void setAddCancelTab() {
        final MessageBottomSheetFragment fragment = this;
        mRootView.findViewById(R.id.cancel_tab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment.dismissAllowingStateLoss();
            }
        });
    }

    private void requestPermission() {
        if (getActivity() != null) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_READ_EXTERNAL_PERMISSION);
        }
    }

    private void setTakePhotoTab() {
        if (getContext() != null) {
            PackageManager pm = getContext().getPackageManager();
            if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                mRootView.findViewById(R.id.take_photo_tab).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (getActivity() != null) {
                            mPhotoHandler.dispatchTakePhotoIntent(getActivity());
                        }
                    }
                });
            } else {
                mRootView.findViewById(R.id.take_photo_tab).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getContext(), "Camera Not Available!", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_EXTERNAL_PERMISSION: {
                Log.d(TAG," REQUEST_READ_EXTERNAL_PERMISSION()");
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG," dispatchUploadImageIntent()");
                    dispatchUploadImageIntent();
                }
            }
        }
    }

    private void dispatchUploadImageIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        getActivity().startActivityForResult(intent, REQUEST_IMAGE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult ");
        if (requestCode == PhotoHandler.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            handleImageCaptureResult();
        } else if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == RESULT_OK) {
            handleImageGalleryResult(data);
        }
        this.dismissAllowingStateLoss();
    }

    private void handleImageCaptureResult() {
        String currentPhotoPath = mPhotoHandler.getCurrentPhotoPath();
        if (currentPhotoPath != null) {
            Log.d(TAG, "Current photo path: " + currentPhotoPath);
            imagesEncodedList = new ArrayList<>();
            imagesEncodedList.add(currentPhotoPath);
            mOnPhotosObtainedListener.onPhotosObtained(imagesEncodedList);
        } else {
            Toast.makeText(getContext(), "SomeThing went wrong try again later!", Toast.LENGTH_SHORT).show();
        }
    }

    private void handleImageGalleryResult(Intent data) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        imagesEncodedList = new ArrayList<>();
        Log.d(TAG, "data is not null");
        if (data.getClipData() != null && getActivity() != null) {
            ClipData mClipData = data.getClipData();
            ArrayList<Uri> mArrayUri = new ArrayList<>();
            for (int i = 0; i < mClipData.getItemCount(); i++) {
                ClipData.Item item = mClipData.getItemAt(i);
                Uri uri = item.getUri();
                mArrayUri.add(uri);
                // Get the cursor
                Cursor cursor = getActivity().getContentResolver()
                        .query(uri, filePathColumn, null, null, null);
                // Move to first row
                if (cursor != null) {
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imageEncoded = cursor.getString(columnIndex);
                    imagesEncodedList.add(imageEncoded);
                    cursor.close();
                }
            }
            mOnPhotosObtainedListener.onPhotosObtained(imagesEncodedList);
            Log.d(TAG, imagesEncodedList.toString());
            Log.d(TAG, "Selected Images " + mArrayUri.size());
        }
        else if(data.getData() != null && getActivity() != null){
            String path = GlobalHandler.getInstance().getAbsolutePathFromURI(getActivity(), data.getData());
            imagesEncodedList.add(path);
            mOnPhotosObtainedListener.onPhotosObtained(imagesEncodedList);
        }
    }
}

