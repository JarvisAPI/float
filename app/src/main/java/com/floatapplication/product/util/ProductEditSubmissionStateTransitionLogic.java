package com.floatapplication.product.util;

/**
 * Created by william on 2018-06-19.
 *
 */

public interface ProductEditSubmissionStateTransitionLogic {
    boolean shouldUploadProductDetails();
    boolean shouldUploadNewImages();
    boolean shouldChangeMainImageWithNewImage();
    boolean shouldDeleteMainImage();
    boolean shouldSwapMainImage();
    boolean shouldDeleteImages();
    boolean noImagesRemain();
}
