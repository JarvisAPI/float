package com.floatapplication.server;

import com.floatapplication.server.MultipartRequest.DataPart;

import java.util.List;
import java.util.Map;

/**
 * Created by william on 2017/12/8.
 *
 */
public class ServerContact implements ServerInterface {
    private final AppControlInterface mAppControl;

    public ServerContact(AppControlInterface appControl) {
        mAppControl = appControl;
    }

    @Override
    public void asyncReq(int method, String path, Map<String, String> headers,
                          ServerCallback callback, ServerErrorCallback errorCallback) {
        String url = URL + path;
        CustomStringRequest strRequest = new CustomStringRequest(method, url, callback,
                errorCallback, headers);
        mAppControl.addToRequestQueue(strRequest);
    }

    @Override
    public void asyncReq(int method, String path, Map<String, String> headers, String jsonBody,
                         ServerCallback callback, ServerErrorCallback errorCallback) {
        String url = URL + path;
        CustomStringRequest strRequest = new CustomStringRequest(method, url, callback,
                errorCallback, headers);
        strRequest.setRequestBody(jsonBody);
        mAppControl.addToRequestQueue(strRequest);
    }

    @Override
    public void asyncFileUpload(String path, Map<String, String> headers, List<DataPart> dataParts,
                                ServerCallback callback, ServerErrorCallback errorCallback) {
        String url = URL + path;
        MultipartRequest multipartRequest = new MultipartRequest(url, headers, dataParts,
                callback, errorCallback);
        mAppControl.addToRequestQueue(multipartRequest);
    }


}
