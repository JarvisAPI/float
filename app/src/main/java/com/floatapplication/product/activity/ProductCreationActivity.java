package com.floatapplication.product.activity;

import android.Manifest.permission;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images.Media;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;
import android.widget.Toast;

import com.floatapplication.R;
import com.floatapplication.config.ConfigStore;
import com.floatapplication.image.activity.ImageScrollActivity;
import com.floatapplication.product.models.ProductCreator;
import com.floatapplication.product.util.ProductCreationCategoryItemPresenter;
import com.floatapplication.util.GlobalHandler;
import com.floatapplication.util.PhotoHandler;
import com.simplexorg.customviews.adapter.SimpleImageItemAdapter;
import com.simplexorg.customviews.adapter.SimpleImageItemAdapter.ImageItemsListener;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

/**
 *
 * Activity to create/edit a product.
 */
public class ProductCreationActivity extends AppCompatActivity
        implements
        OnClickListener,
        ImageItemsListener {
    private static final String TAG = ProductCreationActivity.class.getSimpleName();
    private static final int REQUEST_IMAGE_SCROLL = 10;
    private static final int REQUEST_IMAGE_GALLERY = 11;
    private static final int REQUEST_READ_EXTERNAL_PERMISSION = 12;

    private ProductCreator mProductCreator;

    private PhotoHandler mPhotoHandler;
    private SimpleImageItemAdapter mProductImageAdapter;
    private TextView mMainImageLabel;
    private MenuItem mDoneButton;
    private boolean mStartingImageScroll;
    private ProductCreationCategoryItemPresenter mPresenter;

    private TextInputEditText mProductNameText;
    private TextInputEditText mProductPriceText;
    private TextInputEditText mProductDescriptionText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_creation);
        mStartingImageScroll = false;
        obtainViews();
        setupActionBar();
        mProductCreator = new ProductCreator();
        mPhotoHandler = new PhotoHandler();
        setListeners();
        setupProductImageAdapter();
        setupImageRecyclerView();
        setupProductCategories();
    }

    private void setupProductCategories() {
        mPresenter = new ProductCreationCategoryItemPresenter(this);
        GridLayout layout = findViewById(R.id.product_categories_grid_layout);
        mPresenter.bind(layout, getResources().getStringArray(R.array.product_categories_array));
        mPresenter.present();
    }

    private void obtainViews() {
        mProductNameText = findViewById(R.id.input_product_name);
        mProductPriceText = findViewById(R.id.input_product_price);
        mProductDescriptionText = findViewById(R.id.input_product_description);
        mMainImageLabel = findViewById(R.id.main_image_label);
    }

    private void setupProductImageAdapter() {
        mProductImageAdapter = SimpleImageItemAdapter.newInstance(this);
        mProductImageAdapter.setImageItemsListener(this);
    }

    private void setListeners() {
        findViewById(R.id.photo_button).setOnClickListener(this);
        findViewById(R.id.image_button).setOnClickListener(this);
        TextView mainImageLabel = findViewById(R.id.main_image_label);
        mainImageLabel.setOnClickListener((View view) -> {
            new SimpleTooltip.Builder(view.getContext())
                    .anchorView(view)
                    .text(R.string.product_edit_main_image_icon_tooltip)
                    .textColor(Color.WHITE)
                    .gravity(Gravity.END)
                    .animated(true)
                    .transparentOverlay(false)
                    .build()
                    .show();
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_READ_EXTERNAL_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    dispatchUploadImageIntent();
                }
            }
        }
    }

    private void dispatchUploadImageIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, Media.INTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_IMAGE_GALLERY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PhotoHandler.REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                String currentPhotoPath = mPhotoHandler.getCurrentPhotoPath();
                if (currentPhotoPath != null) {
                    Log.d(TAG, "Current photo path: " + currentPhotoPath);
                    mProductImageAdapter.addImagePath(currentPhotoPath);
                }
            }
        } else if (requestCode == REQUEST_IMAGE_SCROLL) {
            mStartingImageScroll = false;
            if (resultCode == RESULT_OK) {
                processImageScrollResult(data);
            }
        } else if (requestCode == REQUEST_IMAGE_GALLERY) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                if (selectedImage != null) {
                    String path = GlobalHandler.getInstance().getAbsolutePathFromURI(this, selectedImage);
                    if (!mProductImageAdapter.containsImagePath(path)) {
                        mProductImageAdapter.addImagePath(path);
                    } else {
                        Toast.makeText(this, getString(R.string.image_already_added), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }
    }

    /**
     * Request permission to read external storage.
     */
    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{permission.READ_EXTERNAL_STORAGE},
                REQUEST_READ_EXTERNAL_PERMISSION);
    }

    /**
     * After image scroll activity images might have been deleted so we must update.
     *
     * @param data the data returned from image scroll activity.
     */
    private void processImageScrollResult(Intent data) {
        List<String> imagePaths = data.getStringArrayListExtra(ImageScrollActivity.IMAGE_PATHS);
        for (String path : imagePaths) {
            mProductImageAdapter.removeImagePath(path);
        }
    }

    private void setupImageRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(llm);
        recyclerView.setAdapter(mProductImageAdapter);
    }

    private void setupActionBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.product_edit_title);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.confirm_menu, menu);
        mDoneButton = menu.findItem(R.id.action_done);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                onComplete();
                return true;
            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }

    private void onComplete() {
        if (validateProductInfo()) {
            if (mDoneButton != null) {
                mDoneButton.setVisible(false);
            }
            fillProductCreator();
            Intent intent = new Intent();
            intent.putExtra("data", mProductCreator);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private void fillProductCreator() {
        String productName = mProductNameText.getText().toString();
        String productPrice = mProductPriceText.getText().toString();
        String productDescription = mProductDescriptionText.getText().toString();
        mProductCreator.setProductName(productName);
        mProductCreator.setProductPrice(productPrice);
        mProductCreator.setProductDescription(productDescription);
        if (mProductImageAdapter.getItemCount() > 0) {
            mProductCreator.setMainImagePath(mProductImageAdapter.getMainImagePath());
            if (mProductImageAdapter.getItemCount() > 1) {
                mProductCreator.setImagePaths(mProductImageAdapter.getSubImagePaths());
            }
        }
        mProductCreator.genProductId();
        String category = mPresenter.getSelectedCategory();
        if (category != null) {
            mProductCreator.setProductCategory(category.toLowerCase());
        }
    }

    private boolean validateProductInfo() {
        boolean noError = true;
        mProductNameText.setError(null);
        mProductPriceText.setError(null);
        if (StringUtils.isEmpty(mProductNameText.getText().toString())) {
            mProductNameText.setError("Required");
            noError = false;
        }
        if (StringUtils.isEmpty(mProductPriceText.getText().toString())) {
            mProductPriceText.setError("Required");
            noError = false;
        }
        return noError;
    }

    @Override
    public void onImageCountChange(int itemCount) {
        if (itemCount > 0) {
            if (mMainImageLabel.getVisibility() == View.GONE) {
                mMainImageLabel.setVisibility(View.VISIBLE);
            }
        } else {
            if (mMainImageLabel.getVisibility() == View.VISIBLE) {
                mMainImageLabel.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onImageClick(String imagePath) {
        if (mStartingImageScroll) {
            return;
        }
        mStartingImageScroll = true;
        Intent intent = new Intent(this, ImageScrollActivity.class);
        List<String> imagePaths = mProductImageAdapter.getImagePaths();
        intent.putStringArrayListExtra(ImageScrollActivity.IMAGE_PATHS, new ArrayList<>(imagePaths));
        intent.putExtra(ImageScrollActivity.SELECTED_IMAGE_PATH, imagePath);
        startActivityForResult(intent, REQUEST_IMAGE_SCROLL);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.photo_button:
            case R.id.image_button:{
                if (mProductImageAdapter.getItemCount() < ConfigStore.getInstance().getMaxProductImageUploads(this)) {
                    if (id == R.id.photo_button) {
                        PackageManager pm = getPackageManager();
                        if (pm.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
                            mPhotoHandler.dispatchTakePhotoIntent(ProductCreationActivity.this);
                        } else {
                            Toast.makeText(getApplicationContext(), "Camera Not Available!", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        if (ContextCompat.checkSelfPermission(getApplicationContext(), permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {
                            Log.d(TAG, "Requesting permission");
                            requestPermission();
                        } else {
                            dispatchUploadImageIntent();
                        }
                    }
                } else {
                    Toast.makeText(this, getString(R.string.image_limit_reached), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}