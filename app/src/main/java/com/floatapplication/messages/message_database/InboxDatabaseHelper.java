package com.floatapplication.messages.message_database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;
import android.util.Log;

import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.models.Dialog;
import com.floatapplication.messages.models.User;
import com.floatapplication.messages.server_connection.ChatConnectionService;

import java.util.ArrayList;
import java.util.List;


public class InboxDatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "InboxDatabaseHelper";

    private final String inboxJid;

    static final String TABLE_NAME = "inbox";
    static final String TABLE_PRIM_KEY_CLIENT_JID = "client_jid";
    static final String TABLE_PRIM_KEY_BARE_PEER = "bare_peer";
    private final String COL_LAST_MESSAGE_TEXT = "last_message_text";
    private final String COL_LAST_MESSAGE_TIME_STAMP = "last_message_time_stamp"; // Unix time stamp in milliseconds
    private final String COL_UNREAD_COUNT = "unread_count";
    private final String COL_MESSAGE_ID = "message_id";
    private final String COL_ARCHIVE_ID = "archive_id";
    private final int TABLE_PRIM_KEY_COL_NUM = 0;
    private final int TABLE_PRIM_KEY2_COL_NUM = 1;
    private final int COL_LAST_MESSAGE_TEXT_NUM = 2;
    private final int COL_LAST_MESSAGE_TIME_STAMP_NUM = 3;
    private final int COL_UNREAD_COUNT_NUM = 4;
    private final int COL_MESSAGE_ID_NUM = 5;
    private final int COL_ARCHIVE_ID_NUM = 6;

    private final String NULL_ARCHIVE_ID = "-1";

    public InboxDatabaseHelper(Context context, String clientJid) {
        super(context, TABLE_NAME, null, 1);
        inboxJid = clientJid;
        onCreate(getWritableDatabase());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG,"oncreate() inbox database helper");
        String createTable = "CREATE TABLE IF NOT EXISTS "
                + TABLE_NAME
                + "(" + TABLE_PRIM_KEY_CLIENT_JID + " TEXT,"
                + TABLE_PRIM_KEY_BARE_PEER +" TEXT,"
                + COL_LAST_MESSAGE_TEXT +" TEXT,"
                + COL_LAST_MESSAGE_TIME_STAMP +" INT,"
                + COL_UNREAD_COUNT + " INT,"
                + COL_MESSAGE_ID   + " TEXT,"
                + COL_ARCHIVE_ID   + " TEXT,"
                +"PRIMARY KEY("+TABLE_PRIM_KEY_CLIENT_JID +"," + TABLE_PRIM_KEY_BARE_PEER +")"
                //   + COL4 +" TEXT" TODO add picture
                + ")";
        Log.d(TAG,createTable);
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        Log.d(TAG,"onUpgrade()");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    /**
     * Add data to table
     */
    public void addData(CustomMessage message) {
        Cursor curs = null;
        try {
            Log.d(TAG, "addData()");
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(TABLE_PRIM_KEY_CLIENT_JID, inboxJid);
            contentValues.put(TABLE_PRIM_KEY_BARE_PEER, message.getUser().getId());
            contentValues.put(COL_LAST_MESSAGE_TEXT, message.getText());
            contentValues.put(COL_LAST_MESSAGE_TIME_STAMP, message.getCreatedAt().getTime());
            contentValues.put(COL_MESSAGE_ID, message.getId());
            String archiveId = message.getArchiveId();
            if (archiveId != null) {
                contentValues.put(COL_ARCHIVE_ID, archiveId);
            } else {
                contentValues.put(COL_ARCHIVE_ID, NULL_ARCHIVE_ID);
            }

            String query = "SELECT * FROM " + TABLE_NAME
                    + " WHERE " + TABLE_PRIM_KEY_CLIENT_JID  + "=" + getSQLString(inboxJid)
                    + " AND "   + TABLE_PRIM_KEY_BARE_PEER + "=" + getSQLString(message.getUser().getId());

            curs = db.rawQuery(query, null);
            int unreadCount;

            if (message.getMessageType().equals(ChatConnectionService.MessageType.SEND)) {
                if (curs.getCount() != 0) {
                    curs.moveToFirst();
                }
            } else if (curs.getCount() == 0) {
                unreadCount = 1;
                contentValues.put(COL_UNREAD_COUNT, unreadCount);
            } else {
                curs.moveToFirst();
                unreadCount = curs.getInt(COL_UNREAD_COUNT_NUM) + 1;
                contentValues.put(COL_UNREAD_COUNT, unreadCount);
            }
            db.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
        }  finally {
            if(curs != null){
                curs.close();
            }
        }
    }

    /**
     * Add data to table
     */
    public void addData(Dialog dialog) {
        Log.d(TAG, "addData()");
        SQLiteDatabase db = this.getWritableDatabase();
        Dialog dialogToAdd = dialog;
        dialog = checkIfMostRecent(dialog);
        if(!dialog.equals(dialogToAdd)){
            return;
        }
        ContentValues contentValues = new ContentValues();
        contentValues.put(TABLE_PRIM_KEY_CLIENT_JID, inboxJid);
        contentValues.put(TABLE_PRIM_KEY_BARE_PEER, dialog.getId());
        contentValues.put(COL_LAST_MESSAGE_TEXT, dialog.getLastMessage().getText());
        contentValues.put(COL_LAST_MESSAGE_TIME_STAMP, dialog.getLastMessage().getCreatedAt().getTime());
        contentValues.put(COL_MESSAGE_ID, dialog.getLastMessage().getId());
        String archiveId = dialog.getLastMessage().getArchiveId();
        if(archiveId != null) {
            contentValues.put(COL_ARCHIVE_ID, archiveId);
        }
        else {
            contentValues.put(COL_ARCHIVE_ID, NULL_ARCHIVE_ID);
        }
        contentValues.put(COL_UNREAD_COUNT, dialog.getUnreadCount());
        db.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
    }

    private Dialog checkIfMostRecent(Dialog dialog){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor curs = null;
        try {
        String query = "SELECT * FROM " + TABLE_NAME
                + " WHERE " + TABLE_PRIM_KEY_CLIENT_JID  + "=" + getSQLString(inboxJid)
                + " AND "   + TABLE_PRIM_KEY_BARE_PEER + "=" + getSQLString(dialog.getId());
        curs = db.rawQuery(query ,null);
        if(curs.getCount() == 0){
            return dialog;
        }
        else{
            curs.moveToFirst();
            Dialog mostRecentDialogInDatabase = buildDialog(curs);
            if(dialog.getLastMessage().getCreatedAt().getTime()
                    >= mostRecentDialogInDatabase.getLastMessage().getCreatedAt().getTime()
                    || mostRecentDialogInDatabase.equals(dialog)){
                Log.d(TAG,"dialog in database is most recent");
                return dialog;
            }
            else{
                Log.d(TAG, Long.toString(dialog.getLastMessage().getCreatedAt().getTime()));
                Log.d(TAG, Long.toString(mostRecentDialogInDatabase.getLastMessage().getCreatedAt().getTime()));
                Log.d(TAG,"dialog in database is old");
                mostRecentDialogInDatabase.setUnreadCount(dialog.getUnreadCount() + mostRecentDialogInDatabase.getUnreadCount());
                return mostRecentDialogInDatabase;
            }
        }
        } finally {
            if(curs != null){
                curs.close();
            }
        }
    }

    public List<Dialog> getDialogs() {
        Cursor curs = null;
        try {
            Log.d(TAG, "getDialogs()");
            SQLiteDatabase db = this.getWritableDatabase();
            String query = "SELECT * FROM " + TABLE_NAME
                    + " WHERE " + TABLE_PRIM_KEY_CLIENT_JID + "=" + getSQLString(inboxJid)
                    + " ORDER BY " + COL_LAST_MESSAGE_TIME_STAMP + " DESC";

            ArrayList<Dialog> dialogList = new ArrayList<>();
            curs = db.rawQuery(query, null);
            while (curs.moveToNext()) {
                dialogList.add(buildDialog(curs));
            }
            return dialogList;
        } finally {
            if (curs != null) {
                curs.close();
            }
        }
    }

    @NonNull
    private Dialog buildDialog(Cursor curs) {
        Log.d(TAG, "buildDialog()");
        String dialogId = curs.getString(TABLE_PRIM_KEY2_COL_NUM);
        String message_id = curs.getString(COL_MESSAGE_ID_NUM);
        long date = curs.getLong(COL_LAST_MESSAGE_TIME_STAMP_NUM);
        String text = curs.getString(COL_LAST_MESSAGE_TEXT_NUM);

        User user = new User(dialogId);
        ArrayList<User> users = new ArrayList<>();
        users.add(user);
        CustomMessage lastMsg = new CustomMessage(message_id, user, date, text, null, null);
        int unreadCount = curs.getInt(COL_UNREAD_COUNT_NUM);

        return new Dialog(dialogId,
                users,
                lastMsg,
                unreadCount
        );
    }

    public void setDialogAsRead(String barePeer) {
        Log.d(TAG,"setDialogAsRead(): " + barePeer );
        SQLiteDatabase db =  this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_UNREAD_COUNT, 0);
        db.update(TABLE_NAME, contentValues,
                TABLE_PRIM_KEY_CLIENT_JID  + "=" + getSQLString(inboxJid)
                + " AND " + TABLE_PRIM_KEY_BARE_PEER + "=" + getSQLString(barePeer)
                , null);
    }

    public ArrayList<CustomMessage> getLastMessages() {
        Cursor curs = null;
        try {
            Log.d(TAG, "getLastMessages()");
            SQLiteDatabase db = getWritableDatabase();
            String query = "SELECT * FROM "
                    + TABLE_NAME + " WHERE " + TABLE_PRIM_KEY_CLIENT_JID + "=" + getSQLString(inboxJid)
                    + " ORDER BY " + COL_LAST_MESSAGE_TIME_STAMP + " DESC";
            curs = db.rawQuery(query, null);
            ArrayList<CustomMessage> chatMessages = new ArrayList<>();
            while (curs.moveToNext()) {
                CustomMessage message;
                User usr = new User(curs.getString(TABLE_PRIM_KEY2_COL_NUM));
                String archiveId = curs.getString(COL_ARCHIVE_ID_NUM);
                if (NULL_ARCHIVE_ID.equals(archiveId)) {
                    archiveId = null;
                }

                message = new CustomMessage(curs.getString(COL_MESSAGE_ID_NUM),
                        usr,
                        curs.getLong(COL_LAST_MESSAGE_TIME_STAMP_NUM),
                        curs.getString(COL_LAST_MESSAGE_TEXT_NUM),
                        archiveId,
                        null);
                chatMessages.add(message);
            }
            return chatMessages;
        } finally {
            if (curs != null) {
                curs.close();
            }
        }
    }

    private String getSQLString(String str){
        return "'" + str.toLowerCase() +"'";
    }

    public void flushData(){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        db.close();
    }
}
