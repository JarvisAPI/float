package com.floatapplication.shop.edit;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.floatapplication.R;
import com.floatapplication.image.activity.ImageScrollActivity;
import com.floatapplication.shop.adapter.ShopEditImagePagerAdapter;
import com.floatapplication.shop.edit.ShopAboutEditContract.Model.Observer;
import com.floatapplication.util.GlobalHandler;
import com.floatapplication.util.PhotoHandler;
import com.floatapplication.util.UploadImageHandler;
import com.simplexorg.customviews.adapter.SimpleImageItemAdapter;

import butterknife.ButterKnife;

/**
 * Fragment that controls the MVP.
 */

public class ShopAboutEditFragment extends Fragment {
    private ShopAboutEditModel mModel;
    private ShopAboutEditView mView;
    private ShopAboutEditPresenter mPresenter;
    static final int REQUEST_IMAGE_SCROLL = 10;
    private boolean mStartingOtherActivity;

    public ShopAboutEditFragment() {
        mModel = new ShopAboutEditModel();
        mPresenter = new ShopAboutEditPresenter();
        mView = new ShopAboutEditView();

        mView.mPhotoHandler = new PhotoHandler();
        mView.mUploadImageHandler = new UploadImageHandler();
        mPresenter.attach(mView, mModel);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mView.mUploadImagesAdapter = SimpleImageItemAdapter.newTrackerInstance(context, mPresenter);
        mView.mShopImageAdapter = new ShopEditImagePagerAdapter(context);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mStartingOtherActivity = false;
        if (resultCode != Activity.RESULT_OK) {
            return;
        }
        if (requestCode == PhotoHandler.REQUEST_IMAGE_CAPTURE) {
            String currentPhotoPath = mView.mPhotoHandler.getCurrentPhotoPath();
            if (currentPhotoPath != null) {
                mPresenter.onAddUploadImage(currentPhotoPath);
            }
        } else if (requestCode == UploadImageHandler.REQUEST_IMAGE_GALLERY) {
            Uri selectedImage = data.getData();
            mPresenter.onAddUploadImage(GlobalHandler.getInstance()
                    .getAbsolutePathFromURI(getContext(), selectedImage));
        } else if (requestCode == REQUEST_IMAGE_SCROLL) {
            mPresenter.onRemoveUploadImages(data.getStringArrayListExtra(
                    ImageScrollActivity.IMAGE_PATHS));
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        if (!mStartingOtherActivity) {
            mStartingOtherActivity = true;
            super.startActivityForResult(intent, requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case UploadImageHandler.REQUEST_READ_EXTERNAL_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mView.pickUploadImage();
                }
            }
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shop_about_edit, container, false);
        Bundle args = getArguments();
        if (args != null) {
            mModel.init(getContext(), args.getParcelable("shop"));
        }
        ButterKnife.bind(mView, v);
        mView.mFragment = this;
        mPresenter.subscribe();
        return v;
    }

    public boolean submit() {
        return mPresenter.onSubmitEdit();
    }

    public void addModelObserver(Observer observer) {
        mModel.addObserver(observer);
    }

}
