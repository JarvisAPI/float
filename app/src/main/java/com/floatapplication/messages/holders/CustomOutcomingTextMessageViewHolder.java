package com.floatapplication.messages.holders;

import android.view.View;

import com.floatapplication.messages.chatkit.messages.MessageHolders;
import com.floatapplication.messages.models.CustomMessage;

public class CustomOutcomingTextMessageViewHolder
        extends MessageHolders.OutcomingTextMessageViewHolder<CustomMessage> {

    public CustomOutcomingTextMessageViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onBind(CustomMessage message) {
        super.onBind(message);
    }
}
