package com.floatapplication.shop.util;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.R;
import com.floatapplication.product.models.Product;
import com.floatapplication.shop.adapter.ShopEditAdapter;
import com.floatapplication.shop.adapter.ShopEditAdapter.OnProductClickListener;
import com.floatapplication.shop.adapter.ShopEditAdapter.OnProductDeleteListener;
import com.floatapplication.util.GlideRequests;
import com.floatapplication.util.Util;

/**
 * Created by william on 2018-05-24.
 * Class that implements the MVP pattern with ShopEditAdapter.
 */

public class ShopEditProductPresenter
        implements
        OnClickListener {
    private GlideRequests mGlide;
    private ShopEditAdapter mAdapter;
    private boolean mDeleteStatus;

    private OnProductDeleteListener mOnProductDeleteListener;
    private OnProductClickListener mOnProductClickListener;

    public ShopEditProductPresenter setGlide(GlideRequests glide) {
        mGlide = glide;
        return this;
    }

    public ShopEditProductPresenter setAdapter(ShopEditAdapter adapter) {
        mAdapter = adapter;
        return this;
    }

    public void setOnProductDeleteListener(OnProductDeleteListener listener) {
        mOnProductDeleteListener = listener;
    }

    public void setOnProductClickListener(OnProductClickListener listener) {
        mOnProductClickListener = listener;
    }

    public void present(ImageView productImage, TextView productName,
                        TextView productPrice, ImageView productDelete,
                        Product product, int position) {
        productName.setText(product.getProductName());
        productPrice.setText(product.getProductPrice());
        String productMainImageFile = product.getProductMainImageFile();

        if ("null".equals(productMainImageFile) || productMainImageFile == null) {
            mGlide.load(R.drawable.default_no_image)
                    .into(productImage);
        } else {
            String url = Util.getInstance().constructImageUrl(productMainImageFile);
            mGlide.load(url)
                    .placeholder(R.drawable.default_no_image)
                    .into(productImage);
        }
        if (mDeleteStatus) {
            productDelete.setVisibility(View.VISIBLE);
            setProductDeleteListener(productDelete, product, position);
        } else {
            productDelete.setVisibility(View.GONE);
        }

        setupProductClickListener(productImage, product);
    }

    private void setupProductClickListener(ImageView productImage, Product product) {
        productImage.setTag(R.id.tag0, product);
        productImage.setOnClickListener(this);
    }

    private void setProductDeleteListener(ImageView productDelete, Product product,
                                          int position) {
        productDelete.setTag(R.id.tag0, position);
        productDelete.setTag(R.id.tag1, product);
        productDelete.setOnClickListener(this);
    }

    public void setDelete(boolean delete) {
        if (mDeleteStatus != delete) {
            mDeleteStatus = delete;
            mAdapter.notifyDataSetChanged();
        }
    }

    public boolean getDeleteStatus() {
        return mDeleteStatus;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.product_delete:
                deleteProduct(view);
                break;
            case R.id.product_image:
                onProductClicked(view);
        }
    }

    private void onProductClicked(View view) {
        Product product = (Product) view.getTag(R.id.tag0);
        if (mOnProductClickListener != null) {
            mOnProductClickListener.onProductClick(product);
        }
    }

    private void deleteProduct(View view) {
        int position = (int) view.getTag(R.id.tag0);
        Product product = (Product) view.getTag(R.id.tag1);
        if (mOnProductDeleteListener != null) {
            mOnProductDeleteListener.onProductDelete(product);
        }
        mAdapter.removeProduct(position);
    }
}
