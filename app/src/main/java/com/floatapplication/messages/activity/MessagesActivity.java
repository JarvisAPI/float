package com.floatapplication.messages.activity;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.image.activity.CustomChatZoomInImageActivity;
import com.floatapplication.messages.activity.activity_helpers.MessageLoader;
import com.floatapplication.messages.activity.activity_helpers.MessageSender;
import com.floatapplication.messages.chatkit.commons.ImageLoader;
import com.floatapplication.messages.chatkit.messages.MessageBottomSheetFragment;
import com.floatapplication.messages.chatkit.messages.MessageHolders;
import com.floatapplication.messages.chatkit.messages.MessageInput;
import com.floatapplication.messages.chatkit.messages.MessagesList;
import com.floatapplication.messages.chatkit.messages.MessagesListAdapter;
import com.floatapplication.messages.chatkit.messages.MessagesListAdapter.Formatter;
import com.floatapplication.messages.chatkit.messages.InfoBottomSheetFragment;
import com.floatapplication.messages.holders.CustomIncomingImageMessageViewHolder;
import com.floatapplication.messages.holders.CustomIncomingTextMessageViewHolder;
import com.floatapplication.messages.holders.CustomOutcomingImageMessageViewHolder;
import com.floatapplication.messages.holders.CustomOutcomingTextMessageViewHolder;
import com.floatapplication.messages.message_database.ChatUserCache;
import com.floatapplication.messages.message_database.ChatUserCache.ChatUserCacheCallback;
import com.floatapplication.messages.message_database.InboxDatabaseHelper;
import com.floatapplication.messages.message_database.MessageDatabaseHelper;
import com.floatapplication.messages.models.ChatUserCacheEntry;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.models.User;
import com.floatapplication.messages.server_connection.ChatConnection;
import com.floatapplication.messages.server_connection.ChatConnectionService;
import com.floatapplication.shop.activity.ShopViewActivity;
import com.floatapplication.shop.util.ShopHandler;
import com.floatapplication.user.activity.UserActivity;
import com.floatapplication.user.util.UserSessionInterface;
import com.floatapplication.util.GlobalHandler;
import com.floatapplication.util.PhotoHandler;
import com.floatapplication.util.TransitionHandler;
import com.floatapplication.util.Util;
import com.simplexorg.customviews.util.VUtil;

import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessagesActivity extends AppCompatActivity
        implements MessagesListAdapter.SelectionListener,
        MessagesListAdapter.OnMessageLongClickListener<CustomMessage>,
        MessagesListAdapter.OnLoadMoreListener,
        MessageInput.InputListener,
        MessagesListAdapter.OnMessageViewClickListener<CustomMessage>,
        MessageInput.AttachmentsListener,
        MessageBottomSheetFragment.OnPhotosObtainedListener,
        InfoBottomSheetFragment.OnTabSelectedListener {

    private static final String TAG = "MessagesActivity";

    protected String mContactJid;
    protected MessagesListAdapter<CustomMessage> mMessagesAdapter;
    private Snackbar mSnackbar;
    private String mUsername;
    private User.Type mChatUserType;
    private Menu menu;
    private int selectionCount;
    private InboxDatabaseHelper mInboxDatabaseHelper;
    private MessagesList messagesList;
    private ChatConnectionService mChatConnectionService;
    private boolean mShouldUnbindService;
    private BroadcastReceiver mMessageBroadcastReceiver;
    private BroadcastReceiver mConnectionStateBroadcastReceiver;
    private TransitionHandler mTransitionHandler;
    private MessageBottomSheetFragment mBottomSheetFragment;
    private boolean firstBindFlag = true;
    MessageLoader mMessageLoader;
    MessageSender mMessageSender;

    protected ImageLoader mImageLoader = new ImageLoader() {
        @Override
        public void loadImage(final ImageView imageView, final String url) {
            FloatApplication.getUserSessionInterface()
                    .getUserAccessToken(getApplicationContext(), new UserSessionInterface.TokenCallback() {
                        @Override
                        public void onTokenObtained(String token) {
                            Log.d(TAG, "token obtained");
                            Log.d(TAG, mUsername);
                            handleGlideTokenCallback(imageView, url, token);
                        }

                        @Override
                        public void onTokenError(String errorMsg) {
                            Log.d(TAG, "token error");
                            Log.d(TAG, mUsername);
                        }
                    });
        }
    };

    private void handleGlideTokenCallback(ImageView imageView, String url, String token) {
        LazyHeaders.Builder builder = new LazyHeaders.Builder()
                .addHeader("Authorization", "Bearer " + token);
        GlideUrl glideUrl = null;
        if (url != null && !url.equals("")) {
            Map<String, String> queries = new HashMap<>();
            if (!FloatApplication.getUserSessionInterface().getUsername().equals(mUsername)) {
                queries.put("shop_id", mUsername);
                url += Util.getInstance().buildQuery(queries);
            }
            Log.d(TAG, url);
            glideUrl = new GlideUrl(url, builder.build());
        }
        GlobalHandler.getInstance()
                .loadImage(imageView, glideUrl, R.drawable.default_no_image);
    }

    private ServiceConnection mChatConnectionServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            Log.d(TAG, "onServiceConnected()");
            mInboxDatabaseHelper = FloatApplication.getFactoryInterface().getInboxDatabase(mUsername);
            mChatConnectionService = ((ChatConnectionService.ChatConnectionServiceBinder) iBinder).getService();
            if (mChatConnectionService.getState(mUsername).equals(ChatConnection.ConnectionState.DISCONNECTED)) {
                mChatConnectionService.reconnect(mUsername);
            }
            dialogMarkReadRequest();
            MessageDatabaseHelper messageDatabaseHelper = FloatApplication.getFactoryInterface().getMessageDatabase(mUsername);
            if (firstBindFlag) {
                mMessageLoader = new MessageLoader(messageDatabaseHelper.getConversation(mContactJid), mMessagesAdapter, mUsername);
                firstBindFlag = false;
            }
            messageDatabaseHelper.close();
            mMessagesAdapter.setMessageLoader(mMessageLoader);
            mChatConnectionService = ((ChatConnectionService.ChatConnectionServiceBinder) iBinder).getService();
            mChatConnectionService.setCurrentContactView(mUsername,mContactJid);
            mInboxDatabaseHelper.setDialogAsRead(mContactJid);
            mMessageSender.setConnectionService(mChatConnectionService);
            mMessageLoader.setConnectionService(mChatConnectionService);
            mMessageLoader.setIsEndOfConversation(false);
            mMessageLoader.loadMessages();
            mMessageLoader.loadNewMessages();

            if (!mChatConnectionService.getState(mUsername).equals(ChatConnection.ConnectionState.AUTHENTICATED)) {
                showConnectingSnackbar();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mInboxDatabaseHelper.setDialogAsRead(mContactJid);
            mInboxDatabaseHelper.close();
            mChatConnectionService = null;
        }
    };

    @Override
    public void onMessageLongClick(CustomMessage message) {
        Log.d(TAG, "message clicked");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()");
        mTransitionHandler = new TransitionHandler();
        setContentView(R.layout.activity_custom_holder_messages);
        messagesList = findViewById(R.id.messagesList);
        getChatAttributes();
        initMessageInput();
        setupActionBar();
        initMessageListTouchListeners();
        initMessageAdapter();
        initMessageBroadcastReceiver();
        initConnectionStateBroadcastReceiver();
        mMessageSender = new MessageSender(this, mMessagesAdapter, mContactJid, mUsername);
    }

    public void getChatAttributes() {
        Intent intent = getIntent();
        mContactJid = intent.getStringExtra(getString(R.string.contact));
        Log.d(TAG, "contact jid:" + mContactJid);
        mUsername = intent.getStringExtra(getString(R.string.chat_username));
        FloatApplication.getFactoryInterface().getChatUserCache().getUser(mContactJid, new ChatUserCache.ChatUserCacheCallback() {
            @Override
            public void successCallback(ChatUserCacheEntry entry) {
                mChatUserType = entry.getType();
                String conversationName = entry.getName();
                setupActionBarTitle(conversationName);
            }

            @Override
            public void errorCallback() {
                // Nothing happens
            }
        });
    }

    private void setupActionBarTitle(String conversationName) {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(StringUtils.capitalize(conversationName));
        }
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_icon:
                if (mMessagesAdapter.getSelectedMessages().size() != 0) {
                    mMessagesAdapter.copySelectedMessagesText(this, getMessageStringFormatter(), true);
                } else {
                    showInfoBottomSheetFrag();
                }
                break;
            case android.R.id.home:
                finish();
                return true;
        }
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.chat_actions_menu, menu);
        onSelectionChanged(0);
        return true;
    }

    @Override
    public void onSelectionChanged(int count) {
        selectionCount = count;
        if (count > 0) {
            menu.findItem(R.id.action_icon).setIcon(R.drawable.ic_action_copy);
        } else {
            menu.findItem(R.id.action_icon).setIcon(R.drawable.ic_info_outline_white_24dp);
        }
    }

    private void initMessageInput() {
        MessageInput input = findViewById(R.id.input);
        input.setInputListener(this);
        input.setAttachmentsListener(this);
    }

    private void initMessageListTouchListeners() {
        messagesList.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(rv.getWindowToken(), 0);
                    }
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {/* none*/}
        });
    }

    private void initMessageAdapter() {
        MessageHolders holdersConfig = new MessageHolders()
                .setIncomingTextConfig(
                        CustomIncomingTextMessageViewHolder.class,
                        R.layout.item_custom_incoming_text_message)
                .setOutcomingTextConfig(
                        CustomOutcomingTextMessageViewHolder.class,
                        R.layout.item_custom_outcoming_text_message)
                .setIncomingImageConfig(
                        CustomIncomingImageMessageViewHolder.class,
                        R.layout.item_custom_incoming_image_message)
                .setOutcomingImageConfig(
                        CustomOutcomingImageMessageViewHolder.class,
                        R.layout.item_custom_outcoming_image_message);
        mMessagesAdapter = new MessagesListAdapter<>(holdersConfig, mImageLoader);
        mMessagesAdapter.setLoadMoreListener(this);
        mMessagesAdapter.setOnMessageViewClickListener(this);
        mMessagesAdapter.enableSelectionMode(this);
        mMessagesAdapter.registerViewClickListener(R.id.messageUserAvatar,
                (view, message) -> showInfoBottomSheetFrag());
        messagesList.setAdapter(mMessagesAdapter);
    }

    private void showInfoBottomSheetFrag() {
        Bundle data = new Bundle();
        data.putString(getString(R.string.chat_user_type), mChatUserType.toString());
        InfoBottomSheetFragment bottomSheetFragment = new InfoBottomSheetFragment();
        bottomSheetFragment.setArguments(data);
        bottomSheetFragment.show(getSupportFragmentManager(), bottomSheetFragment.getTag());

    }

    private void initConnectionStateBroadcastReceiver() {
        mConnectionStateBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "state changed");
                if (mChatConnectionService != null && mChatConnectionService.getState(mUsername).equals(ChatConnection.ConnectionState.AUTHENTICATED)) {
                    Log.d(TAG, "authenticated to xmpp server loading messages");
                    mMessageLoader.setIsEndOfConversation(false);
                    mMessageLoader.setConnectionService(mChatConnectionService);
                    mMessageLoader.loadMessages();
                    mMessageLoader.loadNewMessages();
                    if (mSnackbar != null && mSnackbar.isShown())
                        mSnackbar.dismiss();
                } else {
                    // if snackbar is null show snack bar
                    if (mSnackbar == null) {
                        showConnectingSnackbar();
                    } else {
                        // if snackbar is not null if not shown show snackbar
                        if (!mSnackbar.isShown())
                            mSnackbar.show();
                    }
                }
            }
        };
        IntentFilter filter2 = new IntentFilter(ChatConnectionService.CONNECTION_STATE_CHANGE);
        registerReceiver(mConnectionStateBroadcastReceiver, filter2);
    }

    private void initMessageBroadcastReceiver() {
        mMessageBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                CustomMessage chatMessage = intent.getParcelableExtra("msg");
                if (chatMessage.getUser().getId().equals(mContactJid.toLowerCase())) {
                    mMessagesAdapter.addToStart(chatMessage, true);
                    mInboxDatabaseHelper.setDialogAsRead(mContactJid);
                }
            }
        };
        IntentFilter filter1 = new IntentFilter(ChatConnectionService.INCOMING_MESSAGE);
        registerReceiver(mMessageBroadcastReceiver, filter1);
    }

    public void showConnectingSnackbar() {
        CoordinatorLayout coordinatorLayout = findViewById(R.id.snackbar_coordinator);
        mSnackbar = Snackbar.make(coordinatorLayout, "Connecting to message server", Snackbar.LENGTH_INDEFINITE);
        ViewGroup contentLay = (ViewGroup) mSnackbar.getView().findViewById(android.support.design.R.id.snackbar_text).getParent();
        ProgressBar item = new ProgressBar(getApplicationContext());
        contentLay.addView(item, 0);
        mSnackbar.show();
    }

    public void dialogMarkReadRequest() {
        mInboxDatabaseHelper.setDialogAsRead(mContactJid);
        Map<String, String> variables = new HashMap<>();
        variables.put(ChatConnectionService.MARK_DIALOG_REQUEST_KEY_BARE_PEER, mContactJid);
        variables.put(ChatConnectionService.MARK_DIALOG_REQUEST_KEY_COUNT, "0");
        Map<String, String> queries = new HashMap<>();
        if (!FloatApplication.getUserSessionInterface().getUsername().equals(mUsername)) {
            queries.put("shop_id", mUsername);
        }
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.PUT,
                this, HypermediaInterface.SET_CONVERSATION_MESSAGE_COUNT, variables, queries, null,
                response -> Log.e(TAG, response), error -> {
                    Log.e(TAG, "Error: " + error.getMessage());
                    error.printStackTrace();
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTransitionHandler.onResume();
        FloatApplication.getMessageFeatureHandlerInterface().initXMPPConnection();
        bindService();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
        unregisterReceiver(mMessageBroadcastReceiver);
        unregisterReceiver(mConnectionStateBroadcastReceiver);
        unbindService();
        mMessagesAdapter.unselectAllItems();
        mInboxDatabaseHelper.close();
    }

    @Override
    protected void onPause() {
        Log.d(TAG, Integer.toString(mMessagesAdapter.getItemCount()));
        Log.d(TAG, "onPause");
        super.onPause();
        dialogMarkReadRequest();
        unbindService();
    }

    private void bindService() {
        if (bindService(new Intent(MessagesActivity.this, ChatConnectionService.class),
                mChatConnectionServiceConnection, Context.BIND_AUTO_CREATE)) {
            mShouldUnbindService = true;
        } else {
            Log.d(TAG, "Unable to request ChatConnectionService for binding");
        }
    }

    private void unbindService() {
        if (mShouldUnbindService) {
            mChatConnectionService.setCurrentContactView(mUsername,""); // set currently view contact as nothing
            unbindService(mChatConnectionServiceConnection);
            mShouldUnbindService = false;
        }
    }

    @Override
    public void onBackPressed() {
        dialogMarkReadRequest();
        if (selectionCount == 0) {
            super.onBackPressed();
        } else {
            mMessagesAdapter.unselectAllItems();
        }
    }

    @Override
    public boolean onSubmit(final CharSequence input) {
        mInboxDatabaseHelper.setDialogAsRead(mContactJid);
        return mMessageSender.sendTextMessage(input);
    }

    @Override
    public void onAddAttachments() {
        mBottomSheetFragment = new MessageBottomSheetFragment();
        mBottomSheetFragment.show(getSupportFragmentManager(), mBottomSheetFragment.getTag());
    }


    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        Log.d("TAG", "pageCount: " + Integer.toString(page) + " totalItemsCount: " + Integer.toString(totalItemsCount));
        mMessageLoader.loadMessages();
    }


    private Formatter<CustomMessage> getMessageStringFormatter() {
        return message -> {
            String text = message.getText();
            if (text == null) {
                text = "[attachment]";
            }
            return text;
        };
    }

    @Override
    public void onPhotosObtained(List<String> imagesEncodedList) {
        mMessageSender.sendImageMessage(imagesEncodedList);
    }

    @Override
    public void onMessageViewClick(View view, CustomMessage message) {
        if (message.getImageUrl() != null) {
            Log.d(TAG, message.getText());
            Log.d(TAG, "message view clicked");
            Log.d(TAG, ViewCompat.getTransitionName(view));
            int randomStringLen = 8;
            view.setTransitionName(VUtil.getInstance().genRandomString(randomStringLen));
            Intent intent = new Intent(this, CustomChatZoomInImageActivity.class);
            intent.putExtra(VUtil.EXTRA_IMAGE_DATA, message.getImageUrl());
            intent.putExtra(VUtil.EXTRA_TRANSITION_NAME, view.getTransitionName());
            intent.putExtra(CustomChatZoomInImageActivity.USERNAME, mUsername);

            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, view,
                    view.getTransitionName());
            startActivity(intent, optionsCompat.toBundle());
        }
    }

    @Override
    public void onGoToShopSelected() {
        if (mChatUserType != null) {
            new ShopHandler().getShop(mContactJid, shop -> {
                Intent data = new Intent();
                data.putExtra("shop", shop);
                mTransitionHandler.transition(getApplicationContext(), ShopViewActivity.class, data);
            });
        }
    }

    @Override
    public void onViewUserProfileSelected() {
        if (mChatUserType != null) {
            FloatApplication.getFactoryInterface().getChatUserCache().getUser(mContactJid, new ChatUserCache.ChatUserCacheCallback() {
                @Override
                public void successCallback(ChatUserCacheEntry entry) {
                    Intent data = new Intent().putExtra(com.floatapplication.user.models.User.USERNAME, mContactJid);
                    data.putExtra(com.floatapplication.user.models.User.USERNAME, mContactJid);
                    data.putExtra(com.floatapplication.user.models.User.NAME, entry.getName());
                    mTransitionHandler.transition(getApplicationContext(), UserActivity.class, data);
                }

                @Override
                public void errorCallback() {

                }
            });

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MessageBottomSheetFragment.REQUEST_IMAGE_GALLERY ||
                requestCode == PhotoHandler.REQUEST_IMAGE_CAPTURE) {
            mBottomSheetFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        mBottomSheetFragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

}
