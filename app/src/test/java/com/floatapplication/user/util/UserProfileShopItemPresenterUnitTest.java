package com.floatapplication.user.util;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.R;
import com.floatapplication.user.models.UserShopItem;
import com.floatapplication.user.util.UserProfileShopItemPresenter.UserShopItemListener;
import com.floatapplication.util.Util;
import com.floatapplication.util.Util.OnAddressObtainedListener;
import com.simplexorg.mapfragment.model.GeoPoint;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by william on 2018-06-07.
 * Test the different buttons.
 */
public class UserProfileShopItemPresenterUnitTest {
    private UserProfileShopItemPresenter presenter;
    @Mock private TextView shopNameText;
    @Mock private TextView shopLocationText;
    @Mock private ImageView shopImage;
    @Mock private ImageView goToShopButton;
    @Mock private ImageView goToShopLocationButton;
    @Mock private ImageView goToShopInboxButton;
    @Mock private ImageView goToShopEditButton;
    @Mock private UserShopItem shopItem;
    @Mock private UserShopItemListener listener;
    @Mock private Util mUtil;

    @Before
    public void setup() {
        initMocks(this);
        Util.setUtil(mUtil);
        presenter = new UserProfileShopItemPresenter();
        when(shopItem.getShopName()).thenReturn("shop");
        when(shopItem.getShopId()).thenReturn("id");
        when(shopItem.getShopLocation()).thenReturn(new GeoPoint(1, 2));

        when(goToShopButton.getId()).thenReturn(R.id.go_to_shop_image_view);
        when(goToShopButton.getTag(R.id.tag0)).thenReturn(shopItem);

        when(goToShopLocationButton.getId()).thenReturn(R.id.shop_location_icon);
        when(goToShopLocationButton.getTag(R.id.tag0)).thenReturn(shopItem);

        when(goToShopInboxButton.getId()).thenReturn(R.id.shop_messages_icon);
        when(goToShopInboxButton.getTag(R.id.tag0)).thenReturn(shopItem);

        when(goToShopEditButton.getId()).thenReturn(R.id.shop_edit_icon);
        when(goToShopEditButton.getTag(R.id.tag0)).thenReturn(shopItem);

        presenter.setUserShopItemListener(listener);
    }

    @After
    public void cleanup() {
        Util.setUtil(null);
    }

    private void present() {
        presenter.present(shopNameText, shopLocationText, shopImage,
                goToShopButton, goToShopLocationButton,
                goToShopEditButton, goToShopInboxButton, shopItem);
    }

    @Test
    public void testPresent_setsUpUiCorrectly() {
        String address = "address";
        doAnswer((InvocationOnMock invocation) -> {
            OnAddressObtainedListener listener = invocation.getArgumentAt(2, OnAddressObtainedListener.class);
            listener.onAddressObtained(address);
            return null;
        }).when(mUtil).getAddress(any(Context.class), any(GeoPoint.class), any(OnAddressObtainedListener.class));
        present();
        verify(shopNameText).setText(shopItem.getShopName());
        verify(shopLocationText).setText(address);
        verify(goToShopButton).setOnClickListener(presenter);
        verify(goToShopLocationButton).setOnClickListener(presenter);
    }

    @Test
    public void showEditButtonForOwner() {
        presenter.setIsProfileOwner();
        present();
        verify(goToShopEditButton).setVisibility(View.VISIBLE);
        verify(goToShopEditButton).setOnClickListener(presenter);
    }

    @Test
    public void hideEditButtonForNotOwner() {
        present();
        verify(goToShopEditButton).setVisibility(View.INVISIBLE);
    }

    @Test
    public void onClickGoToShop_invokesCorrectCallback() {
        present();
        presenter.onClick(goToShopButton);
        verify(listener).onGoToShopClicked(shopItem.getShopId());
    }

    @Test
    public void onClickGoToShopEdit_invokesCorrectCallback() {
        present();
        presenter.onClick(goToShopEditButton);
        verify(listener).onShopEditClicked(shopItem.getShopId(), shopItem.getShopName());
    }

    @Test
    public void onClickShopLocation_invokesCorrectCallback() {
        present();
        presenter.onClick(goToShopLocationButton);
        verify(listener).onGoToShopLocationClicked(shopItem.getShopLocation());
    }
}
