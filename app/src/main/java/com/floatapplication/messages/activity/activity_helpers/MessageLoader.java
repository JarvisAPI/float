package com.floatapplication.messages.activity.activity_helpers;

import android.util.Log;

import com.floatapplication.messages.chatkit.messages.MessagesListAdapter;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.server_connection.ChatConnection;
import com.floatapplication.messages.server_connection.ChatConnectionService;
import com.floatapplication.messages.server_connection.ChatConnectionService.OnArchiveMessageReceivedListener;

import java.util.List;

/**
 * Message Loader that is used to load messages into MessagesActivity.
 * This class also tracks the conversation load state.
 * This class should only be accessed with a single thread.
 */

public class MessageLoader
        implements OnArchiveMessageReceivedListener{
    private final static String TAG = MessageLoader.class.getSimpleName();
    public static final int PAGE_SIZE = 20;
    private ChatConnectionService mChatConnectionService;
    private List<CustomMessage> mMessageList;
    private boolean isEndOfConversation;
    private MessagesListAdapter<CustomMessage> mMessagesAdapter;
    private String mUsername;

    private boolean mMessageLoading;

    public MessageLoader(List<CustomMessage> conversation, MessagesListAdapter<CustomMessage> messagesListAdapter, String username) {
        mMessagesAdapter = messagesListAdapter;
        mMessageList = conversation;
        isEndOfConversation = false;
        mMessageLoading = false;
        mUsername = username;
    }

    public boolean getIsEndOfConversation() {
        return isEndOfConversation;
    }

    public boolean getIsMessageLoading(){ return mMessageLoading;}

    public void setIsEndOfConversation(boolean end) {
        isEndOfConversation = end;
    }

    public void setConnectionService(ChatConnectionService connection) {
        Log.d(TAG,"setConnectionService");
        mChatConnectionService = connection;
        mChatConnectionService.setOnArchiveMessageReceivedListener(this);
    }

    public void loadNewMessages(){
        Log.d(TAG,"loadNewMessages()");
        CustomMessage mostRecentMessage = mMessagesAdapter.getMostRecentMessage();
        if(mostRecentMessage != null && mChatConnectionService.connectionReady(mUsername)) {
            mChatConnectionService.getMostRecentMessages(mUsername, mMessagesAdapter.getMostRecentMessage());
        }
    }

    @Override
    public void onMostRecentMessagesReceived(List<CustomMessage> archiveMessages) {
        Log.d(TAG,"onMostRecentMessagesReceived()");
        for(CustomMessage msg: archiveMessages){
            mMessagesAdapter.addToStart(msg,false);
        }
    }

    public void loadMessages() {
        Log.d(TAG,"loadMessages()");
        if (mMessageList.size() - PAGE_SIZE >=  0) {
            Log.d(TAG, "loading page from cache");
            mMessagesAdapter.addToEnd(mMessageList.subList(mMessageList.size() - PAGE_SIZE, mMessageList.size()), true);
            mMessageList = mMessageList.subList(0, mMessageList.size() - PAGE_SIZE);
        } else if (!mMessageList.isEmpty()) {
            Log.d(TAG, "loading last page from cache");
            Log.d(TAG, "mMessageList Size:" + Integer.toString(mMessageList.size()));
            mMessagesAdapter.addToEnd(mMessageList, true);
            mMessageList.clear();
        } else if (mChatConnectionService.connectionReady(mUsername)) {
            Log.d(TAG, "loading page from archives");
            fetchArchives();
        } else{
            isEndOfConversation = true;
        }
    }

    private void fetchArchives() {
        Log.d(TAG, "fetchArchives()");
        if (!mMessageLoading) {
            mMessageLoading = true;
            mMessagesAdapter.showLoadingSpinner();
            CustomMessage tailMessage = mMessagesAdapter.getLastMessage();
            if (tailMessage == null) {
                Log.d(TAG, "end of messages");
                mMessagesAdapter.hideLoadingSpinner();
                mMessageLoading = false;
                isEndOfConversation = true;
            } else {
                Log.d(TAG, tailMessage.getText());
                mChatConnectionService.fetchArchiveMessages(mUsername, tailMessage);
            }
        }
    }

    @Override
    public void onArchiveMessagesReceived(List<CustomMessage> archiveMessages) {
        Log.d(TAG,"onArchiveMessagesReceived()");
        Log.d(TAG, "number of Archive Messages fetched: " + Integer.toString(archiveMessages.size()));
        mMessagesAdapter.hideLoadingSpinner();
        if (archiveMessages.size() < PAGE_SIZE &&
                mChatConnectionService.getState(mUsername).equals(ChatConnection.ConnectionState.AUTHENTICATED)) {
            Log.d(TAG, "end of conversation!");
            isEndOfConversation = true;
        }else{
            isEndOfConversation = false;
        }

        if (mMessagesAdapter.getLastMessage() != null && !archiveMessages.isEmpty()) {
            Log.d(TAG, "loading archives to adapter");
            mMessagesAdapter.addToEnd(archiveMessages, true);
        }
        mMessageLoading = false;
    }
}
