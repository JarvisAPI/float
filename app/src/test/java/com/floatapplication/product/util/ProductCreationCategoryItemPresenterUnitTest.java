package com.floatapplication.product.util;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.GridLayout.LayoutParams;

import com.floatapplication.R;
import com.floatapplication.TestApplication;
import com.pchmn.materialchips.ChipView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by william on 2018-06-14.
 * Test displaying the product categories is set up correctly.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ProductCreationCategoryItemPresenterUnitTest {
    private ProductCreationCategoryItemPresenter presenter;
    @Mock private GridLayout layout;
    private String[] categories;
    private Context context;

    @Before
    public void setup() {
        initMocks(this);
        context = RuntimeEnvironment.application.getApplicationContext();
        presenter = new ProductCreationCategoryItemPresenter(context);
    }

    private void present() {
        presenter.bind(layout, categories);
        presenter.present();
    }

    @Test
    public void verifyPresentChip_executesCorrectSequence() {
        categories = new String[]{"1", "2"};
        present();
        ArgumentCaptor<ChipView> captor = ArgumentCaptor.forClass(ChipView.class);
        verify(layout, times(categories.length)).addView(captor.capture(), any(LayoutParams.class));
        List<ChipView> chips = captor.getAllValues();
        assertEquals(categories.length, chips.size());

        for (int i = 0; i < categories.length; i++) {
            ChipView chipView = chips.get(i);
            assertEquals(categories[i], chipView.getLabel());
        }
    }

    @Test
    public void noChipClicked_shouldGiveNullSelectedCategory() {
        categories = new String[]{"1"};
        present();
        assertNull(presenter.getSelectedCategory());
    }

    @Test
    public void onChipClick_shouldExecute_sequenceCorrectly() {
        categories = new String[]{"1"};
        ChipView chipView = mock(ChipView.class);
        when(chipView.getLabel()).thenReturn(categories[0]);
        present();
        presenter.onChipClick(chipView);
        verify(chipView).setChipBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
        assertEquals(categories[0], presenter.getSelectedCategory());
        // After clicking, a chip is selected. Selecting same chip again should deselect it.
        presenter.onChipClick(chipView);
        verify(chipView).setChipBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimaryLight));
        assertNull(presenter.getSelectedCategory());
    }

    @Test
    public void selectOneChipFirst_thenOtherChip_shouldOnlyShowLastChipAsSelected() {
        categories = new String[]{"1", "2"};
        ChipView chipView0 = mock(ChipView.class);
        ChipView chipView1 = mock(ChipView.class);
        present();
        presenter.onChipClick(chipView0);
        presenter.onChipClick(chipView1);
        ArgumentCaptor<Integer> captor = ArgumentCaptor.forClass(Integer.class);
        verify(chipView0, times(categories.length)).setChipBackgroundColor(captor.capture());
        List<Integer> chipColors = captor.getAllValues();
        assertEquals((Integer) ContextCompat.getColor(context, R.color.colorPrimary), chipColors.get(0)); // First time selected set to darker color.
        assertEquals((Integer) ContextCompat.getColor(context, R.color.colorPrimaryLight), chipColors.get(1)); // Deselect set to normal color.
        verify(chipView1).setChipBackgroundColor(ContextCompat.getColor(context, R.color.colorPrimary));
    }
}
