package com.floatapplication.product.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.floatapplication.R;
import com.floatapplication.config.ConfigStore;
import com.floatapplication.product.adapter.ProductAdapter;
import com.floatapplication.product.adapter.ProductAdapter.OnBottomReachedListener;
import com.floatapplication.product.models.Product;
import com.floatapplication.product.util.ProductInfiniteScrollLogic;
import com.floatapplication.product.util.ProductInfiniteScrollLogic.OnProductsObtainedListener;
import com.floatapplication.product.util.ProductInfiniteScrollStateMachine;
import com.floatapplication.product.util.ProductShopQueryQueue;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.shop.util.ShopViewProductPresenter.OnProductClickListener;
import com.floatapplication.util.TransitionHandler;
import com.floatapplication.util.Util;
import com.simplexorg.mapfragment.model.GeoPoint;

import java.util.List;

/**
 * Created by william on 2018-05-28.
 * Allows user to browse through an infinite list of products.
 */

public class ProductInfiniteScrollActivity extends AppCompatActivity
        implements
        OnBottomReachedListener,
        OnProductsObtainedListener,
        OnProductClickListener {
    /*
     * To support infinite product scrolling, we get shops nearby the user's current camera location
     * on the map then we get some products from each one of those shops once we run out of products to
     * display we get more shops nearby to load.
     */
    private ProductInfiniteScrollLogic mLogic;
    private ProductAdapter mAdapter;
    private TransitionHandler mTransitionHandler;
    private StaggeredGridLayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_infinite_scroll);
        GeoPoint pos = getIntent().getParcelableExtra("location");
        mLogic = new ProductInfiniteScrollLogic(pos, new ProductInfiniteScrollStateMachine(),
                new ProductShopQueryQueue());
        mTransitionHandler = new TransitionHandler();
        setupActionBar();
        setupRecyclerView();
        setupListeners();
    }

    @Override
    public void onResume() {
        super.onResume();
        mTransitionHandler.onResume();
    }

    private void setupActionBar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Nearby Products");
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_shop_view_products, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_reorder:
                Util.getInstance().reorderItems(this, mLayoutManager);
        }
        return true;
    }

    private void setupRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        int spanCount = ConfigStore.getInstance().getProductColumnSpanCount(this);
        mLayoutManager = new StaggeredGridLayoutManager(spanCount,
                StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ProductAdapter();
        recyclerView.setAdapter(mAdapter);
    }

    private void setupListeners() {
        mAdapter.setOnBottomReachedListener(this);
        mAdapter.setOnProductClickListener(this);
        mLogic.setOnProductsObtainedListener(this);
    }

    @Override
    public void onBottomReached() {
        mLogic.onProductScrollBottomReached();
    }

    @Override
    public void onProductsObtained(List<Product> products) {
        mAdapter.addProducts(products);
    }

    @Override
    public void onProductClick(Product product, View itemView) {
        Intent data = new Intent();
        data.putExtra("product", product);
        data.putExtra(Shop.ID, product.getProductShopId());
        data.putExtra("transition", ViewCompat.getTransitionName(itemView));
        mTransitionHandler.sharedElementTransition(this, ProductActivity.class,
                data, itemView);
    }
}
