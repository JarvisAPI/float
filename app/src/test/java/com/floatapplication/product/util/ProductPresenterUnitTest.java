package com.floatapplication.product.util;

import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayout;
import android.transition.Transition;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.FloatApplication;
import com.floatapplication.TestApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.product.activity.ProductActivity;
import com.floatapplication.product.adapter.ProductImagePagerAdapter;
import com.floatapplication.product.models.Product;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.util.Util;
import com.pchmn.materialchips.ChipView;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by william on 2018-06-13.
 * Tests presenting product ui logic.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ProductPresenterUnitTest {
    private ProductPresenter presenter;
    @Mock private ProductActivity activity;
    @Mock private Product product;
    @Mock private Intent data;
    @Mock private ProductImagePagerAdapter adapter;
    @Mock private ViewPager viewPager;
    @Mock private TextView productName;
    @Mock private TextView productPrice;
    @Mock private TextView productDescription;
    @Mock private GridLayout category;
    @Mock private TabLayout tabDots;
    @Mock private ImageView shopHomeIcon;
    @Mock private ImageButton messageIcon;
    @Mock private HypermediaInterface hypermediaInterface;
    @Mock private Util mUtil;
    @Mock private Window window;

    @Before
    public void setup() {
        initMocks(this);
        when(category.getContext()).thenReturn(RuntimeEnvironment.application.getApplicationContext());
        FloatApplication.setHypermediaInterface(hypermediaInterface);
        Util.setUtil(mUtil);
        when(mUtil.constructImageUrl(anyString())).thenReturn("imageUrl");
        when(activity.getWindow()).thenReturn(window);
        presenter = new ProductPresenter(activity, product, data);
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    private void present() {
        presenter.present(viewPager, adapter, productName,
                productPrice, productDescription, category,
                tabDots, shopHomeIcon, messageIcon);
    }

    @Test
    public void verifyPresent_executesCorrectSequence() {
        String transitionName = "name";
        String pName = "productName";
        String pPrice = "productPrice";
        when(data.getStringExtra("transition")).thenReturn(transitionName);
        when(product.getProductName()).thenReturn(pName);
        when(product.getProductPrice()).thenReturn(pPrice);
        present();
        verify(activity).supportPostponeEnterTransition();
        verify(adapter).setTransitionName(transitionName);
        verify(viewPager).setAdapter(adapter);
        verify(tabDots).setupWithViewPager(viewPager);
        verify(productName).setText(pName);
        verify(productPrice).setText(pPrice);
        verify(adapter).setListener(presenter);
        verify(adapter).addImageUrl(anyString());
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP) {
            verify(window).setEnterTransition(any(Transition.class));
            verify(window).setExitTransition(any(Transition.class));
        }
        verify(hypermediaInterface).follow(eq(Hypermedia.GET), eq(HypermediaInterface.PRODUCT_DETAILS), anyMapOf(String.class, String.class),
                anyMapOf(String.class, String.class), any(ServerCallback.class), any(ServerErrorCallback.class));
    }

    @Test
    public void onGlideLoadFailed_shouldStartEnterTransition() {
        present();
        presenter.onLoadFailed(null, null, null, false);
        verify(activity).supportStartPostponedEnterTransition();
    }

    @Test
    public void onGlideResourceReady_shouldStartEnterTransition() {
        present();
        presenter.onResourceReady(null, null, null, null, false);
        verify(activity).supportStartPostponedEnterTransition();
    }

    @Test
    public void whenObtainProductInfoFromServer_updateUiCorrectly() {
        String desc = "desc";
        String cate = "category";
        String response = String.format("{'product': {'product_description':'%s', 'product_image_files':['123']}}", desc);
        when(product.getProductDescription()).thenReturn(desc);
        when(product.getProductCategory()).thenReturn(cate);
        present();
        presenter.onSuccessResponse(response);
        verify(product).augmentProductInfo(any(JSONObject.class));
        verify(adapter).addImageUrl(anyString());
        verify(productDescription).setText(desc);
        verify(category).addView(any(ChipView.class));
    }

    @Test
    public void onBackPressed_resetViewPagerItem() {
        present();
        final int NOT_ZERO = 1;
        when(viewPager.getCurrentItem()).thenReturn(NOT_ZERO);
        presenter.onBackPressed();
        verify(viewPager).setCurrentItem(anyInt(), anyBoolean());
    }
}
