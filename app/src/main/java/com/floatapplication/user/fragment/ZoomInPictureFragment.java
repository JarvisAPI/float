package com.floatapplication.user.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.floatapplication.R;
import com.floatapplication.util.GlideApp;


/**
 * Created by jamescho on 2018-05-16.
 *
 */

public class ZoomInPictureFragment extends Fragment {
    private static final String TAG = ZoomInPictureFragment.class.getSimpleName();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_picture_zoom_in, container, false);
        ImageView photoView = view.findViewById(R.id.content);
        if(getActivity() != null && getArguments() != null){
            Bundle args = getArguments();
            String url = args.getString("url");
            Log.d(TAG, url);
            GlideApp.with(getActivity().getApplicationContext())
                    .load(url)
                    .into(photoView);
        }
        return view;
    }
}
