package com.floatapplication.messages.models;

public class ChatUserCacheEntry {
    private String jid;
    private String name;
    private String profileImage;
    private User.Type type;  // user | shop

    public ChatUserCacheEntry(String jid, String name, String profileImage, User.Type type){
        this.jid = jid;
        this.name = name;
        this.profileImage = profileImage;
        this.type = type;
    }

    public String getJid(){
        return jid;
    }

    public String getName(){
        return name;
    }

    public String getProfileImage(){
        return profileImage;
    }

    public User.Type getType(){
        return type;
    }
}
