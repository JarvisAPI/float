package com.floatapplication.messages.chatkit.utils;

/**
 * Created by jamescho on 2018-06-04.
 * Stub
 */

public class MessageParser {
    public static final String PHOTO_MESSAGE_PREFIX = "[ photo_message_url: ";
    public static final int PHOTO_MESSAGE_PREFIX_LENGTH = PHOTO_MESSAGE_PREFIX.length();
    public static final String PHOTO_MESSAGE_SUFIX = " ]";

    public static String buildImageMessage(String url){
        return PHOTO_MESSAGE_PREFIX + url + PHOTO_MESSAGE_SUFIX;
    }

    public static String extractImageUrl(String text){
        if(text.startsWith(PHOTO_MESSAGE_PREFIX)){
            return text.substring(PHOTO_MESSAGE_PREFIX_LENGTH -1 , text.indexOf(PHOTO_MESSAGE_SUFIX));
        }
        else{
            return "";
        }
    }
}
