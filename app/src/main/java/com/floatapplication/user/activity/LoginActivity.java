package com.floatapplication.user.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.map.activity.MapMainActivity;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.user.util.UserHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;


/**
 * Created by william on 2018-04-25.
 * Activity for user to login or head to sign up.
 */
public class LoginActivity extends Activity
        implements ServerCallback, ServerErrorCallback {

    private final static String TAG = LoginActivity.class.getSimpleName();
    private static final int REQUEST_SIGN_UP = 1;
    private static final int RC_SIGN_IN = 9001;
    private static final String OAUTH_FACEBOOK = "facebook";
    private static final String OAUTH_GOOGLE = "google";

    private EditText mUsernameText;
    private EditText mPasswordText;
    private CircularProgressButton mLoginButton;
    private LoginButton mFBLoginButton;
    private Button mGoogleLoginButton;
    private CallbackManager fbCallbackManager;
    private GoogleSignInClient mGoogleSignInClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setGoogleSignInClient();
        obtainViews();
        setListeners();
        setExternalAuthBtnConfigs();
    }

    private void setGoogleSignInClient(){
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

    }

    /**
     * Referenced in the xml.
     * @param view the clicked view.
     */
    public void onClickFacebookButton(View view) {
        if (view.getId() == R.id.custom_facebook_login_button) {
            mFBLoginButton.performClick();
        }
    }

    private void obtainViews() {
        mUsernameText = findViewById(R.id.input_username);
        mPasswordText = findViewById(R.id.input_password);
        mLoginButton = findViewById(R.id.btn_login);
        mGoogleLoginButton = findViewById(R.id.google_sign_in_button);
        mFBLoginButton = findViewById(R.id.facebook_login_button);
    }

    private void setExternalAuthBtnConfigs(){
        fbCallbackManager = CallbackManager.Factory.create();
        List<String> permissions = new ArrayList<>();
        permissions.add("public_profile");
        permissions.add("email");
        mFBLoginButton.setReadPermissions(permissions);
        // Callback registration
        mFBLoginButton.registerCallback(fbCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                final AccessToken accessToken = loginResult.getAccessToken();
                Log.d(TAG,accessToken.getToken());
                sendOAuthLoginRequest(accessToken.getToken(), OAUTH_FACEBOOK);
                mLoginButton.setEnabled(false);
                mLoginButton.startAnimation();
            }

            @Override
            public void onCancel() {
                if(getApplicationContext() != null) {
                    Toast.makeText(getApplicationContext(), "Facebook sign in cancelled", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onError(FacebookException exception) {
                if(getApplicationContext() != null) {
                    Toast.makeText(getApplicationContext(), "Facebook sign in failed", Toast.LENGTH_LONG).show();
                }
            }
        });

        mGoogleLoginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void setListeners() {
        mLoginButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        String createAccountText = getString(R.string.create_account_option);
        SpannableString ss = new SpannableString(createAccountText);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Intent intent = new Intent(getApplicationContext(), SignUpActivity.class);
                startActivityForResult(intent, REQUEST_SIGN_UP);
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        ss.setSpan(clickableSpan,
                createAccountText.length() - 10,
                createAccountText.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView signUpLink = findViewById(R.id.link_sign_up);
        signUpLink.setText(ss);
        signUpLink.setMovementMethod(LinkMovementMethod.getInstance());

        TextView skipLoginLink = findViewById(R.id.link_skip_login);
        skipLoginLink.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                saveSkipLoginState();
                launchMainActivity();
            }
        });
    }

    /**
     * Save the state that the user decided to skip login, so that in the
     * future user is not prompted with login screen.
     */
    private void saveSkipLoginState() {
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(getString(R.string.skip_login), true);
        editor.apply();
    }

    private void login() {
        mUsernameText.setError(null);
        mPasswordText.setError(null);
        if (validate()) {
            mLoginButton.setEnabled(false);
            mLoginButton.startAnimation();
            sendLoginRequest(mUsernameText.getText().toString(), mPasswordText.getText().toString());
        }
    }

    private void sendLoginRequest(String username, String password) {
        String authValue = username + ":" + password;
        String encoded = Base64.encodeToString(authValue.getBytes(), Base64.DEFAULT);
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Basic " + encoded);
        FloatApplication.getHypermediaInterface()
                .follow(Hypermedia.POST, HypermediaInterface.LOGIN, null, headers, this, this);
    }

    private void sendOAuthLoginRequest(String accessToken, String type) {
        try {
            Log.d(TAG, "sending OAuth request");
            JSONObject user = new JSONObject()
                    .put("access_token", accessToken )
                    .put("type", type);
            FloatApplication.getHypermediaInterface()
                    .follow(Hypermedia.POST, HypermediaInterface.OAUTH, null, null, user.toString(), this, this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Validates the username and password that is inputted.
     *
     * @return true if everything is entered correctly, false otherwise.
     */
    private boolean validate() {
        String username = mUsernameText.getText().toString();
        String password = mPasswordText.getText().toString();
        boolean error = false;
        if ("".equals(username)) {
            mUsernameText.setError("Username not entered");
            error = true;
        }
        if ("".equals(password)) {
            mPasswordText.setError("Password not entered");
            error = true;
        }
        return !error;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        fbCallbackManager.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SIGN_UP) {
            if (resultCode == RESULT_OK) {
                Bundle extras = data.getExtras();
                if (extras != null) {
                    String username = extras.getString("username");
                    String password = extras.getString("password");
                    mUsernameText.setText(username);
                    mPasswordText.setText(password);
                    login();
                }
            }
        } else if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                Log.d(TAG, account.getIdToken());
                Log.d(TAG, account.getId());
                Log.d(TAG, account.getEmail());
                sendOAuthLoginRequest(account.getIdToken(), OAUTH_GOOGLE);
                mLoginButton.setEnabled(false);
                mLoginButton.startAnimation();
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Toast.makeText(this, "Google sign in failed", Toast.LENGTH_SHORT).show();
                Log.w(TAG, "Google sign in failed", e);
                // [START_EXCLUDE]
            }
        }
    }

    @Override
    public void onSuccessResponse(String response) {
        Log.d(TAG, "response" + response);
        try {
            JSONObject jsonBody = new JSONObject(response);
            String access_token = jsonBody.getString("access_token");
            String refresh_token = jsonBody.getString("refresh_token");

            FloatApplication.getUserSessionInterface().setRefreshToken(refresh_token);
            FloatApplication.getUserSessionInterface().updateAccessToken(this, access_token);

            SharedPreferences preferences = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(getString(R.string.refresh_token), refresh_token);
            editor.putString(getString(R.string.access_token), access_token);
            editor.apply();
            onSuccessfulLogin(access_token);
            LoginManager.getInstance().logOut();
        } catch (JSONException e) {
            Toast.makeText(this, "Server response error", Toast.LENGTH_SHORT).show();
            mLoginButton.revertAnimation();
            mLoginButton.setEnabled(true);
        }
    }

    private void onSuccessfulLogin(String access_token) {
        UserHandler.getChatToken(this, FloatApplication.getHypermediaInterface(),
                access_token, new UserHandler.ChatTokenCallback() {
                    @Override
                    public void callback(boolean isSuccess) {
                        Log.d(TAG, "Login successful:" + Boolean.toString(isSuccess));
                        if (isSuccess) {
                            FloatApplication.getMessageFeatureHandlerInterface().syncChatUserDatabase();
                            FloatApplication.getMessageFeatureHandlerInterface().initXMPPConnection();
                        }
                        launchMainActivity();
                    }
                });
    }

    private void launchMainActivity() {
        Intent intent = new Intent(LoginActivity.this, MapMainActivity.class);
        intent.setAction(MapMainActivity.ACTION_LOGIN);
        startActivity(intent);
        finish();
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Log.d(TAG, "Error: " + error.getMessage());
        error.printStackTrace();
        if (error.networkResponse != null) {
            switch (error.networkResponse.statusCode) {
                case 401:
                    Toast.makeText(this, "Incorrect username or password", Toast.LENGTH_SHORT).show();
                    break;
                case 500:
                    Toast.makeText(this, "Server error", Toast.LENGTH_SHORT).show();
                    break;
                case 403:
                    Toast.makeText(this, "Please Verify Email", Toast.LENGTH_SHORT).show();
                    break;
                default:
                    Toast.makeText(this, "Unknown error", Toast.LENGTH_SHORT).show();
                    break;
            }
        } else {
            Toast.makeText(this, "No connection", Toast.LENGTH_SHORT).show();
        }
        mLoginButton.revertAnimation();
        mLoginButton.setEnabled(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mLoginButton.dispose();
    }
}

