package com.floatapplication.map.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.floatapplication.R;
import com.floatapplication.shop.models.Shop;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by william on 2018-05-24.
 * Displays shop item card.
 */

public class ShopItemCardFragment extends Fragment {
    public interface MapActivityNavigation {
        void startShopViewActivity(Shop shop);
    }

    private Shop mShop;

    private MapActivityNavigation mNavigation;

    @BindView(R.id.shop_name)
    TextView mShopName;
    @BindView(R.id.shop_description)
    TextView mShopDescription;
    @BindView(R.id.view_button)
    Button mViewButton;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = getActivity();
        if (activity instanceof MapActivityNavigation) {
            mNavigation = (MapActivityNavigation) activity;
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shop_item_card, container, false);
        ButterKnife.bind(this, v);
        setupButtonListeners();
        return v;
    }

    public void setShop(Shop shop) {
        mShop = shop;
        mShopName.setText(mShop.getShopName());
        mShopDescription.setText(mShop.getShopDescription());
    }

    private void setupButtonListeners() {
        mViewButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mNavigation.startShopViewActivity(mShop);
            }
        });
    }
}
