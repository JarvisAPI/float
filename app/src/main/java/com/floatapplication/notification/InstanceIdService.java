package com.floatapplication.notification;

/*
 * Created by jamescho on 2018-05-31.
 *
 */

import android.util.Log;

import com.android.volley.VolleyError;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;

import java.util.HashMap;
import java.util.Map;

public class InstanceIdService extends FirebaseInstanceIdService {
    public InstanceIdService() {
        super();
    }

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String token = FirebaseInstanceId.getInstance().getToken();
        sendToServer(token);
    }

    private void sendToServer(String token) {
        if (FloatApplication.getUserSessionInterface().isLoggedIn()) {
            Log.d("TAG",token);
            Map<String, String> variables = new HashMap<>();
            variables.put("notification_token", token);
            FloatApplication.getHypermediaInterface().authFollow(Hypermedia.POST, getApplicationContext(),
                    Hypermedia.UPDATE_CHAT_NOTIFICATION_TOKEN, variables, null,
                    new ServerCallback() {
                        @Override
                        public void onSuccessResponse(String response) {
                            // nothing needs to be done
                        }
                    },
                    new ServerErrorCallback() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });
        }
    }
}
