package com.floatapplication.message.MessageTestMockObjects;

import com.floatapplication.messages.activity.activity_helpers.MessageLoader;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.models.User;
import com.floatapplication.messages.server_connection.ChatConnectionService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * Created by jamescho on 2018-06-15.
 *
 */

public class ArchiveServerDatabaseMock {
    public final static String CLIENT_USERNAME = "test_user";
    public final static String DIALOG_JID1 = "user1";
    public final static String DIALOG_JID2 = "user2";
    public final static String DIALOG_JID3 = "user3";
    public final static String DIALOG_JID4 = "user4";
    public final static String FIXED_TIMESTAMP = "1528402980000";
    public final static String FIXED_ARCHIVE_ID = "1528402980000000";

    private int counter = 0;
    private Set<CustomMessage> mostRecentMessages;

    private static ArchiveServerDatabaseMock archiveServerDatabaseMock;

    private HashMap<String, HashMap<String,List<CustomMessage>>> mockdb = new HashMap<>();


    private ArchiveServerDatabaseMock(){
    }

    public static ArchiveServerDatabaseMock getInstance(){
        if(archiveServerDatabaseMock == null){
            archiveServerDatabaseMock = new ArchiveServerDatabaseMock();
        }
        return archiveServerDatabaseMock;
    }

    public ArrayList<CustomMessage> getArchiveMessagesAfter(CustomMessage message, int numMessages){
        List<CustomMessage> messages = mockdb.get(CLIENT_USERNAME).get(message.getUser().getId());
        Collections.sort(messages,new sortMessages());
        for(CustomMessage msg: messages){
            if(  msg.getCreatedAt().getTime() < message.getCreatedAt().getTime()){
                messages.remove(msg);
            }
        }
        return new ArrayList<>(messages.size() > numMessages ? (messages.subList(0, numMessages)) : messages);
    }

    public String loadMockDatabase1(){
        mockdb = new HashMap<>();
        HashMap<String,List<CustomMessage>> userDialogs = new HashMap<>();
        ArrayList<CustomMessage> conversation1 = new ArrayList<>();
        CustomMessage msg = getMessageFixture(DIALOG_JID1);
        conversation1.add(msg);
        userDialogs.put(msg.getUser().getId() ,conversation1);

        ArrayList<CustomMessage> conversation2 = new ArrayList<>();
        msg = getMessageFixture(DIALOG_JID2);
        conversation2.add(msg);
        userDialogs.put(msg.getUser().getId() ,conversation2);

        ArrayList<CustomMessage> conversation3 = new ArrayList<>();
        msg = getMessageFixture(DIALOG_JID3);
        conversation3.add(msg);
        userDialogs.put(msg.getUser().getId() ,conversation3);

        ArrayList<CustomMessage> conversation4 = new ArrayList<>();
        msg = getMessageFixture(DIALOG_JID4);
        conversation4.add(msg);
        userDialogs.put(msg.getUser().getId() ,conversation4);
        mockdb.put(CLIENT_USERNAME,userDialogs);

        this.mostRecentMessages = new HashSet<>();
        mostRecentMessages.add(conversation1.get(conversation1.size()-1));
        mostRecentMessages.add(conversation2.get(conversation2.size()-1));
        mostRecentMessages.add(conversation3.get(conversation3.size()-1));
        mostRecentMessages.add(conversation4.get(conversation4.size()-1));

        String response = "";
        try {
            response = createJsonResponse(userDialogs);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }


    public String loadMockDatabase2(){
        mockdb = new HashMap<>();
        HashMap<String,List<CustomMessage>> userDialogs = new HashMap<>();
        ArrayList<CustomMessage> conversation1 = new ArrayList<>();
        CustomMessage msg = getMessageFixture(DIALOG_JID1);
        conversation1.add(msg);
        userDialogs.put(msg.getUser().getId() ,conversation1);

        ArrayList<CustomMessage> conversation2 = new ArrayList<>();
        msg = getMessageFixture(DIALOG_JID2);
        conversation2.add(msg);
        userDialogs.put(msg.getUser().getId() ,conversation2);

        ArrayList<CustomMessage> conversation3 = new ArrayList<>();
        msg = getMessageFixture(DIALOG_JID3);
        conversation3.add(msg);
        userDialogs.put(msg.getUser().getId() ,conversation3);

        ArrayList<CustomMessage> conversation4 = new ArrayList<>();
        msg = getMessageFixture(DIALOG_JID4);
        conversation4.add(msg);
        userDialogs.put(msg.getUser().getId() ,conversation4);
        mockdb.put(CLIENT_USERNAME,userDialogs);

        this.mostRecentMessages = new HashSet<>();
        mostRecentMessages.add(conversation1.get(conversation1.size()-1));
        mostRecentMessages.add(conversation2.get(conversation2.size()-1));
        mostRecentMessages.add(conversation3.get(conversation3.size()-1));
        mostRecentMessages.add(conversation4.get(conversation4.size()-1));

        String response = "";
        try {
            response = createJsonResponse(userDialogs);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String loadMockDatabase3(){
        mockdb = new HashMap<>();
        HashMap<String,List<CustomMessage>> userDialogs = new HashMap<>();
        ArrayList<CustomMessage> conversation1 = new ArrayList<>();
        CustomMessage mostRecentMessageConv1 = null;
        for(int n = 0; n < MessageLoader.PAGE_SIZE+1 ; n++){
            mostRecentMessageConv1 = createRandomMessage(DIALOG_JID1);
            conversation1.add(mostRecentMessageConv1);
        }
        userDialogs.put(DIALOG_JID1, conversation1);

        ArrayList<CustomMessage> conversation2 = new ArrayList<>();
        CustomMessage mostRecentMessageConv2 = null;
        for(int n = 0; n < MessageLoader.PAGE_SIZE-1 ; n++){
            mostRecentMessageConv2 = createRandomMessage(DIALOG_JID2);
            conversation2.add(mostRecentMessageConv2);
        }
        userDialogs.put(DIALOG_JID2, conversation2);

        ArrayList<CustomMessage> conversation3 = new ArrayList<>();
        CustomMessage mostRecentMessageConv3 = null;
        for(int n = 0; n < (MessageLoader.PAGE_SIZE) ; n++){
            mostRecentMessageConv3 = createRandomMessage(DIALOG_JID3);
            conversation3.add(mostRecentMessageConv3);
        }
        userDialogs.put(DIALOG_JID3,conversation3);

        ArrayList<CustomMessage> conversation4 = new ArrayList<>();
        CustomMessage mostRecentMessageConv4 = getMessageFixture(DIALOG_JID4);
        conversation4.add(mostRecentMessageConv4);
        userDialogs.put(DIALOG_JID4,conversation4);

        this.mostRecentMessages = new HashSet<>();
        mostRecentMessages.add(mostRecentMessageConv1);
        mostRecentMessages.add(mostRecentMessageConv2);
        mostRecentMessages.add(mostRecentMessageConv3);
        mostRecentMessages.add(mostRecentMessageConv4);


        mockdb.put(CLIENT_USERNAME,userDialogs);

        String response = "";
        try {
            response = createJsonResponse(userDialogs);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }

    public Set<CustomMessage> getActualMostRecentMessages(){
        return mostRecentMessages;
    }

    private String createJsonResponse(HashMap<String,List<CustomMessage>> dialogs) throws JSONException {
        Set<String> jids = dialogs.keySet();
        JSONObject response = new JSONObject();
        JSONArray JsonDialogs = new JSONArray();

        for(String jid: jids) {
            List<CustomMessage> conversation = dialogs.get(jid);
            CustomMessage mostRecentMsg = conversation.get(conversation.size() - 1);
            JSONObject dialog = new JSONObject();
            dialog.put("username", CLIENT_USERNAME);
            dialog.put("timestamp", mostRecentMsg.getArchiveId());
            dialog.put("bare_peer", jid);
            dialog.put("xml", "id='" +mostRecentMsg.getId() + "'");
            dialog.put("txt", mostRecentMsg.getText());
            dialog.put("unread_count", "0");
            JsonDialogs.put(dialog);
        }
        response.put("dialog_list",JsonDialogs);
        return response.toString();
    }

    public static CustomMessage getMessageFixture(String dialogJid){
        switch (dialogJid){
            case DIALOG_JID1: {
                String id = "6nO0j-94";
                User user = new User(DIALOG_JID1);
                long timeStamp = Long.valueOf(FIXED_TIMESTAMP);
                String text = "abc";
                ChatConnectionService.MessageType msgType = ChatConnectionService.MessageType.SEND;
                return new CustomMessage(id, user, timeStamp, text, FIXED_ARCHIVE_ID, msgType);
            }
            case DIALOG_JID2: {
                String id = "NDw4a-61";
                User user = new User(ArchiveServerDatabaseMock.DIALOG_JID2);
                long timeStamp = Long.valueOf(FIXED_TIMESTAMP);
                String text = "abc";
                ChatConnectionService.MessageType msgType = ChatConnectionService.MessageType.SEND;
                return new CustomMessage(id, user, timeStamp, text, FIXED_ARCHIVE_ID, msgType);
            }
            case DIALOG_JID3: {
                String id = "qIn3O-49";
                User user = new User(ArchiveServerDatabaseMock.DIALOG_JID3);
                long timeStamp = Long.valueOf(FIXED_TIMESTAMP);
                String text = "abc";
                ChatConnectionService.MessageType msgType = ChatConnectionService.MessageType.SEND;
                return new CustomMessage(id, user, timeStamp, text, FIXED_ARCHIVE_ID, msgType);
            }
            case DIALOG_JID4: {
                String id = "6eO0g-94";
                User user = new User(DIALOG_JID4);
                long timeStamp = Long.valueOf(FIXED_TIMESTAMP);
                String text = "abc";
                ChatConnectionService.MessageType msgType = ChatConnectionService.MessageType.SEND;
                return new CustomMessage(id, user, timeStamp, text, FIXED_ARCHIVE_ID, msgType);
            }
        }
        return null;
    }

    private CustomMessage createRandomMessage(String jid){
        Random r = new Random();
        String id = Long.toString(r.nextLong());
        User user = new User(jid);
        long timeStamp = Long.valueOf(createTimeStamp());
        String archiveId = createArchiveID(Long.toString(timeStamp));
        String text = "abc";
        ChatConnectionService.MessageType msgType = ChatConnectionService.MessageType.SEND;
        return new CustomMessage(id, user, timeStamp, text, archiveId, msgType);
    }

    private String createTimeStamp(){
        String additionalTime = Integer.toString(counter);
        counter ++;
        return FIXED_TIMESTAMP.substring(0, FIXED_TIMESTAMP.length() - additionalTime.length()) + additionalTime;
    }

    private String createArchiveID(String timestamp){
        return timestamp + "000";
    }

    private class sortMessages implements Comparator<CustomMessage> {
        // Used for sorting in desc order
        public int compare(CustomMessage a, CustomMessage b) {
            return new BigInteger(a.getArchiveId()).compareTo(new BigInteger(b.getArchiveId()));
        }
    }
}
