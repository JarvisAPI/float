package com.floatapplication.search;

import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.product.models.Product;
import com.floatapplication.search.SearchPageContract.OnItemsLoadedListener;
import com.floatapplication.util.Extractor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;
import java.util.Map;

import static com.floatapplication.hypermedia.HypermediaInterface.SEARCH_PRODUCTS;
import static com.floatapplication.search.SearchQuery.LIMIT;
import static com.floatapplication.search.SearchQuery.OFFSET;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class SearchPageModelUnitTest {
    private SearchPageModel mModel;
    @Mock private HypermediaInterface mHypermedia;
    @Mock private SearchQuery mSearchQuery;
    @Mock private Map<String, String> mQueries;
    @Mock private Extractor mExtractor;

    @Before
    public void setup() {
        initMocks(this);
        mModel = new SearchPageModel();
        FloatApplication.setHypermediaInterface(mHypermedia);
        when(mSearchQuery.toQueryParams()).thenReturn(mQueries);
        Extractor.setExtractor(mExtractor);
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
        Extractor.setExtractor(null);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_reload() {
        mModel.reload(mSearchQuery);
        verify(mHypermedia).follow(eq(HypermediaInterface.GET), eq(SEARCH_PRODUCTS), anyMap(),
                 anyMap(),eq(mQueries), eq(mModel), eq(mModel));

        verify(mQueries).put(OFFSET, String.valueOf(mModel.mOffset));
        verify(mQueries).put(LIMIT, String.valueOf(mModel.mLimit));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_loadMoreItems_afterReload_and_SuccessResponse() {
        List<Product> products = mock(List.class);
        when(products.size()).thenReturn(10);
        when(mExtractor.extractProducts(anyString())).thenReturn(products);
        OnItemsLoadedListener<Product> listener = mock(OnItemsLoadedListener.class);
        mModel.setOnItemsLoadedListener(listener);

        mModel.reload(mSearchQuery);
        mModel.onSuccessResponse("response");
        mModel.loadMoreItems();

        verify(listener).onItemsLoaded(products);
        assertEquals(products.size(), mModel.mOffset);
        verify(mHypermedia, times(2)).follow(eq(HypermediaInterface.GET), eq(SEARCH_PRODUCTS), anyMap(), anyMap(),
                eq(mQueries), eq(mModel), eq(mModel));
        verify(mQueries).put(OFFSET, String.valueOf(mModel.mOffset));
    }
}
