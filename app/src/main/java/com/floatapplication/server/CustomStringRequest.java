package com.floatapplication.server;

import com.android.volley.AuthFailureError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by william on 2018/1/25.
 * A class that allows you to send string requests with a string body to an http server.
 */
public class CustomStringRequest extends StringRequest {
    private Map<String, String> mHeaders;
    private String mBodyContentType;
    private String mRequestBody;

    CustomStringRequest(int method, String url, final ServerCallback callback,
                        final ServerErrorCallback errorCallback,
                        Map<String, String> headers) {
        super(method, url,
                (String response) -> {
                    if (callback != null) {
                        callback.onSuccessResponse(response);
                    }
                },
                (VolleyError error) -> {
                    if (errorCallback != null) {
                        errorCallback.onErrorResponse(error);
                    }
                });
        // Should cache for only specific endpoints, but that is not implemented currently
        setShouldCache(false);
        mHeaders = headers;
        mBodyContentType = "application/json; charset=utf-8";
    }

    void setRequestBody(String requestBody) {
        mRequestBody = requestBody;
    }

    @Override
    public String getBodyContentType() {
        return mBodyContentType;
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        try {
            return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
            return null;
        }
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return (mHeaders != null) ? mHeaders : super.getHeaders();
    }
}


