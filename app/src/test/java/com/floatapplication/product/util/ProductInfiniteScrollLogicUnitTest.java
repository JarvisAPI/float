package com.floatapplication.product.util;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.TestApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.map.models.Icon;
import com.floatapplication.product.models.Product;
import com.floatapplication.product.util.ProductInfiniteScrollLogic.OnProductsObtainedListener;
import com.floatapplication.product.util.ProductInfiniteScrollStateMachine.Event;
import com.floatapplication.product.util.ProductShopQueryQueue.ProductShopQuery;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.simplexorg.mapfragment.model.GeoPoint;

import org.hamcrest.collection.IsCollectionWithSize;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by william on 2018-05-28.
 * Test product infinite scroll logic.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ProductInfiniteScrollLogicUnitTest {
    private ProductInfiniteScrollLogic logic;
    private ProductInfiniteScrollStateMachine stateMachine;
    private ProductShopQueryQueue productShopQueryQueue;
    private HypermediaInterface hypermedia;

    @Before
    public void setup() {
        hypermedia = mock(HypermediaInterface.class);
        FloatApplication.setHypermediaInterface(hypermedia);
        GeoPoint pos = new GeoPoint(0, 0);
        stateMachine = mock(ProductInfiniteScrollStateMachine.class);
        productShopQueryQueue = mock(ProductShopQueryQueue.class);
        logic = new ProductInfiniteScrollLogic(pos, stateMachine, productShopQueryQueue);
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    private JSONObject genStubIconJson(String icon_id) throws Exception {
        return new JSONObject()
                .put(Icon.ID, icon_id);
    }

    private String genStubIconsJsonArray(int size) throws Exception {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < size; i++) {
            jsonArray.put(genStubIconJson(String.valueOf(i)));
        }
        return new JSONObject()
                .put("icons", jsonArray).toString();
    }

    private JSONObject genStubIconProduct(String productId) throws Exception {
        return new JSONObject()
                .put(Product.ID, productId)
                .put(Product.NAME, "name")
                .put(Product.PRICE, "10")
                .put(Product.MAIN_IMAGE, "file");
    }

    private String genStubProductsJsonArray(int size) throws Exception {
        JSONArray jsonArray = new JSONArray();
        for (int i = 0; i < size; i++) {
            jsonArray.put(genStubIconProduct(String.valueOf(i)));
        }
        return new JSONObject()
                .put("product_list", jsonArray).toString();
    }

    @Test
    public void getShopsSuccessful_shouldCallStateMachine_withShopsObtained() throws Exception {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerCallback callback = invocation.getArgumentAt(3, ServerCallback.class);
                callback.onSuccessResponse(genStubIconsJsonArray(1));
                return null;
            }
        }).when(hypermedia).follow(eq(Hypermedia.GET), any(String.class), anyMapOf(String.class, String.class), anyMapOf(String.class, String.class),
                any(ServerCallback.class), any(ServerErrorCallback.class));
        logic.getShops();
        verify(stateMachine).run(Event.OBTAINED_SHOPS);
    }

    @Test
    public void getShopsUnsuccessful_shouldCallStateMachine_withNoShopsObtained() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerErrorCallback errorCallback = invocation.getArgumentAt(4, ServerErrorCallback.class);
                errorCallback.onErrorResponse(new VolleyError("error"));
                return null;
            }
        }).when(hypermedia).follow(eq(Hypermedia.GET), any(String.class), anyMapOf(String.class, String.class), anyMapOf(String.class, String.class),
                any(ServerCallback.class), any(ServerErrorCallback.class));
        logic.getShops();
        verify(stateMachine).run(Event.NO_SHOPS_OBTAINED);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void createProductShopQuery_thenExecuteQuery_getServerSuccessWithLessThanLimit_shouldRemoveShopFromQuery() {
        String shopId = "id";
        int offset = 0;
        int limit = 5;
        final int lessThanLimit = 2;
        OnProductsObtainedListener listener = mock(OnProductsObtainedListener.class);
        logic.setOnProductsObtainedListener(listener);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                String response = genStubProductsJsonArray(lessThanLimit);
                ServerCallback callback = invocation.getArgumentAt(3, ServerCallback.class);
                callback.onSuccessResponse(response);
                return null;
            }
        }).when(hypermedia).follow(eq(Hypermedia.GET), any(String.class), anyMapOf(String.class, String.class),
                anyMapOf(String.class, String.class), any(ServerCallback.class), any(ServerErrorCallback.class));
        ProductShopQuery productShopQuery = logic.create(shopId, offset, limit);
        productShopQuery.execute();
        verify(listener).onProductsObtained((List) argThat(IsCollectionWithSize.hasSize(equalTo(lessThanLimit))));
        verify(productShopQueryQueue).removeShopFromQuery(shopId);
    }

    @Test
    public void createProductShopQuery_thenExecuteQuery_getServerSuccessWithZeroProducts_shouldRemoveShopFromQuery() {
        String shopId = "id";
        int offset = 0;
        int limit = 5;
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                String response = genStubProductsJsonArray(0);
                ServerCallback callback = invocation.getArgumentAt(3, ServerCallback.class);
                callback.onSuccessResponse(response);
                return null;
            }
        }).when(hypermedia).follow(eq(Hypermedia.GET), any(String.class), anyMapOf(String.class, String.class),
                anyMapOf(String.class, String.class), any(ServerCallback.class), any(ServerErrorCallback.class));
        ProductShopQuery productShopQuery = logic.create(shopId, offset, limit);
        productShopQuery.execute();
        verify(productShopQueryQueue).removeShopFromQuery(shopId);
    }

    @Test
    public void createProductShopQuery_thenExecuteQuery_getServerError_shouldRemoveShopFromQuery() {
        String shopId = "id";
        int offset = 0;
        int limit = 5;
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                VolleyError error = new VolleyError(new NetworkResponse(500, null, null, false));
                ServerErrorCallback errorCallback = invocation.getArgumentAt(4, ServerErrorCallback.class);
                errorCallback.onErrorResponse(error);
                return null;
            }
        }).when(hypermedia).follow(eq(Hypermedia.GET), any(String.class), anyMapOf(String.class, String.class),
                anyMapOf(String.class, String.class), any(ServerCallback.class), any(ServerErrorCallback.class));
        ProductShopQuery productShopQuery = logic.create(shopId, offset, limit);
        productShopQuery.execute();
        verify(productShopQueryQueue).removeShopFromQuery(shopId);
    }
}
