package com.floatapplication.shop.util;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.TestApplication;
import com.floatapplication.product.models.Product;
import com.floatapplication.shop.adapter.ShopEditAdapter;
import com.floatapplication.util.GlideRequest;
import com.floatapplication.util.GlideRequests;
import com.floatapplication.util.GlobalHandler;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by william on 2018-05-24.
 * Tests presenting and deleting.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ShopEditProductPresenterUnitTest {
    private ShopEditProductPresenter presenter;
    @Mock private ShopEditAdapter adapter;
    @Mock private ImageView productImage;
    @Mock private TextView productName;
    @Mock private TextView productPrice;
    @Mock private ImageView productDelete;
    @Mock private Product product;
    @Mock private GlobalHandler globalHandler;

    @Before
    @SuppressWarnings("unchecked")
    public void setup() {
        initMocks(this);
        GlideRequests glideRequests = mock(GlideRequests.class);
        GlideRequest gRequest = mock(GlideRequest.class);
        when(gRequest.placeholder(anyInt())).thenReturn(gRequest);
        when(glideRequests.load(any(String.class))).thenReturn(gRequest);
        GlobalHandler.setGlobalHandler(globalHandler);
        presenter = new ShopEditProductPresenter()
                .setGlide(glideRequests)
                .setAdapter(adapter);
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    private void present(int position) {
        presenter.present(productImage, productName, productPrice, productDelete,
                product, position);
    }

    @Test
    public void verifyPresentSequence_isCorrect() {
        String name = "name";
        String price = "10";
        when(product.getProductName()).thenReturn(name);
        when(product.getProductPrice()).thenReturn(price);
        present(0);
        verify(productName).setText(name);
        verify(productPrice).setText(price);
        verify(productImage).setTag(R.id.tag0, product);
        verify(productImage).setOnClickListener(presenter);
    }

    @Test
    public void deleteIconIsVisible_ifDeleteStatusIsTrue() {
        presenter.setDelete(true);
        int pos = 0;
        present(pos);
        verify(productDelete).setVisibility(View.VISIBLE);
        verify(productDelete).setTag(R.id.tag0, pos);
        verify(productDelete).setTag(R.id.tag1, product);
        verify(productDelete).setOnClickListener(presenter);
        verify(adapter).notifyDataSetChanged();
    }

    @Test
    public void deleteIconIsGone_ifDeleteStatusIsFalse() {
        present(0);
        verify(productDelete).setVisibility(View.GONE);
    }
}
