package com.floatapplication.util;

import android.Manifest.permission;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.provider.MediaStore.Images.Media;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;

public class UploadImageHandler {
    private static final String TAG = UploadImageHandler.class.getSimpleName();
    public static final int REQUEST_READ_EXTERNAL_PERMISSION = 12;
    public static final int REQUEST_IMAGE_GALLERY = 11;

    public void dispatchUploadImageIntent(Activity activity) {
        if (ContextCompat.checkSelfPermission(activity, permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Requesting permission");
            ActivityCompat.requestPermissions(activity,
                    new String[]{permission.READ_EXTERNAL_STORAGE},
                    REQUEST_READ_EXTERNAL_PERMISSION);
            return;
        }
        Intent intent = new Intent(Intent.ACTION_PICK, Media.INTERNAL_CONTENT_URI);
        activity.startActivityForResult(intent, REQUEST_IMAGE_GALLERY);
    }

    public void dispatchUploadImageIntent(Fragment fragment) {
        if (fragment.getContext() != null) {
            if (ContextCompat.checkSelfPermission(fragment.getContext(), permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED) {
                Log.d(TAG, "Requesting permission");
                fragment.requestPermissions(
                        new String[]{permission.READ_EXTERNAL_STORAGE},
                        REQUEST_READ_EXTERNAL_PERMISSION);
                return;
            }
            Intent intent = new Intent(Intent.ACTION_PICK, Media.INTERNAL_CONTENT_URI);
            fragment.startActivityForResult(intent, REQUEST_IMAGE_GALLERY);
        }
    }
}
