package com.floatapplication.server;

/**
 * Created by william on 2018-12-24.
 * keeps track of the different types of errors sent back from the server.
 */

public class ServerErrorCode {
    public static final String FIELD_ERROR = "error";
    public static final String FIELD_MESSAGE = "message";
    public static final String FIELD_TYPE = "type";

    public static final String ERR_SHOP_TOO_CLOSE = "ERR_SHOP_TOO_CLOSE";
    public static final String ERR_FILE_TOO_LARGE = "ERR_FILE_TOO_LARGE";
    public static final String ERR_SHOP_LIMIT_REACHED = "ERR_SHOP_LIMIT_REACHED";
    public static final String ERR_PRODUCT_LIMIT_REACHED = "ERR_PRODUCT_LIMIT_REACHED";
}
