package com.floatapplication.search;


import com.floatapplication.product.models.Product;
import com.simplexorg.mapfragment.model.GeoPoint;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class SearchPagePresenterUnitTest {
    private SearchPagePresenter mPresenter;
    @Mock private SearchPageContract.View<Product> mView;
    @Mock private SearchPageContract.Model<Product> mModel;
    @Mock private SearchQuery mInitialSearchQuery;

    @Before
    public void setup() {
        initMocks(this);
        mPresenter = new SearchPagePresenter();
        mPresenter.attach(mView, mModel, mInitialSearchQuery);
    }

    @Test
    public void test_onSearch() {
        String query = "query";
        SearchQuery searchQuery = mock(SearchQuery.class);
        when(mInitialSearchQuery.modify(query)).thenReturn(searchQuery);

        mPresenter.onSearch(query);

        verify(mView).clearSearchResults();
        assertEquals(searchQuery, mPresenter.mInitialSearchQuery);
        verify(mModel).reload(searchQuery);
    }

    @Test
    public void test_onItemClick() {
        Product item = mock(Product.class);
        mPresenter.onItemClick(item);

        verify(mView).displayDetails(item);
    }

    @Test
    public void test_onLoadMore() {
        mPresenter.onLoadMore();

        verify(mModel).loadMoreItems();
    }

    @Test
    public void test_subscribe() {
        mPresenter.mInitialSearchQuery = new SearchQuery("query", mock(GeoPoint.class));
        mPresenter.subscribe();

        verify(mView).setSearchText(mPresenter.mInitialSearchQuery.query);
        verify(mModel).setOnItemsLoadedListener(mPresenter);
        verify(mModel).reload(mPresenter.mInitialSearchQuery);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_onItemsLoaded() {
        List<Product> items = mock(List.class);
        mPresenter.onItemsLoaded(items);

        verify(mView).displaySearchResults(items);
    }
}
