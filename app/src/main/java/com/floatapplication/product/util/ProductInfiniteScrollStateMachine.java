package com.floatapplication.product.util;

/**
 * Created by william on 2018-05-28.
 * State machine for achieving infinite scrolling product page.
 */

public class ProductInfiniteScrollStateMachine {
    public enum State {
        GET_NEARBY_SHOPS, LOAD_PRODUCTS_FROM_SHOPS, END
    }
    public enum Event {
        OBTAINED_SHOPS, NO_SHOPS_OBTAINED, HIT_BOTTOM_SHOP_LIST_EMPTY, HIT_BOTTOM_SHOP_LIST_NOT_EMPTY
    }
    private State mState;

    private ProductInfiniteScrollStateLogic mLogic;

    void setLogicImplementer(ProductInfiniteScrollStateLogic logic) {
        mLogic = logic;
    }

    public void start() {
        mState = State.GET_NEARBY_SHOPS;
        mLogic.getShops();
    }

    public void run(Event event) {
        switch (mState) {
            case GET_NEARBY_SHOPS:
                runGetNearbyShops(event);
                break;
            case LOAD_PRODUCTS_FROM_SHOPS:
                runLoadProductsFromShops(event);
                break;
            case END:
        }
    }

    private void runGetNearbyShops(Event event) {
        switch (event) {
            case OBTAINED_SHOPS:
                mLogic.loadProductFromShops();
                mState = State.LOAD_PRODUCTS_FROM_SHOPS;
                break;
            case NO_SHOPS_OBTAINED:
                mState = State.END;
                break;
        }
    }

    private void runLoadProductsFromShops(Event event) {
        switch (event) {
            case HIT_BOTTOM_SHOP_LIST_NOT_EMPTY:
                mLogic.loadProductFromShops();
                break;
            case HIT_BOTTOM_SHOP_LIST_EMPTY:
                mLogic.getShops();
                mState = State.GET_NEARBY_SHOPS;
                break;
        }
    }
}
