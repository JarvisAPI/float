package com.floatapplication.messages.message_database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.messages.models.ChatUserCacheEntry;
import com.floatapplication.messages.models.User;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.util.Util;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ChatUserCache extends SQLiteOpenHelper {

    public interface ChatUserCacheCallback{
        void successCallback(ChatUserCacheEntry entry);
        void errorCallback();
    }

    private interface ServerDataCallback{
        void successCallback(ArrayList<ChatUserCacheEntry> entries);
        void errorCallback();
    }

    private String SERVER_JSON_KEY_SHOPS = "shop";
    private String SERVER_JSON_KEY_USERS = "user";
    private String SERVER_JSON_KEY_IMAGE = "image";
    private String SERVER_JSON_KEY_NAME = "name";

    private Context mContext;
    private static final String TAG = "ChatUserCache";
    private static final String TABLE_NAME = "known_chat_jids";
    private static final String COL_JID = "jid";
    private static final String COL_NAME = "name";
    private static final String COL_PROFILE_IMAGE = "profile_image";
    private static final String COL_TYPE = "type";  // user | shop
    private static final int COL_JID_NUM = 0;
    private static final int COL_NAME_NUM = 1;
    private static final int COL_PROFILE_IMAGE_NUM = 2;
    private static final int COL_TYPE_NUM = 3;

    public ChatUserCache(Context context) {
        super(context, TABLE_NAME, null, 1);
        mContext = context;
        onCreate(getWritableDatabase());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG,"onCreate() message Database");
        String createTable = "CREATE TABLE IF NOT EXISTS "
                + TABLE_NAME
                + "("
                + COL_JID + " TEXT,"
                + COL_NAME + " TEXT,"
                + COL_PROFILE_IMAGE +" TEXT,"
                + COL_TYPE + " TEXT,"
                + "PRIMARY KEY (" + COL_JID+")"
                + ")";
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        Log.d(TAG,"onUpgrade()");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void updateCachedUsers(ArrayList<String> users){
        JSONArray usernames = new JSONArray();
        for(String jid:users){
            usernames.put(User.formatJid(jid));
        }
        requestDataToServer(usernames, new ServerDataCallback() {
            @Override
            public void successCallback(ArrayList<ChatUserCacheEntry> entries) {
                for(ChatUserCacheEntry entry: entries){
                    insertData(entry);
                }
            }

            @Override
            public void errorCallback() {
            }
        });
    }

    public void getUser(String jid, ChatUserCacheCallback callback){
        Log.d(TAG, "getCachedUser()");
        jid = User.formatJid(jid);
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME
                + " WHERE " + COL_JID + "=" + "'" + jid + "'";

        Log.d(TAG, query);
        Cursor curs = db.rawQuery(query, null);
        try  {
            if (curs.getCount() == 0) {
                Log.d(TAG, "cache Miss -----------");
                JSONArray username = new JSONArray();
                username.put(jid);
                requestDataToServer(username, new ServerDataCallback() {
                    @Override
                    public void successCallback(ArrayList<ChatUserCacheEntry> entries) {
                        if (entries.size() == 0) {
                            callback.errorCallback();
                        } else {
                            insertData(entries.get(0));
                            callback.successCallback(entries.get(0));

                        }
                    }

                    @Override
                    public void errorCallback() {
                        callback.errorCallback();
                    }
                });
            } else {
                curs.moveToFirst();
                Log.d(TAG, "GETTING cache entry -----------");
                Log.d(TAG, curs.getString(COL_JID_NUM));
                Log.d(TAG, curs.getString(COL_NAME_NUM));
                Log.d(TAG, curs.getString(COL_PROFILE_IMAGE_NUM));
                Log.d(TAG, User.Type.valueOf(curs.getString(COL_TYPE_NUM)).toString());
                Log.d(TAG, "-----------------");
                callback.successCallback(
                        new ChatUserCacheEntry(
                                curs.getString(COL_JID_NUM),
                                curs.getString(COL_NAME_NUM),
                                curs.getString(COL_PROFILE_IMAGE_NUM),
                                User.Type.valueOf(curs.getString(COL_TYPE_NUM))));
            }
        } finally {
            curs.close();
        }
    }

    private void requestDataToServer(JSONArray jids, ServerDataCallback callback){
        Map<String, String> vMap = new HashMap<>();
        vMap.put("users", jids.toString());
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.GET,
                mContext, HypermediaInterface.GET_CHAT_PROFILES, vMap,null, new ServerCallback() {
                    @Override
                    public void onSuccessResponse(String response) {
                        Log.d(TAG, "chat user profiles retrieved");
                        try {
                            Log.d(TAG,response);
                            ArrayList<ChatUserCacheEntry> entries = new ArrayList<>();
                            for(int i=0; i<jids.length();i++){
                                String jid = jids.getString(i);
                                JSONObject res = new JSONObject(response);
                                JSONObject shops = res.getJSONObject(SERVER_JSON_KEY_SHOPS);
                                JSONObject users = res.getJSONObject(SERVER_JSON_KEY_USERS);
                                if(shops.has(jid)){
                                    JSONObject profile = shops.getJSONObject(jid);
                                    String profileImage = profile.getString(SERVER_JSON_KEY_IMAGE);
                                    profileImage = profileImage.equals("") ? "" : Util.getInstance().constructImageUrl(profileImage);
                                    ChatUserCacheEntry cacheEntry = new ChatUserCacheEntry(jid, profile.getString(SERVER_JSON_KEY_NAME), profileImage, User.Type.shop);
                                    entries.add(cacheEntry);
                                } else if(users.has(jid)){
                                    JSONObject profile = users.getJSONObject(jid);
                                    String profileImage = profile.getString(SERVER_JSON_KEY_IMAGE);
                                    profileImage = profileImage.equals("") ? "" : Util.getInstance().constructImageUrl(profileImage);
                                    ChatUserCacheEntry cacheEntry = new ChatUserCacheEntry(jid, profile.getString(SERVER_JSON_KEY_NAME), profileImage, User.Type.user);
                                    entries.add(cacheEntry);
                                }
                            }
                            callback.successCallback(entries);
                        } catch (Exception e) {
                            e.printStackTrace();
                            callback.errorCallback();
                        }
                    }
                },
                new ServerErrorCallback() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        callback.errorCallback();
                    }
                });
    }

    private void insertData(ChatUserCacheEntry cacheEntry){
        Log.d(TAG, "adding to cache entry -----------");
        Log.d(TAG, cacheEntry.getJid());
        Log.d(TAG, cacheEntry.getName());
        Log.d(TAG, cacheEntry.getProfileImage());
        Log.d(TAG, cacheEntry.getType().toString());
        Log.d(TAG, "-----------------");
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_JID, cacheEntry.getJid());
        contentValues.put(COL_NAME, cacheEntry.getName());
        contentValues.put(COL_PROFILE_IMAGE, cacheEntry.getProfileImage());
        contentValues.put(COL_TYPE, cacheEntry.getType().toString());
        db.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
    }



}
