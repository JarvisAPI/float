package com.floatapplication.map.models;

import com.floatapplication.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by william on 2017/12/24.
 * Represents a map icon.
 */
public class Icon {
    public static class IconPair {
        public final int selectedRes;
        public final int res;
        public final int type;
        IconPair(int res, int selectedRes, int type) {
            this.res = res;
            this.selectedRes = selectedRes;
            this.type = type;
        }
    }
    public static final String ID = "icon_id";
    public static final String LATITUDE = "icon_latitude";
    public static final String LONGITUDE = "icon_longitude";
    public static final String TYPE = "icon_type";

    private static final int BASIC_RESTAURANT = 0;
    private static final int BASIC_GENERAL_STORE = 1;
    private static final int BASIC_MARKET = 2;

    private String mIconId;
    private double mIconLat;
    private double mIconLng;
    private int mIconType;

    private Icon() {

    }

    public static Icon createIcon(JSONObject iconJson) throws JSONException {
        Icon icon = new Icon();
        icon.mIconId = iconJson.getString(Icon.ID);
        icon.mIconLat = iconJson.getDouble(Icon.LATITUDE);
        icon.mIconLng = iconJson.getDouble(Icon.LONGITUDE);
        icon.mIconType = iconJson.getInt(Icon.TYPE);
        return icon;
    }

    public String getIconId() {
        return mIconId;
    }

    /**
     * Get icon type from the drawable resource.
     * @param resource the drawable resource, the non-selected version.
     * @return the icon type associated with the resource.
     */
    public static int getIconType(int resource) {
        switch (resource) {
            case R.drawable.basic_restaurant_icon_v0:
            case R.drawable.basic_restaurant_icon_selected_v0:
                return BASIC_RESTAURANT;

            case R.drawable.basic_general_store_icon_v0:
            case R.drawable.basic_general_store_icon_selected_v0:
                return BASIC_GENERAL_STORE;

            case R.drawable.basic_market_icon_v0:
            case R.drawable.basic_market_icon_selected_v0:
                return BASIC_MARKET;

            default:
                return BASIC_RESTAURANT;
        }
    }

    /**
     * Given the icon type return the icon resource pair.
     * @param type icon type.
     * @return resource pair.
     */
    public static IconPair getIconResourcePair(int type) {
        switch (type) {
            case BASIC_RESTAURANT:
                return new IconPair(R.drawable.basic_restaurant_icon_v0, R.drawable.basic_restaurant_icon_selected_v0, BASIC_RESTAURANT);
            case BASIC_GENERAL_STORE:
                return new IconPair(R.drawable.basic_general_store_icon_v0, R.drawable.basic_general_store_icon_selected_v0, BASIC_GENERAL_STORE);
            case BASIC_MARKET:
                return new IconPair(R.drawable.basic_market_icon_v0, R.drawable.basic_market_icon_selected_v0, BASIC_MARKET);
            default:
                return new IconPair(R.drawable.basic_restaurant_icon_v0, R.drawable.basic_restaurant_icon_selected_v0, BASIC_RESTAURANT);
        }
    }

    /**
     * Get all the icon id pairs.
     * @return list containing all the icon id pairs.
     */
    public static List<IconPair> getIconIds() {
        List<IconPair> iconIds = new ArrayList<>();
        iconIds.add(new IconPair(R.drawable.basic_restaurant_icon_v0, R.drawable.basic_restaurant_icon_selected_v0, BASIC_RESTAURANT));
        iconIds.add(new IconPair(R.drawable.basic_general_store_icon_v0, R.drawable.basic_general_store_icon_selected_v0, BASIC_GENERAL_STORE));
        iconIds.add(new IconPair(R.drawable.basic_market_icon_v0, R.drawable.basic_market_icon_selected_v0, BASIC_MARKET));
        return iconIds;
    }
}
