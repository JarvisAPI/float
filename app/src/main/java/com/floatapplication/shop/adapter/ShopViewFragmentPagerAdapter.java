package com.floatapplication.shop.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/**
 * Created by william on 2018-06-06.
 *
 */

public class ShopViewFragmentPagerAdapter extends FragmentPagerAdapter {
    private List<Fragment> mFragments;

    public ShopViewFragmentPagerAdapter(List<Fragment> fragments,
                                        FragmentManager fm) {
        super(fm);
        mFragments = fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Bundle args = mFragments.get(position).getArguments();
        if (args != null) {
            return args.getString("title");
        }
        return null;
    }
}
