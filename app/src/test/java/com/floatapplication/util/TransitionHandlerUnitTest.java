package com.floatapplication.util;

import android.app.Activity;
import android.content.Intent;

import org.junit.Before;
import org.junit.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * Created by william on 2018-06-05.
 *
 */

public class TransitionHandlerUnitTest {
    private Activity activity;
    private TransitionHandler transitionHandler;

    @Before
    public void setup() {
        activity = mock(Activity.class);
        transitionHandler = new TransitionHandler();
        transitionHandler.onResume();
    }

    @Test
    public void transitionOnlyOnceToActivity() {
        transitionHandler.transition(activity, Activity.class);
        transitionHandler.transition(activity, Activity.class);
        verify(activity).startActivity(any(Intent.class));
    }
}
