package com.floatapplication.image.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.Toolbar.OnMenuItemClickListener;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.floatapplication.R;
import com.floatapplication.image.adapter.ImageScrollAdapter;
import com.floatapplication.util.Util;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by william on 2017/12/31.
 *
 */
public class ImageScrollActivity extends AppCompatActivity
    implements OnMenuItemClickListener {
    private static final String TAG = ImageScrollActivity.class.getSimpleName();

    public static final String IMAGE_PATHS = "imagePaths";
    public static final String SELECTED_IMAGE_PATH = "selectedImagePath";

    private ImageScrollAdapter mImageScrollAdapter;
    private ViewPager mViewPager;
    private ArrayList<String> mDeletedImages;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_scroll);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            ArrayList<String> imagePaths = extras.getStringArrayList(IMAGE_PATHS);
            String imagePath = extras.getString(SELECTED_IMAGE_PATH);
            setupViewPager(imagePaths, imagePath);
            setupToolBar();
        } else {
            Log.e(TAG, "No image uris passed in!");
            finish();
        }
        mDeletedImages = new ArrayList<>();
    }

    private void setupViewPager(ArrayList<String> imagePaths, String imagePath) {
        mImageScrollAdapter = new ImageScrollAdapter(getApplicationContext());
        mImageScrollAdapter.setOnImageClickListener((View view) -> {
            final long duration = getResources().getInteger(R.integer.actionbar_show_hide_duration);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                if (actionBar.isShowing()) {
                    Util.getInstance().hideActionBar(actionBar, mToolbar, duration);
                } else {
                    Util.getInstance().showActionBar(actionBar, mToolbar, duration);
                }
            }
        });
        mImageScrollAdapter.setImagePaths(imagePaths);
        mViewPager = findViewById(R.id.image_scroll_view_pager);
        mViewPager.setAdapter(mImageScrollAdapter);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setCurrentItem(imagePaths.indexOf(imagePath));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.image_scroll_menu, menu);
        return true;
    }

    private void setupToolBar() {
        mToolbar = findViewById(R.id.image_scroll_toolbar);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle("Image Scroll");
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        mToolbar.setOnMenuItemClickListener(this);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Log.d(TAG, "item clicked: " + item);
        switch (item.getItemId()) {
            case R.id.image_scroll_delete:
                removeImage();
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "Option item clicked: " + item);
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return true;
    }


    private void removeImage() {
        int currentItem = mViewPager.getCurrentItem();
        String path = mImageScrollAdapter.getImagePath(currentItem);
        File imageFile = new File(path);
        if (imageFile.exists()) {
            if (!imageFile.delete()) {
                Log.e(TAG, "Unable to delete image file!");
            }
        }
        mDeletedImages.add(path);
        mImageScrollAdapter.removeImagePath(currentItem);
        mImageScrollAdapter.notifyDataSetChanged();
        if (mImageScrollAdapter.getCount() == 0) {
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        if (!mDeletedImages.isEmpty()) {
            Intent retIntent = new Intent();
            retIntent.putStringArrayListExtra(IMAGE_PATHS, mDeletedImages);
            setResult(RESULT_OK, retIntent);
        }
        finish();
    }
}
