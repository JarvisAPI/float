package com.floatapplication.search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.floatapplication.R;
import com.floatapplication.product.activity.ProductActivity;
import com.floatapplication.product.models.Product;
import com.floatapplication.search.SearchPageContract.Presenter;
import com.floatapplication.util.Util;
import com.simplexorg.searchfragment.search.SearchFragment;

import java.util.List;

/**
 * For searching products in local area.
 */

public class SearchActivity extends AppCompatActivity
        implements
        SearchPageContract.View<Product> {
    private static final String TAG = SearchActivity.class.getSimpleName();
    public static final String TRANSITION_NAME = "searchBar";
    private SearchPageContract.Presenter<Product> mPresenter;
    private SearchFragment mSearchFragment;
    private SearchPageFragment mSearchPageFragment;
    private boolean mTransitioning;
    private View mSharedView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        SearchQuery searchQuery = getIntent().getParcelableExtra(Util.EXTRA_SEARCH_DATA);
        if (searchQuery != null) {
            mSearchPageFragment = (SearchPageFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.search_page);
            mSearchFragment = (SearchFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.search);
            mSearchFragment.addIcons(new int[]{R.drawable.baseline_reorder_black_24});
            mSearchFragment.attachOnClickListener(0, (View view) -> {
                mSearchPageFragment.onReorderItemsClicked();
            });

            SearchPagePresenter searchPagePresenter = new SearchPagePresenter();
            searchPagePresenter.attach(this, new SearchPageModel(), searchQuery);

            if (mSearchFragment.getView() != null) {
                ViewCompat.setTransitionName(mSearchFragment.getView(), TRANSITION_NAME);
            }

            mSearchFragment.setOnSearchClickListener((String query) ->
                    mPresenter.onSearch(query));

            mSearchPageFragment.setOnProductClickListener((Product product, View itemView) -> {
                mSharedView = itemView;
                mPresenter.onItemClick(product);
            });

            mSearchPageFragment.setOnBottomReachedListener(mPresenter::onLoadMore);

            mPresenter.subscribe();
        } else {
            finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mTransitioning = false;
    }

    @Override
    public void setPresenter(Presenter<Product> presenter) {
        mPresenter = presenter;
    }

    @Override
    public void displaySearchResults(List<Product> items) {
        mSearchPageFragment.displaySearchResults(items);
    }

    @Override
    public void displayDetails(Product item) {
        if (!mTransitioning) {
            mTransitioning = true;
            Intent intent = new Intent(this, ProductActivity.class);
            intent.putExtra(Util.EXTRA_PRODUCT, item);
            intent.putExtra(Util.EXTRA_TRANSITION, ViewCompat.getTransitionName(mSharedView));

            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this,
                    mSharedView, ViewCompat.getTransitionName(mSharedView));
            ActivityCompat.startActivity(this, intent, optionsCompat.toBundle());
        }
    }

    @Override
    public void clearSearchResults() {
        mSearchPageFragment.clearSearchResults();
    }

    @Override
    public void setSearchText(String text) {
        mSearchFragment.setSearchText(text);
    }
}
