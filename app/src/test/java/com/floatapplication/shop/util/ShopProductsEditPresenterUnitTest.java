package com.floatapplication.shop.util;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;

import com.android.volley.VolleyError;
import com.floatapplication.R;
import com.floatapplication.TestApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.product.models.Product;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.shop.adapter.ShopEditAdapter;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.shop.util.ShopProductsEditPresenter.OnShopProductsEditClickListener;
import com.floatapplication.user.util.UserSessionInterface;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowToast;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by william on 2018-06-12.
 *
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ShopProductsEditPresenterUnitTest {
    private ShopProductsEditPresenter presenter;
    @Mock private RecyclerView recyclerView;
    @Mock private FloatingActionButton addProductButton;
    @Mock private ShopEditAdapter adapter;
    @Mock private ShopProductLoader loader;
    @Mock private Shop shopEdit;
    @Mock private UserSessionInterface userSessionInterface;
    @Mock private HypermediaInterface hypermediaInterface;
    @Mock private OnShopProductsEditClickListener listener;

    @Before
    public void setup() {
        initMocks(this);
        TestApplication.setUserSessionInterface(userSessionInterface);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerErrorCallback errorCallback = invocation.getArgumentAt(4, ServerErrorCallback.class);
                errorCallback.onErrorResponse(new VolleyError("Error"));
                return null;
            }
        }).when(hypermediaInterface).authFollow(eq(Hypermedia.DELETE), any(Context.class), anyString(), anyMapOf(String.class, String.class),
                any(ServerCallback.class), any(ServerErrorCallback.class));
        TestApplication.setHypermediaInterface(hypermediaInterface);
        when(recyclerView.getContext()).thenReturn(RuntimeEnvironment.application.getApplicationContext());
        presenter = new ShopProductsEditPresenter();
        presenter.present(recyclerView, addProductButton, adapter, loader, shopEdit);
        presenter.setOnShopProductsEditClickListener(listener);
    }

    @After
    public void cleanup() {
        TestApplication.clear();
    }

    @Test
    public void presentSequence_isCorrect() {
        verify(recyclerView).setLayoutManager(any(LayoutManager.class));
        verify(recyclerView).setAdapter(any(ShopEditAdapter.class));
        verify(addProductButton).setOnClickListener(presenter);
        verify(loader).setListener(presenter);
        verify(loader).getShopProducts();
        verify(adapter).setOnBottomReachedListener(presenter);
        verify(adapter).setOnProductDeleteListener(presenter);
    }

    @Test
    public void onBottomReached_shouldGetShopProducts() {
        reset(loader);
        presenter.onBottomReached();
        verify(loader).getShopProducts();
    }

    @Test
    public void onProductLoaded_addShopItems() {
        List<Product> products = new ArrayList<>();
        presenter.onProductLoaded(products);
        verify(adapter).addShopItems(products);
    }

    @Test
    public void onProductDelete_errorShouldShowToast() {
        Product product = mock(Product.class);
        presenter.onProductDelete(product);
        assertEquals("Failed To Delete Product!", ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void onAddProductButtonClick_shouldTriggerCallback() {
        when(addProductButton.getId()).thenReturn(R.id.fab);
        presenter.onClick(addProductButton);
        verify(listener).onAddProductClicked();
    }

    @Test
    public void onProductDeleteClicked_withShopItems_shouldSetDeleteStatusProperly() {
        when(adapter.getShopItemsCount()).thenReturn(1);
        when(adapter.getDeleteStatus()).thenReturn(false);
        presenter.onProductDeleteClicked();
        verify(adapter).setDelete(true);
        when(adapter.getDeleteStatus()).thenReturn(true);
        presenter.onProductDeleteClicked();
        verify(adapter).setDelete(false);
    }

    @Test
    public void onProductDeleteClicked_withNoShopItems_shouldSetDeleteStatusAsFalse() {
        when(adapter.getShopItemsCount()).thenReturn(0);
        presenter.onProductDeleteClicked();
        verify(adapter).setDelete(false);
    }

    @Test
    public void onProductUploaded_shouldRefreshShopProducts() {
        reset(loader);
        presenter.onProductUploaded();
        verify(loader).getShopProducts();
    }

    @Test
    public void onProductClick_shouldTriggerProductClickCallback() {
        Product product = mock(Product.class);
        String shopId = "shop-id";
        when(shopEdit.getShopId()).thenReturn(shopId);
        presenter.onProductClick(product);
        verify(product).setProductShopId(shopId);
        verify(listener).onProductClicked(product);
    }
}
