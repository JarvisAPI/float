package com.floatapplication;

import android.app.Application;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.floatapplication.factory.AbstractFactory;
import com.floatapplication.factory.FactoryInterface;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.messages.server_connection.MessageFeatureHandler;
import com.floatapplication.messages.server_connection.MessageFeatureHandlerInterface;
import com.floatapplication.server.AppControl;
import com.floatapplication.server.AppControlInterface;
import com.floatapplication.server.ServerContact;
import com.floatapplication.server.ServerInterface;
import com.floatapplication.user.util.UserSession;
import com.floatapplication.user.util.UserSessionInterface;

/**
 * Created by william on 2018-06-11.
 * Sets up the single instances used throughout the app.
 */

public class FloatApplication extends Application {
    private static final String TAG = FloatApplication.class.getSimpleName();
    private static HypermediaInterface mHypermediaInterface;
    private static ServerInterface mServerInterface;
    private static AppControlInterface mAppControlInterface;
    private static UserSessionInterface mUserSessionInterface;
    private static FactoryInterface mFactoryInterface;
    private static MessageFeatureHandlerInterface mMessageFeatureHandlerInterface;

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate");
        super.onCreate();
        setAppControlInterface(new AppControl(getApplicationContext()));
        setServerInterface(new ServerContact(getAppControlInterface()));
        setHypermediaInterface(new Hypermedia());
        setUserSessionInterface(new UserSession());
        setFactoryInterface(new AbstractFactory(getApplicationContext()));
        setMessageFeatureHandlerInterface(new MessageFeatureHandler(getApplicationContext()));
    }

    protected void defaultOnCreate() {
        super.onCreate();
    }


    public static synchronized void setAppControlInterface(AppControlInterface appControlInterface) {
        if (mAppControlInterface == null) {
            mAppControlInterface = appControlInterface;
        }
    }

    public static AppControlInterface getAppControlInterface() {
        return mAppControlInterface;
    }


    public static synchronized void setServerInterface(ServerInterface serverInterface) {
        if (mServerInterface == null) {
            mServerInterface = serverInterface;
        }
    }

    public static ServerInterface getServerInterface() {
        return mServerInterface;
    }

    public static synchronized void setHypermediaInterface(HypermediaInterface hypermediaInterface) {
        if (mHypermediaInterface == null) {
            mHypermediaInterface = hypermediaInterface;
        }
    }

    public static HypermediaInterface getHypermediaInterface() {
        return mHypermediaInterface;
    }

    public static synchronized void setUserSessionInterface(UserSessionInterface userSessionInterface) {
        if (mUserSessionInterface == null) {
            mUserSessionInterface = userSessionInterface;
        }
    }

    public static UserSessionInterface getUserSessionInterface() {
        return mUserSessionInterface;
    }

    public static void setFactoryInterface(FactoryInterface factoryInterface) {
        if (mFactoryInterface == null) {
            mFactoryInterface = factoryInterface;
        }
    }

    public static FactoryInterface getFactoryInterface() {
        return mFactoryInterface;
    }

    public static synchronized void setMessageFeatureHandlerInterface(MessageFeatureHandlerInterface messageFeatureHandlerInterface) {
        if (mMessageFeatureHandlerInterface == null) {
            mMessageFeatureHandlerInterface = messageFeatureHandlerInterface;
        }
    }

    public static MessageFeatureHandlerInterface getMessageFeatureHandlerInterface() {
        return mMessageFeatureHandlerInterface;
    }

    @VisibleForTesting
    public static void clear() {
        mFactoryInterface = null;
        mUserSessionInterface = null;
        mHypermediaInterface = null;
        mServerInterface = null;
        mAppControlInterface = null;
        mMessageFeatureHandlerInterface = null;
    }
}
