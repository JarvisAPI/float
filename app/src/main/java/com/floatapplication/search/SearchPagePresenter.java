package com.floatapplication.search;

import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import com.floatapplication.product.models.Product;
import com.floatapplication.search.SearchPageContract.OnItemsLoadedListener;

import java.util.List;

public class SearchPagePresenter implements SearchPageContract.Presenter<Product>,
        OnItemsLoadedListener<Product> {
    private static final String TAG = SearchPagePresenter.class.getSimpleName();
    private SearchPageContract.View<Product> mView;
    private SearchPageContract.Model<Product> mModel;
    @VisibleForTesting
    SearchQuery mInitialSearchQuery;

    SearchPagePresenter() {

    }

    void attach(
            @NonNull SearchPageContract.View<Product> view,
            @NonNull SearchPageContract.Model<Product> model,
            SearchQuery initialSearchQuery) {
        mView = view;
        mModel = model;
        mView.setPresenter(this);
        mInitialSearchQuery = initialSearchQuery;
    }

    @Override
    public void onSearch(String query) {
        mView.clearSearchResults();
        mInitialSearchQuery = mInitialSearchQuery.modify(query);
        mModel.reload(mInitialSearchQuery);
    }

    @Override
    public void onItemClick(Product item) {
        mView.displayDetails(item);
    }

    @Override
    public void onLoadMore() {
        mModel.loadMoreItems();
    }

    @Override
    public void subscribe() {
        mView.setSearchText(mInitialSearchQuery.query);
        mModel.setOnItemsLoadedListener(this);
        mModel.reload(mInitialSearchQuery);
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void onItemsLoaded(List<Product> items) {
        mView.displaySearchResults(items);
    }
}
