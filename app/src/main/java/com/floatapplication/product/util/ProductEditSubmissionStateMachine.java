package com.floatapplication.product.util;

import android.util.Log;

/**
 * Created by william on 2018-06-18.
 * State machine to simplify product editing process.
 */

public class ProductEditSubmissionStateMachine {
    private static final String TAG = ProductEditSubmissionStateMachine.class.getSimpleName();

    public enum State {
        BEGIN,
        CHECK_SHOULD_UPLOAD_NEW_IMAGES,
        UPLOAD_PRODUCT_DETAILS,
        UPLOAD_NEW_IMAGES,
        UPLOAD_NEW_MAIN_IMAGE,
        SUBMIT_FAIL,
        SUBMIT_SUCCESS,
        DELETE_MAIN_IMAGE,
        SWAP_MAIN_IMAGE_AUTO_SELECTED,
        SWAP_MAIN_IMAGE_USER_SELECTED,
        DELETE_IMAGES
    }

    public enum Event {
        SUCCESS, FAILURE
    }

    private State mState;
    private ProductEditSubmissionStateLogic mStateLogic;
    private ProductEditSubmissionStateTransitionLogic mTransitionLogic;

    void bind(ProductEditSubmissionStateLogic stateLogic,
              ProductEditSubmissionStateTransitionLogic transitionLogic) {
        mStateLogic = stateLogic;
        mTransitionLogic = transitionLogic;
    }

    public void start() {
        mState = State.BEGIN;
        run(Event.SUCCESS);
    }

    private void fail() {
        mState = State.SUBMIT_FAIL;
        mStateLogic.onSubmitFailed();
    }

    private void runUploadProductDetails(Event event) {
        switch (event) {
            case SUCCESS:
                mState = State.CHECK_SHOULD_UPLOAD_NEW_IMAGES;
                run(Event.SUCCESS);
                break;
            case FAILURE:
                fail();
                break;
        }
    }

    private void checkToDeleteImages() {
        if (mTransitionLogic.shouldDeleteImages()) {
            mState = State.DELETE_IMAGES;
            mStateLogic.deleteImages();
        } else {
            mState = State.SUBMIT_SUCCESS;
            mStateLogic.onSubmitSuccess();
        }
    }

    private void runUploadNewImages(Event event) {
        switch (event) {
            case SUCCESS:
                if (mTransitionLogic.shouldChangeMainImageWithNewImage()) {
                    mState = State.UPLOAD_NEW_MAIN_IMAGE;
                    mStateLogic.uploadNewMainImage();
                } else {
                    if (mTransitionLogic.shouldDeleteMainImage()) {
                        if (mTransitionLogic.noImagesRemain()) {
                            mState = State.DELETE_MAIN_IMAGE;
                            mStateLogic.deleteMainImage();
                        } else {
                            mState = State.SWAP_MAIN_IMAGE_AUTO_SELECTED;
                            mStateLogic.swapMainImageAutoSelected();
                        }
                    } else {
                        if (mTransitionLogic.shouldSwapMainImage()) {
                            mState = State.SWAP_MAIN_IMAGE_USER_SELECTED;
                            mStateLogic.swapMainImageUserSelected();
                        } else {
                            checkToDeleteImages();
                        }
                    }
                }
                break;
            case FAILURE:
                fail();
                break;
        }
    }

    private void runCheckDeleteImages(Event event) {
        switch (event) {
            case SUCCESS:
                checkToDeleteImages();
                break;
            case FAILURE:
                fail();
                break;
        }
    }

    private void runFinalReport(Event event) {
        switch (event) {
            case SUCCESS:
                mState = State.SUBMIT_SUCCESS;
                mStateLogic.onSubmitSuccess();
                break;
            case FAILURE:
                fail();
                break;
        }
    }

    private void runDeleteMainImage(Event event) {
        switch (event) {
            case SUCCESS:
                mState = State.DELETE_IMAGES;
                mStateLogic.deleteImages();
                break;
            case FAILURE:
                fail();
                break;
        }
    }

    public void run(Event event) {
        Log.d(TAG, "State: " + mState);
        Log.d(TAG, "Event: " + event);
        switch (mState) {
            case BEGIN:
                if (mTransitionLogic.shouldUploadProductDetails()) {
                    mState = State.UPLOAD_PRODUCT_DETAILS;
                    mStateLogic.uploadProductDetails();
                } else {
                    mState = State.CHECK_SHOULD_UPLOAD_NEW_IMAGES;
                    run(Event.SUCCESS);
                    return;
                }
                break;
            case CHECK_SHOULD_UPLOAD_NEW_IMAGES:
                if (mTransitionLogic.shouldUploadNewImages()) {
                    mState = State.UPLOAD_NEW_IMAGES;
                    mStateLogic.uploadNewImages();
                } else {
                    mState = State.UPLOAD_NEW_IMAGES;
                    run(Event.SUCCESS);
                    return;
                }
                break;
            case UPLOAD_PRODUCT_DETAILS:
                runUploadProductDetails(event);
                break;
            case UPLOAD_NEW_IMAGES:
                runUploadNewImages(event);
                break;
            case UPLOAD_NEW_MAIN_IMAGE:
            case SWAP_MAIN_IMAGE_AUTO_SELECTED:
            case SWAP_MAIN_IMAGE_USER_SELECTED:
                runCheckDeleteImages(event);
                break;
            case DELETE_MAIN_IMAGE:
                runDeleteMainImage(event);
                break;
            case DELETE_IMAGES:
                runFinalReport(event);
                break;
        }
    }
}
