package com.floatapplication.shop.edit;

import android.content.Context;

import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.shop.edit.ShopAboutEditContract.Model.Observer;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.util.IFactory;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class ShopAboutEditModelUnitTest {
    private ShopAboutEditModel mModel;
    @Mock private HypermediaInterface mHypermedia;
    @Mock private IFactory mFactory;
    @Mock private Context mContext;
    @Mock private Shop mShop;
    @Mock private Observer mObserver0;
    @Mock private Observer mObserver1;

    @Before
    public void setup() {
        initMocks(this);
        FloatApplication.setHypermediaInterface(mHypermedia);
        IFactory.setIFactory(mFactory);
        mModel = new ShopAboutEditModel();
        mModel.init(mContext, mShop);
        mModel.addObserver(mObserver0);
        mModel.addObserver(mObserver1);
    }

    @After
    public void cleanup() {
        FloatApplication.setHypermediaInterface(null);
        IFactory.setIFactory(null);
    }

    private void checkObserverNotified(int event) {
        verify(mObserver0).update(event);
        verify(mObserver1).update(event);
    }

    @Test
    public void test_retrieveShop() {
        mModel.retrieveShopInfo();

        ArgumentCaptor<ServerCallback> argumentCaptor = ArgumentCaptor.forClass(ServerCallback.class);
        verify(mHypermedia).follow(eq(Hypermedia.GET), anyString(), anyMapOf(String.class, String.class), anyMapOf(String.class, String.class),
                argumentCaptor.capture(), any(ServerErrorCallback.class));

        String response = "response";
        argumentCaptor.getValue().onSuccessResponse(response);
        verify(mShop).augmentShopInfo(response);

        checkObserverNotified(Observer.INFO_RECEIVED);
    }
}
