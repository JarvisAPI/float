package com.floatapplication.product.activity;

import android.content.Intent;
import android.view.View;

import com.floatapplication.R;
import com.floatapplication.TestApplication;
import com.floatapplication.image.activity.ImageScrollActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;

import static junit.framework.Assert.assertEquals;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by william on 2018-05-23.
 * Test that product edit activity can transition properly to other activities and that
 * it only sends the product if there is no error.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ProductCreationActivityUnitTest {
    private ProductCreationActivity activity;

    @Before
    public void setup() {
        activity = Robolectric.buildActivity(ProductCreationActivity.class)
                .create()
                .start()
                .resume()
                .get();
    }

    @Test
    public void mainProductLabelShouldBeGone_ifThereAreNoImages() {
        View label = activity.findViewById(R.id.main_image_label);
        assertEquals(View.GONE, label.getVisibility());
        activity.onImageCountChange(1);
        assertEquals(View.VISIBLE, label.getVisibility());
    }

    @Test
    public void clickOnImage_shouldStartImageScrollActivity() {
        activity.onImageClick("0");
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent intent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = shadowOf(intent);
        assertEquals(ImageScrollActivity.class, shadowIntent.getIntentClass());
    }
}
