package com.floatapplication.image.activity;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.messages.chatkit.commons.ImageLoader;
import com.floatapplication.user.models.User;
import com.floatapplication.user.util.UserSessionInterface;
import com.floatapplication.util.GlobalHandler;
import com.floatapplication.util.Util;
import com.simplexorg.customviews.activity.ZoomInImageActivity;
import com.simplexorg.customviews.util.VUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by william on 2018-12-26.
 * Zoom into an image.
 */

public class CustomChatZoomInImageActivity extends ZoomInImageActivity
        implements RequestListener<Drawable> {
    public static final String USERNAME = User.USERNAME;
    private String mUsername;

    protected ImageLoader mImageLoader = (final ImageView imageView, final String url) ->
            FloatApplication.getUserSessionInterface()
                    .getUserAccessToken(getApplicationContext(), new UserSessionInterface.TokenCallback() {
                        @Override
                        public void onTokenObtained(String token) {
                            handleGlideTokenCallback(imageView, url, token);
                        }

                        @Override
                        public void onTokenError(String errorMsg) {
                            // Do nothing
                            Toast.makeText(getApplicationContext(), "Authentication error", Toast.LENGTH_SHORT).show();
                            startPostponedEnterTransition();
                        }
                    });

    private void handleGlideTokenCallback(ImageView imageView, String url, String token) {
        LazyHeaders.Builder builder = new LazyHeaders.Builder()
                .addHeader("Authorization", "Bearer " + token);
        GlideUrl glideUrl = null;
        if (url != null && !url.equals("")) {
            Map<String, String> queries = new HashMap<>();
            if (!FloatApplication.getUserSessionInterface().getUsername().equals(mUsername)) {
                queries.put("shop_id", mUsername);
                url += Util.getInstance().buildQuery(queries);
            }
            glideUrl = new GlideUrl(url, builder.build());
        }
        GlobalHandler.getInstance()
                .loadImage(imageView, glideUrl, R.drawable.default_no_image,
                        this);
    }

    @Override
    protected void setupImageView(ImageView imageView) {
        mUsername = getIntent().getStringExtra(USERNAME);
        String uri = getIntent().getStringExtra(VUtil.EXTRA_IMAGE_DATA);

        mImageLoader.loadImage(imageView, uri);
    }

    @Override
    public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                Target<Drawable> target, boolean isFirstResource) {
        startPostponedEnterTransition();
        return false;
    }

    @Override
    public boolean onResourceReady(Drawable resource, Object model,
                                   Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
        startPostponedEnterTransition();
        return false;
    }
}
