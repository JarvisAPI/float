package com.floatapplication.factory;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.floatapplication.async_tasks.BitmapDecodeTask;
import com.floatapplication.messages.message_database.ChatUserCache;
import com.floatapplication.messages.message_database.InboxDatabaseHelper;
import com.floatapplication.messages.message_database.MessageDatabaseHelper;
import com.floatapplication.messages.message_database.MessageUserDatabaseHelper;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.server_connection.ArchiveMessageManager;
import com.floatapplication.messages.server_connection.ChatConnection;
import com.floatapplication.messages.server_connection.MessageSyncManager;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.util.GlideRequests;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by william on 2018/1/25.
 * A factory used for constructing different types of objects required by different components in the app.
 */
public interface FactoryInterface {
    AsyncTask<String, Void, Void> createUploadImageTask(String uploadPath, String dataName, List<String> filepaths, Map<String, String> headers,
                                                        ServerCallback callback, ServerErrorCallback errorCallback);

    /**
     * Creates an async task that decodes a bitmap.
     * @param imageView the view to put bitmap image in once decoding completes.
     * @return task to decode bitmap.
     */
    BitmapDecodeTask createBitmapDecodeTask(ImageView imageView);

    GlideRequests getGlideRequest(Context context);

    XMPPConnection createXMPPConnection(XMPPTCPConnectionConfiguration conf);

    ArchiveMessageManager createArchiveMessageManager(XMPPTCPConnection connection, String jid);

    MessageSyncManager createMessageSyncManager(ArrayList<CustomMessage> lm, ChatConnection connection);

    InboxDatabaseHelper getInboxDatabase(String clientJid);

    MessageDatabaseHelper getMessageDatabase(String clientJid);

    MessageUserDatabaseHelper getMessageUserDatabase();

    ChatUserCache getChatUserCache();
}
