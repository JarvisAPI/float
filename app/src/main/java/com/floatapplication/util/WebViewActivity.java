package com.floatapplication.util;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

import com.floatapplication.R;

public class WebViewActivity extends AppCompatActivity {


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        WebView webView = findViewById(R.id.webview);
        Bundle data = getIntent().getExtras();
        if(data != null){
            webView.loadUrl(data.getString("url"));
        }
    }
}