package com.floatapplication.messages.server_connection;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.floatapplication.messages.activity.activity_helpers.MessageLoader;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.server_connection.ChatConnectionService.OnArchiveMessageReceivedListener;
import com.floatapplication.messages.server_connection.ChatConnectionService.OnMessageSentListener;

import java.util.ArrayList;
import java.util.List;

/**
 * This class handles the XMPP connection with the chat server on a separate thread.
 * Looper is used to receive messages that is exclusively sent from ChatConnectionService.
 * The ChatConnection reference must not be accessed by any other class other then ChatConnectionService or else consequences will follow.
 *
 */
public class XMPPThread extends HandlerThread {
    private static final String TAG = XMPPThread.class.getSimpleName();
    private String mUsername;
    private boolean mIsActive;
    private Handler mHandler;
    private ChatConnection mConnection;
    private List<Message> messagesQueue = new ArrayList<>();
    static final int MSG_DISCONNECT = 0;
    static final int MSG_RECONNECT = 1;
    static final int MSG_SEND_TEXT = 2;
    static final int MSG_RET_ARCHIVE = 3;
    static final int MSG_GET_MOST_RECENT = 4;

    XMPPThread(ChatConnection chatConnection,String jid) {
        super("XMPPThread");
        mConnection = chatConnection;
        mUsername = jid;
        mIsActive = false;
    }


    /**
     * initiates connection between the XMPP server
     */
    private void initConnection() {
        Log.d(TAG, "initConnection()");
        if(mUsername == null){
            Log.d(TAG, "initConnection() username null");
            return;
        }
        try {
            mConnection.initXMPPConnection(mUsername);
        } catch (Exception e) {
            mIsActive = false;
            e.printStackTrace();
        }
    }

    Handler getHandler() {
        return mHandler;
    }

    ChatConnection getConnection() {
        return mConnection;
    }

    @Override
    public void start() {
        if (!mIsActive) {
            mIsActive = true;
            super.start();
        }
    }

    void dispose(){
        Log.d(TAG,"dispose()");
        this.quitSafely();
        this.interrupt();
        mIsActive = false;
    }

    @Override
    protected void onLooperPrepared() {
        Log.d(TAG,"onLooperPrepared()");
        initConnection();
        XMPPHandlerCallBack callBack = new XMPPHandlerCallBack();
        mHandler = new Handler(getLooper(), callBack);
        resolveMessageQueue();
    }

    void addToMessageQueue(Message msg){
        messagesQueue.add(msg);
    }

    private void resolveMessageQueue(){
        if (!messagesQueue.isEmpty()) {
            Log.d(TAG,"message queue is not empty");
            for (Message message : messagesQueue) {
                mHandler.sendMessage(message);
            }
            messagesQueue.clear();
        }
    }

    private class XMPPHandlerCallBack implements Handler.Callback {

        @Override
        public boolean handleMessage(Message msg) {
            Log.d(TAG,"handleMessage");
            int msgCode = msg.what;
            switch (msgCode) {
                case MSG_DISCONNECT:
                    mConnection.disconnect();
                    break;
                case MSG_RECONNECT:
                    reconnect(msg);
                    break;
                case MSG_SEND_TEXT:
                    sendTextMessage(msg);
                    break;
                case MSG_RET_ARCHIVE:
                    fetchArchives(msg);
                    break;
                case MSG_GET_MOST_RECENT:
                    getMostRecentMessages(msg);
                    break;

            }
            return true;
        }

        private void sendTextMessage(Message message) {
            Bundle data = message.getData();
            String textMessage = data.getString("text");
            String contactJid = data.getString("jid");
            final CustomMessage customMessage = mConnection.sendMessage(textMessage, contactJid);
            if (message.obj instanceof OnMessageSentListener) {
                final OnMessageSentListener onMessageSentListener = (OnMessageSentListener) message.obj;
                Handler mainHandler = new Handler(Looper.getMainLooper());
                mainHandler.post(() -> onMessageSentListener.onMessageSent(customMessage));
            }
        }

        private void fetchArchives(Message message){
            Log.d(TAG,"fetchArchives() xmpp thread called");
            Log.d(TAG, Boolean.toString(message.obj instanceof OnArchiveMessageReceivedListener));
            Bundle data = message.getData();
            CustomMessage tailMessage = data.getParcelable("msg");
            if(tailMessage != null && tailMessage.getArchiveId() == null){
                Log.d(TAG,"archive id is null");
            }
            else if (tailMessage != null && tailMessage.getArchiveId() != null){
                Log.d(TAG,"archive id:" + tailMessage.getArchiveId() );
            }
            else{
                Log.d(TAG,"tail message is null");
            }
            final List<CustomMessage> archiveMessages =
                    mConnection.getArchiveMessageManager().getArchivePageBefore(tailMessage, MessageLoader.PAGE_SIZE);
            Log.d(TAG, Integer.toString(archiveMessages.size()));
            if (message.obj instanceof OnArchiveMessageReceivedListener) {
                final OnArchiveMessageReceivedListener onArchiveMessageReceivedListener = (OnArchiveMessageReceivedListener) message.obj;
                Handler mainHandler = new Handler(Looper.getMainLooper());
                mainHandler.post(() -> onArchiveMessageReceivedListener.onArchiveMessagesReceived(archiveMessages));
            }
        }

        private void getMostRecentMessages(Message message){
            final int maxRetVal = 100;
            Bundle data = message.getData();
            CustomMessage mostRecentMessage = data.getParcelable("msg");
            final List<CustomMessage> archiveMessages
                    = mConnection.getArchiveMessageManager().getArchiveMessagesAfter(mostRecentMessage, maxRetVal);
            if (message.obj instanceof OnArchiveMessageReceivedListener) {
                final OnArchiveMessageReceivedListener onArchiveMessageReceivedListener = (OnArchiveMessageReceivedListener) message.obj;
                Handler mainHandler = new Handler(Looper.getMainLooper());
                mainHandler.post(() -> onArchiveMessageReceivedListener.onMostRecentMessagesReceived(archiveMessages));
            }
        }

        private void reconnect(Message message) {
            mConnection.disconnect();
            mUsername = (String) message.obj;
            try {
                mConnection.initXMPPConnection(mUsername);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
