package com.floatapplication.server;

import com.android.volley.Request;
import com.android.volley.toolbox.ImageLoader;

/**
 * Created by william on 2017/12/27.
 * An interface used to keep track of network resources, e.g. image cache, request queue, etc.
 */

public interface AppControlInterface {
    /**
     *
     * @return the image loader used for image caching purposes.
     */
    ImageLoader getImageLoader();

    /**
     * Adds the specified request to the global queue using the default TAG
     *
     * @param req the request to add to the request queue
     */
    <T> void addToRequestQueue(Request<T> req);

    /**
     * Adds the specific request to the global queue, if tag is specified then it's
     * used, otherwise default tag is used
     *
     * @param req the request to add to the request queue
     * @param tag the tag to associate the request with
     */
    <T> void addToRequestQueue(Request<T> req, String tag);

    /**
     * Cancels all pending request made without specifying an explicit tag.
     */
    void cancelPendingRequest();

    /**
     * Cancels all pending request by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled
     *
     * @param tag the tag associated with the requests that is to be cancelled
     */
    void cancelPendingRequest(Object tag);
}
