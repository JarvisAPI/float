package com.floatapplication.user.models;

import android.content.Context;
import android.util.Log;

import com.simplexorg.mapfragment.model.GeoPoint;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by william on 2018-05-07.
 *
 */

public class UserShopItem {
    private String mShopId;
    private String mShopName;
    private GeoPoint mLoc;
    private int mShopIconType;

    private UserShopItem() {

    }

    public static UserShopItem create(Context context, JSONObject jsonObject) throws JSONException {
        UserShopItem shopItem = new UserShopItem();
        Log.d("TAG", jsonObject.toString());
        shopItem.mShopId = jsonObject.getString("shop_id");
        shopItem.mShopName = jsonObject.getString("shop_name");
        double lat = jsonObject.getDouble("shop_icon_latitude");
        double lon = jsonObject.getDouble("shop_icon_longitude");
        shopItem.mLoc = new GeoPoint(lat, lon);
        shopItem.mShopIconType = jsonObject.getInt("shop_icon_type");
        return shopItem;
    }

    public String getShopId() {
        return mShopId;
    }

    public String getShopName() {
        return mShopName;
    }

    public GeoPoint getShopLocation() {
        return mLoc;
    }
}
