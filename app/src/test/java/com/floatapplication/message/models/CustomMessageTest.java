package com.floatapplication.message.models;



import com.floatapplication.FloatApplication;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.models.User;
import com.floatapplication.messages.server_connection.ChatConnectionService;
import com.floatapplication.messages.server_connection.MessageFeatureHandlerInterface;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.delay.packet.DelayInformation;
import org.jivesoftware.smackx.forward.packet.Forwarded;
import org.junit.After;
import org.junit.Test;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.stringprep.XmppStringprepException;

import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by jamescho on 2018-05-07.
 *
 */
public class CustomMessageTest {

    private String ARCHIVE_ID_DNE = null;

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    @Test
    public void testMessageConstructorPassingInSmackMessage() throws XmppStringprepException {
        Message smackMessage = new Message();
        smackMessage.setFrom(JidCreate.from("test"));
        smackMessage.setTo(JidCreate.from("admin"));
        smackMessage.setBody("hi this is a test");
        User userTest = new User("test");
        User userAdmin = new User("admin");

        CustomMessage appMessageTest = new CustomMessage(smackMessage, userTest, ChatConnectionService.MessageType.SEND);
        CustomMessage appMessageAdmin = new CustomMessage(smackMessage, userAdmin, ChatConnectionService.MessageType.RECEIVE);

        assertEquals(appMessageTest.getId(), smackMessage.getStanzaId());
        assertEquals(appMessageAdmin.getId(), smackMessage.getStanzaId());

        assertEquals(appMessageTest.getMessageType(), ChatConnectionService.MessageType.SEND);
        assertEquals(appMessageAdmin.getMessageType(), ChatConnectionService.MessageType.RECEIVE);

        assertEquals(appMessageTest.getUser().getId(), userTest.getId());
        assertEquals(appMessageAdmin.getUser().getId(), userAdmin.getId());

        assertEquals(appMessageTest.getArchiveId(), ARCHIVE_ID_DNE);
        assertEquals(appMessageAdmin.getArchiveId(), ARCHIVE_ID_DNE);

        assertEquals(appMessageTest.getText(), smackMessage.getBody());
    }

    @Test
    public void testMessageConstructorPassingInForwardedMessage() throws XmppStringprepException {
        MessageFeatureHandlerInterface messageFeatureHandlerInterface = mock(MessageFeatureHandlerInterface.class);
        when(messageFeatureHandlerInterface.getUsername()).thenReturn("test");
        FloatApplication.setMessageFeatureHandlerInterface(messageFeatureHandlerInterface);

        Message smackMessageTest = new Message();
        smackMessageTest.setFrom(JidCreate.from("test@ec2-5412521"));
        smackMessageTest.setTo(JidCreate.from("admin@ec2-521512512"));
        String text1 = "hi this is test";
        smackMessageTest.setBody(text1);
        Date timeStampTest = new Date();
        DelayInformation infTest = new DelayInformation(timeStampTest);
        Forwarded forwardedMessageTest = new Forwarded(infTest, smackMessageTest);
        CustomMessage appMessageTest = new CustomMessage(forwardedMessageTest);

        Message smackMessageAdmin = new Message();
        smackMessageAdmin.setFrom(JidCreate.from("admin"));
        smackMessageAdmin.setTo(JidCreate.from("test"));
        String text2 = "hi this is admin";
        smackMessageAdmin.setBody(text2);
        Date timeStampAdmin = new Date();
        DelayInformation infAdmin = new DelayInformation(timeStampAdmin);
        Forwarded forwardedMessageAdmin = new Forwarded(infAdmin, smackMessageAdmin);
        CustomMessage appMessageAdmin = new CustomMessage(forwardedMessageAdmin);

        assertEquals(appMessageTest.getArchiveId(), Long.toString(infTest.getStamp().getTime()));
        assertEquals(appMessageAdmin.getArchiveId()  , Long.toString(infAdmin.getStamp().getTime()));
        assertEquals(appMessageTest.getText(), text1);
        assertEquals(appMessageAdmin.getText(), text2);
        assertEquals(appMessageTest.getUser().getId(), "admin");
        assertEquals(appMessageAdmin.getUser().getId(), "admin");
        FloatApplication.clear();
    }


    @Test
    public void  testMessageConstructor() throws XmppStringprepException {
        String id = "123";
        User user = new User("test");
        long timeStamp = 1525799580;
        String text = "abc";
        String archiveId = "15235295";
        ChatConnectionService.MessageType msgType = ChatConnectionService.MessageType.SEND;

        CustomMessage msg = new CustomMessage(id, user, timeStamp, text, archiveId, msgType);
        assertEquals(msg.getId(), id);
        assertEquals(msg.getUser().getId(), "test");
        assertEquals(msg.getText(), text);
        assertEquals(msg.getArchiveId(), archiveId);
        assertEquals(msg.getMessageType(), msgType);
    }
}
