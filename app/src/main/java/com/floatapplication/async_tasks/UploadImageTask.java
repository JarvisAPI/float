package com.floatapplication.async_tasks;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.server.MultipartRequest.DataPart;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.server.ServerErrorCode;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Used to decode bitmap of an image and send image to server.
 * Used by hypermedia, don't use directly, instead use the hypermedia.
 */

public class UploadImageTask extends AsyncTask<String, Void, Void> {
    private static final String TAG = UploadImageTask.class.getSimpleName();
    private static final int IMAGE_QUALITY = 80;
    private static final int SIZE_LIMIT = 1024 * 1024;
    private static final int MAX_SIZE_LIMIT = 50 * 1024 * 1024;
    // The size of the image above which, the image will be sub sampled.
    private static final int SUB_SAMPLE_SIZE_LIMIT = 10 * 1024 * 1024;

    private String mDataName;
    private String mUploadPath;
    private List<String> mFilePaths;
    private Map<String, String> mHeaders;
    private ServerCallback mCallback;
    private ServerErrorCallback mErrorCallback;

    /**
     * Creates a task to upload images to the server.
     *
     * @param uploadPath    the path to do the upload
     * @param dataName      the name of the multipart data.
     * @param filepaths     the file paths of the images to upload on the android device.
     * @param headers       the headers to include in the request.
     * @param callback      the callback to invoke on success.
     * @param errorCallback the callback to invoke on error.
     */
    public UploadImageTask(String uploadPath, String dataName, List<String> filepaths, Map<String, String> headers,
                           ServerCallback callback, ServerErrorCallback errorCallback) {
        mUploadPath = uploadPath;
        mDataName = dataName;
        mFilePaths = filepaths;
        mHeaders = headers;
        mCallback = callback;
        mErrorCallback = errorCallback;
    }

    @NonNull
    private static String getMimeType(@NonNull String uri) {
        String type = null;
        final String extension = MimeTypeMap.getFileExtensionFromUrl(uri);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase());
        }
        if (type == null) {
            type = "image/*"; // fallback type. You might set it to */*
        }
        return type;
    }

    @Override
    protected Void doInBackground(String... uris) {
        try {
            Log.d(TAG, "Doing in background");
            List<DataPart> data = new ArrayList<>();
            for (String filepath : mFilePaths) {
                BitmapFactory.Options options = new Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(filepath, options);

                int imageWidth = options.outWidth;
                int imageHeight = options.outHeight;

                options = new Options();
                if (imageWidth * imageHeight > SUB_SAMPLE_SIZE_LIMIT) {
                    options.inSampleSize = 2;
                }

                Bitmap bitmap = BitmapFactory.decodeFile(filepath, options);

                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                Log.d(TAG, "ByteCount: " + bitmap.getByteCount());
                String mimeType = getMimeType(filepath);
                Log.d(TAG, "mimeType: " + mimeType);
                String contentType;

                if (bitmap.getByteCount() > MAX_SIZE_LIMIT) {
                    Log.e(TAG, filepath + ", file too large");
                    JSONObject jsonError = new JSONObject();
                    jsonError.put(ServerErrorCode.FIELD_TYPE, ServerErrorCode.ERR_FILE_TOO_LARGE);
                    VolleyError error = new VolleyError(new NetworkResponse(400, jsonError.toString().getBytes(), null, true));
                    mErrorCallback.onErrorResponse(error);
                    return null;
                }

                Log.d(TAG, "Byte Count: " + bitmap.getByteCount());
                if (bitmap.getByteCount() >= SIZE_LIMIT) {
                    contentType = "image/jpeg";
                    bitmap.compress(CompressFormat.JPEG, IMAGE_QUALITY, bos);
                } else {
                    Log.d(TAG, "No compression");
                    contentType = mimeType;
                    bitmap.compress(CompressFormat.PNG, 100, bos);
                }
                data.add(new DataPart(mDataName, filepath, bos.toByteArray(), contentType));
            }

            FloatApplication.getServerInterface()
                    .asyncFileUpload(mUploadPath, mHeaders, data, mCallback, mErrorCallback);
        } catch (Exception e) {
            VolleyError error = new VolleyError(new NetworkResponse(400, null, null, true));
            mErrorCallback.onErrorResponse(error);
            e.printStackTrace();
        }
        return null;
    }
}
