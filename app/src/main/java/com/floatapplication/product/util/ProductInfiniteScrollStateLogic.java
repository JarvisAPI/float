package com.floatapplication.product.util;

/**
 * Created by william on 2018-05-28.
 * State logic for infinite product scroll state machine.
 */
public interface ProductInfiniteScrollStateLogic {
    void getShops();
    void loadProductFromShops();
}
