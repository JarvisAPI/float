package com.floatapplication.messages.holders;

import android.view.View;

import com.floatapplication.messages.chatkit.messages.MessageHolders;
import com.floatapplication.messages.models.CustomMessage;

public class CustomOutcomingImageMessageViewHolder
        extends MessageHolders.OutcomingImageMessageViewHolder<CustomMessage> {

    public CustomOutcomingImageMessageViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onBind(CustomMessage message) {
        super.onBind(message);
        String text = time.getText().toString();
        time.setText(text);
    }
}