package com.floatapplication.search;

import com.floatapplication.BasePresenter;
import com.floatapplication.BaseView;

import java.util.List;

public interface SearchPageContract {
    interface View<T> extends BaseView<Presenter<T>> {
        void displaySearchResults(List<T> items);

        void displayDetails(T item);

        void clearSearchResults();

        void setSearchText(String text);
    }

    interface Presenter<T> extends BasePresenter {
        void onSearch(String query);

        void onItemClick(T item);

        void onLoadMore();
    }

    interface OnItemsLoadedListener<T> {
        void onItemsLoaded(List<T> items);
    }

    interface Model<T> {
        void setOnItemsLoadedListener(OnItemsLoadedListener<T> loadedListener);

        void loadMoreItems();

        void reload(SearchQuery searchQuery);
    }
}
