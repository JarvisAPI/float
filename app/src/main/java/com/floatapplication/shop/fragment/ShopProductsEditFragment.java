package com.floatapplication.shop.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.floatapplication.R;
import com.floatapplication.shop.adapter.ShopEditAdapter;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.shop.util.ShopProductLoader;
import com.floatapplication.shop.util.ShopProductsEditPresenter;
import com.floatapplication.shop.util.ShopProductsEditPresenter.OnShopProductsEditClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by william on 2018-06-11.
 *
 */

public class ShopProductsEditFragment extends Fragment {
    private ShopProductsEditPresenter mPresenter;

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.fab)
    FloatingActionButton mAddProductButton;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = getActivity();
        mPresenter = new ShopProductsEditPresenter();
        if (activity instanceof OnShopProductsEditClickListener) {
            mPresenter.setOnShopProductsEditClickListener((OnShopProductsEditClickListener) activity);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shop_products_edit, container, false);
        ButterKnife.bind(this, v);
        Bundle args = getArguments();
        if (args != null) {
            Shop shopEdit = args.getParcelable("shop");
            if (shopEdit != null) {
                mPresenter.present(mRecyclerView, mAddProductButton,
                        new ShopEditAdapter(getContext()), new ShopProductLoader(shopEdit.getShopId()), shopEdit);
            }
        }
        return v;
    }

    public ShopProductsEditPresenter getPresenter() {
        return mPresenter;
    }

    public void onReorderProductsClicked() {
        if (mPresenter != null) {
            mPresenter.onReorderProductsClicked();
        }
    }
}
