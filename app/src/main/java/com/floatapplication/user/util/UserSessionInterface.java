package com.floatapplication.user.util;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by william on 2018/1/2.
 * Keeps track of any session state of the app, for instance user logged in state, etc.
 */

public interface UserSessionInterface {
    interface TokenCallback {
        void onTokenObtained(String token);
        void onTokenError(String errorMsg);
    }
    /**
     *
     * @return true if there is a user is logged in, false otherwise.
     */
    boolean isLoggedIn();

    /**
     * Logout of the current user session.
     */
    void logout();

    /**
     *
     *  provides access token of user used for authentication. If user
     *  is not logged in then callback is not invoked.
     */
    void getUserAccessToken(Context context, TokenCallback callback);

    /**
     *
     * @return refresh token of user used for token renewal.
     *
     */
    String getUserRefreshToken();

    /**
     * Precondition: an JWT token must be passed into the session with the
     * setUserToken method.
     *
     * @return username of user, or null if no user set.
     */
    String getUsername();

    void setRefreshToken(@NonNull String refreshToken);

    /**
     * Update user's tokens.
     *
     * Obtains the username from the "sub" field of the jwt token and
     * sets it as the internal username of this user session.
     *
     * @param context the context used to read the public key from resources.
     * @param accessToken a jwt token.
     */
    void updateAccessToken(Context context, String accessToken);
}
