package com.floatapplication.product.util;

import com.floatapplication.product.util.ProductShopQueryQueue.ProductShopQuery;
import com.floatapplication.TestApplication;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by william on 2018-06-05.
 *
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ProductShopQueryQueueUnitTest {
    private ProductShopQueryQueue productShopQueryQueue;
    private ProductShopQueryFactory productShopQueryFactory;

    @Before
    public void setup() {
        productShopQueryFactory = mock(ProductShopQueryFactory.class);
        productShopQueryQueue = new ProductShopQueryQueue();
        productShopQueryQueue.setProductShopQueryFactory(productShopQueryFactory);
    }

    @Test
    public void addShopToQuery_thenRemoveShopFromQuery_sizeShouldBeEmpty() {
        String shopId = "id";
        int limit = 5;
        productShopQueryQueue.addShopToQuery(shopId, limit);
        productShopQueryQueue.removeShopFromQuery(shopId);
        assertEquals(0, productShopQueryQueue.size());
    }

    @Test
    public void addOneShopToQuery_thenLoadProductFromShops() {
        String shopId = "id";
        int offset = 0;
        int limit = 5;
        ProductShopQuery productShopQuery = mock(ProductShopQuery.class);
        when(productShopQueryFactory.create(shopId, offset, limit)).thenReturn(productShopQuery);
        productShopQueryQueue.addShopToQuery(shopId, limit);
        productShopQueryQueue.loadProductFromShops();
        verify(productShopQuery).execute();
    }

    @Test
    public void wrapAroundDuring_enqueueProductQuery_whenShopsAreRemovedFromQuery() {
        int size = 10;
        int limit = 5;
        for (int i = 0; i < size; i++) {
            productShopQueryQueue.addShopToQuery(String.valueOf(i), limit);
        }
        ProductShopQuery productShopQuery = mock(ProductShopQuery.class);
        when(productShopQueryFactory.create(any(String.class), anyInt(), anyInt())).thenReturn(productShopQuery);
        productShopQueryQueue.loadProductFromShops();
        verify(productShopQuery, times(ProductShopQueryQueue.MAX_QUERIES)).execute();
        int leftOver = 2;
        for (int i = leftOver; i < size; i++) {
            productShopQueryQueue.removeShopFromQuery(String.valueOf(i));
        }
        reset(productShopQuery);
        productShopQueryQueue.loadProductFromShops();
        verify(productShopQuery, times(leftOver)).execute();
    }
}
