package com.floatapplication.user.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton;

/**
 * Created by william on 2018-04-25.
 *
 */
public class SignUpActivity extends Activity
        implements ServerCallback, ServerErrorCallback {
    private final String TAG = SignUpActivity.class.getSimpleName();
    public int MIN_USERNAME_LENGTH;
    public int MAX_USERNAME_LENGTH;
    public int MIN_PASSWORD_LENGTH;
    public int MAX_PASSWORD_LENGTH;

    private EditText mUsernameText;
    private EditText mPasswordText;
    private EditText mConfirmPasswordText;
    private EditText mUserEmailText;
    private EditText mNameText;

    private CircularProgressButton mSignUpButton;

    private String mUsername;
    private String mPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        MIN_USERNAME_LENGTH = getResources().getInteger(R.integer.min_username_length);
        MAX_USERNAME_LENGTH = getResources().getInteger(R.integer.max_username_length);
        MIN_PASSWORD_LENGTH = getResources().getInteger(R.integer.min_password_length);
        MAX_PASSWORD_LENGTH = getResources().getInteger(R.integer.max_password_length);

        obtainViews();
        setListeners();
    }

    private void setListeners() {
        mSignUpButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                signUp();
            }
        });

        String alreadyMemberText = getString(R.string.already_a_member);
        SpannableString ss = new SpannableString(alreadyMemberText);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                finish();
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };
        ss.setSpan(clickableSpan,
                alreadyMemberText.length() - 5,
                alreadyMemberText.length(),
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        TextView loginLink = findViewById(R.id.link_login);
        loginLink.setText(ss);
        loginLink.setMovementMethod(LinkMovementMethod.getInstance());
    }

    private void obtainViews() {
        mUsernameText = findViewById(R.id.input_username);
        mPasswordText = findViewById(R.id.input_password);
        mConfirmPasswordText = findViewById(R.id.input_password_confirm);
        mNameText = findViewById(R.id.input_name);
        mUserEmailText = findViewById(R.id.input_email);

        mSignUpButton = findViewById(R.id.btn_sign_up);
    }

    private void signUp() {
        Log.d(TAG, "Sign Up");

        if (validate()) {
            mSignUpButton.setEnabled(false);
            mSignUpButton.startAnimation();

            mUsername = mUsernameText.getText().toString();
            mPassword = mPasswordText.getText().toString();
            String email = mUserEmailText.getText().toString();
            String name = mNameText.getText().toString();

            sendSignUpRequest(mUsername, mPassword,email,name);
        }
    }

    private void sendSignUpRequest(String username, String password, String email, String name) {
        try {
            JSONObject user = new JSONObject()
                    .put("username", username)
                    .put("password", password)
                    .put("email", email)
                    .put("name", name);
            String jsonBody = new JSONObject()
                    .put("user", user)
                    .toString();
            Log.d(TAG,jsonBody);
            FloatApplication.getHypermediaInterface()
                    .follow(Hypermedia.POST, HypermediaInterface.SIGN_UP, null, null, jsonBody,
                            this, this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSignUpButton.dispose();
    }

    @Override
    public void onSuccessResponse(String response) {
        Intent data = new Intent();
        Bundle extras = new Bundle();
        extras.putString("username", mUsername);
        extras.putString("password", mPassword);
        data.putExtras(extras);
        setResult(RESULT_OK, data);
        finish();
    }

    /**
     * Validates that the user has inputted to every field correctly.
     *
     * @return true if every field is inputted correctly, false otherwise.
     */
    private boolean validate() {
        mUsernameText.setError(null);
        mPasswordText.setError(null);
        mConfirmPasswordText.setError(null);

        boolean noError;

        String username = mUsernameText.getText().toString();
        String password = mPasswordText.getText().toString();
        String email = mUserEmailText.getText().toString();
        String confirmPassword = mConfirmPasswordText.getText().toString();

        noError = validateUsername(username);
        if (password.length() < MIN_PASSWORD_LENGTH) {
            mPasswordText.setError(String.format(Locale.CANADA, "Password must be greater than %d characters", MIN_PASSWORD_LENGTH));
            noError = false;
        } else if (password.length() > MAX_PASSWORD_LENGTH) {
            mPasswordText.setError("Password is too long");
            noError = false;
        }
        if (!confirmPassword.equals(password)) {
            mConfirmPasswordText.setError("Passwords do not match");
            noError = false;
        }
        if(email.isEmpty()){
            mUserEmailText.setError("Missing Email");
            noError = false;
        }

        if(!email.isEmpty() && !email.contains("@")){
            mUserEmailText.setError("Invalid email");
            noError = false;
        }

        return noError;
    }

    private boolean validateUsername(String username) {
        if (username.length() < MIN_USERNAME_LENGTH) {
            mUsernameText.setError(String.format(Locale.CANADA, "Username must be greater than %d letters", MIN_USERNAME_LENGTH));
            return false;
        }
        if (username.contains(" ")) {
            mUsernameText.setError("Username must not contain spaces");
            return false;
        }
        return true;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if(error.networkResponse != null && error.networkResponse.statusCode == 409) {
            String responseBody = new String(error.networkResponse.data);
            Log.d(TAG, "Error: " + responseBody);
            try {
                JSONObject err = new JSONObject(responseBody);
                if (err.getString("field").equals("username")) {
                    mUsernameText.setError("User already exists");
                    Toast.makeText(this, err.getString("message"), Toast.LENGTH_SHORT).show();
                    Drawable verifySuccessDrawable = getApplicationContext().getResources().getDrawable( R.drawable.ic_check_circle_green_24dp);
                    mUserEmailText.setCompoundDrawablesWithIntrinsicBounds(null,null, verifySuccessDrawable,null);
                } else if (err.getString("field").equals("email")){
                    mUserEmailText.setError(err.getString("message"));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "Server Error", Toast.LENGTH_SHORT).show();
        }
        mSignUpButton.setEnabled(true);
        mSignUpButton.revertAnimation();
    }
}