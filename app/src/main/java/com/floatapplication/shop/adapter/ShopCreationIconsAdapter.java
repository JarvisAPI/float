package com.floatapplication.shop.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.floatapplication.R;
import com.floatapplication.map.models.Icon;
import com.floatapplication.map.models.Icon.IconPair;
import com.floatapplication.shop.util.ShopIconPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by william on 2018-05-23.
 * Display list of shop icons in Shop Creation Fragment.
 */
public class ShopCreationIconsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ShopIconPresenter mPresenter;

    private List<IconPair> mIconIds;

    public ShopCreationIconsAdapter() {
        mIconIds = new ArrayList<>();
        mPresenter = new ShopIconPresenter();
    }

    public void setIconIds(List<IconPair> iconIds) {
        mIconIds.clear();
        mIconIds.addAll(iconIds);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_shop_creation_icons_item, parent, false);
        return new IconViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        mPresenter.present(mIconIds.get(position), (ImageView) holder.itemView);
    }

    @Override
    public int getItemCount() {
        return mIconIds.size();
    }

    public void setSelectedIconType(int type) {
        mPresenter.setSelectedIconPair(Icon.getIconResourcePair(type));
    }

    public IconPair getSelectedIconPair() {
        return mPresenter.getSelectedIconPair();
    }

    private class IconViewHolder extends RecyclerView.ViewHolder {
        private IconViewHolder(View v) {
            super(v);
        }
    }
}
