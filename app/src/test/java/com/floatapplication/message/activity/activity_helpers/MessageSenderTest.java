package com.floatapplication.message.activity.activity_helpers;

import android.content.Context;

import com.floatapplication.FloatApplication;
import com.floatapplication.TestApplication;
import com.floatapplication.async_tasks.UploadImageTask;
import com.floatapplication.factory.FactoryInterface;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.messages.activity.MessagesActivity;
import com.floatapplication.messages.activity.activity_helpers.MessageSender;
import com.floatapplication.messages.chatkit.messages.MessagesListAdapter;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.models.User;
import com.floatapplication.messages.server_connection.ChatConnectionService;
import com.floatapplication.messages.server_connection.MessageFeatureHandlerInterface;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.user.util.UserSessionInterface;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by jamescho on 2018-06-11.
 *
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class MessageSenderTest {
    @Mock private ChatConnectionService chatConnectionService;
    private MessagesActivity activity;
    @Mock private HypermediaInterface hypermediaInterface;
    @Mock private MessagesListAdapter<CustomMessage> messageMessagesListAdapter;
    @Mock private UserSessionInterface userSessionInterface;
    @Mock private FactoryInterface factoryInterface;
    @Mock private MessageFeatureHandlerInterface messageFeatureHandlerInterface;
    private MessageSender messageSender;

    @Before
    public void setup() {
        initMocks(this);
        String clientContact = "user";
        initMocks(this);
        activity = Robolectric.buildActivity(MessagesActivity.class).get();
        messageSender = new MessageSender(activity , messageMessagesListAdapter, clientContact);
        messageSender.setConnectionService(chatConnectionService);
        FloatApplication.setUserSessionInterface(userSessionInterface);
        FloatApplication.setHypermediaInterface(hypermediaInterface);
        FloatApplication.setFactoryInterface(factoryInterface);
        FloatApplication.setMessageFeatureHandlerInterface(messageFeatureHandlerInterface);
        setChatConnectionService();
        setUserSessionInterfaceGetUsername();
        setMessageFeatureHandlerInterfaceGetUsername();
        setMessageFeatureHandlerInterfaceGetUsername();
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    private void setCreateUploadImageTask() {
        UploadImageTask uploadImageTask = mock(UploadImageTask.class);
        when(factoryInterface.createUploadImageTask(anyString(),anyString(),anyListOf(String.class), anyMapOf(String.class, String.class),
                any(ServerCallback.class), any(ServerErrorCallback.class))).thenReturn(uploadImageTask);
    }

    private void setChatConnectionService(){
        doAnswer(new Answer() {
            @Override
            public CustomMessage answer(InvocationOnMock invocation) throws Throwable {
                messageSender.onMessageSent(mock(CustomMessage.class));
                return null;
            }
        }).when(chatConnectionService).sendMessage(anyString(),anyString());
    }

    private void setUserSessionInterfaceGetUsername() {
        doAnswer(new Answer() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return "test";
            }
        }).when(userSessionInterface).getUsername();
    }

    private void setMessageFeatureHandlerInterfaceGetUsername(){
        doAnswer(new Answer() {
            @Override
            public String answer(InvocationOnMock invocation) throws Throwable {
                return "test";
            }
        }).when(messageFeatureHandlerInterface).getUsername();
    }

    private void setTokenError() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UserSessionInterface.TokenCallback callback = invocation.getArgumentAt(1, UserSessionInterface.TokenCallback.class);
                callback.onTokenError("Token error");
                return null;
            }
        }).when(userSessionInterface).getUserAccessToken(any(Context.class), any(UserSessionInterface.TokenCallback.class));
    }

    private void setTokenObtain() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UserSessionInterface.TokenCallback callback = invocation.getArgumentAt(1, UserSessionInterface.TokenCallback.class);
                callback.onTokenObtained("Token");
                return null;
            }
        }).when(userSessionInterface).getUserAccessToken(any(Context.class), any(UserSessionInterface.TokenCallback.class));
    }

    private void setUploadImageTask() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return mock(UploadImageTask.class);
            }
        }).when(factoryInterface).createUploadImageTask(anyString(), anyString(), any(List.class),
                any(Map.class), any(ServerCallback.class), any(ServerErrorCallback.class));
    }

    private void doImageUpload(){
        doAnswer(new Answer() {
            @Override
            public CustomMessage answer(InvocationOnMock invocation) throws Throwable {
                String id = invocation.getArgumentAt(0,String.class);
                User user = new User("test");
                long timeStamp = 1525799580;
                String text = "abc";
                String archiveId = "15235295";
                ChatConnectionService.MessageType msgType = ChatConnectionService.MessageType.SEND;
                CustomMessage msg = new CustomMessage(id, user, timeStamp, text, archiveId, msgType);
                messageSender.onMessageSent(msg);
                return null;
            }
        }).when(chatConnectionService).sendMessage(anyString(),anyString());

        final List<String> imageFileList = new ArrayList<>();
        imageFileList.add("file/path1");
        imageFileList.add("file/path2");
        imageFileList.add("file/path3");
        messageSender.sendImageMessage(imageFileList);
    }

    private void sendMessage(String msgId){
        User user = new User("test");
        long timeStamp = 1525799580;
        String text = "abc";
        String archiveId = "15235295";
        ChatConnectionService.MessageType msgType = ChatConnectionService.MessageType.SEND;
        CustomMessage msg1 = new CustomMessage(msgId, user, timeStamp, text, archiveId, msgType);
        messageSender.onMessageSent(msg1);
    }

    @Test
    public void sendTextMessageTest() {
        String msgId1 = "1";
        sendMessage(msgId1);
        verify(messageMessagesListAdapter,times(1)).addToStart(any(CustomMessage.class), anyBoolean());


        String msgId2 = "2";
        sendMessage(msgId2);
        verify(messageMessagesListAdapter,times(1 + 1 )).addToStart(any(CustomMessage.class), anyBoolean());

        String msgId3 = "3";
        sendMessage(msgId3);
        verify(messageMessagesListAdapter,times(1 + 1 + 1)).addToStart(any(CustomMessage.class), anyBoolean());
    }

    @Test
    public void sendImageMessagesTest() {
        setUploadImageTask();
        setTokenObtain();
        doImageUpload();
        ArgumentCaptor<ServerCallback> captor = ArgumentCaptor.forClass(ServerCallback.class);
        verify(factoryInterface, times(3)).createUploadImageTask(anyString(), anyString(), any(List.class),
                any(Map.class),captor.capture(), any(ServerErrorCallback.class));
        int n = 0;
        for (ServerCallback callback : captor.getAllValues()) {
            callback.onSuccessResponse("{ 'image_url': '1234" + Integer.toString(n) + "' }");
            n++;
        }
    }


    @Test
    public void messageSendFailTest() {
        doAnswer(new Answer() {
            @Override
            public CustomMessage answer(InvocationOnMock invocation) throws Throwable {
                messageSender.onMessageSent(null);
                return null;
            }
        }).when(chatConnectionService).sendMessage(anyString(),anyString());
        messageSender.sendTextMessage("1234");
    }

    @Test
    public void imageUploadTokenFailTest() {
        setTokenError();
        setCreateUploadImageTask();
        doImageUpload();
    }
}
