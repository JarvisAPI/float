package com.floatapplication.user.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.TestApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.robolectric.Shadows.shadowOf;

@SuppressWarnings("unchecked")
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class SignUpActivityUnitTest {
    private SignUpActivity activity;
    private TextView usernameText;
    private TextView passwordText;
    private TextView passwordConfirmText;
    private HypermediaInterface hypermediaInterface;

    @Before
    public void setup() {
        hypermediaInterface = mock(HypermediaInterface.class);
        FloatApplication.setHypermediaInterface(hypermediaInterface);
        activity = Robolectric.buildActivity(SignUpActivity.class)
                .create()
                .start()
                .resume()
                .get();
        usernameText = activity.findViewById(R.id.input_username);
        passwordText = activity.findViewById(R.id.input_password);
        passwordConfirmText = activity.findViewById(R.id.input_password_confirm);
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    @Test
    public void testLoginLinkClick_shouldFinish() {
        activity.findViewById(R.id.link_login).performClick();
        assertTrue(activity.isFinishing());
    }

    @Test
    public void testInvalidPasswordMatch_shouldFlagError() {
        usernameText.setText("ValidUsername");
        passwordText.setText("ValidPassword");
        passwordConfirmText.setText("Password");
        activity.findViewById(R.id.btn_sign_up).performClick();
        assertNotNull(passwordConfirmText.getError());
    }

    @Test
    public void testValidSignUp_shouldSetUsernameAndPasswordResult_thenFinish() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerCallback callback = invocation.getArgumentAt(3, ServerCallback.class);
                callback.onSuccessResponse(null);
                return null;
            }
        }).when(hypermediaInterface).follow(eq(Hypermedia.POST), anyString(), anyMap(), anyMap(),
                anyString(), any(ServerCallback.class), any(ServerErrorCallback.class));
        String username = "ValidUsername";
        String password = "ValidPassword";
        usernameText.setText(username);
        passwordText.setText(password);
        passwordConfirmText.setText(password);
        activity.findViewById(R.id.btn_sign_up).performClick();
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent data = shadowActivity.getResultIntent();
        Bundle bundle = data.getExtras();
        assertNotNull(bundle);
        assertEquals(username, bundle.getString("username"));
        assertEquals(password, bundle.getString("password"));
        assertTrue(activity.isFinishing());
    }

    @Test
    public void testSignUpWithSpaceInUsername_shouldFlagError() {
        usernameText.setText("Valid Username ");
        passwordText.setText("ValidPassword");
        passwordConfirmText.setText("ValidPassword");
        activity.findViewById(R.id.btn_sign_up).performClick();
        assertNotNull(usernameText.getError());
    }
}
