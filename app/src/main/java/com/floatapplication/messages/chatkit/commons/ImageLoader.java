package com.floatapplication.messages.chatkit.commons;

/**
 * Created by jamescho on 2018-04-25.
 */

import android.widget.ImageView;

/**
 * Callback for implementing images loading in message list
 */
public interface ImageLoader {
    void loadImage(ImageView imageView, String url);
}
