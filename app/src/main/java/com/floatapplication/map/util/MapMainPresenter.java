package com.floatapplication.map.util;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.floatapplication.R;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.user.models.User;
import com.floatapplication.user.util.UserSessionInterface;
import com.floatapplication.util.GlideRequests;
import com.floatapplication.util.Util;
import com.simplexorg.searchfragment.decorator.NavDrawerSearchDecorator.OnMenuClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Presents the navigation drawer.
 */

public class MapMainPresenter
        implements OnMenuClickListener {
    private GlideRequests mGlide;
    private UserSessionInterface mUserSession;
    private HypermediaInterface mHypermedia;
    private NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private String mName; // Name of the user displayed in the navigation drawer

    public MapMainPresenter(GlideRequests glideRequests,
                            UserSessionInterface userSessionInterface,
                            HypermediaInterface hypermediaInterface) {
        mGlide = glideRequests;
        mUserSession = userSessionInterface;
        mHypermedia = hypermediaInterface;
    }

    public void present(NavigationView navigationView,
                        DrawerLayout drawerLayout) {
        mNavigationView = navigationView;
        mDrawerLayout = drawerLayout;
        switchNavigationMenu();
    }

    /**
     * User logged out then logged in, potentially or they logged out then come back in, so we
     * need to check and refresh the navigation drawer layout.
     */
    public void onLogin() {
        switchNavigationMenu();
    }

    public void onLogout() {
        switchNavigationMenu();
    }

    public void closeDrawer() {
        mDrawerLayout.closeDrawer(Gravity.START, false);
    }

    /**
     * Switch navigation menu based on login status of user.
     */
    private void switchNavigationMenu() {
        mNavigationView.getMenu().clear();
        if (mUserSession.isLoggedIn()) {
            mNavigationView.inflateMenu(R.menu.activity_main_drawer_login);
            setNavigationHeaderImages(mUserSession.getUsername());
        } else {
            mNavigationView.inflateMenu(R.menu.activity_main_drawer_logout);
            setNavigationHeaderText(null);
            setNavigationHeaderImagesDefault();
        }
    }

    private void setNavigationHeaderText(String text) {
        View headerView = mNavigationView.getHeaderView(0);
        if (headerView != null) {
            TextView headerText = headerView.findViewById(R.id.nav_header_large);
            headerText.setText(text);
        }
    }

    private void setNavigationHeaderImagesDefault() {
        View headerView = mNavigationView.getHeaderView(0);
        if (headerView != null) {
            RelativeLayout headerBackground = headerView.findViewById(R.id.background);
            ImageView headerProfile = headerView.findViewById(R.id.image_view);
            headerProfile.setImageResource(R.drawable.default_user_profile_image);
            headerBackground.setBackgroundResource(R.drawable.default_user_profile_background);
        }
    }

    public void setNavigationHeaderImages(String username) {
        View headerView = mNavigationView.getHeaderView(0);
        if (headerView != null) {
            RelativeLayout headerBackground = headerView.findViewById(R.id.background);
            ImageView headerProfile = headerView.findViewById(R.id.image_view);
            getProfileImages(username, headerBackground, headerProfile);
        }
    }

    private void getProfileImages(String username, final RelativeLayout headerBackground, final ImageView headerProfile) {
        Map<String, String> vMap = new HashMap<>();
        vMap.put("username", username);
        mHypermedia.follow(Hypermedia.GET, HypermediaInterface.USER_PROFILE, vMap, null,
                (String response) -> {
                        try {
                            JSONObject userData = new JSONObject(response);
                            mName = userData.getString(User.NAME);
                            setNavigationHeaderText(StringUtils.capitalize(mName));
                            setProfileImage(userData, headerProfile);
                            setBackgroundImage(userData, headerBackground);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                },
                VolleyError::printStackTrace
        );
    }

    /**
     * Get the name of the user displayed in the navigation drawer.
     */
    public String getName() {
        return mName;
    }

    private void setProfileImage(JSONObject userData, ImageView headerProfile) throws JSONException {
        if (userData.has(User.PROFILE_IMAGES)) {
            JSONArray profileImage = userData.getJSONArray(User.PROFILE_IMAGES);
            if (profileImage.length() != 0) {
                String imageFile = profileImage.getJSONObject(0).getString("url");
                String url = Util.getInstance().constructImageUrl(imageFile);
                mGlide
                        .load(url)
                        .into(headerProfile);
                return;
            }
        }
        headerProfile.setImageResource(R.drawable.default_user_profile_image);
    }

    private void setBackgroundImage(JSONObject userData, final RelativeLayout headerBackground) throws JSONException {
        if (userData.has(User.BACKGROUND_IMAGES)) {
            JSONArray backgroundImage = userData.getJSONArray(User.BACKGROUND_IMAGES);
            if (backgroundImage.length() != 0) {
                String imageFile = backgroundImage.getJSONObject(0).getString("url");
                String url = Util.getInstance().constructImageUrl(imageFile);
                mGlide
                        .load(url)
                        .into(new SimpleTarget<Drawable>() {
                            @Override
                            public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                                headerBackground.setBackground(resource);
                            }
                        });
                return;
            }
        }
        headerBackground.setBackgroundResource(R.drawable.default_user_profile_background);
    }

    @Override
    public void onMenuClick() {
        mDrawerLayout.openDrawer(Gravity.START, true);
    }
}
