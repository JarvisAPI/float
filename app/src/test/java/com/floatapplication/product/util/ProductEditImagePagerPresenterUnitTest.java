package com.floatapplication.product.util;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.TestApplication;
import com.floatapplication.factory.FactoryInterface;
import com.floatapplication.util.GlideRequest;
import com.floatapplication.util.GlideRequests;
import com.simplexorg.customviews.adapter.StatusMediator;
import com.simplexorg.customviews.adapter.StatusMediator.Observer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by william on 2018-06-19.
 * Tests presenting the images in the view pager adapter in the product edit activity.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ProductEditImagePagerPresenterUnitTest {
    private ProductEditImagePagerPresenter presenter;
    @Mock private ImageView imageView;
    @Mock private ImageView deleteButton;
    @Mock private ImageView mainImageMarker;
    @Mock private StatusMediator statusInformer;
    @Mock private PagerAdapter adapter;
    @Mock private FactoryInterface factoryInterface;
    @Mock private GlideRequests glideRequests;
    @Mock private GlideRequest request;
    @Mock private OnClickListener onClickListener;

    @SuppressWarnings("unchecked")
    @Before
    public void setup() {
        initMocks(this);
        presenter = new ProductEditImagePagerPresenter();
        when(glideRequests.load(anyString())).thenReturn(request);
        when(factoryInterface.getGlideRequest(any(Context.class))).thenReturn(glideRequests);
        FloatApplication.setFactoryInterface(factoryInterface);
        presenter.setStatusMediator(statusInformer);
        presenter.setOnClickListener(onClickListener);
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    private void verifyGeneralPresentSequence(int position) {
        presenter.present(imageView, deleteButton, mainImageMarker, position);
        verify(deleteButton).setVisibility(View.VISIBLE);
        verify(deleteButton).setTag(R.id.tag0, position);
        verify(deleteButton).setOnClickListener(presenter);
        verify(imageView).setTag(R.id.tag0, position);
        verify(imageView).setTag(R.id.tag1, mainImageMarker);
        verify(imageView).setOnClickListener(onClickListener);
        verify(imageView).setOnLongClickListener(presenter);
    }

    @Test
    public void presentMainImage_setsMainImageMarker_andExecutesProperSequence() {
        presenter.addImageUrl("0", adapter);
        verifyGeneralPresentSequence(0);
        verify(mainImageMarker).setTag(R.id.tag0, "0");
        verify(mainImageMarker).setVisibility(View.VISIBLE);
        verify(mainImageMarker).setOnClickListener(presenter);
    }

    @Test
    public void presentNotMainImage_shouldNotSetMainImageMarker() {
        presenter.addImageUrl("0", adapter);
        presenter.addImageUrl("1", adapter);
        verifyGeneralPresentSequence(1);
        verify(mainImageMarker, times(0)).setTag(R.id.tag0, "1");
        verify(mainImageMarker, times(0)).setVisibility(View.VISIBLE);
        verify(mainImageMarker, times(0)).setOnClickListener(presenter);
    }

    @Test
    public void addImageUrl_notifiesAdapter() {
        presenter.addImageUrl("0", adapter);
        verify(adapter).notifyDataSetChanged();
    }

    @Test
    public void addImageUrls_notifiesAdapter() {
        List<String> imageUrls = new ArrayList<>();
        imageUrls.add("0");
        presenter.addImageUrls(imageUrls, adapter);
        verify(adapter).notifyDataSetChanged();
    }

    @Test
    public void clickOnDeleteImage_addImageUrlToDeleteSet() {
        assertTrue(presenter.getImageDeleteSet().isEmpty());
        presenter.addImageUrl("0", adapter);
        when(deleteButton.getTag(R.id.tag0)).thenReturn(0);
        when(deleteButton.getId()).thenReturn(R.id.delete);
        presenter.onClick(deleteButton);
        assertTrue(presenter.getImageDeleteSet().contains("0"));
    }

    @Test
    public void clickOnDeleteImage_presentItemAgain_shouldDisplayCorrectDeleteStatus() {
        presenter.addImageUrl("0", adapter);
        when(deleteButton.getTag(R.id.tag0)).thenReturn(0);
        when(deleteButton.getId()).thenReturn(R.id.delete);
        presenter.onClick(deleteButton);
        verify(deleteButton).setImageResource(R.drawable.baseline_restore_from_trash_black_24);
        reset(deleteButton);
        presenter.present(imageView, deleteButton, mainImageMarker, 0);
        verify(deleteButton).setImageResource(R.drawable.baseline_restore_from_trash_black_24);
    }

    @Test
    public void clickOnDeleteImageTwice_removesImageUrlFromDeleteSet() {
        assertTrue(presenter.getImageDeleteSet().isEmpty());
        presenter.addImageUrl("0", adapter);
        when(deleteButton.getTag(R.id.tag0)).thenReturn(0);
        when(deleteButton.getId()).thenReturn(R.id.delete);
        when(deleteButton.getContext()).thenReturn(RuntimeEnvironment.application.getApplicationContext());
        presenter.onClick(deleteButton);
        presenter.onClick(deleteButton);
        assertTrue(presenter.getImageDeleteSet().isEmpty());
    }

    @Test
    public void longClickOnSubImage_shouldMarkItAsMainImage() {
        presenter.addImageUrl("0", adapter);
        presenter.addImageUrl("1", adapter);
        presenter.present(imageView, deleteButton, mainImageMarker, 0);
        ImageView otherImageView = mock(ImageView.class);
        ImageView otherDeleteButton = mock(ImageView.class);
        ImageView otherMainImageMarker = mock(ImageView.class);
        presenter.present(otherImageView, otherDeleteButton, otherMainImageMarker, 1);

        when(otherImageView.getTag(R.id.tag0)).thenReturn(1);
        when(otherImageView.getTag(R.id.tag1)).thenReturn(otherMainImageMarker);
        presenter.onLongClick(otherImageView);
        verify(mainImageMarker).setVisibility(View.INVISIBLE);
        verify(otherMainImageMarker).setVisibility(View.VISIBLE);
        verify(statusInformer).requestToTrackStatus(presenter);
    }

    @Test
    public void hasMainImage_onlyWhenMainImageMarkerIsVisible() {
        presenter.addImageUrl("0", adapter);
        presenter.present(imageView, deleteButton, mainImageMarker, 0);
        when(mainImageMarker.getVisibility()).thenReturn(View.VISIBLE);
        assertTrue(presenter.hasMainImage());
        when(mainImageMarker.getVisibility()).thenReturn(View.INVISIBLE);
        assertFalse(presenter.hasMainImage());
    }

    @Test
    public void setMainImageMarkerInvisible_whenClearMainImageState() {
        presenter.addImageUrl("0", adapter);
        presenter.present(imageView, deleteButton, mainImageMarker, 0);
        presenter.update(Observer.CLEAR_STATUS);
        verify(mainImageMarker).setVisibility(View.INVISIBLE);
    }

    @Test
    public void getMainImageUrl_onlyReturnsImageUrl_ifMainImageMarkerIsVisible() {
        presenter.addImageUrl("0", adapter);
        presenter.present(imageView, deleteButton, mainImageMarker, 0);
        when(mainImageMarker.getVisibility()).thenReturn(View.VISIBLE);
        when(mainImageMarker.getTag(R.id.tag0)).thenReturn("0");
        assertEquals("0", presenter.getMainImageUrl());
        when(mainImageMarker.getVisibility()).thenReturn(View.INVISIBLE);
        assertNull(presenter.getMainImageUrl());
    }
}
