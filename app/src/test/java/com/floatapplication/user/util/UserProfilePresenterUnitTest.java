package com.floatapplication.user.util;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.TestApplication;
import com.floatapplication.user.adapter.UserProfileAdapter;
import com.floatapplication.user.util.UserProfilePresenter.OnUserProfileClickListener;
import com.floatapplication.util.GlideRequest;
import com.floatapplication.util.GlideRequests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.fakes.RoboMenuItem;
import org.springframework.util.StringUtils;

import de.hdodenhof.circleimageview.CircleImageView;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by william on 2018-06-07.
 * Tests the presenter that is responsible for presenting the user fragment.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class UserProfilePresenterUnitTest {
    private HypermediaInterface hypermediaInterface;
    private UserSessionInterface userSessionInterface;
    private UserProfilePresenter presenter;
    private ImageView shopWallpaper;
    private RecyclerView recyclerView;
    private UserProfileAdapter adapter;
    private ImageView backgroundImage;
    private CircleImageView profileImage;
    private FloatingActionButton messageButton;
    private Toolbar toolbar;
    private TextView usernameText;
    private String username;
    private OnUserProfileClickListener listener;
    private UserProfileShopItemPresenter shopItemPresenter;

    @SuppressWarnings("unchecked")
    @Before
    public void setup() {
        GlideRequests glideRequests = mock(GlideRequests.class);
        when(glideRequests.load(any(String.class))).thenReturn(mock(GlideRequest.class));
        presenter = new UserProfilePresenter(glideRequests);
        listener = mock(OnUserProfileClickListener.class);
        presenter.setOnUserProfileClickListener(listener);
        hypermediaInterface = mock(HypermediaInterface.class);
        userSessionInterface = mock(UserSessionInterface.class);
        FloatApplication.setHypermediaInterface(hypermediaInterface);
        FloatApplication.setUserSessionInterface(userSessionInterface);
        username = "user";
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    private void present() {
        shopWallpaper = mock(ImageView.class);
        recyclerView = mock(RecyclerView.class);

        adapter = mock(UserProfileAdapter.class);
        shopItemPresenter = mock(UserProfileShopItemPresenter.class);
        when(adapter.getPresenter()).thenReturn(shopItemPresenter);

        backgroundImage = mock(ImageView.class);
        profileImage = mock(CircleImageView.class);
        messageButton = mock(FloatingActionButton.class);
        toolbar = mock(Toolbar.class);
        usernameText = mock(TextView.class);
        presenter.present(shopWallpaper, recyclerView, adapter, backgroundImage,
                profileImage, messageButton, toolbar, usernameText, username);
    }

    @Test
    public void testPresentProcess_thingsAreCreatedProperly() {
        present();
        verify(recyclerView).setLayoutManager(any(LayoutManager.class));
        verify(recyclerView).setAdapter(adapter);
        verify(usernameText).setText(StringUtils.capitalize(username));
        verify(hypermediaInterface).follow(eq(Hypermedia.GET), eq(HypermediaInterface.USER_PROFILE), anyMapOf(String.class, String.class),
                anyMapOf(String.class, String.class), any(ServerCallback.class), any(ServerErrorCallback.class));
        verify(hypermediaInterface).follow(eq(Hypermedia.GET), eq(HypermediaInterface.USER_SHOPS), anyMapOf(String.class, String.class),
                anyMapOf(String.class, String.class), any(ServerCallback.class), any(ServerErrorCallback.class));
        verify(messageButton).setOnClickListener(presenter);
    }

    @Test
    public void testCorrectToolbarMenuInflated_forOwnerOfProfile() {
        when(userSessionInterface.getUsername()).thenReturn(username);
        present();
        verify(toolbar).setNavigationIcon(R.drawable.round_arrow_back_white_24);
        verify(toolbar).setNavigationOnClickListener(any(OnClickListener.class));
        verify(toolbar).inflateMenu(R.menu.fragment_user_profile);
        verify(toolbar).setOnMenuItemClickListener(presenter);
    }

    @Test
    public void testCorrectToolbarMenuInflated_forNotOwnerOfProfile() {
        when(userSessionInterface.getUsername()).thenReturn(null);
        present();
        verify(toolbar).setNavigationIcon(R.drawable.round_arrow_back_white_24);
        verify(toolbar).setNavigationOnClickListener(any(OnClickListener.class));
        verify(toolbar, times(0)).inflateMenu(R.menu.fragment_user_profile);
        verify(toolbar, times(0)).setOnMenuItemClickListener(presenter);
    }

    @Test
    public void onUserCoverChangeClicked_shouldChangeProfileCover() {
        present();
        RoboMenuItem item = new RoboMenuItem(R.id.user_cover_change);
        presenter.onMenuItemClick(item);
        verify(listener).onChangeProfileBackgroundClicked();
    }

    @Test
    public void onUserProfileChangeClicked_shouldChangeProfileImage() {
        present();
        RoboMenuItem item = new RoboMenuItem(R.id.user_prof_change);
        presenter.onMenuItemClick(item);
        verify(listener).onChangeProfileImageClicked();
    }

    private void mockOnClick(int id) {
        View view = mock(View.class);
        when(view.getId()).thenReturn(id);
        presenter.onClick(view);
    }

    @Test
    public void onUserProfileBackgroundImageClicked_shouldCallback() {
        present();
        mockOnClick(R.id.user_profile_background);
        verify(listener).onProfileBackgroundImageClicked(anyString());
    }

    @Test
    public void onUserProfileImageClicked_shouldCallback() {
        present();
        mockOnClick(R.id.user_profile_image);
        verify(listener).onProfileImageClicked(anyString());
    }

    @Test
    public void onMessageButtonClicked_shouldCallback() {
        present();
        mockOnClick(R.id.message_fab);
        verify(listener).onMessageButtonClicked(username);
    }

    @Test
    public void callSetIsProfileOwner_forUserProfileAdapter_whenUserIsOwnerOfProfile() {
        when(userSessionInterface.getUsername()).thenReturn(username);
        present();
        verify(shopItemPresenter).setIsProfileOwner();
    }

    @Test
    public void doNotCallSetIsProfileOwner_forUserProfileAdapter_whenUserIsNotOwnerOfProfile() {
        when(userSessionInterface.getUsername()).thenReturn(null);
        present();
        verify(shopItemPresenter, times(0)).setIsProfileOwner();
    }
}
