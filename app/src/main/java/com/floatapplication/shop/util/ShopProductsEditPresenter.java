package com.floatapplication.shop.util;

import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.config.ConfigStore;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.product.adapter.ProductAdapter.OnBottomReachedListener;
import com.floatapplication.product.models.Product;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.shop.adapter.ShopEditAdapter;
import com.floatapplication.shop.adapter.ShopEditAdapter.OnProductClickListener;
import com.floatapplication.shop.adapter.ShopEditAdapter.OnProductDeleteListener;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.shop.util.ShopProductLoader.OnProductLoadedListener;
import com.floatapplication.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by william on 2018-06-11.
 * Presents the products in the shop and allow them to be modified.
 *
 * This class is responsible for the UI only and any upload logic for the
 * product should be handled else where.
 */

public class ShopProductsEditPresenter
        implements
        OnClickListener,
        OnProductDeleteListener,
        OnProductClickListener,
        OnProductLoadedListener,
        OnBottomReachedListener {
    private static final String TAG = ShopProductsEditPresenter.class.getSimpleName();
    public interface OnShopProductsEditClickListener {
        void onAddProductClicked();
        void onProductClicked(Product product);
    }

    private ShopEditAdapter mShopEditAdapter;
    private OnShopProductsEditClickListener mOnShopProductsEditClickListener;
    private ShopProductLoader mShopProductLoader;
    private Shop mShopEdit;
    private Context mContext;
    private StaggeredGridLayoutManager mLayoutManager;

    public void setOnShopProductsEditClickListener(OnShopProductsEditClickListener listener) {
        mOnShopProductsEditClickListener = listener;
    }

    public void present(RecyclerView recyclerView, FloatingActionButton addProductButton,
                        ShopEditAdapter adapter, ShopProductLoader shopProductLoader, Shop shopEdit) {
        mContext = recyclerView.getContext();
        mShopEditAdapter = adapter;
        mShopProductLoader = shopProductLoader;
        mShopEdit = shopEdit;
        addProductButton.setOnClickListener(this);
        setupShopProductLoader();
        setupShopEditAdapterListener();
        setupRecyclerView(recyclerView);
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        int spanCount = ConfigStore.getInstance().getProductColumnSpanCount(mContext);
        mLayoutManager = new StaggeredGridLayoutManager(spanCount, StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(mShopEditAdapter);
    }

    private void setupShopProductLoader() {
        mShopProductLoader.setListener(this);
        mShopProductLoader.getShopProducts();
    }

    private void setupShopEditAdapterListener() {
        mShopEditAdapter.setOnProductDeleteListener(this);
        mShopEditAdapter.setOnBottomReachedListener(this);
        mShopEditAdapter.setOnProductClickListener(this);
    }

    @Override
    public void onBottomReached() {
        mShopProductLoader.getShopProducts();
    }

    @Override
    public void onProductLoaded(List<Product> products) {
        mShopEditAdapter.addShopItems(products);
    }

    @Override
    public void onProductDelete(Product product) {
        Map<String, String> vMap = new HashMap<>();
        vMap.put("shop_id", mShopEdit.getShopId());
        vMap.put("product_id", product.getProductId());
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.DELETE, mContext,
                Hypermedia.DELETE_PRODUCT, vMap, null,
                new ServerCallback() {
                    @Override
                    public void onSuccessResponse(String response) {
                        // Do nothing.
                    }
                },
                new ServerErrorCallback() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(mContext, "Failed To Delete Product!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onProductClick(Product product) {
        product.setProductShopId(mShopEdit.getShopId());
        mOnShopProductsEditClickListener.onProductClicked(product);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.fab:
                mOnShopProductsEditClickListener.onAddProductClicked();
        }
    }

    /**
     * Display delete buttons for products.
     */
    public void onProductDeleteClicked() {
        if (mShopEditAdapter.getShopItemsCount() > 0) {
            mShopEditAdapter.setDelete(!mShopEditAdapter.getDeleteStatus());
        } else {
            mShopEditAdapter.setDelete(false);
        }
    }

    /**
     * When products are uploaded to the server, call this function to refresh the products in the shop.
     */
    public void onProductUploaded() {
        mShopProductLoader.getShopProducts();
    }

    public void refreshProduct(String productId) {
        Log.d(TAG, "refreshProduct");
        Map<String, String> vMap = new HashMap<>();
        vMap.put("shop_id", mShopEdit.getShopId());
        vMap.put("product_id", productId);
        FloatApplication.getHypermediaInterface().follow(Hypermedia.GET, HypermediaInterface.PRODUCT_DETAILS,
                vMap, null,
                new ServerCallback() {
                    @Override
                    public void onSuccessResponse(String response) {
                        try {
                            JSONObject productJson = new JSONObject(response).getJSONObject("product");
                            Product product = Product.createBasicProduct(productJson);
                            product.augmentProductInfo(productJson);
                            mShopEditAdapter.refreshProduct(product);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new ServerErrorCallback() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
    }

    public void onReorderProductsClicked() {
        Util.getInstance().reorderItems(mContext, mLayoutManager);
    }
}
