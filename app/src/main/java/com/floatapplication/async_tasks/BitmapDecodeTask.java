package com.floatapplication.async_tasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

/**
 *
 * Used to decode bitmap off the ui thread.
 */

public class BitmapDecodeTask extends AsyncTask<String, Void, Bitmap> {
    private static final String TAG = BitmapDecodeTask.class.getSimpleName();
    private final WeakReference<ImageView> mImageViewReference;
    private int mThumbnailSize;
    private boolean mDecodeIntoThumbnail;
    private static final int MAX_FULL_RES_IMAGE_SIZE = 1024 * 1024;

    public BitmapDecodeTask(ImageView imageView) {
        mImageViewReference = new WeakReference<>(imageView);
    }

    public void setThumbnailSize(int thumbnailSize) {
        mDecodeIntoThumbnail = true;
        mThumbnailSize = thumbnailSize;
    }

    @Nullable
    private Bitmap getThumbnailBitmap(String path) {
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bounds);
        if ((bounds.outWidth == -1) || (bounds.outHeight == -1)) {
            return null;
        }
        int originalSize = (bounds.outHeight > bounds.outWidth) ? bounds.outHeight : bounds.outWidth;
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = originalSize / mThumbnailSize;
        return BitmapFactory.decodeFile(path, opts);
    }

    @Override
    protected Bitmap doInBackground(String... imagePaths) {
        String path = imagePaths[0];
        if (mDecodeIntoThumbnail) {
            return getThumbnailBitmap(path);
        }

        BitmapFactory.Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        int imageHeight = options.outHeight;
        int imageWidth = options.outWidth;

        Log.d(TAG, "size: " + imageHeight * imageWidth);
        options = new Options();
        if (imageHeight * imageWidth > MAX_FULL_RES_IMAGE_SIZE) {
            options.inSampleSize = 2;
        }
        return BitmapFactory.decodeFile(path, options);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (bitmap != null) {
            final ImageView imageView = mImageViewReference.get();
            Log.d(TAG, "Reference: " + imageView);
            Log.d(TAG, "Bitmap: " + bitmap);
            if (imageView != null) {
                imageView.setImageBitmap(bitmap);
                imageView.invalidate();
            }
        }
    }
}
