package com.floatapplication.shop.util;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.ImageView;

import com.floatapplication.map.models.Icon.IconPair;


/**
 * Created by william on 2018-05-23.
 * Class that implements the MVP pattern with ShopCreationIconsAdapter.
 */

public class ShopIconPresenter {
    private IconPair mSelectedIcon;
    private ImageView mSelectedImageView;

    public void present(final IconPair iconPair, final ImageView imageView) {
        if (mSelectedIcon == null || mSelectedIcon.type == iconPair.type) {
            imageView.setImageResource(iconPair.selectedRes);
            mSelectedIcon = iconPair;
            mSelectedImageView = imageView;
        } else {
            imageView.setImageResource(iconPair.res);
        }
        imageView.setOnClickListener((View view) -> {
            // Compare by reference is fine, as long as icon list is not set again.
            if (mSelectedIcon != iconPair) {
                mSelectedImageView.setImageResource(mSelectedIcon.res);
                imageView.setImageResource(iconPair.selectedRes);
                mSelectedIcon = iconPair;
                mSelectedImageView = imageView;
            }
        });
    }

    public IconPair getSelectedIconPair() {
        return mSelectedIcon;
    }

    public void setSelectedIconPair(@NonNull IconPair iconPair) {
        mSelectedIcon = iconPair;
    }
}
