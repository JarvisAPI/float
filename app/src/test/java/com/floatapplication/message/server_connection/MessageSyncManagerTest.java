package com.floatapplication.message.server_connection;

import android.content.Context;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.TestApplication;
import com.floatapplication.factory.FactoryInterface;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.message.MessageTestMockObjects.ArchiveServerDatabaseMock;
import com.floatapplication.message.MessageTestMockObjects.MockInboxDatabase;
import com.floatapplication.message.MessageTestMockObjects.MockMessageDatabase;
import com.floatapplication.messages.activity.activity_helpers.MessageLoader;
import com.floatapplication.messages.message_database.InboxDatabaseHelper;
import com.floatapplication.messages.message_database.MessageDatabaseHelper;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.models.Dialog;
import com.floatapplication.messages.server_connection.ArchiveMessageManager;
import com.floatapplication.messages.server_connection.ChatConnection;
import com.floatapplication.messages.server_connection.ChatConnectionService;
import com.floatapplication.messages.server_connection.MessageFeatureHandlerInterface;
import com.floatapplication.messages.server_connection.MessageSyncManager;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.user.util.UserSessionInterface;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by jamescho on 2018-06-14.
 *
 */
@SuppressWarnings("unchecked")
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class MessageSyncManagerTest {
    private MessageSyncManager messageSyncManager;
    private ChatConnectionService chatConnectionService;
    @Mock private HypermediaInterface hypermediaInterface;
    @Mock private UserSessionInterface userSessionInterface;
    @Mock private FactoryInterface factoryInterface;
    @Mock private ChatConnection chatConnection;
    @Mock private ArchiveMessageManager archiveMessageManager;
    @Mock private MessageDatabaseHelper messageDatabaseHelper;
    @Mock private InboxDatabaseHelper inboxDatabaseHelper;
    @Mock private MessageFeatureHandlerInterface messageFeatureHandlerInterface;
    private ArchiveServerDatabaseMock archiveServerDatabaseMock;
    private MockMessageDatabase messageDB;
    private MockInboxDatabase inboxDB;
    private String mockedHttpResponse;

    @Before
    public void setup() {
        messageDB = new MockMessageDatabase();
        inboxDB = new MockInboxDatabase();
        initMocks(this);
        chatConnectionService = Robolectric.buildService(ChatConnectionService.class).get();
        archiveServerDatabaseMock = ArchiveServerDatabaseMock.getInstance();

        FloatApplication.setUserSessionInterface(userSessionInterface);
        FloatApplication.setHypermediaInterface(hypermediaInterface);
        FloatApplication.setFactoryInterface(factoryInterface);
        FloatApplication.setMessageFeatureHandlerInterface(messageFeatureHandlerInterface);
        setMessageDatabase();
        setInboxDatabase();
        setArchiveMessageManager();
        setHypermediaInterface();
        setTokenObtain();
        setChatConnectionState();
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }


    private void setGetArchiveMessagesAfter(){
        doAnswer(new Answer() {
            @Override
            public List<CustomMessage> answer(InvocationOnMock invocation) throws Throwable {
                CustomMessage arg1 = invocation.getArgumentAt(0,CustomMessage.class);
                int arg2 = invocation.getArgumentAt(1,Integer.class);
                List<CustomMessage> retVal = archiveServerDatabaseMock.getArchiveMessagesAfter(arg1,arg2);
                messageDB.insertData(retVal);
                return retVal;
            }
        }).when(archiveMessageManager).getArchiveMessagesAfter(any(CustomMessage.class),anyInt());
    }

    private void setHypermediaInterface(){
        doAnswer(new Answer() {
            @Override
            public String answer(InvocationOnMock invocation) {
                return "1";
            }
        }).when(userSessionInterface).getUsername();
    }

    private void setMessageDatabaseAddData(){
        doAnswer(new Answer() {
            @Override
            public List<CustomMessage> answer(InvocationOnMock invocation) throws Throwable {
                List<CustomMessage> l = new ArrayList<>();
                CustomMessage arg1 = invocation.getArgumentAt(0, CustomMessage.class);
                l.add(arg1);
                messageDB.insertData(l);
                return null;
            }
        }).when(messageDatabaseHelper).addData(any(CustomMessage.class));
    }


    private void setInboxDatabaseAddData(){
        doAnswer(new Answer() {
            @Override
            public List<CustomMessage> answer(InvocationOnMock invocation) throws Throwable {
                Dialog arg1 = invocation.getArgumentAt(0, Dialog.class);
                inboxDB.insertData(arg1);
                return null;
            }
        }).when(inboxDatabaseHelper).addData(any(Dialog.class));
    }

    private void setArchiveMessageManager(){
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                archiveMessageManager = mock(ArchiveMessageManager.class);
                return archiveMessageManager;
            }
        }).when(chatConnection).getArchiveMessageManager();
    }

    private void setMessageDatabase(){
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                messageDatabaseHelper = mock(MessageDatabaseHelper.class);
                return messageDatabaseHelper;
            }
        }).when(factoryInterface).getMessageDatabase(anyString());
    }

    private void setInboxDatabase(){
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                inboxDatabaseHelper = mock(InboxDatabaseHelper.class);
                return inboxDatabaseHelper;
            }
        }).when(factoryInterface).getInboxDatabase(anyString());// random username
    }

    private void setTokenError() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UserSessionInterface.TokenCallback callback = invocation.getArgumentAt(1, UserSessionInterface.TokenCallback.class);
                callback.onTokenError("Token error");
                return null;
            }
        }).when(userSessionInterface).getUserAccessToken(any(Context.class), any(UserSessionInterface.TokenCallback.class));
    }

    private void setChatConnectionState(){
        doAnswer(new Answer() {
            @Override
            public ChatConnection.ConnectionState answer(InvocationOnMock invocation) {
                return ChatConnection.ConnectionState.AUTHENTICATED;
            }
        }).when(chatConnection).getState();
    }

    private void setHypermediaSuccess(){
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerCallback callback = invocation.getArgumentAt(4, ServerCallback.class);
                callback.onSuccessResponse(mockedHttpResponse);
                return null;

            }
        }).when(hypermediaInterface).follow(eq(Hypermedia.GET), anyString(),
                anyMap(), anyMap(), anyMap(), any(ServerCallback.class),any(ServerErrorCallback.class));
    }

    private void setTokenObtain() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                UserSessionInterface.TokenCallback callback = invocation.getArgumentAt(1, UserSessionInterface.TokenCallback.class);
                callback.onTokenObtained("Token");
                return null;
            }
        }).when(userSessionInterface).getUserAccessToken(any(Context.class), any(UserSessionInterface.TokenCallback.class));
    }


    private void setHypermediaFailure() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerErrorCallback errorCallback = invocation.getArgumentAt(3, ServerErrorCallback.class);
                errorCallback.onErrorResponse(new VolleyError("Test-Volley-Error"));
                return null;
            }
        }).when(hypermediaInterface).authFollow(eq(Hypermedia.GET), any(Context.class), anyString(),
                any(ServerCallback.class), any(ServerErrorCallback.class));
    }


    @Test
    public void hypermediaFailTest(){
        setHypermediaFailure();
        setTokenError();
        ArrayList<CustomMessage> lm = new ArrayList<>();
        messageSyncManager = new MessageSyncManager(lm, chatConnectionService, chatConnection);
        messageSyncManager.fetchServerDialogs();
        verify(chatConnection, times(1)).onMessageSyncComplete();
        verify(inboxDatabaseHelper, times(0)).addData(any(CustomMessage.class));
        verify(messageDatabaseHelper,times(0)).addData(anyListOf(CustomMessage.class));
        verify(messageDatabaseHelper, times(0)).addData(any(CustomMessage.class));
    }

    @Test
    public void freshDialogLoadTest(){
        setHypermediaSuccess();

        ArrayList<CustomMessage> lm = new ArrayList<>();

        messageSyncManager = new MessageSyncManager(lm, chatConnectionService, chatConnection);
        setInboxDatabaseAddData();
        setMessageDatabaseAddData();
        setGetArchiveMessagesAfter();
        mockedHttpResponse = archiveServerDatabaseMock.loadMockDatabase1();
        messageSyncManager.fetchServerDialogs();
        Set<CustomMessage> expected = archiveServerDatabaseMock.getActualMostRecentMessages();
        Set<CustomMessage> actual = inboxDB.getLastMessages();
        verify(chatConnection, times(1)).onMessageSyncComplete();
        assertTrue(messageDB.conversationExists(ArchiveServerDatabaseMock.DIALOG_JID1));
        assertTrue(messageDB.conversationExists(ArchiveServerDatabaseMock.DIALOG_JID2));
        assertTrue(messageDB.conversationExists(ArchiveServerDatabaseMock.DIALOG_JID3));
        assertTrue(inboxDB.dialogExists(ArchiveServerDatabaseMock.DIALOG_JID1));
        assertTrue(inboxDB.dialogExists(ArchiveServerDatabaseMock.DIALOG_JID2));
        assertTrue((inboxDB.dialogExists(ArchiveServerDatabaseMock.DIALOG_JID3)));
        assertEquals(1, messageDB.getConversation(ArchiveServerDatabaseMock.DIALOG_JID1).size());
        assertEquals(1, messageDB.getConversation(ArchiveServerDatabaseMock.DIALOG_JID2).size());
        assertEquals( 1, messageDB.getConversation(ArchiveServerDatabaseMock.DIALOG_JID3).size());
        assertTrue(expected.equals(actual));
    }

    @Test
    public void dialogMissMatchTest(){
        setHypermediaSuccess();

        ArrayList<CustomMessage> lm = new ArrayList<>();

        CustomMessage msg = ArchiveServerDatabaseMock.getMessageFixture(ArchiveServerDatabaseMock.DIALOG_JID1);
        ArrayList<CustomMessage> conversation = new ArrayList<>();
        conversation.add(msg);
        messageDB.insertData(conversation);
        lm.add(msg);

        msg = ArchiveServerDatabaseMock.getMessageFixture(ArchiveServerDatabaseMock.DIALOG_JID2);
        conversation = new ArrayList<>();
        conversation.add(msg);
        messageDB.insertData(conversation);
        lm.add(msg);

        msg = ArchiveServerDatabaseMock.getMessageFixture(ArchiveServerDatabaseMock.DIALOG_JID3);
        conversation = new ArrayList<>();
        conversation.add(msg);
        messageDB.insertData(conversation);
        lm.add(msg);

        messageSyncManager = new MessageSyncManager(lm, chatConnectionService, chatConnection);
        setInboxDatabaseAddData();
        setMessageDatabaseAddData();
        setGetArchiveMessagesAfter();

        mockedHttpResponse = archiveServerDatabaseMock.loadMockDatabase2();
        messageSyncManager.fetchServerDialogs();
        Set<CustomMessage> expected = archiveServerDatabaseMock.getActualMostRecentMessages();
        Set<CustomMessage> actual = inboxDB.getLastMessages();
        verify(chatConnection, times(1)).onMessageSyncComplete();
        assertTrue(messageDB.conversationExists(ArchiveServerDatabaseMock.DIALOG_JID1));
        assertTrue(messageDB.conversationExists(ArchiveServerDatabaseMock.DIALOG_JID2));
        assertTrue(messageDB.conversationExists(ArchiveServerDatabaseMock.DIALOG_JID3));
        assertTrue(messageDB.conversationExists(ArchiveServerDatabaseMock.DIALOG_JID4));
        assertTrue(inboxDB.dialogExists(ArchiveServerDatabaseMock.DIALOG_JID1));
        assertTrue(inboxDB.dialogExists(ArchiveServerDatabaseMock.DIALOG_JID2));
        assertTrue((inboxDB.dialogExists(ArchiveServerDatabaseMock.DIALOG_JID3)));
        assertTrue(inboxDB.dialogExists(ArchiveServerDatabaseMock.DIALOG_JID4));
        assertEquals(1, messageDB.getConversation(ArchiveServerDatabaseMock.DIALOG_JID1).size());
        assertEquals(1, messageDB.getConversation(ArchiveServerDatabaseMock.DIALOG_JID2).size());
        assertEquals( 1 , messageDB.getConversation(ArchiveServerDatabaseMock.DIALOG_JID3).size());
        assertEquals( 1 , messageDB.getConversation(ArchiveServerDatabaseMock.DIALOG_JID4).size());
        assertTrue(expected.equals(actual));
    }

    @Test
    public void dialogHeadMissMatchTest(){
        setHypermediaSuccess();

        ArrayList<CustomMessage> lm = new ArrayList<>();

        CustomMessage msg = ArchiveServerDatabaseMock.getMessageFixture(ArchiveServerDatabaseMock.DIALOG_JID1);
        ArrayList<CustomMessage> conversation = new ArrayList<>();
        conversation.add(msg);
        messageDB.insertData(conversation);
        lm.add(msg);

        msg = ArchiveServerDatabaseMock.getMessageFixture(ArchiveServerDatabaseMock.DIALOG_JID2);
        conversation = new ArrayList<>();
        conversation.add(msg);
        messageDB.insertData(conversation);
        lm.add(msg);

        msg = ArchiveServerDatabaseMock.getMessageFixture(ArchiveServerDatabaseMock.DIALOG_JID3);
        conversation = new ArrayList<>();
        conversation.add(msg);
        messageDB.insertData(conversation);
        lm.add(msg);

        messageSyncManager = new MessageSyncManager(lm, chatConnectionService, chatConnection);
        setInboxDatabaseAddData();
        setMessageDatabaseAddData();
        setGetArchiveMessagesAfter();
        mockedHttpResponse = archiveServerDatabaseMock.loadMockDatabase3();
        messageSyncManager.fetchServerDialogs();

        Set<CustomMessage> expected = archiveServerDatabaseMock.getActualMostRecentMessages();
        Set<CustomMessage> actual = inboxDB.getLastMessages();
        verify(chatConnection, times(1)).onMessageSyncComplete();
        verify(messageDatabaseHelper,times(1)).flushConversation(ArchiveServerDatabaseMock.DIALOG_JID1);
        assertTrue(messageDB.conversationExists(ArchiveServerDatabaseMock.DIALOG_JID1));
        assertTrue(messageDB.conversationExists(ArchiveServerDatabaseMock.DIALOG_JID2));
        assertTrue(messageDB.conversationExists(ArchiveServerDatabaseMock.DIALOG_JID3));
        assertTrue(messageDB.conversationExists(ArchiveServerDatabaseMock.DIALOG_JID4));
        assertTrue(inboxDB.dialogExists(ArchiveServerDatabaseMock.DIALOG_JID1));
        assertTrue(inboxDB.dialogExists(ArchiveServerDatabaseMock.DIALOG_JID2));
        assertTrue((inboxDB.dialogExists(ArchiveServerDatabaseMock.DIALOG_JID3)));
        assertTrue(inboxDB.dialogExists(ArchiveServerDatabaseMock.DIALOG_JID4));
        assertEquals(1, messageDB.getConversation(ArchiveServerDatabaseMock.DIALOG_JID1).size());
        assertEquals(MessageLoader.PAGE_SIZE - 1, messageDB.getConversation(ArchiveServerDatabaseMock.DIALOG_JID2).size());
        assertEquals( MessageLoader.PAGE_SIZE , messageDB.getConversation(ArchiveServerDatabaseMock.DIALOG_JID3).size());
        assertEquals( 1 , messageDB.getConversation(ArchiveServerDatabaseMock.DIALOG_JID4).size());
        assertTrue(expected.equals(actual));
    }

}
