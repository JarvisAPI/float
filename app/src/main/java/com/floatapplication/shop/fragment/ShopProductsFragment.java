package com.floatapplication.shop.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.floatapplication.R;
import com.floatapplication.shop.adapter.ShopViewAdapter;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.shop.util.ShopProductLoader;
import com.floatapplication.shop.util.ShopProductsPresenter;
import com.floatapplication.shop.util.ShopViewProductPresenter.OnProductClickListener;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by william on 2018-06-06.
 *
 */

public class ShopProductsFragment extends Fragment {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private ShopProductsPresenter mPresenter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mPresenter = new ShopProductsPresenter();
        Activity activity = getActivity();
        if (activity instanceof OnProductClickListener) {
            mPresenter.setOnProductClickedListener((OnProductClickListener) activity);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shop_products, container, false);
        ButterKnife.bind(this, v);
        Bundle args = getArguments();
        if (args != null) {
            Shop shop = args.getParcelable("shop");
            if (shop != null) {
                mPresenter.present(recyclerView, new ShopViewAdapter(getContext()),
                        new ShopProductLoader(shop.getShopId()));
            }
        }
        return  v;
    }

    public void onReorderItemsClicked() {
        if (mPresenter != null) {
            mPresenter.onReorderItemsClicked();
        }
    }
}
