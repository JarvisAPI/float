package com.floatapplication.product.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by william on 2018-06-14.
 * Base product model, representing the complete product that can be obtained from the server.
 * This class only contain getter methods.
 */
@SuppressWarnings("WeakerAccess")
public class ProductBase implements Parcelable {
    public static final String ID = "product_id";
    public static final String NAME = "product_name";
    public static final String PRICE = "product_price";
    public static final String CATEGORY = "product_category";
    public static final String DESCRIPTION = "product_description";
    public static final String MAIN_IMAGE = "product_main_image_file";
    public static final String IMAGES = "product_image_files";
    public static final String SHOP_ID = "shop_id";
    public static final String LIST = "product_list";

    public static final String NONE_CATEGORY = "none";

    protected String mProductId;
    protected String mProductName;
    protected String mProductPrice;
    protected String mProductCategory;
    protected String mProductDescription;
    protected String mProductMainImageFile;
    protected List<String> mProductImageFiles;
    protected String mProductShopId;

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<ProductBase>() {
        @Override
        public ProductBase createFromParcel(Parcel parcel) {
            return new ProductBase(parcel);
        }

        @Override
        public ProductBase[] newArray(int size) {
            return new ProductBase[size];
        }
    };

    protected ProductBase() {
        mProductImageFiles = new ArrayList<>();
    }

    protected ProductBase(Parcel parcel) {
        mProductId = parcel.readString();
        mProductName = parcel.readString();
        mProductPrice = parcel.readString();
        mProductCategory = parcel.readString();
        mProductDescription = parcel.readString();
        mProductMainImageFile = parcel.readString();
        mProductImageFiles = new ArrayList<>();
        parcel.readStringList(mProductImageFiles);
        mProductShopId = parcel.readString();
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeString(mProductId);
        dest.writeString(mProductName);
        dest.writeString(mProductPrice);
        dest.writeString(mProductCategory);
        dest.writeString(mProductDescription);
        dest.writeString(mProductMainImageFile);
        dest.writeStringList(mProductImageFiles);
        dest.writeString(mProductShopId);
    }

    public String getProductId() {
        return mProductId;
    }

    public String getProductName() {
        return mProductName;
    }

    public String getProductPrice() {
        return mProductPrice;
    }

    public String getProductCategory() {
        return mProductCategory;
    }

    public String getProductDescription() {
        return mProductDescription;
    }

    public String getProductMainImageFile() {
        return mProductMainImageFile;
    }

    public List<String> getProductImageFiles() {
        return mProductImageFiles;
    }

    public String getProductShopId() {
        return mProductShopId;
    }
}
