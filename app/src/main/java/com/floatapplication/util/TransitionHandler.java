package com.floatapplication.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.view.View;

/**
 * Created by william on 2018-05-28.
 * Handles activity transition on the map.
 */

public class TransitionHandler {
    private static final String TAG = TransitionHandler.class.getSimpleName();
    private boolean mTransitioning;

    public void onResume() {
        mTransitioning = false;
    }

    public void onActivityResult() {
        onResume();
    }

    private void setIntentData(Intent intent, Intent data) {
        if (data != null) {
            intent.putExtras(data);
            intent.setAction(data.getAction());
        }
    }

    public void transition(Context context, Class<?> to, Intent data) {
        if (!mTransitioning) {
            mTransitioning = true;
            Intent intent = new Intent(context, to);
            setIntentData(intent, data);
            context.startActivity(intent);
        }
    }

    public void transition(Context context, Class<?> to) {
        transition(context, to, null);
    }

    public void transitionForResult(Activity activity, Class<?> to, int requestCode) {
        transitionForResult(activity, to, null, requestCode);
    }

    public void transitionForResult(Activity activity, Class<?> to, Intent data, int requestCode) {
        if (!mTransitioning) {
            mTransitioning = true;
            Intent intent = new Intent(activity, to);
            if (data != null) {
                setIntentData(intent, data);
            }
            activity.startActivityForResult(intent, requestCode);
        }
    }

    public void transitionForResult(Activity activity, String action, Uri uri, int requestCode) {
        if (!mTransitioning) {
            mTransitioning = true;
            Intent intent = new Intent(action, uri);
            activity.startActivityForResult(intent, requestCode);
        }
    }

    public void sharedElementTransition(Activity activity, Class<?> to, Intent data, View sharedView) {
        if (!mTransitioning) {
            mTransitioning = true;
            Intent intent = new Intent(activity, to);
            setIntentData(intent, data);
            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(activity,
                    sharedView, ViewCompat.getTransitionName(sharedView));
            ActivityCompat.startActivity(activity, intent, optionsCompat.toBundle());
        }
    }

    public void sharedElementTransitionForResult(Activity activity, Class<?> to, Intent data, int requestCode, View sharedView) {
        if (!mTransitioning) {
            mTransitioning = true;
            Intent intent = new Intent(activity, to);
            setIntentData(intent, data);
            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(activity,
                    sharedView, ViewCompat.getTransitionName(sharedView));
            ActivityCompat.startActivityForResult(activity, intent, requestCode, optionsCompat.toBundle());
        }
    }
}
