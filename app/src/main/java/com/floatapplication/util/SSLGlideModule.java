package com.floatapplication.util;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.volley.RequestQueue;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Registry;
import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.integration.volley.VolleyUrlLoader.Factory;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.module.LibraryGlideModule;
import com.floatapplication.server.AppControl;

import java.io.InputStream;

/**
 * Created by william on 2018-05-17.
 *
 */
@GlideModule
public class SSLGlideModule extends LibraryGlideModule {
    @Override
    public void registerComponents(@NonNull Context context, @NonNull Glide glide, @NonNull Registry registry) {
        RequestQueue requestQueue = AppControl.getSSLRequestQueue(context);
        registry.replace(GlideUrl.class, InputStream.class, new Factory(requestQueue));
    }
}
