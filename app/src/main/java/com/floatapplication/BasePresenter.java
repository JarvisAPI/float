package com.floatapplication;

public interface BasePresenter {
    void subscribe();

    void unsubscribe();
}
