package com.floatapplication.messages.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.floatapplication.messages.chatkit.commons.models.IUser;

public class User implements IUser,Parcelable {
    private String id;

    public enum Type{
        shop,user
    }

    public User(String id) {
        this.id = formatJid(id);
    }


    @Override
    public String getId() {
        return id;
    }

    public static String formatJid(String bareJid){
        String contactJid="";
        if ( bareJid.contains("@"))
        {
            contactJid = bareJid.split("@")[0];
        }else
        {
            contactJid = bareJid;
        }
        return contactJid.toLowerCase();
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<User>() {
        @Override
        public User createFromParcel(Parcel parcel) {
            return new User(parcel);
        }
        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    private User(Parcel parcel){
        this.id = parcel.readString();
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
    }
}
