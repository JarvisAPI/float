package com.floatapplication.util;

import com.bumptech.glide.annotation.GlideModule;
import com.bumptech.glide.module.AppGlideModule;

/**
 *
 * Made to work with glide generated API.
 */
@GlideModule
public class MapAppGlideModule extends AppGlideModule {
}
