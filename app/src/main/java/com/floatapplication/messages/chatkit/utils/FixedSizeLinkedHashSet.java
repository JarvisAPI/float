package com.floatapplication.messages.chatkit.utils;

import java.util.Iterator;
import java.util.LinkedHashSet;

/**
 * Created by jamescho on 2018-06-06.
 */

public class FixedSizeLinkedHashSet<T> extends LinkedHashSet<T> {
    private long maxSize;

    public FixedSizeLinkedHashSet(long maxSize) {
        this.maxSize = maxSize;
    }

    @Override
    public boolean add(T item) {
        if(size() == maxSize) {
            removeFirst();
        }
        return super.add(item);
    }

    private void removeFirst() {
        if(size() > 0) {
            Iterator<T> iterator = iterator();
            T item = iterator.next();
            remove(item);
        }
    }
}
