package com.floatapplication.product.util;

import com.floatapplication.TestApplication;
import com.floatapplication.product.util.ProductEditSubmissionStateMachine.Event;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by william on 2018-06-20.
 * Tests that the state machine transitions are correct.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ProductEditSubmissionStateMachineUnitTest {
    private ProductEditSubmissionStateMachine stateMachine;
    @Mock private ProductEditSubmissionStateLogic stateLogic;
    @Mock private ProductEditSubmissionStateTransitionLogic transitionLogic;
    private InOrder stateLogicOrder;

    @Before
    public void setup() {
        initMocks(this);
        setupStateLogicMock();
        stateLogicOrder = inOrder(stateLogic);
        stateMachine = new ProductEditSubmissionStateMachine();
        stateMachine.bind(stateLogic, transitionLogic);
    }

    private ProductEditSubmissionStateLogic success() {
        return doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                stateMachine.run(Event.SUCCESS);
                return null;
            }
        }).when(stateLogic);
    }

    private void setupStateLogicMock() {
        success().uploadProductDetails();
        success().uploadNewImages();
        success().uploadNewMainImage();
        success().onSubmitFailed();
        success().onSubmitSuccess();
        success().deleteMainImage();
        success().swapMainImageAutoSelected();
        success().swapMainImageUserSelected();
        success().deleteImages();
    }

    @Test
    public void uploadProductDetails_thenEnd() {
        when(transitionLogic.shouldUploadProductDetails()).thenReturn(true);
        when(transitionLogic.shouldUploadNewImages()).thenReturn(false);
        when(transitionLogic.shouldChangeMainImageWithNewImage()).thenReturn(false);
        when(transitionLogic.shouldDeleteMainImage()).thenReturn(false);
        when(transitionLogic.shouldSwapMainImage()).thenReturn(false);
        when(transitionLogic.shouldDeleteImages()).thenReturn(false);
        stateMachine.start();
        stateLogicOrder.verify(stateLogic).uploadProductDetails();
        stateLogicOrder.verify(stateLogic).onSubmitSuccess();
        verifyNoMoreInteractions(stateLogic);
    }

    @Test
    public void uploadProductDetails_uploadNewImages_uploadNewMainImage_thenEnd() {
        when(transitionLogic.shouldUploadProductDetails()).thenReturn(true);
        when(transitionLogic.shouldUploadNewImages()).thenReturn(true);
        when(transitionLogic.shouldChangeMainImageWithNewImage()).thenReturn(true);
        when(transitionLogic.shouldDeleteImages()).thenReturn(false);
        stateMachine.start();
        stateLogicOrder.verify(stateLogic).uploadProductDetails();
        stateLogicOrder.verify(stateLogic).uploadNewImages();
        stateLogicOrder.verify(stateLogic).uploadNewMainImage();
        stateLogicOrder.verify(stateLogic).onSubmitSuccess();
        verifyNoMoreInteractions(stateLogic);
    }

    @Test
    public void deleteMainImage_thenEnd() {
        when(transitionLogic.shouldUploadProductDetails()).thenReturn(false);
        when(transitionLogic.shouldUploadNewImages()).thenReturn(false);
        when(transitionLogic.shouldChangeMainImageWithNewImage()).thenReturn(false);
        when(transitionLogic.shouldDeleteMainImage()).thenReturn(true);
        when(transitionLogic.noImagesRemain()).thenReturn(true);
        stateMachine.start();
        stateLogicOrder.verify(stateLogic).deleteMainImage();
        stateLogicOrder.verify(stateLogic).deleteImages();
        stateLogicOrder.verify(stateLogic).onSubmitSuccess();
        verifyNoMoreInteractions(stateLogic);
    }

    @Test
    public void swapMainImageAutoSelected_deleteImages_thenEnd() {
        when(transitionLogic.shouldUploadProductDetails()).thenReturn(false);
        when(transitionLogic.shouldUploadNewImages()).thenReturn(false);
        when(transitionLogic.shouldChangeMainImageWithNewImage()).thenReturn(false);
        when(transitionLogic.shouldDeleteMainImage()).thenReturn(true);
        when(transitionLogic.shouldDeleteImages()).thenReturn(true);
        stateMachine.start();
        stateLogicOrder.verify(stateLogic).swapMainImageAutoSelected();
        stateLogicOrder.verify(stateLogic).deleteImages();
        stateLogicOrder.verify(stateLogic).onSubmitSuccess();
        verifyNoMoreInteractions(stateLogic);
    }

    @Test
    public void swapMainImageUserSelected_deleteImages_thenEnd() {
        when(transitionLogic.shouldUploadProductDetails()).thenReturn(false);
        when(transitionLogic.shouldUploadNewImages()).thenReturn(false);
        when(transitionLogic.shouldChangeMainImageWithNewImage()).thenReturn(false);
        when(transitionLogic.shouldDeleteMainImage()).thenReturn(false);
        when(transitionLogic.shouldSwapMainImage()).thenReturn(true);
        when(transitionLogic.shouldDeleteImages()).thenReturn(true);
        stateMachine.start();
        stateLogicOrder.verify(stateLogic).swapMainImageUserSelected();
        stateLogicOrder.verify(stateLogic).deleteImages();
        stateLogicOrder.verify(stateLogic).onSubmitSuccess();
        verifyNoMoreInteractions(stateLogic);
    }
}
