package com.floatapplication.util;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.widget.ImageView;

import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.request.RequestListener;
import com.floatapplication.FloatApplication;

/**
 * Created by william on 2018-05-08.
 * Handler that provides global utility functions.
 */

public class GlobalHandler {

    private static GlobalHandler mGlobalHandler;

    private GlobalHandler() {

    }

    public String constructImageFile(@NonNull String imageUrl) {
        int lastSlash = imageUrl.lastIndexOf('/');
        if (imageUrl.length() > lastSlash + 1) {
            return imageUrl.substring(lastSlash + 1);
        }
        return imageUrl;
    }

    @Nullable
    public String getAbsolutePathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] arr = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri, arr,null,null,null);
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }
            return null;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public boolean fileIsImage(String path) {
        BitmapFactory.Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        return options.outWidth != -1 && options.outHeight != -1;
    }

    public void loadImage(@NonNull ImageView imageView, String url, int placeHolderRes) {
        try {
            FloatApplication.getFactoryInterface()
                    .getGlideRequest(imageView.getContext())
                    .load(url)
                    .error(placeHolderRes)
                    .into(imageView);
        } catch (IllegalArgumentException e) {
            // When activity is destroyed then load will throw an exception.
            e.printStackTrace();
        }
    }

    public void loadImage(@NonNull ImageView imageView, GlideUrl glideUrl, int placeHolderRes) {
        try {
            FloatApplication.getFactoryInterface()
                    .getGlideRequest(imageView.getContext())
                    .load(glideUrl)
                    .error(placeHolderRes)
                    .into(imageView);
        } catch (IllegalArgumentException e) {
            // When activity is destroyed then load will throw an exception.
            e.printStackTrace();
        }
    }

    public void loadImage(@NonNull ImageView imageView, GlideUrl glideUrl, int placeHolderRes,
                          RequestListener<Drawable> listener) {
        try {
            FloatApplication.getFactoryInterface()
                    .getGlideRequest(imageView.getContext())
                    .load(glideUrl)
                    .listener(listener)
                    .error(placeHolderRes)
                    .into(imageView);
        } catch (IllegalArgumentException e) {
            // When activity is destroyed then load will throw an exception.
            e.printStackTrace();
        }
    }

    public static GlobalHandler getInstance() {
        if (mGlobalHandler == null) {
            mGlobalHandler = new GlobalHandler();
        }
        return mGlobalHandler;
    }

    @VisibleForTesting
    public static void setGlobalHandler(GlobalHandler globalHandler) {
        mGlobalHandler = globalHandler;
    }
}
