package com.floatapplication.message.models;

import com.floatapplication.messages.models.User;

import org.junit.Test;
import org.jxmpp.stringprep.XmppStringprepException;

import static junit.framework.Assert.assertEquals;


/**
 * Created by jamescho on 2018-05-08.
 *
 */

public class UserTest {
    @Test
    public void  testMessageConstructor() throws XmppStringprepException {
        String jid = "test@ec2-20241";
        User user = new User(jid);
        assertEquals(user.getId(),"test"); // assert if equals bare id
    }

}
