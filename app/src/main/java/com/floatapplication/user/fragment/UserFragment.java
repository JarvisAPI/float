package com.floatapplication.user.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.R;
import com.floatapplication.user.adapter.UserProfileAdapter;
import com.floatapplication.user.models.User;
import com.floatapplication.user.util.UserProfilePresenter;
import com.floatapplication.user.util.UserProfilePresenter.OnUserProfileClickListener;
import com.floatapplication.user.util.UserProfileShopItemPresenter.UserShopItemListener;
import com.floatapplication.util.Util;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by jamescho on 2018-05-16.
 *
 */

public class UserFragment extends Fragment {
    private String TAG = UserFragment.class.getSimpleName();

    private UserProfilePresenter mPresenter;
    private UserShopItemListener mUserShopItemListener;

    @BindView(R.id.default_shops_wallpaper)
    ImageView mShopsWallpaperView;
    @BindView(R.id.user_name)
    TextView mUserNameText;
    @BindView(R.id.user_profile_background)
    ImageView mUserProfileBackground;
    @BindView(R.id.user_profile_image)
    CircleImageView mUserProfileImage;
    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    @BindView(R.id.message_fab)
    FloatingActionButton mMessageButton;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mPresenter = new UserProfilePresenter(Util.getInstance().glide(context));
        Activity activity = getActivity();
        if (activity instanceof OnUserProfileClickListener) {
            mPresenter.setOnUserProfileClickListener((OnUserProfileClickListener) activity);
        }
        if (activity instanceof UserShopItemListener) {
            mUserShopItemListener = (UserShopItemListener) activity;
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_user, container, false);
        ButterKnife.bind(this, v);
        Bundle args = getArguments();
        if(args != null) {
            String username = args.getString(User.USERNAME);
            String name = args.getString(User.NAME);
            if (username != null && name != null) {
                UserProfileAdapter adapter = new UserProfileAdapter();
                adapter.getPresenter().setUserShopItemListener(mUserShopItemListener);
                mPresenter.present(mShopsWallpaperView, mRecyclerView, adapter, mUserProfileBackground,
                        mUserProfileImage, mMessageButton, mToolbar, mUserNameText, username, name);
            }
        }
        return v;
    }

    public UserProfilePresenter getPresenter() {
        return mPresenter;
    }
}
