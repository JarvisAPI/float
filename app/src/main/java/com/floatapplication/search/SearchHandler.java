package com.floatapplication.search;


import android.util.Log;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.util.Extractor;
import com.floatapplication.util.IFactory;
import com.floatapplication.util.Util;
import com.simplexorg.mapfragment.model.GeoPoint;
import com.simplexorg.searchfragment.model.Suggestion;
import com.simplexorg.searchfragment.search.SearchData;
import com.simplexorg.searchfragment.search.SearchSuggestionSupplier;

import java.util.List;
import java.util.Map;

import static com.floatapplication.search.SearchQuery.LOCATION;
import static com.floatapplication.search.SearchQuery.QUERY;

public class SearchHandler implements SearchSuggestionSupplier {
    private static final String TAG = SearchHandler.class.getSimpleName();

    /**
     * Gets local products.
     * @param searchData the search data used to query.
     * @param listener the listener for callback.
     */
    @Override
    public void getSuggestion(SearchData searchData, OnSuggestionObtainedListener listener) {
        String query = (String) searchData.get(QUERY);
        GeoPoint location = (GeoPoint) searchData.get(LOCATION);

        Map<String, String> queries = IFactory.getInstance().createMap();
        queries.put(QUERY, query);
        queries.put(LOCATION, location.toGeoJSON());
        Util.getInstance().buildQuery(queries);
        FloatApplication.getHypermediaInterface()
                .follow(Hypermedia.GET, HypermediaInterface.SEARCH_PRODUCTS, null, null, queries,
                        (String response) -> {
                            List<Suggestion> suggestions = Extractor.getInstance().extractSuggestions(response);
                            listener.onSuggestionObtained(suggestions);
                        },
                        (VolleyError error) -> {
                            Log.e(TAG, "Unable to get search suggestions!");
                            Log.e(TAG, "Error: " + error.getMessage());
                        });
    }
}
