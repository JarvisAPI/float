package com.floatapplication.messages.holders;

import android.view.View;

import com.floatapplication.R;
import com.floatapplication.messages.chatkit.messages.MessageHolders;
import com.floatapplication.messages.models.CustomMessage;

public class CustomIncomingImageMessageViewHolder
        extends MessageHolders.IncomingImageMessageViewHolder<CustomMessage> {

    private View onlineIndicator;

    public CustomIncomingImageMessageViewHolder(View itemView) {
        super(itemView);
        onlineIndicator = itemView.findViewById(R.id.onlineIndicator);
    }

    @Override
    public void onBind(CustomMessage message) {
        super.onBind(message);

    }
}