package com.floatapplication.factory;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.floatapplication.async_tasks.BitmapDecodeTask;
import com.floatapplication.async_tasks.UploadImageTask;
import com.floatapplication.messages.message_database.ChatUserCache;
import com.floatapplication.messages.message_database.InboxDatabaseHelper;
import com.floatapplication.messages.message_database.MessageDatabaseHelper;
import com.floatapplication.messages.message_database.MessageUserDatabaseHelper;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.server_connection.ArchiveMessageManager;
import com.floatapplication.messages.server_connection.ChatConnection;
import com.floatapplication.messages.server_connection.MessageSyncManager;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.util.GlideApp;
import com.floatapplication.util.GlideRequests;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.mam.MamManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by william on 2018/1/25.
 *
 */

public class AbstractFactory implements FactoryInterface {

    private Context floatAppContext;

    public AbstractFactory(Context context){
        floatAppContext = context;
    }

    @Override
    public AsyncTask<String, Void, Void> createUploadImageTask(String uploadPath, String dataName, List<String> filepaths, Map<String, String> headers,
                                                               ServerCallback callback, ServerErrorCallback errorCallback) {
        return new UploadImageTask(uploadPath, dataName, filepaths, headers, callback, errorCallback);
    }

    @Override
    public BitmapDecodeTask createBitmapDecodeTask(ImageView imageView) {
        return new BitmapDecodeTask(imageView);
    }

    @Override
    public GlideRequests getGlideRequest(Context context) {
        return GlideApp.with(context);
    }

    @Override
    public XMPPConnection createXMPPConnection(XMPPTCPConnectionConfiguration conf){return new XMPPTCPConnection(conf);}

    @Override
    public ArchiveMessageManager createArchiveMessageManager(XMPPTCPConnection connection, String jid){
        return new ArchiveMessageManager(MamManager.getInstanceFor(connection), jid);
    }

    @Override
    public MessageSyncManager createMessageSyncManager(ArrayList<CustomMessage> lm, ChatConnection connection){
        return new MessageSyncManager(lm, floatAppContext, connection);
    }

    @Override
    public InboxDatabaseHelper getInboxDatabase(String clientJid){
        return new InboxDatabaseHelper(floatAppContext,clientJid);
    }

    @Override
    public MessageDatabaseHelper getMessageDatabase(String clientJid){
        return new MessageDatabaseHelper(floatAppContext, clientJid);
    }

    @Override
    public MessageUserDatabaseHelper getMessageUserDatabase(){
        return new MessageUserDatabaseHelper(floatAppContext);
    }

    @Override
    public ChatUserCache getChatUserCache(){
        return new ChatUserCache(floatAppContext);
    }
}
