package com.floatapplication.product.util;

import android.util.Log;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.map.models.Icon;
import com.floatapplication.product.models.Product;
import com.floatapplication.product.util.ProductInfiniteScrollStateMachine.Event;
import com.floatapplication.product.util.ProductShopQueryQueue.ProductShopQuery;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.simplexorg.mapfragment.model.GeoPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by william on 2018-05-28.
 * Interacts with the product infinite scroll activity which holds the views and the
 * product infinite scroll state machine to achieve infinite product scroll functionality.
 */

public class ProductInfiniteScrollLogic
        implements
        ProductInfiniteScrollStateLogic,
        ProductShopQueryFactory {
    public interface OnProductsObtainedListener {
        /**
         * Each product returned in the list has its shop id set.
         * @param products list of products that also has information about their shop id.
         */
        void onProductsObtained(List<Product> products);
    }

    private static final String TAG = ProductInfiniteScrollLogic.class.getSimpleName();
    private static final int MAX_SHOP_LIMIT = 30; // Max number of shops returned by server in one response.
    private static final int MIN_DIST = 0;
    private static final int MAX_DIST = 100 * 1000; // Units in meters, if there is no shops within this distance of the current user then nothing will be displayed.
    private ProductInfiniteScrollStateMachine mStateMachine;
    private GeoPoint mPos;

    private int mOffset;
    private ProductShopQueryQueue mProductShopQueryQueue;
    private OnProductsObtainedListener mOnProductsObtainedListener;
    private Set<String> mShopIds;

    public ProductInfiniteScrollLogic(GeoPoint pos, ProductInfiniteScrollStateMachine stateMachine,
                                      ProductShopQueryQueue productShopQueryQueue) {
        mPos = pos;
        mShopIds = new HashSet<>();
        mStateMachine = stateMachine;
        mStateMachine.setLogicImplementer(this);
        mProductShopQueryQueue = productShopQueryQueue;
        mProductShopQueryQueue.setProductShopQueryFactory(this);
        mStateMachine.start();
    }

    public void setOnProductsObtainedListener(OnProductsObtainedListener listener) {
        mOnProductsObtainedListener = listener;
    }

    public void onProductScrollBottomReached() {
        if (mProductShopQueryQueue.size() == 0) {
            mStateMachine.run(Event.HIT_BOTTOM_SHOP_LIST_EMPTY);
        } else {
            mStateMachine.run(Event.HIT_BOTTOM_SHOP_LIST_NOT_EMPTY);
        }
    }

    private void extractShops(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray = jsonObject.getJSONArray("icons");
            int limit = 5;
            for (int i = 0; i < jsonArray.length(); i++) {
                String shopId = jsonArray.getJSONObject(i).getString(Icon.ID);
                if (!mShopIds.contains(shopId)) {
                    mProductShopQueryQueue.addShopToQuery(shopId, limit);
                    mShopIds.add(shopId);
                }
            }
            mOffset += jsonArray.length();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getShops() {
        Log.d(TAG, "getShops");
        Map<String, String> vMap = new HashMap<>();
        vMap.put("latitude", String.valueOf(mPos.latitude));
        vMap.put("longitude", String.valueOf(mPos.longitude));
        vMap.put("min_distance", String.valueOf(MIN_DIST));
        vMap.put("max_distance", String.valueOf(MAX_DIST));
        vMap.put("offset", String.valueOf(mOffset));
        vMap.put("limit", String.valueOf(MAX_SHOP_LIMIT));
        FloatApplication.getHypermediaInterface()
                .follow(Hypermedia.GET, HypermediaInterface.NEARBY_SHOPS, vMap, null,
                        new ServerCallback() {
                            @Override
                            public void onSuccessResponse(String response) {
                                extractShops(response);
                                mStateMachine.run(Event.OBTAINED_SHOPS);
                            }
                        }, new ServerErrorCallback() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d(TAG, "Unable to acquire shops");
                                error.printStackTrace();
                                mStateMachine.run(Event.NO_SHOPS_OBTAINED);
                            }
                        });
    }

    @Override
    public void loadProductFromShops() {
        mProductShopQueryQueue.loadProductFromShops();
    }

    @Override
    public ProductShopQuery create(final String shopId, final int offset, final int limit) {
        return new ProductShopQuery() {
            @Override
            public void execute() {
                Map<String, String> vMap = new HashMap<>();
                vMap.put("shop_id", shopId);
                vMap.put("offset", String.valueOf(offset));
                vMap.put("limit", String.valueOf(limit));
                FloatApplication.getHypermediaInterface()
                        .follow(Hypermedia.GET, HypermediaInterface.SHOP_PRODUCTS, vMap, null,
                                new ServerCallback() {
                                    @Override
                                    public void onSuccessResponse(String response) {
                                        onProductListObtained(response, shopId, limit);
                                    }
                                },
                                new ServerErrorCallback() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        onErrorObtained(error, shopId);
                                    }
                                });
            }
        };
    }

    private void onProductListObtained(String response, String shopId, int limit) {
        try {
            JSONArray products = new JSONObject(response).getJSONArray("product_list");
            if (products.length() > 0) {
                addToProductList(products, shopId);
            }
            if (products.length() == 0 || products.length() < limit) {
                mProductShopQueryQueue.removeShopFromQuery(shopId);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void onErrorObtained(VolleyError error, String shopId) {
        if (error.networkResponse != null) {
            if (error.networkResponse.statusCode == 500) {
                mProductShopQueryQueue.removeShopFromQuery(shopId);
            }
        }
    }

    private void addToProductList(JSONArray productsJsonArr, String shopId) throws JSONException {
        if (mOnProductsObtainedListener != null) {
            List<Product> productList = new ArrayList<>();
            for (int i = 0; i < productsJsonArr.length(); i++) {
                JSONObject productJson = productsJsonArr.getJSONObject(i);
                Product product = Product.createBasicProduct(productJson);
                product.setProductShopId(shopId);
                productList.add(product);
            }
            mOnProductsObtainedListener.onProductsObtained(productList);
        }
    }
}
