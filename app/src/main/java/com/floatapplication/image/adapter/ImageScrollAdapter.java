package com.floatapplication.image.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.github.chrisbanes.photoview.PhotoView;
import com.floatapplication.R;
import com.floatapplication.async_tasks.BitmapDecodeTask;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by william on 2017/12/31.
 *
 */
public class ImageScrollAdapter extends PagerAdapter {
    private LayoutInflater mLayoutInflater;
    private List<String> mImagePaths;
    private List<View> mViewList;
    private OnClickListener mOnImageClickListener;

    public ImageScrollAdapter(Context context) {
        mImagePaths = new ArrayList<>();
        mViewList = new ArrayList<>();
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void setImagePaths(List<String> imagePaths) {
        mImagePaths.clear();
        mImagePaths.addAll(imagePaths);
        for (int i = 0; i < mImagePaths.size(); i++) {
            mViewList.add(i, null);
        }
    }

    public String getImagePath(int position) {
        return mImagePaths.get(position);
    }

    public List<String> getImagePaths() {
        return mImagePaths;
    }

    public void removeImagePath(int position) {
        mImagePaths.remove(position);
        mViewList.remove(position);
    }

    public void setOnImageClickListener(OnClickListener listener) {
        mOnImageClickListener = listener;
    }

    @Override
    public int getCount() {
        return mImagePaths.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
        return view == obj;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.product_image_scroll_view_pager_item, container, false);
        final PhotoView imageView = itemView.findViewById(R.id.product_image_scroll_view_pager_image_view);
        imageView.setOnClickListener(mOnImageClickListener);
        BitmapDecodeTask task = new BitmapDecodeTask(imageView);
        task.execute(mImagePaths.get(position));
        container.addView(itemView);
        mViewList.set(position, imageView);
        return itemView;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        if (mViewList.contains((View) object)) {
            return mViewList.indexOf((View) object);
        }
        return POSITION_NONE;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object obj) {
        container.removeView((View) obj);
    }
}
