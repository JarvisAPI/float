package com.floatapplication.product.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.messages.activity.MessagesActivity;
import com.floatapplication.product.adapter.ProductImagePagerAdapter;
import com.floatapplication.product.models.Product;
import com.floatapplication.product.util.ProductPresenter;
import com.floatapplication.search.SearchActivity;
import com.floatapplication.search.SearchQuery;
import com.floatapplication.shop.activity.ShopViewActivity;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.user.activity.LoginActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.floatapplication.util.Util.EXTRA_PRODUCT;
import static com.floatapplication.util.Util.EXTRA_SEARCH_DATA;

/**
 * Created by william on 2018-05-18.
 * Activity for displaying product in detail.
 */

public class ProductActivity extends AppCompatActivity implements ProductPresenter.OnUtilBarIconClickListener {
    private static final String TAG = ProductActivity.class.getSimpleName();
    public static final String INCLUDE_HOME_ICON = "includeHomeIcon";
    private ProductPresenter mPresenter;
    private Product mProduct;

    @BindView(R.id.product_name)
    TextView mProductName;
    @BindView(R.id.product_price)
    TextView mProductPrice;
    @BindView(R.id.product_description)
    TextView mProductDescription;
    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    @BindView(R.id.product_categories_grid_layout)
    GridLayout mCategoryGrid;
    @BindView(R.id.tab_dots)
    TabLayout mTabDots;
    @BindView(R.id.shop_home)
    ImageView mShopHomeIcon;
    @BindView(R.id.message_icon)
    ImageView mMessageIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        ButterKnife.bind(this);

        boolean includeHomeIcon = getIntent().getBooleanExtra(INCLUDE_HOME_ICON, true);
        if (!includeHomeIcon) {
            mShopHomeIcon.setVisibility(View.GONE);
        }

        Product product = getIntent().getParcelableExtra(EXTRA_PRODUCT);
        mProduct = product;
        mPresenter = new ProductPresenter(this, product, getIntent());
        mPresenter.present(mViewPager, new ProductImagePagerAdapter(),
                mProductName, mProductPrice, mProductDescription, mCategoryGrid, mTabDots,
                mShopHomeIcon, mMessageIcon);

        mPresenter.setOnUtilBarItemClickListener(this);
    }

    @Override
    public void onBackPressed() {
        mPresenter.onBackPressed();
        super.onBackPressed();
    }

    @Override
    public void onMessageIconClick() {
        if(FloatApplication.getUserSessionInterface().isLoggedIn()) {
            Intent intent = new Intent(this, MessagesActivity.class);
            intent.putExtra(getString(R.string.contact), mProduct.getProductShopId());
            intent.putExtra(getString(R.string.chat_username), FloatApplication.getUserSessionInterface().getUsername());
            startActivity(intent);
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public void onShopHomeIconClick(Shop shop) {
        Intent intent = new Intent(this, ShopViewActivity.class);
        intent.putExtra("shop", shop);
        startActivity(intent);
    }

    @Override
    public void finish() {
        SearchQuery localSearch = getIntent().getParcelableExtra(EXTRA_SEARCH_DATA);
        if (localSearch != null) {
            Intent intent = new Intent(this, SearchActivity.class);
            intent.putExtra(EXTRA_SEARCH_DATA,
                    new SearchQuery(mProduct.getProductName(), localSearch.location));
            startActivity(intent);
        }
        super.finish();
    }
}
