package com.floatapplication.shop.edit;

import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;

import com.floatapplication.BasePresenter;
import com.floatapplication.BaseView;
import com.floatapplication.shop.models.Shop;

import java.util.List;

/**
 * Used to implement MVP pattern in editing shop's information.
 */
public interface ShopAboutEditContract {
    interface Model {
        interface Observer {
            int INFO_RECEIVED = 200;
            int SHOP_DELETED = 201;
            int SHOP_DELETE_FAILED = 202;
            int INFO_UPDATED = 203;
            int ERROR = 400;
            int ERROR_NO_INFO = 500;
            void update(int event);
        }

        void retrieveShopInfo();

        void submitEditUpdate(String shopDescription);

        /**
         * Submit upload images to the server.
         * @return true if images are uploaded, false otherwise.
         */
        boolean submitUploadImages();

        void deleteShop();

        /**
         * Add at most two observer.
         * @param observer the observer.
         */
        void addObserver(Observer observer);

        Shop getShop();

        void addUploadImage(String imagePath);

        void removeUploadImages(List<String> imagePaths);

        void removeUploadImage(String imagePath);

        boolean containsUploadImage(String imagePath);

        List<String> getUploadImagePaths();

        /**
         * Submit a new image as the main image.
         */
        void submitNewMainImage(String imagePath);

        /**
         * Delete images from the server.
         * @param imageFiles the list of image files to delete.
         */
        void deleteImages(List<String> imageFiles);

        /**
         * Change the main image for the shop. The main image is the image that
         * is shown first when user enters the shop.
         * @param imageFile the image file of the main image.
         */
        void changeMainImage(String imageFile);

        /**
         *
         * @return the number of images that will be uploaded to the server.
         */
        int getNumUploadImages();

        /**
         *
         * @return maximum number of shop images that server can accept.
         */
        int getMaxShopImageUploads();
    }

    interface View extends BaseView<Presenter> {
        /**
         * Called to initialize the view, should
         * be called when subscribing to presenter.
         */
        void init();

        void setOwnerName(String ownerName);

        void setShopDeleteButtonText(@StringRes int resId);

        void clearShopDesTextFocus();

        void setShopDesTextFocus();

        void setShopDescriptionText(String description);

        void setShopDeleteButtonEnable(boolean enable);

        void setShopDeleteButtonSpin(boolean spin);

        void displayToast(@StringRes int resId);

        void displayDialog(DialogFragment dialogFragment);

        String getShopDescription();

        void displayUploadImage(String imagePath);

        void takePhoto();

        void pickUploadImage();

        void startImageScroll(List<String> imagePaths, String imagePath);

        void removeUploadImage(String imagePath);

        void setShopImages(List<String> imageLinks);

        void clearMainImageIndicator();

        void clearUploadMainImageIndicator();

        /**
         *
         * @return true if there is main image indicator on top view pager, showing currently
         * existing shop images, false otherwise.
         */
        boolean hasTopMainImageIndicator();

        /**
         * Get the image that is marked by the main image indicator in the upload images.
         * @return the upload image path marked as main image, null if none is marked.
         */
        String getUploadMainImagePath();

        /**
         * Get the list of image ids that are to be deleted by the user. The image
         * ids are the image ids that are obtained from the server which is used to construct the
         * image url.
         * @return this list of image ids that are marked for deletion.
         */
        List<String> getImagesToBeDeleted();

        /**
         * Get the image file of the new main image if it was set to an image that
         * already exists on the server.
         *
         * @return the image file of the new main image or null if the main
         *  image was not changed to an existing image.
         */
        String getChangedMainImageFile();

        /**
         * Starts the activity to zoom into the image by transitioning from a view.
         * @param data the data that reference the view to transition on.
         */
        void startZoomInImageActivity(Object data);

    }

    interface Presenter extends BasePresenter {
        void onShopImageClick(Object data);

        void onShopDescriptionFocusChange(boolean hasFocus);

        void onShopDescriptionClick();

        void onRootClick();

        void onShopDeleteButtonClick();

        void onPhotoButtonClick();

        void onImageButtonClick();

        /**
         *
         * @return true if shop is modified and changes needs to be submitted,
         *  false otherwise.
         */
        boolean onSubmitEdit();

        void onAddUploadImage(String imagePath);

        void onRemoveUploadImages(List<String> imagePaths);

        void onUploadImageClick(String imagePath);

        void onMainImageIndicatorSet();
    }
}
