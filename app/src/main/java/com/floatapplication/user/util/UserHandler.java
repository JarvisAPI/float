package com.floatapplication.user.util;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.VolleyError;
import com.floatapplication.R;
import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.messages.message_database.InboxDatabaseHelper;
import com.floatapplication.messages.message_database.MessageDatabaseHelper;
import com.floatapplication.messages.message_database.MessageUserDatabaseHelper;
import com.floatapplication.messages.server_connection.ChatConnectionService;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by william on 2018-05-08.
 * This class provide methods to handle token refreshes.
 */

public class UserHandler {
    private static final String TAG = UserHandler.class.getSimpleName();

    public interface ChatTokenCallback {
        void callback(boolean isSuccess);
    }

    public static void refreshToken(Context context, String response, UserSessionInterface userSession) {
        try {
            JSONObject jsonBody = new JSONObject(response);
            String access_token = jsonBody.getString("access_token");
            SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(context.getString(R.string.access_token), access_token);
            editor.apply();
            userSession.updateAccessToken(context, access_token);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void logout(Context context) {
        FloatApplication.getUserSessionInterface().logout();
        Log.d(TAG, "logging out islogged out:" + Boolean.toString(FloatApplication.getUserSessionInterface().isLoggedIn()));
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(context.getString(R.string.refresh_token));
        editor.remove(context.getString(R.string.access_token));
        editor.remove(context.getString(R.string.chat_token));
        (new InboxDatabaseHelper(context,FloatApplication.getUserSessionInterface().getUsername())).flushData();
        (new MessageDatabaseHelper(context,null)).flushData();
        (new MessageUserDatabaseHelper(context)).flushData();
        FloatApplication.getMessageFeatureHandlerInterface().flushNotificationToken();
        Intent i1 = new Intent(context, ChatConnectionService.class);
        context.stopService(i1);
        editor.apply();
    }

    public static void getChatToken(final Context context, final HypermediaInterface hypermedia,
                                    String access_token, @NonNull final ChatTokenCallback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + access_token);
        hypermedia.follow(Hypermedia.GET, HypermediaInterface.GET_CHAT_TOKEN, null, headers,
                new ServerCallback() {
                    @Override
                    public void onSuccessResponse(String response) {
                        callback.callback(storeChatToken(response));
                    }
                },
                new ServerErrorCallback() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        callback.callback(false);
                    }
                });
    }

    private static boolean storeChatToken(String response) {
        try {
            JSONObject jsonBody = new JSONObject(response);
            String chat_token = jsonBody.getString("chat_token");
            FloatApplication.getMessageFeatureHandlerInterface().addChatUser(FloatApplication.getUserSessionInterface().getUsername(),chat_token);
            FloatApplication.getMessageFeatureHandlerInterface().syncChatUserDatabase();
            return true;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}
