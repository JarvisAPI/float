package com.floatapplication.shop.fragment;


import android.content.Context;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.floatapplication.R;
import com.floatapplication.TestApplication;
import com.floatapplication.shop.activity.ShopCreationActivity;
import com.floatapplication.shop.fragment.ShopCreationFragment.OnCompleteListener;
import com.floatapplication.util.Util;
import com.floatapplication.util.Util.OnAddressObtainedListener;
import com.simplexorg.mapfragment.model.GeoPoint;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.fakes.RoboMenuItem;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by william on 2018-05-11.
 * Test shop creation.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ShopCreationFragmentUnitTest {
    private ShopCreationActivity activity;
    private ShopCreationFragment fragment;
    @Mock private OnClickListener clickListener;
    @Mock private Util mUtil;

    @Before
    public void setup() {
        initMocks(this);
        Util.setUtil(mUtil);
        activity = Robolectric.buildActivity(ShopCreationActivity.class)
                .create()
                .start()
                .resume()
                .get();
        fragment = new ShopCreationFragment();
        fragment.setListeners(clickListener, null);
        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
        transaction.add(fragment, null);
        transaction.commit();
    }

    @After
    public void cleanup() {
        Util.setUtil(null);
    }

    @Test
    public void testSetClickListener_registersClick() {
        View view = fragment.getView();
        assertNotNull(view);
        View button = view.findViewById(R.id.set_location_button);
        button.performClick();
        Mockito.verify(clickListener).onClick(button);
    }

    @Test
    public void testOnResumeFragment_setShopAddress() {
        String address = "address";
        doAnswer((InvocationOnMock invocation) -> {
            OnAddressObtainedListener listener = invocation.getArgumentAt(2, OnAddressObtainedListener.class);
            listener.onAddressObtained(address);
            return null;
        }).when(mUtil).getAddress(any(Context.class), any(GeoPoint.class), any(OnAddressObtainedListener.class));
        View view = fragment.getView();
        assertNotNull(view);
        TextView addressText = view.findViewById(R.id.address);
        assertEquals("", addressText.getText());
        fragment.setShopLocation(new GeoPoint(0, 0));
        fragment.onResumeFragment(null);
        assertEquals(address, addressText.getText());
    }

    @Test
    public void testMakeShopWithOutName_shouldFlagNameError_andNoCallbackInvoked() {
        OnCompleteListener testListener = mock(OnCompleteListener.class);
        fragment.setListeners(null, testListener);
        View view = fragment.getView();
        assertNotNull(view);
        RoboMenuItem menuItem = new RoboMenuItem();
        menuItem.setItemId(R.id.action_done);
        fragment.onOptionsItemSelected(menuItem);
        TextInputEditText shopNameEdit = view.findViewById(R.id.input_shop_name);
        assertNotNull(shopNameEdit.getError());
        verify(testListener, times(0)).onComplete();
    }

    @Test
    public void testMakeValidShop_shouldInvokeOnComplete() {
        OnCompleteListener testListener = mock(OnCompleteListener.class);
        fragment.setListeners(null, testListener);
        View view = fragment.getView();
        assertNotNull(view);

        TextInputEditText shopNameEdit = view.findViewById(R.id.input_shop_name);
        shopNameEdit.setText("Test Shop Name");
        fragment.setShopLocation(new GeoPoint(0, 0));

        RoboMenuItem menuItem = new RoboMenuItem();
        menuItem.setItemId(R.id.action_done);
        fragment.onOptionsItemSelected(menuItem);
        verify(testListener).onComplete();
    }
}
