package com.floatapplication.messages.holders;

import android.view.View;

import com.floatapplication.R;
import com.floatapplication.messages.chatkit.messages.MessageHolders;
import com.floatapplication.messages.models.CustomMessage;

public class CustomIncomingTextMessageViewHolder
        extends MessageHolders.IncomingTextMessageViewHolder<CustomMessage> {

    private View onlineIndicator;

    public CustomIncomingTextMessageViewHolder(View itemView) {
        super(itemView);
        onlineIndicator = itemView.findViewById(R.id.onlineIndicator);
    }

    @Override
    public void onBind(CustomMessage message) {
        super.onBind(message);
    }
}
