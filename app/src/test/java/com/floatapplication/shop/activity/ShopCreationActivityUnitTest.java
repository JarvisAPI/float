package com.floatapplication.shop.activity;

import android.content.Context;
import android.content.Intent;

import com.android.volley.VolleyError;
import com.google.android.gms.maps.model.LatLng;
import com.floatapplication.FloatApplication;
import com.floatapplication.TestApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.shop.fragment.ShopCreationFragment;
import com.floatapplication.shop.fragment.ShopSetLocationFragment;
import com.simplexorg.mapfragment.model.GeoPoint;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowToast;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by william on 2018-05-11.
 * Tests the activity that manipulates the creation fragments.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ShopCreationActivityUnitTest {
    private ShopCreationActivity activity;
    private ShopCreationFragment shopCreationFragment;
    private ShopSetLocationFragment shopSetLocationFragment;
    private HypermediaInterface hypermediaInterface;

    @Before
    public void setup() {
        hypermediaInterface = mock(HypermediaInterface.class);
        FloatApplication.setHypermediaInterface(hypermediaInterface);
        activity = Robolectric.buildActivity(ShopCreationActivity.class)
                .create()
                .start()
                .resume()
                .get();
        shopCreationFragment = mock(ShopCreationFragment.class);
        shopSetLocationFragment = mock(ShopSetLocationFragment.class);
        activity.mShopCreationFragment = shopCreationFragment;
        activity.mShopSetLocationFragment = shopSetLocationFragment;
        activity.setupFragments();
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    private void setHypermediaSuccess() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerCallback callback = invocation.getArgumentAt(3, ServerCallback.class);
                callback.onSuccessResponse("");
                return null;
            }
        }).when(hypermediaInterface).authFollow(eq(Hypermedia.POST), any(Context.class), anyString(), anyString(),
                any(ServerCallback.class), any(ServerErrorCallback.class));
    }

    private void setHypermediaFailure() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerErrorCallback errorCallback = invocation.getArgumentAt(4, ServerErrorCallback.class);
                errorCallback.onErrorResponse(new VolleyError("Test-Volley-Error"));
                return null;
            }
        }).when(hypermediaInterface).authFollow(eq(Hypermedia.POST), any(Context.class), anyString(), anyString(),
                any(ServerCallback.class), any(ServerErrorCallback.class));
    }

    @Test
    public void testMakeShopServerError_shouldSignalShopCreationFragment() {
        setHypermediaFailure();
        activity.onComplete();
        verify(shopCreationFragment).onShopCreationError();
    }

    @Test
    public void testMakeShopSuccess_shouldDisplayToast_andFinishActivity() {
        setHypermediaSuccess();
        activity.onComplete();
        assertEquals("Shop Created!", ShadowToast.getTextOfLatestToast());
        assertTrue(activity.isFinishing());
    }

    @Test
    public void makeShopSuccess_shouldSetShopLocation_asResult() {
        when(shopCreationFragment.getShopLocation()).thenReturn(new GeoPoint(0, 0));
        setHypermediaSuccess();
        activity.onComplete();
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent data = shadowActivity.getResultIntent();
        assertNotNull(data);
        LatLng shopLocation = data.getParcelableExtra("shop_location");
        assertNotNull(shopLocation);
    }
}
