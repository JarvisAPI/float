package com.floatapplication.notification;

/*
 * Created by jamescho on 2018-05-31.
 *
 */

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.floatapplication.messages.message_database.InboxDatabaseHelper;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.models.Dialog;
import com.floatapplication.messages.models.User;
import com.floatapplication.messages.server_connection.ChatConnectionService;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.floatapplication.R;
import com.floatapplication.messages.activity.DialogsActivity;
import com.floatapplication.messages.chatkit.utils.MessageParser;
import com.floatapplication.messages.message_database.ChatUserCache;
import com.floatapplication.messages.models.ChatUserCacheEntry;

import java.util.ArrayList;

public class MessageReceiver extends FirebaseMessagingService {
    public MessageReceiver() {
        super();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        final String receiver = remoteMessage.getData().get("to");
        final String barePeer = remoteMessage.getData().get("bare_peer");
        final String messageId = remoteMessage.getData().get("message_id");
        String message = remoteMessage.getData().get("body");
        User user = new User(barePeer);
        CustomMessage msg = new CustomMessage(messageId, user, remoteMessage.getSentTime(),message,null,ChatConnectionService.MessageType.RECEIVE);
        requestNotification(getApplicationContext(), receiver, msg);
        InboxDatabaseHelper inboxDB = new InboxDatabaseHelper(getApplicationContext(), receiver);
        inboxDB.addData(msg);
        inboxDB.close();
    }



    public static void requestNotification(Context context, String receiver,CustomMessage msg){
        ChatUserCache userCache = new ChatUserCache(context);
        userCache.getUser(msg.getUser().getId(), new ChatUserCache.ChatUserCacheCallback() {
            @Override
            public void successCallback(ChatUserCacheEntry entry) {
                showNotification(context, receiver, entry.getName(), msg.getText()); // title is barePeer

            }
            @Override
            public void errorCallback() {
                //DO nothing
            }
        });
        userCache.close();
    }

    static private void showNotification(Context context, String receiver, String title, String msg) {
        if(msg.startsWith(MessageParser.PHOTO_MESSAGE_PREFIX)){
            msg = "Photo";
        }
        int NOTIFY_ID = title.hashCode();
        String name = "float_messaging_feature";
        String id = "float_messaging"; // The user-visible name of the channel.
        String description = "my_package_first_channel"; // The user-visible description of the channel.

        Intent intent;
        PendingIntent pendingIntent;
        NotificationCompat.Builder builder;

        NotificationManager notifManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (notifManager == null) {
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = notifManager.getNotificationChannel(id);

            if (channel == null) {
                channel = new NotificationChannel(id, name, importance);
                channel.setDescription(description);
                channel.enableVibration(true);
                notifManager.createNotificationChannel(channel);
            }
            builder = new NotificationCompat.Builder(context, id);

            intent = new Intent(context, DialogsActivity.class);
            intent.putExtra("username", receiver);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            builder.setContentTitle(title)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setContentText(msg)
                    .setVibrate(new long[] { 1000, 1000, 1500 });
        } else {
            // for lower api versions
            builder = new NotificationCompat.Builder(context);

            intent = new Intent(context, DialogsActivity.class);
            intent.putExtra("username", receiver);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            builder.setContentTitle(title)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setContentText(msg)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setVibrate(new long[] { 1000, 1000, 1500 });
        }
        Notification notification = builder.build();
        notifManager.notify(NOTIFY_ID, notification);
    }
}
