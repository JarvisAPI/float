package com.floatapplication.shop.edit;

import android.support.annotation.NonNull;

import com.floatapplication.R;
import com.floatapplication.shop.edit.ShopAboutEditContract.Model.Observer;
import com.floatapplication.util.GlobalHandler;
import com.floatapplication.util.IFactory;
import com.floatapplication.util.Util;
import com.simplexorg.customviews.adapter.StatusMediator;
import com.simplexorg.customviews.dialog.CriticalConfirmDialog;

import java.util.List;


/**
 * Presenter for editing shop info.
 */

public class ShopAboutEditPresenter implements
        ShopAboutEditContract.Presenter,
        Observer,
        StatusMediator {
    private static final String TAG = ShopAboutEditPresenter.class.getSimpleName();

    private ShopAboutEditContract.View mView;
    private ShopAboutEditContract.Model mModel;

    void attach(@NonNull ShopAboutEditContract.View view,
                @NonNull ShopAboutEditContract.Model model) {
        mView = view;
        mView.setPresenter(this);
        mModel = model;
        mModel.addObserver(this);
    }

    @Override
    public void subscribe() {
        mView.init();
        mView.setShopDeleteButtonText(R.string.shop_delete);
        mView.setShopDeleteButtonEnable(false);
        mModel.retrieveShopInfo();
    }

    @Override
    public void unsubscribe() {

    }

    @Override
    public void onShopImageClick(Object data) {
        mView.clearShopDesTextFocus();
        mView.startZoomInImageActivity(data);
    }

    @Override
    public void onShopDescriptionFocusChange(boolean hasFocus) {
        if (hasFocus) {
            mView.setShopDesTextFocus();
        }
    }

    @Override
    public void onShopDescriptionClick() {
        mView.setShopDesTextFocus();
    }

    @Override
    public void onRootClick() {
        mView.clearShopDesTextFocus();
    }

    @Override
    public void onShopDeleteButtonClick() {
        CriticalConfirmDialog confirmDialog = IFactory.getInstance().create(CriticalConfirmDialog.class);
        confirmDialog.setOnConfirmListener(() -> {
            mView.setShopDeleteButtonSpin(true);
            mModel.deleteShop();
        });
        mView.displayDialog(confirmDialog);
    }

    @Override
    public void onPhotoButtonClick() {
        int numShopImages = mModel.getShop().getShopImageLinks().size();
        int numUploadImages = mModel.getNumUploadImages();
        if (numShopImages + numUploadImages < mModel.getMaxShopImageUploads()) {
            mView.takePhoto();
        } else {
            mView.displayToast(R.string.image_limit_reached);
        }
    }

    @Override
    public void onImageButtonClick() {
        int numShopImages = mModel.getShop().getShopImageLinks().size();
        int numUploadImages = mModel.getNumUploadImages();
        if (numShopImages + numUploadImages < mModel.getMaxShopImageUploads()) {
            mView.pickUploadImage();
        } else {
            mView.displayToast(R.string.image_limit_reached);
        }
    }

    @Override
    public boolean onSubmitEdit() {
        boolean modified = false;
        String newShopDescription = mView.getShopDescription();
        if (!mModel.getShop().getShopDescription().equals(newShopDescription)) {
            mModel.submitEditUpdate(newShopDescription);
            modified = true;
        }

        String uploadMainImagePath = mView.getUploadMainImagePath();
        if (uploadMainImagePath != null) {
            mModel.submitNewMainImage(uploadMainImagePath);
            mModel.removeUploadImage(uploadMainImagePath);
            modified = true;
        }

        if (mModel.submitUploadImages()) {
            modified = true;
        }

        List<String> imagesToBeDeleted = mView.getImagesToBeDeleted();
        if (!imagesToBeDeleted.isEmpty()) {
            mModel.deleteImages(imagesToBeDeleted);
            modified = true;
        }

        String changedMainImageFile = mView.getChangedMainImageFile();
        if (changedMainImageFile != null) {
            mModel.changeMainImage(changedMainImageFile);
            modified = true;
        }

        return modified;
    }

    @Override
    public void onAddUploadImage(String imagePath) {
        if (mModel.containsUploadImage(imagePath)) {
            mView.displayToast(R.string.image_already_added);
            return;
        }
        if (GlobalHandler.getInstance().fileIsImage(imagePath)) {
            mModel.addUploadImage(imagePath);
            mView.displayUploadImage(imagePath);
        }
    }

    @Override
    public void onRemoveUploadImages(List<String> imagePaths) {
        if (!imagePaths.isEmpty()) {
            mModel.removeUploadImages(imagePaths);
            for (String imagePath : imagePaths) {
                mView.removeUploadImage(imagePath);
            }
        }
    }

    @Override
    public void onUploadImageClick(String imagePath) {
        List<String> imagePaths = mModel.getUploadImagePaths();
        mView.startImageScroll(imagePaths, imagePath);
    }

    @Override
    public void update(int event) {
        switch (event) {
            case INFO_RECEIVED:
                mView.setShopDescriptionText(mModel.getShop().getShopDescription());
                mView.setOwnerName(Util.getInstance().capitalize(mModel.getShop().getOwnerNamePair().name));
                mView.setShopDeleteButtonEnable(true);
                mView.setShopImages(mModel.getShop().getShopImageLinks());
                break;
            case ERROR_NO_INFO:
                mView.setShopImages(null);
                break;
            case SHOP_DELETE_FAILED:
                mView.setShopDeleteButtonSpin(false);
                break;
            case ERROR:
                mView.displayToast(R.string.unable_to_update_shop);
                break;
        }
    }

    @Override
    public void onMainImageIndicatorSet() {
        mView.clearUploadMainImageIndicator();
    }

    @Override
    public boolean shouldTrackStatus(Observer observer) {
        return !mView.hasTopMainImageIndicator();
    }

    @Override
    public void requestToTrackStatus(Observer observer) {
        mView.clearMainImageIndicator();
    }
}
