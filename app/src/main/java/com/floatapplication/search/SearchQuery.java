package com.floatapplication.search;

import android.os.Parcel;
import android.os.Parcelable;

import com.simplexorg.mapfragment.model.GeoPoint;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class SearchQuery implements Parcelable {
    public static final String QUERY = "query";
    public static final String LOCATION = "location";
    public static final String DISTANCE = "distance";
    public static final String OFFSET = "offset";
    public static final String LIMIT = "limit";

    public final String query;
    public final GeoPoint location;
    public final int distance; // km

    public static final Creator<SearchQuery> CREATOR = new Creator<SearchQuery>() {
        @Override
        public SearchQuery createFromParcel(Parcel parcel) {
            return new SearchQuery(parcel);
        }

        @Override
        public SearchQuery[] newArray(int i) {
            return new SearchQuery[i];
        }
    };

    public SearchQuery(String query, GeoPoint location) {
        this.query = query;
        this.location = location;
        this.distance = 12;
    }

    public SearchQuery(String query, GeoPoint location, int distance) {
        this.query = query;
        this.location = location;
        this.distance = distance;
    }

    private SearchQuery(Parcel parcel) {
        query = parcel.readString();
        location = parcel.readParcelable(GeoPoint.class.getClassLoader());
        distance = parcel.readInt();
    }

    public SearchQuery modify(String query) {
        return new SearchQuery(query, location, distance);
    }

    Map<String, String> toQueryParams() {
        Map<String, String> queries = new HashMap<>();
        queries.put(QUERY, query);
        queries.put(LOCATION,
                String.format(Locale.CHINA, "[%f,%f]", location.longitude, location.latitude));
        queries.put(DISTANCE, String.valueOf(distance));
        return queries;
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(query);
        parcel.writeParcelable(location, flags);
        parcel.writeInt(distance);
    }
}
