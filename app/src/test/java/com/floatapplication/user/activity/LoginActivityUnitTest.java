package com.floatapplication.user.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.TextView;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.TestApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.map.activity.MapMainActivity;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.user.util.UserSessionInterface;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;
import org.robolectric.shadows.ShadowToast;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.robolectric.Shadows.shadowOf;

@SuppressWarnings("unchecked")
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class LoginActivityUnitTest {
    private LoginActivity activity;
    private HypermediaInterface hypermediaInterface;

    @Before
    public void setup() {
        hypermediaInterface = mock(HypermediaInterface.class);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerCallback callback = invocation.getArgumentAt(2, ServerCallback.class);
                callback.onSuccessResponse("");
                return null;
            }
        }).when(hypermediaInterface).follow(eq(Hypermedia.GET), anyString(), anyMap(), anyMap(),
                any(ServerCallback.class), any(ServerErrorCallback.class));
        FloatApplication.setHypermediaInterface(hypermediaInterface);
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }


    @Test
    public void testSignUpLinkClick_shouldGoToSignUp() {
        activity = Robolectric.buildActivity(LoginActivity.class)
                .create()
                .start()
                .resume()
                .get();
        activity.findViewById(R.id.link_sign_up).performClick();
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent intent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = shadowOf(intent);
        assertEquals(SignUpActivity.class, shadowIntent.getIntentClass());
    }

    @Test
    public void testSkipLoginClick_shouldGoToMapMain() {
        activity = Robolectric.buildActivity(LoginActivity.class)
                .create()
                .start()
                .resume()
                .get();
        activity.findViewById(R.id.link_skip_login).performClick();
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent intent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = shadowOf(intent);
        assertEquals(MapMainActivity.class, shadowIntent.getIntentClass());
    }

    @Test
    public void testLoginWithNoUsernameAndPassword_shouldFlagError() {
        activity = Robolectric.buildActivity(LoginActivity.class)
                .create()
                .start()
                .resume()
                .get();
        TextView usernameText = activity.findViewById(R.id.input_username);
        TextView passwordText = activity.findViewById(R.id.input_password);
        activity.findViewById(R.id.btn_login).performClick();
        assertNotNull(usernameText.getError());
        assertNotNull(passwordText.getError());
    }

    @Test
    public void testValidLogin_shouldStoreToken_setTokenForSession_andHeadToMap() throws Exception {
        String access_token = "test-access-jwt-token";
        String refresh_token = "test-refresh-token";
        final String response = new JSONObject()
                .put("access_token", access_token)
                .put("refresh_token", refresh_token)
                .toString();
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerCallback callback = invocation.getArgumentAt(3, ServerCallback.class);
                callback.onSuccessResponse(response);
                return null;
            }
        }).when(hypermediaInterface).follow(eq(Hypermedia.POST), anyString(), anyMap(), anyMap(), anyString(),
                any(ServerCallback.class), any(ServerErrorCallback.class));
        UserSessionInterface userSession = mock(UserSessionInterface.class);
        FloatApplication.setUserSessionInterface(userSession);
        activity = Robolectric.buildActivity(LoginActivity.class)
                .create()
                .start()
                .resume()
                .get();
        TextView usernameText = activity.findViewById(R.id.input_username);
        TextView passwordText = activity.findViewById(R.id.input_password);
        usernameText.setText("ValidUsername");
        passwordText.setText("ValidPassword");
        activity.findViewById(R.id.btn_login).performClick();
        // Test access_token and refresh_token
        Mockito.verify(userSession).setRefreshToken(refresh_token);
        Mockito.verify(userSession).updateAccessToken(any(Context.class), eq(access_token));
        Context context = RuntimeEnvironment.application;
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        String stored_access_token = sharedPref.getString(context.getString(R.string.access_token), null);
        String stored_refresh_token = sharedPref.getString(context.getString(R.string.refresh_token), null);
        assertEquals(access_token, stored_access_token);
        assertEquals(refresh_token, stored_refresh_token);
        // Test head to map
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent intent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = shadowOf(intent);
        assertEquals(MapMainActivity.class, shadowIntent.getIntentClass());
    }

    @Test
    public void testLoginWith_incorrectUsernameOrPassword_shouldDisplayToast() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerErrorCallback errorCallback = invocation.getArgumentAt(4, ServerErrorCallback.class);
                NetworkResponse networkResponse = new NetworkResponse(401, null, null, true);
                errorCallback.onErrorResponse(new VolleyError(networkResponse));
                return null;
            }
        }).when(hypermediaInterface).follow(eq(Hypermedia.POST), anyString(), anyMap(), anyMap(), anyString(),
                any(ServerCallback.class), any(ServerErrorCallback.class));
        activity = Robolectric.buildActivity(LoginActivity.class)
                .create()
                .start()
                .resume()
                .get();
        TextView usernameText = activity.findViewById(R.id.input_username);
        TextView passwordText = activity.findViewById(R.id.input_password);
        usernameText.setText("InvalidUsername");
        passwordText.setText("InvalidPassword");
        activity.findViewById(R.id.btn_login).performClick();
        assertEquals("Incorrect username or password", ShadowToast.getTextOfLatestToast());
    }
}
