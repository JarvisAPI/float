package com.floatapplication.product.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.product.models.ProductCreator;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.server.ServerErrorCode;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by william on 2018-05-14.
 * Handles product creation, that is handles the necessary product creation interactions with the server.
 */

public class ProductCreationHandler {
    private static final String TAG = ProductCreationHandler.class.getSimpleName();

    public interface ProgressListener {
        /**
         * @param progress between 0 and 100, or -1 if failure occurred.
         */
        void onProgressUpdate(int progress);
    }

    private Context mContext;
    private ProgressListener mListener;
    private ProductCreator mProductCreator;
    private String mShopId;
    private int mNumRequest; // Number of requests required to create product.
    private int mRequestCount; // Number of requests made.

    ProductCreationHandler(Context context, ProductCreator productCreator, String shopId) {
        mContext = context;
        mProductCreator = productCreator;
        mShopId = shopId;
        mRequestCount = 0;
        calculateRequestAmount();
    }

    private void calculateRequestAmount() {
        mNumRequest = 1; // 1 request for the make product json.
        if (mProductCreator.getProductMainImagePath() != null) {
            mNumRequest++; // 1 request for uploading main image.
        }
        mNumRequest += mProductCreator.getProductImagePaths().size();
    }

    public void setListener(ProgressListener listener) {
        mListener = listener;
    }

    void makeSendProductRequest() {
        Map<String, String> vMap = new HashMap<>();
        vMap.put("shop_id", mShopId);
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.POST, mContext, HypermediaInterface.MAKE_PRODUCT,
                vMap, obtainMakeProductBody(),
                new ServerCallback() {
                    @Override
                    public void onSuccessResponse(String response) {
                        if (mListener != null) {
                            mListener.onProgressUpdate(updateProgress());
                        }
                        checkAndSendProductImages();
                    }
                },
                new ServerErrorCallback() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e(TAG, "Error: " + error.getMessage());
                        error.printStackTrace();
                        if (error.networkResponse == null) {
                            Toast.makeText(mContext, "Unable to connect to server", Toast.LENGTH_SHORT).show();
                        } else {
                            try {
                                String message = new String(error.networkResponse.data);
                                Log.d(TAG, "Error message: "+ message);
                                JSONObject jsonError = new JSONObject(message);
                                String errType = jsonError.getString(ServerErrorCode.FIELD_TYPE);
                                switch (errType) {
                                    case ServerErrorCode.ERR_PRODUCT_LIMIT_REACHED:
                                        Toast.makeText(mContext, jsonError.getString(ServerErrorCode.FIELD_MESSAGE),
                                                Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                Toast.makeText(mContext, "Sorry unknown error occurred", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                        if (mListener != null) {
                            mListener.onProgressUpdate(-1);
                        }
                    }
                });
    }

    private String obtainMakeProductBody() {
        try {
            JSONObject productBody = new JSONObject()
                    .put("product_id", mProductCreator.getProductId())
                    .put("product_name", mProductCreator.getProductName())
                    .put("product_price", mProductCreator.getProductPrice())
                    .put("product_category", mProductCreator.getProductCategory())
                    .put("product_description", mProductCreator.getProductDescription());
            return new JSONObject()
                    .put("product", productBody)
                    .toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "{}";
    }

    private int updateProgress() {
        mRequestCount++;
        if (mRequestCount > mNumRequest) {
            mRequestCount = mNumRequest;
        }
        return Math.round((((float) mRequestCount) / mNumRequest) * 100);
    }


    private void checkAndSendProductImages() {
        sendProductMainImage();
        sendProductSubImages();
    }

    private void sendProductMainImage() {
        if (mProductCreator.getProductMainImagePath() != null) {
            Map<String, String> vMap = new HashMap<>();
            vMap.put("shop_id", mShopId);
            vMap.put("product_id", mProductCreator.getProductId());
            String uploadPath = FloatApplication.getHypermediaInterface().getPath(HypermediaInterface.UPLOAD_PRODUCT_MAIN_IMAGE,
                    vMap);
            sendProductImageRequest(uploadPath, mProductCreator.getProductMainImagePath());
        }
    }

    private void sendProductSubImages() {
        Map<String, String> vMap = new HashMap<>();
        vMap.put("shop_id", mShopId);
        vMap.put("product_id", mProductCreator.getProductId());
        String uploadPath = FloatApplication.getHypermediaInterface().getPath(HypermediaInterface.UPLOAD_PRODUCT_IMAGE,
                vMap);
        for (String imagePath : mProductCreator.getProductImagePaths()) {
            sendProductImageRequest(uploadPath, imagePath);
        }
    }

    /**
     * Sends a product image to the server.
     *
     * @param uploadPath the path to upload the image.
     * @param imagePath the path to the image on the device.
     */
    private void sendProductImageRequest(String uploadPath, String imagePath) {
        FloatApplication.getHypermediaInterface().uploadImage(mContext, uploadPath, imagePath,
                new ServerCallback() {
                    @Override
                    public void onSuccessResponse(String response) {
                        if (mListener != null) {
                            mListener.onProgressUpdate(updateProgress());
                        }
                    }
                },
                new ServerErrorCallback() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Toast.makeText(mContext, "Uploading Product Info Interrupted", Toast.LENGTH_SHORT).show();
                        mListener.onProgressUpdate(-1);
                    }
                });
    }
}
