package com.floatapplication.product.util;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.simplexorg.customviews.adapter.StatusMediator;
import com.simplexorg.customviews.adapter.StatusMediator.Observer;
import com.simplexorg.customviews.model.ImageDataHolder;
import com.simplexorg.customviews.util.VUtil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

/**
 *
 * Presents the product images displayed to the user in product edit.
 */

public class ProductEditImagePagerPresenter
        implements
        OnClickListener,
        OnLongClickListener,
        Observer {
    private static final String TAG = ProductEditImagePagerPresenter.class.getSimpleName();

    /**
     * Rep invariant: mImageUrls only have items added to it, no items in it is ever removed.
     */
    private List<String> mImageUrls;
    private List<String> mTransitionNames;
    private OnClickListener mOnClickListener;
    private int mMainImagePosition;
    private ImageView mMainImageMarker;
    private StatusMediator mProductEditStatusInformer;
    private Set<String> mImageDeleteSet;
    private Toast mLastToast;

    private static final int RANDOM_STRING_LEN =8;

    public ProductEditImagePagerPresenter() {
        mMainImagePosition = 0;
        mImageUrls = new ArrayList<>();
        mTransitionNames = new ArrayList<>();
        mImageDeleteSet = new HashSet<>();
    }

    public void present(ImageView imageView, ImageView deleteButton,
                        ImageView mainImageMarker, int position) {
        deleteButton.setVisibility(View.VISIBLE);
        deleteButton.setTag(R.id.tag0, position);
        deleteButton.setOnClickListener(this);
        if (mImageDeleteSet.contains(mImageUrls.get(position))) {
            deleteButton.setImageResource(R.drawable.baseline_restore_from_trash_black_24);
        }

        imageView.setTag(R.id.tag0, position);
        imageView.setTag(R.id.tag1, mainImageMarker);
        imageView.setTransitionName(mTransitionNames.get(position));

        if (position == mMainImagePosition) {
            setupMainImageMarker(mainImageMarker, position);
        }
        FloatApplication.getFactoryInterface()
                .getGlideRequest(imageView.getContext())
                .load(mImageUrls.get(position))
                .into(imageView);
        imageView.setOnClickListener(mOnClickListener);
        imageView.setOnLongClickListener(this);
    }

    void setStatusMediator(StatusMediator productEditStatusInformer) {
        mProductEditStatusInformer = productEditStatusInformer;
    }

    private void setupMainImageMarker(ImageView mainImageMarker, int position) {
        mMainImageMarker = mainImageMarker;
        mainImageMarker.setTag(R.id.tag0, mImageUrls.get(position));
        mainImageMarker.setVisibility(View.VISIBLE);
        mainImageMarker.setOnClickListener(this);
    }

    public void addImageUrl(String imageUrl, PagerAdapter adapter) {
        mImageUrls.add(imageUrl);
        mTransitionNames.add(VUtil.getInstance().genRandomString(RANDOM_STRING_LEN));
        adapter.notifyDataSetChanged();
    }

    public void addImageUrls(List<String> imageUrls, PagerAdapter adapter) {
        mImageUrls.addAll(imageUrls);
        for (int i = 0; i < imageUrls.size(); i++) {
            mTransitionNames.add(VUtil.getInstance().genRandomString(RANDOM_STRING_LEN));
        }
        adapter.notifyDataSetChanged();
    }

    public void setOnClickListener(OnClickListener listener) {
        mOnClickListener = listener;
    }

    public int size() {
        return mImageUrls.size();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.main_image_icon:
                showToolTip(view);
                break;
            case R.id.delete:
                setDeleteStatus(view);
        }
    }

    private void setDeleteStatus(View view) {
        ImageView deleteButton = (ImageView) view;
        int position = (int) deleteButton.getTag(R.id.tag0);
        String imageUrl = mImageUrls.get(position);
        if (mLastToast != null) {
            mLastToast.cancel();
        }

        Context context = mMainImageMarker.getContext();
        if (mImageDeleteSet.contains(imageUrl)) {
            mImageDeleteSet.remove(imageUrl);
            deleteButton.setImageResource(R.drawable.baseline_delete_black_24);

            mLastToast = Toast.makeText(context, context.getString(R.string.item_unmarked_for_deletion), Toast.LENGTH_SHORT);
            mLastToast.show();
        } else {
            mImageDeleteSet.add(imageUrl);
            deleteButton.setImageResource(R.drawable.baseline_restore_from_trash_black_24);

            mLastToast = Toast.makeText(context, context.getString(R.string.item_marked_for_deletion), Toast.LENGTH_SHORT);
            mLastToast.show();
        }
    }

    private void showToolTip(View view) {
        new SimpleTooltip.Builder(view.getContext())
                .anchorView(view)
                .text(view.getResources().getString(R.string.product_edit_main_image_icon_tooltip))
                .textColor(ContextCompat.getColor(view.getContext(), R.color.white))
                .gravity(Gravity.END)
                .animated(true)
                .transparentOverlay(false)
                .build()
                .show();
    }

    @Override
    public boolean onLongClick(View view) {
        int position = (int) view.getTag(R.id.tag0);
        if (position != mMainImagePosition) {
            mMainImageMarker.setVisibility(View.INVISIBLE);
            mMainImagePosition = position;
            setupMainImageMarker((ImageView) view.getTag(R.id.tag1), position);
            mProductEditStatusInformer.requestToTrackStatus(this);
            return true;
        }
        return false;
    }

    boolean hasMainImage() {
        return mMainImageMarker != null && mMainImageMarker.getVisibility() == View.VISIBLE;
    }

    /**
     * Gets all the product images to be deleted.
     */
    Set<String> getImageDeleteSet() {
        return mImageDeleteSet;
    }

    String getMainImageUrl() {
        if (mMainImageMarker != null && mMainImageMarker.getVisibility() == View.VISIBLE) {
            return (String) mMainImageMarker.getTag(R.id.tag0);
        }
        return null;
    }

    @Override
    public void update(int event) {
        if (event == Observer.CLEAR_STATUS) {
            mMainImagePosition = -1;
            if (mMainImageMarker != null) {
                mMainImageMarker.setVisibility(View.INVISIBLE);
            }
        }
    }

    public ArrayList<ImageDataHolder> getImageDataList() {
        ArrayList<ImageDataHolder> imageDataHolders = new ArrayList<>();
        for (int i = 0; i < mImageUrls.size(); i++) {
            imageDataHolders.add(new ImageDataHolder(mImageUrls.get(i), mTransitionNames.get(i)));
        }
        return imageDataHolders;
    }

    public String getTransitionName(int position) {
        return mTransitionNames.get(position);
    }
}
