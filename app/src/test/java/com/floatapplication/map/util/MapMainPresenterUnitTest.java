package com.floatapplication.map.util;

import android.graphics.drawable.Drawable;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.TestApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.user.util.UserSessionInterface;
import com.floatapplication.util.GlideRequest;
import com.floatapplication.util.GlideRequests;
import com.floatapplication.util.GlobalHandler;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by william on 2018-06-07.
 * Test able to get and display background image, profile image, etc.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class MapMainPresenterUnitTest {
    private MapMainPresenter presenter;
    @Mock private UserSessionInterface userSessionInterface;
    @Mock private HypermediaInterface hypermediaInterface;
    @Mock private GlobalHandler globalHandler;
    private GlideRequests glideRequests;
    private GlideRequest glideRequestLoad;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;

    private TextView headerText;
    private ImageView headerProfile;
    private RelativeLayout headerBackground;


    @SuppressWarnings("unchecked")
    @Before
    public void setup() {
        initMocks(this);
        glideRequests = mock(GlideRequests.class);
        glideRequestLoad = mock(GlideRequest.class);
        when(glideRequests.load(anyString())).thenReturn(glideRequestLoad);
        when(glideRequestLoad.into(any(SimpleTarget.class))).thenAnswer(new Answer<Object>() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                SimpleTarget<Drawable> simpleTarget = invocation.getArgumentAt(0, SimpleTarget.class);
                simpleTarget.onResourceReady(mock(Drawable.class), mock(Transition.class));
                return null;
            }
        });
        GlobalHandler.setGlobalHandler(globalHandler);
        presenter = new MapMainPresenter(glideRequests, userSessionInterface, hypermediaInterface);
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    private void present() {
        navigationView = mock(NavigationView.class);
        when(navigationView.getMenu()).thenReturn(mock(Menu.class));
        mockHeaderView();
        drawerLayout = mock(DrawerLayout.class);
        presenter.present(navigationView, drawerLayout);
    }

    private void mockHeaderView() {
        View headerView = mock(View.class);
        headerText = mock(TextView.class);
        headerBackground = mock(RelativeLayout.class);
        headerProfile = mock(ImageView.class);
        when(headerView.findViewById(R.id.nav_header_large)).thenReturn(headerText);
        when(headerView.findViewById(R.id.background)).thenReturn(headerBackground);
        when(headerView.findViewById(R.id.image_view)).thenReturn(headerProfile);
        when(navigationView.getHeaderView(0)).thenReturn(headerView);
    }

    @Test
    public void onMenuPressOpenDrawer() {
        present();
        presenter.onMenuClick();
        verify(drawerLayout).openDrawer(Gravity.START, true);
    }

    private String genStubUserProfileResponseFull() throws Exception {
        return new JSONObject()
                .put("profile_images", new JSONArray().put(new JSONObject().put("url", "url1")))
                .put("background_images", new JSONArray().put(new JSONObject().put("url", "url2")))
                .toString();
    }

    private String genStubUserProfileResponseEmpty() throws Exception {
        return new JSONObject()
                .put("profile_images", new JSONArray())
                .put("background_images", new JSONArray())
                .toString();
    }

    @Test
    public void userLoggedIn_shouldLoadTheirProfileImages() {
        String username = "user";
        when(userSessionInterface.isLoggedIn()).thenReturn(true);
        when(userSessionInterface.getUsername()).thenReturn(username);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerCallback callback = invocation.getArgumentAt(3, ServerCallback.class);
                callback.onSuccessResponse(genStubUserProfileResponseFull());
                return null;
            }
        }).when(hypermediaInterface).follow(eq(Hypermedia.GET), eq(HypermediaInterface.USER_PROFILE),
                anyMapOf(String.class, String.class), anyMapOf(String.class, String.class),
                any(ServerCallback.class), any(ServerErrorCallback.class));
        present();
        verify(headerText).setText(username);
        verify(glideRequestLoad).into(headerProfile);
        verify(headerBackground).setBackground(any(Drawable.class));
    }

    @Test
    public void userNotLoggedIn_shouldLoadDefaultImages() {
        when(userSessionInterface.isLoggedIn()).thenReturn(false);
        present();
        verify(headerText).setText(null);
        verify(headerProfile).setImageResource(R.drawable.default_user_profile_image);
        verify(headerBackground).setBackgroundResource(R.drawable.default_user_profile_background);
    }

    @Test
    public void userLoggedIn_butHasNoImages_shouldLoadDefaultImages() {
        String username = "user";
        when(userSessionInterface.isLoggedIn()).thenReturn(true);
        when(userSessionInterface.getUsername()).thenReturn(username);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerCallback callback = invocation.getArgumentAt(3, ServerCallback.class);
                callback.onSuccessResponse(genStubUserProfileResponseEmpty());
                return null;
            }
        }).when(hypermediaInterface).follow(eq(Hypermedia.GET), eq(HypermediaInterface.USER_PROFILE),
                anyMapOf(String.class, String.class), anyMapOf(String.class, String.class),
                any(ServerCallback.class), any(ServerErrorCallback.class));
        present();
        verify(headerText).setText(username);
        verify(headerProfile).setImageResource(R.drawable.default_user_profile_image);
        verify(headerBackground).setBackgroundResource(R.drawable.default_user_profile_background);
    }
}
