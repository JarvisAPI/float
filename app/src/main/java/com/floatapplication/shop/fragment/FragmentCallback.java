package com.floatapplication.shop.fragment;

import android.os.Bundle;

/**
 * Created by william on 2018-05-10.
 * Fragment callback functions.
 */

public interface FragmentCallback {
    void onResumeFragment(Bundle data);
}
