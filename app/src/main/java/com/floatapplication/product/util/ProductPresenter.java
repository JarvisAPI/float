package com.floatapplication.product.util;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayout;
import android.transition.Fade;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.product.activity.ProductActivity;
import com.floatapplication.product.adapter.ProductImagePagerAdapter;
import com.floatapplication.product.models.Product;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.image.activity.CustomZoomViewPagerActivity;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.util.IFactory;
import com.floatapplication.util.Util;
import com.pchmn.materialchips.ChipView;
import com.simplexorg.customviews.model.ImageDataHolder;
import com.simplexorg.customviews.util.VUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by william on 2018-06-13.
 * Presents an individual product.
 */

public class ProductPresenter
        implements
        RequestListener<Drawable>,
        ServerCallback,
        ServerErrorCallback,
        OnClickListener {
    private static final String TAG = ProductPresenter.class.getSimpleName();

    public interface OnUtilBarIconClickListener {
        void onMessageIconClick();

        void onShopHomeIconClick(Shop shop);
    }

    private ProductActivity mActivity;
    private Product mProduct;
    private Intent mData;
    private ProductImagePagerAdapter mAdapter;
    private ViewPager mViewPager;
    private TextView mProductName;
    private TextView mProductPrice;
    private TextView mProductDescription;
    private GridLayout mCategory;
    private TabLayout mTabDots;
    private OnUtilBarIconClickListener mListener;

    private boolean mLoadingShopInfo = false;

    public ProductPresenter(ProductActivity activity, Product product, Intent data) {
        mActivity = activity;
        mProduct = product;
        mData = data;
    }

    /**
     * Presents the product to the user. Should be called in onCreate.
     *
     * @param viewPager          the view pager for showing the product images.
     * @param adapter            the adapter for view pager to use.
     * @param productName        the product name view to display.
     * @param productPrice       the product price view to display.
     * @param productDescription the product description view to display.
     * @param category           the product category view to display.
     * @param tabDots            the user friendly dots to show when scrolling through the view pager.
     */
    public void present(ViewPager viewPager, ProductImagePagerAdapter adapter,
                        TextView productName, TextView productPrice, TextView productDescription,
                        GridLayout category, TabLayout tabDots,
                        ImageView shopHomeIcon, ImageView messageIcon) {
        mViewPager = viewPager;
        mAdapter = adapter;
        mProductName = productName;
        mProductPrice = productPrice;
        mProductDescription = productDescription;
        mCategory = category;
        mTabDots = tabDots;
        shopHomeIcon.setOnClickListener(this);
        messageIcon.setOnClickListener(this);

        mActivity.supportPostponeEnterTransition();
        setupViewPager();
        mAdapter.setListener(this);
        setupBasicProductInfo();

        Fade fade = new Fade();
        fade.excludeTarget(android.R.id.statusBarBackground, true);
        fade.excludeTarget(android.R.id.navigationBarBackground, true);

        mActivity.getWindow().setEnterTransition(fade);
        mActivity.getWindow().setExitTransition(fade);

        getProductInfo();

        mAdapter.setOnImageClickListener((View view) -> {
            Intent intent = new Intent(mActivity, CustomZoomViewPagerActivity.class);

            ArrayList<ImageDataHolder> imageDataList = mAdapter.getImageDataList();

            intent.putParcelableArrayListExtra(VUtil.EXTRA_IMAGE_DATA, imageDataList);
            if (imageDataList.size() == 1) {
                // Special case where only one image exist then it might possibly be a null image.
                if (Util.getInstance().isImageUrlNullLink(imageDataList.get(0).imageUri)) {
                    ImageDataHolder dataHolder = imageDataList.get(0);
                    imageDataList.set(0, new ImageDataHolder(Util.PLACEHOLDER_PRODUCT, dataHolder.transitionName));
                }
            }
            intent.putExtra(VUtil.EXTRA_TRANSITION_NAME, mAdapter.getTransitionName(mViewPager.getCurrentItem()));

            if (view.getTransitionName() == null) {
                // Sanity check to prevent null pointer exception, might not be executed if functioning properly.
                final int RANDOM_STRING_LEN = 8;
                view.setTransitionName(VUtil.getInstance().genRandomString(RANDOM_STRING_LEN));
            }
            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity,
                    view, view.getTransitionName());
            mActivity.startActivity(intent, optionsCompat.toBundle());
        });
    }

    @Override
    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
        mActivity.supportStartPostponedEnterTransition();
        return false;
    }

    @Override
    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
        mActivity.supportStartPostponedEnterTransition();
        return false;
    }


    private void setupBasicProductInfo() {
        mAdapter.addImageUrl(Util.getInstance().constructImageUrl(mProduct.getProductMainImageFile()));
        mProductName.setText(mProduct.getProductName());
        mProductPrice.setText(mProduct.getProductPrice());
    }

    private void setupViewPager() {
        String transitionName = mData.getStringExtra(Util.EXTRA_TRANSITION);
        mAdapter.setTransitionName(transitionName);
        mViewPager.setAdapter(mAdapter);
        mTabDots.setupWithViewPager(mViewPager);
    }

    private void getProductInfo() {
        Map<String, String> vMap = new HashMap<>();
        vMap.put("shop_id", mProduct.getProductShopId());
        vMap.put("product_id", mProduct.getProductId());
        FloatApplication.getHypermediaInterface()
                .follow(Hypermedia.GET, HypermediaInterface.PRODUCT_DETAILS, vMap, null,
                        this, this);
    }

    @Override
    public void onSuccessResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            mProduct.augmentProductInfo(jsonObject.getJSONObject("product"));
            mAdapter.addImageUrls(obtainProductImageUrlList());
            mProductDescription.setText(mProduct.getProductDescription());
            addCategory();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void addCategory() {
        ChipView chipView = new ChipView(mCategory.getContext());
        chipView.setLabel(mProduct.getProductCategory());
        chipView.setChipBackgroundColor(ContextCompat.getColor(mCategory.getContext(), R.color.colorPrimaryLight));
        chipView.setLabelColor(ContextCompat.getColor(mCategory.getContext(), R.color.white));
        mCategory.addView(chipView);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
    }

    private List<String> obtainProductImageUrlList() {
        List<String> urls = new ArrayList<>();
        for (String imageFile : mProduct.getProductImageFiles()) {
            urls.add(Util.getInstance().constructImageUrl(imageFile));
        }
        return urls;
    }

    public void onBackPressed() {
        if (mViewPager.getCurrentItem() != 0) {
            mViewPager.setCurrentItem(0, false);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.message_icon:
                if (mListener != null) {
                    mListener.onMessageIconClick();
                }
                break;
            case R.id.shop_home:
                if (mListener != null) {
                    if (!mLoadingShopInfo) {
                        mLoadingShopInfo = true;
                        Map<String, String> vMap = IFactory.getInstance().createMap();
                        vMap.put(Shop.ID, mProduct.getProductShopId());
                        FloatApplication.getHypermediaInterface()
                                .follow(Hypermedia.GET, Hypermedia.SHOP_INFO, vMap, null,
                                        (String response) -> {
                                            try {
                                                Shop shop = Shop.createShop(new JSONObject(response));
                                                mListener.onShopHomeIconClick(shop);
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            mLoadingShopInfo = false;
                                        },
                                        (VolleyError error) -> {
                                            mLoadingShopInfo = false;
                                        });
                    }
                }
        }
    }

    public void setOnUtilBarItemClickListener(OnUtilBarIconClickListener listener) {
        mListener = listener;
    }
}
