package com.floatapplication.messages.server_connection;

import android.util.Log;

import com.floatapplication.FloatApplication;
import com.floatapplication.messages.message_database.MessageDatabaseHelper;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.server.ServerContact;

import org.jivesoftware.smackx.forward.packet.Forwarded;
import org.jivesoftware.smackx.mam.MamManager;
import org.jxmpp.jid.impl.JidCreate;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * The that fetches archive messages from the XMPP server
 */

public class ArchiveMessageManager {
    private final String TAG = "ArchiveMessageManager";
    private MamManager mMamManager;
    private String mUsername;
    private MessageDatabaseHelper mMessageDatabase;
    private final String SERVER_BIG_INTEGER_PLACEHOLDER = "000";

    public ArchiveMessageManager(MamManager mamManager, String jid){
        mMamManager = mamManager;
        mMessageDatabase = FloatApplication.getFactoryInterface().getMessageDatabase(jid);
        mUsername = jid;
    }

    /**
     * Retrives messages from XMPP server
     * @param message Message to get after
     * @param numMessages maximum number of messages to fetch from the server
     */
    public ArrayList<CustomMessage> getArchiveMessagesAfter(CustomMessage message, int numMessages){
        ArrayList<CustomMessage> archiveMessageList = new ArrayList<>();
        try {
            Calendar today = Calendar.getInstance();
            today.add(Calendar.DATE,1);
            Date endTime = today.getTime();

            today.setTime(message.getCreatedAt());
            Date startTime = today.getTime();
            MamManager.MamQueryResult result = mMamManager.queryArchive(numMessages,  startTime, endTime,
                    JidCreate.from(message.getUser().getId() + "@" + ServerContact.HOST), null);
            Log.d(TAG, result.mamFin.toXML().toString());

            for(Forwarded fmessage:result.forwardedMessages){
                Log.d(TAG, fmessage.toXML().toString());
                archiveMessageList.add(new CustomMessage(fmessage, mUsername));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMessageDatabase.addData(archiveMessageList);
        return archiveMessageList;
    }

    /**
     * Retrives messages from XMPP server
     * @param jid jid of conversation opponent
     * @param numMessages maximum number of messages to fetch from the server
     */
    public ArrayList<CustomMessage> getArchivePageAfter(String jid, String dateToGetAfter, int numMessages){
        ArrayList<CustomMessage> archiveMessageList = new ArrayList<>();
        try {
            MamManager.MamQueryResult result = mMamManager.pageAfter(JidCreate.from(jid + "@" + ServerContact.HOST),dateToGetAfter,numMessages);
            Log.d(TAG, result.mamFin.toXML().toString());

            for(Forwarded fmessage:result.forwardedMessages){
                Log.d(TAG, fmessage.toXML().toString());
                archiveMessageList.add(new CustomMessage(fmessage, mUsername));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMessageDatabase.addData(archiveMessageList);
        return archiveMessageList;
    }

    /**
     * Retrives messages from XMPP server
     * @param message last message stored on the device
     * @param numMessages maximum number of messages to fetch from the server
     */
    public ArrayList<CustomMessage> getArchivePageBefore(CustomMessage message, int numMessages){
        if(message.getArchiveId() == null)
            return getArchivePageBefore( message.getUser().getId(), Long.toString(message.getCreatedAt().getTime()) + SERVER_BIG_INTEGER_PLACEHOLDER, numMessages);
        else
            return getArchivePageBefore( message.getUser().getId(), message.getArchiveId(), numMessages);
    }

    /**
     * Retrives messages from XMPP server
     * @param jid jid of conversation opponent
     * @param archiveId archive id to start fetching messages
     * @param numMessages maximum number of messages to fetch from the server
     */
    private ArrayList<CustomMessage> getArchivePageBefore(String jid, String archiveId, int numMessages){
        ArrayList<CustomMessage> archiveMessageList = new ArrayList<>();
        try {
            Log.d(TAG, "fetching archive id: " + archiveId);
            MamManager.MamQueryResult result = mMamManager.pageBefore(JidCreate.from(jid + "@" + ServerContact.HOST), archiveId, numMessages);
            Log.d(TAG, result.mamFin.toXML().toString());

            for(Forwarded fmessage:result.forwardedMessages){
                Log.d(TAG, fmessage.toXML().toString());
                archiveMessageList.add(new CustomMessage(fmessage, mUsername));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMessageDatabase.addData(archiveMessageList);
        return archiveMessageList;
    }

}
