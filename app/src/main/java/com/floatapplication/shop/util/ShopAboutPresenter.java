package com.floatapplication.shop.util;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.R;
import com.floatapplication.image.activity.CustomZoomViewPagerActivity;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.util.Util;
import com.simplexorg.customviews.fragment.ZoomViewPagerFragment;

import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ShopAboutPresenter implements OnClickListener {
    public interface OnShopAboutButtonClickListener {
        void onOwnerIconClicked();
        void onMessageIconClicked();
        void onGotoShopLocationIconClicked();
    }
    private OnShopAboutButtonClickListener mListener;

    public void present(ZoomViewPagerFragment zoomViewPagerFragment,
                        ImageView ownerIcon, TextView ownerName,
                        ImageView gotoShopIcon, ImageView messageIcon,
                        TextView shopDescription, Shop shop) {

        zoomViewPagerFragment.setTransitionClass(CustomZoomViewPagerActivity.class);
        zoomViewPagerFragment.setImageLoader(Util.getInstance().getImageLoader(null));

        ownerIcon.setOnClickListener(this);
        gotoShopIcon.setOnClickListener(this);
        messageIcon.setOnClickListener(this);
        shopDescription.setText(shop.getShopDescription());
        if (!shop.getShopUsernames().isEmpty()) {
            ownerName.setText(StringUtils.capitalize(shop.getOwnerNamePair().name));
        }

        List<String> shopImageLinks = new ArrayList<>();
        for (String serverImageFile : shop.getShopImages()) {
            shopImageLinks.add(Util.getInstance().constructImageUrl(serverImageFile));
        }

        if (shopImageLinks.isEmpty()) {
            shopImageLinks.add(Util.PLACEHOLDER_SHOP);
        }

        zoomViewPagerFragment.setImagesUris(shopImageLinks);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.owner_icon:
                mListener.onOwnerIconClicked();
                break;
            case R.id.message_icon:
                mListener.onMessageIconClicked();
                break;
            case R.id.go_to_location_icon:
                mListener.onGotoShopLocationIconClicked();
        }
    }

    public void setOnShopAboutButtonClickListener(OnShopAboutButtonClickListener listener) {
        mListener = listener;
    }
}
