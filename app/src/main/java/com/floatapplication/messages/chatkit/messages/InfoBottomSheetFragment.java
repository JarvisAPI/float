package com.floatapplication.messages.chatkit.messages;

/*
 * Created by jamescho on 2018-06-04.
 *
 */

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.floatapplication.R;
import com.floatapplication.messages.models.User;

public class InfoBottomSheetFragment extends BottomSheetDialogFragment {
    public interface OnTabSelectedListener{
        void onGoToShopSelected();
        void onViewUserProfileSelected();
    }

    private View mRootView;
    private OnTabSelectedListener mOnTabSelectedListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = getActivity();
        if (activity instanceof OnTabSelectedListener) {
            mOnTabSelectedListener = (OnTabSelectedListener) activity;
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        User.Type type = null;
        if(getArguments() != null) {
            type = User.Type.valueOf(getArguments().getString(getString(R.string.chat_user_type)));
        }
        if(type != null && type.equals(User.Type.shop)) {
            mRootView = inflater.inflate(R.layout.fragment_chat_bottom_sheet_shop, container, false);
            setShopClickListeners();
        }
        else{
            mRootView = inflater.inflate(R.layout.fragment_chat_bottom_sheet_user, container, false);
            setUserClickListeners();
        }
        return mRootView;
    }

    private void setShopClickListeners() {
        final InfoBottomSheetFragment frag = this;
        mRootView.findViewById(R.id.go_to_shop_tab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnTabSelectedListener != null) {
                    mOnTabSelectedListener.onGoToShopSelected();
                }
                frag.dismissAllowingStateLoss();
            }
        });
        mRootView.findViewById(R.id.cancel_tab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag.dismissAllowingStateLoss();
            }
        });
    }

    private void setUserClickListeners() {
        final InfoBottomSheetFragment frag = this;
        mRootView.findViewById(R.id.view_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mOnTabSelectedListener != null) {
                    mOnTabSelectedListener.onViewUserProfileSelected();
                }
                frag.dismissAllowingStateLoss();
            }
        });
        mRootView.findViewById(R.id.cancel_tab).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frag.dismissAllowingStateLoss();
            }
        });
    }
}
