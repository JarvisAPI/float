package com.floatapplication.user.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by william on 2018-12-22.
 * Stores some user info and some keys.
 */

public class User {
    // Name is the display name of the user.
    public static final String NAME = "name";
    // User name is the unique name used to identify the user.
    public static final String USERNAME = "username";
    public static final String PROFILE_IMAGES = "profile_images";
    public static final String BACKGROUND_IMAGES = "background_images";

    public static class NamePair implements Parcelable {
        public final String username;
        public final String name;

        public static final Creator<NamePair> CREATOR = new Creator<NamePair>() {
            @Override
            public NamePair createFromParcel(Parcel parcel) {
                return new NamePair(parcel);
            }

            @Override
            public NamePair[] newArray(int i) {
                return new NamePair[i];
            }
        };

        private NamePair(Parcel parcel) {
            username = parcel.readString();
            name = parcel.readString();
        }

        public NamePair(String username, String name) {
            this.username = username;
            this.name = name;
        }

        @Override
        public int describeContents() {
            return hashCode();
        }

        @Override
        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(username);
            parcel.writeString(name);
        }
    }
}
