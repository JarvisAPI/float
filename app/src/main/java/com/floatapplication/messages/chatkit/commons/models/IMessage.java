package com.floatapplication.messages.chatkit.commons.models;

/**
 * Created by jamescho on 2018-04-25.
 */

import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.server_connection.ChatConnectionService;

import java.util.Date;

/**
 * For implementing by real message model
 */
public interface IMessage {

    /**
     * Returns message identifier
     *
     * @return the message id
     */
    String getId();

    /**
     * Returns message text
     *
     * @return the message text
     */
    String getText();

    /**
     * Returns message author. See the {@link IUser} for more details
     *
     * @return the message author
     */
    IUser getUser();

    /**
     * Returns message creation date
     *
     * @return the message creation date
     */
    Date getCreatedAt();

    boolean getToDisplayTime();

    void setToDisplayTime(boolean todDisplay);

    void setImage(CustomMessage.Image image);

    ChatConnectionService.MessageType getMessageType();

    String getImageUrl();
}
