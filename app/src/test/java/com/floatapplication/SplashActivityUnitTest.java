package com.floatapplication;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.android.volley.VolleyError;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.map.activity.MapMainActivity;
import com.floatapplication.messages.server_connection.MessageFeatureHandlerInterface;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.user.activity.LoginActivity;
import com.floatapplication.user.util.UserSessionInterface;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by william on 2018-05-08.
 * Tests login.
 */
@SuppressWarnings("unchecked")
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class SplashActivityUnitTest {
    private SplashActivity activity;
    private HypermediaInterface hypermediaInterface;
    private UserSessionInterface userSessionInterface;

    @Before
    public void setup() throws Exception {
        userSessionInterface = mock(UserSessionInterface.class);
        hypermediaInterface = mock(HypermediaInterface.class);
        FloatApplication.setHypermediaInterface(hypermediaInterface);
        FloatApplication.setUserSessionInterface(userSessionInterface);
        FloatApplication.setMessageFeatureHandlerInterface(mock(MessageFeatureHandlerInterface.class));
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    private void setInitSuccess() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerCallback callback = invocation.getArgumentAt(1, ServerCallback.class);
                callback.onSuccessResponse(null);
                return null;
            }
        }).when(hypermediaInterface).init(anyMapOf(String.class, String.class), any(ServerCallback.class), any(ServerErrorCallback.class));
    }

    @Test
    public void testStartMainActivity_withToken_shouldRefreshTokens() throws Exception {
        Context context = RuntimeEnvironment.application;
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(context.getString(R.string.access_token), "jwt-token");
        editor.putString(context.getString(R.string.refresh_token), "random-string");
        editor.commit();

        String expectedAccessToken = "new-jwt-token";
        final String response = new JSONObject()
                .put("access_token", expectedAccessToken)
                .toString();

        setInitSuccess();
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerCallback callback = invocation.getArgumentAt(3, ServerCallback.class);
                callback.onSuccessResponse(response);
                return null;
            }
        }).when(hypermediaInterface).follow(eq(HypermediaInterface.POST), anyString(), anyMap(), anyMap(), anyString(),
                any(ServerCallback.class), any(ServerErrorCallback.class));

        activity = Robolectric.buildActivity(SplashActivity.class)
                .create()
                .start()
                .resume()
                .get();
        verify(userSessionInterface).updateAccessToken(any(Context.class), eq(expectedAccessToken));
        String new_access_token = sharedPref.getString(context.getString(R.string.access_token), null);
        assertEquals(expectedAccessToken, new_access_token);
    }

    @Test
    public void testInitHypermediaFailed_shouldFinish() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerErrorCallback errorCallback = invocation.getArgumentAt(2, ServerErrorCallback.class);
                errorCallback.onErrorResponse(new VolleyError("testing volley error"));
                return null;
            }
        }).when(hypermediaInterface).init(anyMapOf(String.class, String.class), any(ServerCallback.class), any(ServerErrorCallback.class));
        activity = Robolectric.buildActivity(SplashActivity.class)
                .create()
                .start()
                .resume()
                .get();
        assertTrue(activity.isFinishing());
    }

    @Test
    public void testStartMainActivity_withNoToken_andNoSkipLogin_shouldGoToLogin() {
        setInitSuccess();
        activity = Robolectric.buildActivity(SplashActivity.class)
                .create()
                .start()
                .resume()
                .get();
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent intent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = shadowOf(intent);
        assertEquals(LoginActivity.class, shadowIntent.getIntentClass());
    }

    @Test
    public void testStartMainActivity_withNoToken_withSkipLogin_shouldGoToMapMain() {
        Context context = RuntimeEnvironment.application;
        SharedPreferences sharedPref = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(context.getString(R.string.skip_login), true);
        editor.commit();
        setInitSuccess();
        activity = Robolectric.buildActivity(SplashActivity.class)
                .create()
                .start()
                .resume()
                .get();
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent intent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = shadowOf(intent);
        assertEquals(MapMainActivity.class, shadowIntent.getIntentClass());
    }
}
