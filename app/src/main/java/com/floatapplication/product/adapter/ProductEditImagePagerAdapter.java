package com.floatapplication.product.adapter;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.floatapplication.R;
import com.floatapplication.product.util.ProductEditImagePagerPresenter;

import java.util.List;

/**
 * Created by william on 2018-06-14.
 * Image pager that allows editing of images.
 */

public class ProductEditImagePagerAdapter extends PagerAdapter {
    private ProductEditImagePagerPresenter mPresenter;

    public ProductEditImagePagerAdapter() {
        mPresenter = new ProductEditImagePagerPresenter();
    }

    public void addImageUrl(String imageUrl) {
        mPresenter.addImageUrl(imageUrl, this);
    }

    public void addImageUrls(List<String> imageUrls) {
        mPresenter.addImageUrls(imageUrls, this);
    }

    public void setOnClickListener(OnClickListener listener) {
        mPresenter.setOnClickListener(listener);
    }

    public ProductEditImagePagerPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public int getCount() {
        return mPresenter.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = LayoutInflater.from(container.getContext()).inflate(R.layout.item_image, container, false);
        ImageView deleteButton = itemView.findViewById(R.id.delete);
        ImageView imageView = itemView.findViewById(R.id.image);
        ImageView mainImageMarker = itemView.findViewById(R.id.main_image_icon);
        mPresenter.present(imageView, deleteButton, mainImageMarker, position);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
