package com.floatapplication.product.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

/**
 * Created by william on 2017/12/10.
 * This class exposes a limited interface for modification, this class
 * is used mainly for storing an product obtained from the server to
 * be displayed on the ui.
 */

public class Product extends ProductBase {
    private static final String TAG = Product.class.getSimpleName();
    public static final String[] PRODUCT_CATEGORIES = {"clothing", "electronics", "phone", "food"};

    // WeakerAccess
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel parcel) {
            return new Product(parcel);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    private Product(Parcel parcel) {
        super(parcel);
    }

    protected Product() {
        mProductMainImageFile = "null";
        mProductCategory = NONE_CATEGORY;
    }

    /**
     * Create product with the most basic information.
     *
     * @param productJson contains the basic information of the product such as name, price, main image and id.
     * @return the newly created product that only contains the basic information.
     */
    public static Product createBasicProduct(JSONObject productJson) {
        Product product = new Product();
        try {
            product.mProductId = productJson.getString(ID);
            product.mProductName = productJson.getString(NAME);
            String productPrice = productJson.getString(PRICE);
            try {
                productPrice = String.format(Locale.CANADA, "%.2f", Float.valueOf(productPrice));
            } catch (Exception e) {
                productPrice = "0";
                e.printStackTrace();
            }
            product.mProductPrice = productPrice;
            if (productJson.has(MAIN_IMAGE)) {
                product.mProductMainImageFile = productJson.getString(MAIN_IMAGE);
            }
            if (productJson.has(SHOP_ID)) {
                product.mProductShopId = productJson.getString(SHOP_ID);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return product;
    }

    /**
     * Augment product with more info.
     *
     * @param productInfoJson contains more detailed info on the product such as category,
     *                        other images and description.
     */
    public void augmentProductInfo(JSONObject productInfoJson) {
        try {
            if (productInfoJson.has(IMAGES)) {
                JSONArray imageFileArr = productInfoJson.getJSONArray(IMAGES);
                for (int i = 0; i < imageFileArr.length(); i++) {
                    String imageFile = imageFileArr.getString(i);
                    if (!mProductImageFiles.contains(imageFile)) {
                        mProductImageFiles.add(imageFile);
                    }
                }
            }
            mProductDescription = productInfoJson.getString(DESCRIPTION);
            if (productInfoJson.has(CATEGORY)) {
                mProductCategory = productInfoJson.getString(CATEGORY);

                if (!isValidCategory(mProductCategory) ||
                        "null".equals(mProductCategory)) {
                    mProductCategory = NONE_CATEGORY;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean isValidCategory(String category) {
        for (String validCategory : PRODUCT_CATEGORIES) {
            if (validCategory.equals(category)) {
                return true;
            }
        }
        return false;
    }

    public void setProductShopId(String shopId) {
        mProductShopId = shopId;
    }
}
