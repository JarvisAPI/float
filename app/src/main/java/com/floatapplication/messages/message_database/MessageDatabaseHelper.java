package com.floatapplication.messages.message_database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.models.User;
import com.floatapplication.messages.server_connection.ChatConnectionService;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jamescho on 2018-01-31.
 *
 */
public class MessageDatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "MessageDatabaseHelper";
    private static final String TABLE_NAME = "messages";
    private static final String COL_MESSAGE_ID = "message_id";
    private static final String COL_CONTACT_ID = "contact_id";
    private static final String COL_TIME_STAMP = "time_stamp"; // Unix time stamp in milliseconds
    private static final String COL_MESSAGE_TYPE = "message_type";
    private static final String COL_MESSAGE_ARCHIVE_ID = "message_archive_id";
    private static final String COL_TEXT = "text";
    private static final String COL_RECEIVER_ID = "receiver";
    private static final int COL_MESSAGE_ID_NUM = 0;
    private static final int COL_CONTACT_ID_NUM = 1;
    private static final int COL_TIME_STAMP_NUM = 2;
    private static final int COL_MESSAGE_TYPE_NUM = 3;
    private static final int COL_TEXT_NUM = 4;
    private static final int COL_MESSAGE_ARCHIVE_ID_NUM = 5;
    private static final int COL_RECEIVER_ID_NUM = 6;
    private final String NULL_ARCHIVE_ID = "-1";

    private final String inboxJid;


    public MessageDatabaseHelper(Context context, String clientJid) {
        super(context, TABLE_NAME, null, 1);
        onCreate(getWritableDatabase());
        inboxJid = clientJid;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG,"onCreate() message Database");
        String createTable = "CREATE TABLE IF NOT EXISTS "
                + TABLE_NAME
                + "("
                + COL_MESSAGE_ID + " TEXT,"
                + COL_CONTACT_ID + " TEXT,"
                + COL_TIME_STAMP + " LONG,"
                + COL_MESSAGE_TYPE + " TEXT,"
                + COL_TEXT + " TEXT,"
                + COL_MESSAGE_ARCHIVE_ID + " TEXT,"
                + COL_RECEIVER_ID + " TEXT,"
                + "PRIMARY KEY (" + COL_MESSAGE_ID + "," + COL_RECEIVER_ID +")"
                + ")";// TODO add cascade
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        Log.d(TAG,"onUpgrade()");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void addData(CustomMessage message) {
        Log.d(TAG,"addData()");
        SQLiteDatabase db = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_MESSAGE_ID, message.getId());
        contentValues.put(COL_MESSAGE_TYPE, message.getMessageType().get());
        contentValues.put(COL_CONTACT_ID, message.getUser().getId().toLowerCase());
        contentValues.put(COL_TIME_STAMP, message.getCreatedAt().getTime());
        contentValues.put(COL_TEXT, message.getText());
        contentValues.put(COL_RECEIVER_ID, inboxJid.toLowerCase());
        String archiveId = message.getArchiveId();
        if(archiveId != null) {
            contentValues.put(COL_MESSAGE_ARCHIVE_ID, archiveId);
        }
        else {
            contentValues.put(COL_MESSAGE_ARCHIVE_ID, NULL_ARCHIVE_ID);
        }

        Log.d(TAG, "addData: Adding message to " + TABLE_NAME);
        Log.d(TAG, "addData: " + message.getUser().getId());
        Log.d(TAG, "addData: " + message.getArchiveId());
        Log.d(TAG, "addData: " + message.getId());
        db.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void addData(List<CustomMessage> messages){
        for(CustomMessage m : messages){
            addData(m);
        }
    }

    public ArrayList<CustomMessage> getConversation(String jid) {
        Cursor curs = null;
        try {
            Log.d(TAG, "getConversation()");
            SQLiteDatabase db = getWritableDatabase();
            String query = "SELECT * FROM " + TABLE_NAME
                    + " WHERE " + COL_CONTACT_ID + "=" + "'" + jid.toLowerCase() + "'"
                    + " AND "   + COL_RECEIVER_ID + "="+ "'" + inboxJid.toLowerCase() + "'"
                    + " ORDER BY " + COL_TIME_STAMP + " ASC";
            curs = db.rawQuery(query, null);

            ArrayList<CustomMessage> chatMessages = new ArrayList<>();
            while (curs.moveToNext()) {
                CustomMessage message;
                // User type not needed in message activity
                User usr = new User(curs.getString(COL_CONTACT_ID_NUM));
                String archiveId = curs.getString(COL_MESSAGE_ARCHIVE_ID_NUM);
                if (archiveId.equals(NULL_ARCHIVE_ID))
                    archiveId = null;

                if (curs.getString(COL_MESSAGE_TYPE_NUM).equals(ChatConnectionService.MessageType.RECEIVE.get())) {
                    message = new CustomMessage(curs.getString(COL_MESSAGE_ID_NUM), usr,
                            curs.getLong(COL_TIME_STAMP_NUM), curs.getString(COL_TEXT_NUM),
                            archiveId, ChatConnectionService.MessageType.RECEIVE);
                    chatMessages.add(message);
                } else {
                    message = new CustomMessage(curs.getString(COL_MESSAGE_ID_NUM), usr,
                            curs.getLong(COL_TIME_STAMP_NUM), curs.getString(COL_TEXT_NUM),
                            archiveId, ChatConnectionService.MessageType.SEND);
                    chatMessages.add(message);
                }
            }
            return chatMessages;
        } finally {
            if (curs != null) {
                curs.close();
            }
        }
    }

    public void flushConversation(String jid){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(TABLE_NAME, COL_CONTACT_ID +"='" + jid + "'"
                + " AND "   + COL_RECEIVER_ID + "="+ "'" + inboxJid.toLowerCase()  + "'", null);
    }


    public void flushData(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }
}
