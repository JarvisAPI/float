package com.floatapplication.shop.util;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.shop.models.Shop;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by william on 2018-05-08.
 * Handles getting info on shop.
 */
public class ShopHandler {
    public interface OnShopConstructedListener {
        void onShopConstructed(Shop shop);
    }

    private static final String TAG = ShopHandler.class.getSimpleName();
    private Set<String> mShopQueries;

    public ShopHandler() {
        mShopQueries = new HashSet<>();
    }

    public void getShop(final String shopId, final OnShopConstructedListener listener) {
        if (!mShopQueries.contains(shopId)) {
            mShopQueries.add(shopId);
            Map<String, String> vMap = new HashMap<>();
            vMap.put("shop_id", shopId);
            FloatApplication.getHypermediaInterface()
                    .follow(Hypermedia.GET, HypermediaInterface.SHOP_INFO, vMap, null,
                            new ServerCallback() {
                                @Override
                                public void onSuccessResponse(String response) {
                                    try {
                                        JSONObject res = new JSONObject(response);
                                        listener.onShopConstructed(Shop.createShop(res));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    mShopQueries.remove(shopId);
                                }
                            },
                            new ServerErrorCallback() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error.printStackTrace();
                                    mShopQueries.remove(shopId);
                                }
                            });
        }
    }
}
