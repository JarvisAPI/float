package com.floatapplication.user.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.R;
import com.floatapplication.user.models.UserShopItem;
import com.floatapplication.user.util.UserProfileShopItemPresenter;

import java.util.ArrayList;
import java.util.List;

public class UserProfileAdapter extends Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = UserProfileAdapter.class.getSimpleName();

    private List<UserShopItem> mUserItems;
    private UserProfileShopItemPresenter mPresenter;

    public UserProfileAdapter() {
        mUserItems = new ArrayList<>();
        mPresenter = new UserProfileShopItemPresenter();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == R.layout.list_profile_shop_item) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_profile_shop_item,
                    parent, false);
            return new ShopItemViewHolder(v);
        }
        throw new IllegalArgumentException("Other views doesn't exist");
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (holder instanceof ShopItemViewHolder) {
            ShopItemViewHolder vh = (ShopItemViewHolder) holder;
            UserShopItem item = mUserItems.get(position);
            mPresenter.present(vh.mShopNameText, vh.mShopLocationText, vh.mShopImage, vh.mGoToShopButton,
                    vh.mGoToShopLocationButton, vh.mGoToShopEditButton, vh.mGoToShopInboxButton, item);
        }
    }

    public UserProfileShopItemPresenter getPresenter() {
        return mPresenter;
    }

    @Override
    public int getItemCount() {
        return mUserItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.list_profile_shop_item;
    }

    public void setUserItems(List<UserShopItem> items) {
        mUserItems.clear();
        mUserItems.addAll(items);
        notifyDataSetChanged();
    }

    public void removeShop(String shopId) {
        for (int i = 0; i < mUserItems.size(); i++) {
            UserShopItem item = mUserItems.get(i);
            if (item.getShopId().equals(shopId)) {
                mUserItems.remove(i);
                notifyItemRemoved(i);
                break;
            }
        }
    }

    public void refreshShop(String shopId){
        for (int i = 0; i < mUserItems.size(); i++) {
            UserShopItem item = mUserItems.get(i);
            if (item.getShopId().equals(shopId)) {
                notifyItemChanged(i);
                break;
            }
        }
    }

    private class ShopItemViewHolder extends RecyclerView.ViewHolder {
        private TextView mShopNameText;
        private TextView mShopLocationText;
        private ImageView mShopImage;
        private ImageView mGoToShopButton;
        private ImageView mGoToShopLocationButton;
        private ImageView mGoToShopEditButton;
        private ImageView mGoToShopInboxButton;

        private ShopItemViewHolder(View view) {
            super(view);
            mShopNameText = view.findViewById(R.id.shop_name);
            mShopLocationText = view.findViewById(R.id.shop_location);
            mShopImage = view.findViewById(R.id.shop_image);
            mGoToShopButton = view.findViewById(R.id.go_to_shop_image_view);
            mGoToShopLocationButton = view.findViewById(R.id.shop_location_icon);
            mGoToShopEditButton = view.findViewById(R.id.shop_edit_icon);
            mGoToShopInboxButton = view.findViewById(R.id.shop_messages_icon);
        }
    }
}
