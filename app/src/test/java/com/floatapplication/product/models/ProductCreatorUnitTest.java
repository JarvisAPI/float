package com.floatapplication.product.models;

import android.os.Parcel;

import com.floatapplication.TestApplication;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * Created by william on 2018-06-14.
 * Test parcelable done correctly.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ProductCreatorUnitTest extends ProductBaseChecker {
    private ProductCreator productCreator;
    private String productMainImagePath;
    private List<String> productImagePaths;

    @Before
    public void setup() {
        productName = "name";
        productPrice = "10";
        productCategory = "food";
        productDescription = "desc";
        productMainImagePath = "main-path";
        productImagePaths = new ArrayList<>();
        productImagePaths.add("image-path");

        productCreator = new ProductCreator();
        productCreator.genProductId();
        productId = productCreator.getProductId();

        productCreator.setProductName(productName);
        productCreator.setProductPrice(productPrice);
        productCreator.setProductCategory(productCategory);
        productCreator.setProductDescription(productDescription);
        productCreator.setMainImagePath(productMainImagePath);
        productCreator.setImagePaths(productImagePaths);
    }

    @Test
    public void writeToParcel_andReconstruct() {
        Parcel parcel = Parcel.obtain();
        productCreator.writeToParcel(parcel, 0);
        parcel.setDataPosition(0);
        ProductCreator result = (ProductCreator) ProductCreator.CREATOR.createFromParcel(parcel);
        check(result);
        assertEquals(productMainImagePath, result.getProductMainImagePath());
        assertEquals(productImagePaths, result.getProductImagePaths());
    }
}
