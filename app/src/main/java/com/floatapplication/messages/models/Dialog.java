package com.floatapplication.messages.models;

import com.floatapplication.messages.chatkit.commons.models.IDialog;

import java.util.ArrayList;

public class Dialog implements IDialog<CustomMessage> {

    private String id;
    private ArrayList<User> users;
    private CustomMessage lastMessage;

    private int unreadCount;

    public Dialog(String id, ArrayList<User> users, CustomMessage lastMessage, int unreadCount) {
        this.id = id;
        this.users = users;
        this.lastMessage = lastMessage;
        this.unreadCount = unreadCount;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public ArrayList<User> getUsers() {
        return users;
    }

    @Override
    public CustomMessage getLastMessage() {
        return lastMessage;
    }

    @Override
    public void setLastMessage(CustomMessage lastMessage) {
        this.lastMessage = lastMessage;
    }

    @Override
    public int getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(int unreadCount) {
        this.unreadCount = unreadCount;
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj == null) return false;
        if (this.getClass() != obj.getClass()) return false;
        Dialog that = (Dialog) obj;
        return this.id.equals(that.id) && this.lastMessage.equals(that.lastMessage);
    }
}
