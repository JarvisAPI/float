package com.floatapplication.messages.models;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.floatapplication.FloatApplication;
import com.floatapplication.messages.chatkit.commons.models.IMessage;
import com.floatapplication.messages.chatkit.commons.models.MessageContentType;
import com.floatapplication.messages.server_connection.ChatConnectionService;
import com.floatapplication.messages.server_connection.ChatConnectionService.MessageType;

import org.jivesoftware.smack.packet.ExtensionElement;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.delay.packet.DelayInformation;
import org.jivesoftware.smackx.forward.packet.Forwarded;

import java.util.Date;

import static com.floatapplication.messages.models.User.formatJid;

public class CustomMessage implements IMessage, Parcelable,
        MessageContentType.Image, /* this is for default image messages implementation */
        MessageContentType /* and this one is for custom content type (in this case - voice message) */ {

    public static String TAG = "CustomMessage";
    private String id;
    private String text;
    private Date createdAt;
    private User user;
    private boolean toDisplayTime = true;
    private ChatConnectionService.MessageType messageType;
    private String archiveId;
    private Boolean isDelayed;
    private Image image;

    public CustomMessage(Message message, User user, ChatConnectionService.MessageType type) {
        this.user = user;
        text = message.getBody();
        id = message.getStanzaId();
        messageType = type;
        // retrieve delay in
        DelayInformation inf = message.getExtension("delay","urn:xmpp:delay");
        if(inf != null) {
            Log.d("CustomMessage", Long.toString(inf.getStamp().getTime()));
            this.createdAt = inf.getStamp();
            isDelayed = true;
        }
        else{
            this.createdAt = new Date(System.currentTimeMillis());
            isDelayed = false;
        }
    }

    public CustomMessage(Forwarded fmessage, String receiver){
        org.jivesoftware.smack.packet.Message message = (org.jivesoftware.smack.packet.Message) fmessage.getForwardedStanza();
        String senderJid = formatJid(message.getFrom().toString());

        if(receiver.equals(senderJid)){
            messageType = ChatConnectionService.MessageType.SEND;
            user = new User(message.getTo().toString());
        }
        else{
            messageType = ChatConnectionService.MessageType.RECEIVE;
            user = new User(senderJid);
        }
        text = message.getBody();
        id = message.getStanzaId();
        archiveId = getArchiveMessageId(fmessage);
        this.createdAt = fmessage.getDelayInformation().getStamp();
    }

    public CustomMessage(String id, User user, long date, String text, String archiveId, ChatConnectionService.MessageType type) {
        this.id = id;
        this.text = text;
        this.user = user;
        this.createdAt = new Date(date);
        this.archiveId = archiveId;
        this.messageType = type;
    }

    public Boolean isDelayed(){
        return isDelayed;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public Date getCreatedAt() {
        return createdAt;
    }

    @Override
    public User getUser() {
        return this.user;
    }

    @Override
    public String getImageUrl() {
        return image == null ? null : image.url;
    }

    public void setUser(User user){
        this.user = user;
    }

    private String getArchiveMessageId(Forwarded fmessage){
        ExtensionElement extension =  fmessage.getForwardedStanza().getExtension("stanza-id","urn:xmpp:sid:0");
        String xmlString;
        if(extension != null){
            xmlString = extension.toXML().toString();
        }
        else {
            return Long.toString(fmessage.getDelayInformation().getStamp().getTime());
        }
        String regex = "id='";
        String msgId = xmlString.substring(xmlString.indexOf(regex)+regex.length());
        return msgId.substring(0, msgId.indexOf("'"));
    }


    public void setText(String text) {
        this.text = text;
    }

    public void setToDisplayTime(boolean todDisplay){
        this.toDisplayTime = todDisplay;
    }

    public boolean getToDisplayTime(){
        return this.toDisplayTime;
    }


    public ChatConnectionService.MessageType getMessageType(){
        return messageType;
    }

    public String getArchiveId(){
        return archiveId;
    }

    @Override
    public void setImage(Image image) {
        this.image = image;
    }


    public static class Image {

        private String url;

        public Image(String url) {
            this.url = url;
        }
    }

    public static class Voice {

        private String url;
        private int duration;

        public Voice(String url, int duration) {
            this.url = url;
            this.duration = duration;
        }

        public String getUrl() {
            return url;
        }

        public int getDuration() {
            return duration;
        }
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<CustomMessage>() {
        @Override
        public CustomMessage createFromParcel(Parcel parcel) {
            return new CustomMessage(parcel);
        }
        @Override
        public CustomMessage[] newArray(int size) {
            return new CustomMessage[size];
        }
    };

    private CustomMessage(Parcel parcel){
        id = parcel.readString();
        text = parcel.readString();
        createdAt = new Date(parcel.readLong());
        user = parcel.readParcelable(User.class.getClassLoader());
        toDisplayTime = parcel.readByte() == 1;
        messageType = (parcel.readString().equals(MessageType.RECEIVE.get())) ? MessageType.RECEIVE: MessageType.SEND;
        archiveId = parcel.readString();
        isDelayed = parcel.readByte() == 1;
        if(!"".equals(parcel.readString())) {
            image = new Image(parcel.readString());
        }
    }

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(text);
        parcel.writeLong(createdAt.getTime());
        parcel.writeParcelable(user,i);
        parcel.writeByte((byte) (toDisplayTime ? 1:0));
        parcel.writeString(messageType.get());
        parcel.writeString(archiveId);
        parcel.writeByte((byte) (isDelayed ? 1:0));
        if(image != null) {
            parcel.writeString(image.url);
        }
        else{
            parcel.writeString("");
        }
    }

    @Override
    public boolean equals(Object obj){
        if (this == obj) return true;
        if (obj == null) return false;
        if (this.getClass() != obj.getClass()) return false;
        CustomMessage that = (CustomMessage) obj;
        return this.id.equals(that.id);
    }

    @Override
    public int hashCode(){
        return this.getId().hashCode();
    }
}
