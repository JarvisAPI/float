package com.floatapplication.product.util;

import com.floatapplication.product.util.ProductShopQueryQueue.ProductShopQuery;

/**
 * Created by william on 2018-06-05.
 *
 */

public interface ProductShopQueryFactory {
    ProductShopQuery create(String shopId, int offset, int limit);
}
