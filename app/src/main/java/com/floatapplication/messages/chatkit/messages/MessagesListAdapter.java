/*******************************************************************************
 * Copyright 2016 stfalcon.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/

package com.floatapplication.messages.chatkit.messages;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import com.floatapplication.messages.activity.activity_helpers.MessageLoader;
import com.floatapplication.messages.chatkit.commons.ImageLoader;
import com.floatapplication.messages.chatkit.commons.LoadSpinnerItem;
import com.floatapplication.messages.chatkit.commons.ViewHolder;
import com.floatapplication.messages.chatkit.commons.models.IMessage;
import com.floatapplication.messages.chatkit.utils.DateFormatter;
import com.floatapplication.messages.chatkit.utils.FixedSizeLinkedHashSet;
import com.floatapplication.messages.chatkit.utils.MessageParser;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.util.Util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Adapter for {@link MessagesList}.
 */
@SuppressWarnings("WeakerAccess")
public class MessagesListAdapter<MESSAGE extends CustomMessage>
        extends RecyclerView.Adapter<ViewHolder>
        implements RecyclerScrollMoreListener.OnLoadMoreListener {
    private static final String TAG = MessagesListAdapter.class.getSimpleName();
    private MessageHolders holders;
    private List<Wrapper> items;

    private int selectedItemsCount;
    private SelectionListener selectionListener;

    protected static boolean isSelectionModeEnabled;;
    private OnLoadMoreListener loadMoreListener;
    private OnMessageClickListener<MESSAGE> onMessageClickListener;
    private OnMessageViewClickListener<MESSAGE> onMessageViewClickListener;
    private OnMessageLongClickListener<MESSAGE> onMessageLongClickListener;
    private OnMessageViewLongClickListener<MESSAGE> onMessageViewLongClickListener;
    private ImageLoader imageLoader;
    private RecyclerView.LayoutManager layoutManager;
    private MessagesListStyle messagesListStyle;
    private DateFormatter.Formatter dateHeadersFormatter;
    private SparseArray<OnMessageViewClickListener> viewClickListenersArray = new SparseArray<>();
    private FixedSizeLinkedHashSet<String> headFSLH = new FixedSizeLinkedHashSet<>(MessageLoader.PAGE_SIZE );
    private FixedSizeLinkedHashSet<String> tailFSLH = new FixedSizeLinkedHashSet<>(MessageLoader.PAGE_SIZE +1);
    private MessageLoader mMessageLoader;

    /**
     * For default list item layout and view holder.
     *
     * @param imageLoader image loading method.
     */
    public MessagesListAdapter( ImageLoader imageLoader) {
        this.holders = new MessageHolders();
        this.imageLoader = imageLoader;
    }

    /**
     * For default list item layout and view holder.
     *
     * @param holders     custom layouts and view holders. See {@link MessageHolders} documentation for details
     * @param imageLoader image loading method.
     */
    public MessagesListAdapter( MessageHolders holders, ImageLoader imageLoader) {
        this.holders = holders;
        this.imageLoader = imageLoader;
        this.items = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return holders.getHolder(parent, viewType, messagesListStyle);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Wrapper wrapper = items.get(position);
        holders.bind(holder, wrapper.item, wrapper.isSelected, imageLoader,
                getMessageClickListener(wrapper),
                getMessageLongClickListener(wrapper),
                dateHeadersFormatter,
                viewClickListenersArray);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        return holders.getViewType(items.get(position).item);
    }

    @Override
    public void onLoadMore(int page, int total) {
        loadMoreListener.onLoadMore(page, total);
    }

    @Override
    public boolean getIsEndOfConversation() {
        return mMessageLoader != null && mMessageLoader.getIsEndOfConversation();
    }

    @Override
    public boolean getIsLoading() {
        return  mMessageLoader != null && mMessageLoader.getIsMessageLoading();
    }


    /*
    * PUBLIC METHODS
    * */

    /**
     * Adds message to bottom of list and scroll if needed.
     *
     * @param message message to add.
     * @param scroll  {@code true} if need to scroll list to bottom when message added.
     */
    @SuppressWarnings("unchecked")
    public void addToStart(MESSAGE message, boolean scroll) {
        if(headFSLH.contains(message.getId())
                || tailFSLH.contains(message.getId())
                || getMostRecentMessage() != null && getMostRecentMessage().getId().equals(message.getId())) {
            return;
        }
        Log.d(TAG, "Message to add:"  + message.getText());
        headFSLH.add(message.getId());

        final boolean isNewMessageToday = !isPreviousSameDate(0, message.getCreatedAt());
        if (isNewMessageToday) {
            items.add(0, new Wrapper<>(message.getCreatedAt()));
        }

        if(message.getText().startsWith(MessageParser.PHOTO_MESSAGE_PREFIX)){
            String url = Util.getInstance()
                    .constructImageUrl(MessageParser.extractImageUrl(message.getText()).trim());
            message.setImage(new CustomMessage.Image(url));
        }

        boolean isSameMinute = false;

        if (isPreviousSameMinute(0, message.getCreatedAt())
                && isPreviousSameAuthor(0, message.getUser().getId())
                 && message.getImageUrl() == null) {
            if(items.get(0).item instanceof IMessage) {
                isSameMinute = true;
                MESSAGE msg = (MESSAGE) items.get(0).item;
                msg.setToDisplayTime(false);
            }
        }

        Wrapper<MESSAGE> element = new Wrapper<>(message);
        items.add(0, element);
        final boolean isSameMin = isSameMinute;
        layoutManager.postOnAnimation( new Runnable() {
            public void run() {
                notifyItemRangeInserted(0, isNewMessageToday ? 2 : 1);
                if(isSameMin){
                    int messagePositionBeforeInsertedMessage = 1;
                    notifyItemChanged(messagePositionBeforeInsertedMessage);
                }
            }
        });
        if (layoutManager != null && scroll) {
            layoutManager.scrollToPosition(0);
        }
    }

    /**
     * Adds messages list in chronological order. Use this method to add history.
     *
     * @param messages messages from history.
     * @param reverse  {@code true} if need to reverse messages before adding.
     */
    public void addToEnd(List<MESSAGE> messages, boolean reverse) {
        hideLoadingSpinner();
        checkDuplicates(messages);
        if (messages.isEmpty()) {
            return;
        }
        if (reverse) Collections.reverse(messages);

        if (!items.isEmpty()) {
            CustomMessage firstMsg = messages.get(0);
            CustomMessage lastMessage = getLastMessage();
            if (lastMessage != null
                    && isPreviousSameMinute(firstMsg.getCreatedAt(), getLastMessage().getCreatedAt())
                    && firstMsg.getImageUrl() == null) {
                firstMsg.setToDisplayTime(false);
            }
            final int lastItemPosition = items.size() - 1;
            if (items.get(lastItemPosition).item instanceof Date) {
                Date lastItem = (Date) items.get(lastItemPosition).item;
                if (DateFormatter.isSameDay(messages.get(0).getCreatedAt(), lastItem)) {
                    items.remove(lastItemPosition);
                    layoutManager.postOnAnimation(new Runnable() {
                        public void run() {
                            notifyItemRemoved(lastItemPosition);
                        }
                    });
                }
            }
        }

        final int oldSize = getItemCount();
        generateDateHeaders(messages);
        layoutManager.postOnAnimation(new Runnable() {
            public void run() {
                Log.d(TAG, "updating data set" + Integer.toString(getItemCount() - oldSize));
                notifyItemRangeInserted(oldSize, getItemCount() - oldSize);
            }
        });
    }

    public void showLoadingSpinner(){
        if (!items.isEmpty()) {
            Wrapper<LoadSpinnerItem> element = new Wrapper<>(new LoadSpinnerItem());
            if (!(items.get(items.size() - 1).item instanceof LoadSpinnerItem)) {
                Log.d(TAG,"show spinner");
                final int insertPosition = items.size();
                items.add(insertPosition, element);
                layoutManager.postOnAnimation( new Runnable() {
                    public void run() {
                        notifyItemInserted(insertPosition);
                    }
                });
            }
        }
    }

    public void hideLoadingSpinner() {
        if (!items.isEmpty()) {
            final int lastItemPosition = items.size() - 1;
            if (items.get(lastItemPosition).item instanceof LoadSpinnerItem) {
                Log.d(TAG,"hide spinner");
                items.remove(lastItemPosition);
                layoutManager.postOnAnimation( new Runnable() {
                    public void run() {
                        notifyItemRemoved(lastItemPosition);
                    }
                });
            }
        }
    }

    private void checkDuplicates(List<MESSAGE> messages){
        List<MESSAGE> duplicateMessages = new ArrayList<>();
        for(MESSAGE msg: messages){
            if(tailFSLH.contains(msg.getId()) || headFSLH.contains(msg.getId())) {
                duplicateMessages.add(msg);
            } else {
                tailFSLH.add(msg.getId());
            }
        }
        messages.removeAll(duplicateMessages);
        Log.d(TAG, Integer.toString(duplicateMessages.size()));
        Log.d(TAG, Integer.toString(messages.size()));
    }

    @SuppressWarnings("unchecked")
    public Date getLastMessageDate() {
        MESSAGE msg = (MESSAGE) items.get(0).item;
        return msg.getCreatedAt();
    }


    /**
     * Updates message by its id.
     *
     * @param message updated message object.
     */
    public void update(MESSAGE message) {
        update(message.getId(), message);
    }

    /**
     * Updates message by old identifier (use this method if id has changed). Otherwise use {}
     *
     * @param oldId      an identifier of message to update.
     * @param newMessage new message object.
     */
    public void update(String oldId, MESSAGE newMessage) {
        int position = getMessagePositionById(oldId);
        if (position >= 0) {
            Wrapper<MESSAGE> element = new Wrapper<>(newMessage);
            items.set(position, element);
            notifyItemChanged(position);
        }
    }

    /**
     * Deletes message.
     *
     * @param message message to delete.
     */
    public void delete(MESSAGE message) {
        deleteById(message.getId());
    }

    /**
     * Deletes messages list.
     *
     * @param messages messages list to delete.
     */
    public void delete(List<MESSAGE> messages) {
        for (MESSAGE message : messages) {
            int index = getMessagePositionById(message.getId());
            items.remove(index);
            notifyItemRemoved(index);
        }
        recountDateHeaders();
    }

    /**
     * Deletes message by its identifier.
     *
     * @param id identifier of message to delete.
     */
    public void deleteById(String id) {
        int index = getMessagePositionById(id);
        if (index >= 0) {
            items.remove(index);
            notifyItemRemoved(index);
            recountDateHeaders();
        }
    }

    public MESSAGE getLastMessage() {
        if (!items.isEmpty()) {
            for (int i = items.size() - 1; i >= 0; i--) {
                if (items.get(i).item instanceof IMessage) {
                    return (MESSAGE) items.get(i).item;
                }
            }
        }
        return null;
    }

    public MESSAGE getMostRecentMessage(){
        for(Wrapper wrapper: items){
            if(wrapper.item instanceof IMessage){
                return (MESSAGE) wrapper.item;
            }
        }
        return null;
    }

    /**
     * Deletes messages by its identifiers.
     *
     * @param ids array of identifiers of messages to delete.
     */
    public void deleteByIds(String[] ids) {
        for (String id : ids) {
            int index = getMessagePositionById(id);
            items.remove(index);
            notifyItemRemoved(index);
        }
        recountDateHeaders();
    }

    /**
     * Returns {@code true} if, and only if, messages count in adapter is non-zero.
     *
     * @return {@code true} if size is 0, otherwise {@code false}
     */
    public boolean isEmpty() {
        return items.isEmpty();
    }

    /**
     * Clears the messages list.
     */
    public void clear() {
        items.clear();
    }

    /**
     * Enables selection mode.
     *
     * @param selectionListener listener for selected items count. To get selected messages use {@link #getSelectedMessages()}.
     */
    public void enableSelectionMode(SelectionListener selectionListener) {
        if (selectionListener == null) {
            throw new IllegalArgumentException("SelectionListener must not be null. Use `disableSelectionMode()` if you want tp disable selection mode");
        } else {
            this.selectionListener = selectionListener;
        }
    }

    /**
     * Disables selection mode and removes {@link SelectionListener}.
     */
    public void disableSelectionMode() {
        this.selectionListener = null;
        unselectAllItems();
    }

    /**
     * Returns the list of selected messages.
     *
     * @return list of selected messages. Empty list if nothing was selected or selection mode is disabled.
     */
    @SuppressWarnings("unchecked")
    public ArrayList<MESSAGE> getSelectedMessages() {
        ArrayList<MESSAGE> selectedMessages = new ArrayList<>();
        for (Wrapper wrapper : items) {
            if (wrapper.item instanceof IMessage && wrapper.isSelected) {
                selectedMessages.add((MESSAGE) wrapper.item);
            }
        }
        return selectedMessages;
    }

    /**
     * Returns selected messages text and do {@link #unselectAllItems()} for you.
     *
     * @param formatter The formatter that allows you to format your message model when copying.
     * @param reverse   Change ordering when copying messages.
     * @return formatted text by {@link Formatter}. If it's {@code null} - {@code MESSAGE#toString()} will be used.
     */
    public String getSelectedMessagesText(Formatter<MESSAGE> formatter, boolean reverse) {
        String copiedText = getSelectedText(formatter, reverse);
        unselectAllItems();
        return copiedText;
    }

    /**
     * Copies text to device clipboard and returns selected messages text. Also it does {@link #unselectAllItems()} for you.
     *
     * @param context   The context.
     * @param formatter The formatter that allows you to format your message model when copying.
     * @param reverse   Change ordering when copying messages.
     * @return formatted text by {@link Formatter}. If it's {@code null} - {@code MESSAGE#toString()} will be used.
     */
    public String copySelectedMessagesText(Context context, Formatter<MESSAGE> formatter, boolean reverse) {
        String copiedText = getSelectedText(formatter, reverse);
        copyToClipboard(context, copiedText);
        unselectAllItems();
        return copiedText;
    }

    /**
     * Unselect all of the selected messages. Notifies {@link SelectionListener} with zero count.
     */
    public void unselectAllItems() {
        for (int i = 0; i < items.size(); i++) {
            Wrapper wrapper = items.get(i);
            if (wrapper.isSelected) {
                wrapper.isSelected = false;
                notifyItemChanged(i);
            }
        }
        isSelectionModeEnabled = false;
        selectedItemsCount = 0;
        notifySelectionChanged();
    }

    /**
     * Deletes all of the selected messages and disables selection mode.
     * Call {@link #getSelectedMessages()} before calling this method to delete messages from your data source.
     */
    public void deleteSelectedMessages() {
        List<MESSAGE> selectedMessages = getSelectedMessages();
        delete(selectedMessages);
        unselectAllItems();
    }

    /**
     * Sets click listener for item. Fires ONLY if list is not in selection mode.
     *
     * @param onMessageClickListener click listener.
     */
    public void setOnMessageClickListener(OnMessageClickListener<MESSAGE> onMessageClickListener) {
        this.onMessageClickListener = onMessageClickListener;
    }

    public void setMessageLoader(MessageLoader messageLoader){
        this.mMessageLoader = messageLoader;
    }

    /**
     * Sets click listener for message view. Fires ONLY if list is not in selection mode.
     *
     * @param onMessageViewClickListener click listener.
     */
    public void setOnMessageViewClickListener(OnMessageViewClickListener<MESSAGE> onMessageViewClickListener) {
        this.onMessageViewClickListener = onMessageViewClickListener;
    }

    /**
     * Registers click listener for view by id
     *
     * @param viewId                     view
     * @param onMessageViewClickListener click listener.
     */
    public void registerViewClickListener(int viewId, OnMessageViewClickListener<MESSAGE> onMessageViewClickListener) {
        this.viewClickListenersArray.append(viewId, onMessageViewClickListener);
    }

    /**
     * Sets long click listener for item. Fires only if selection mode is disabled.
     *
     * @param onMessageLongClickListener long click listener.
     */
    public void setOnMessageLongClickListener(OnMessageLongClickListener<MESSAGE> onMessageLongClickListener) {
        this.onMessageLongClickListener = onMessageLongClickListener;
    }

    /**
     * Sets long click listener for message view. Fires ONLY if selection mode is disabled.
     *
     * @param onMessageViewLongClickListener long click listener.
     */
    public void setOnMessageViewLongClickListener(OnMessageViewLongClickListener<MESSAGE> onMessageViewLongClickListener) {
        this.onMessageViewLongClickListener = onMessageViewLongClickListener;
    }

    /**
     * Set callback to be invoked when list scrolled to top.
     *
     * @param loadMoreListener listener.
     */
    public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
        this.loadMoreListener = loadMoreListener;
    }

    /**
     * Sets custom {@link DateFormatter.Formatter} for text representation of date headers.
     */
    public void setDateHeadersFormatter(DateFormatter.Formatter dateHeadersFormatter) {
        this.dateHeadersFormatter = dateHeadersFormatter;
    }

    /*
    * PRIVATE METHODS
    * */
    private void recountDateHeaders() {
        List<Integer> indicesToDelete = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            Wrapper wrapper = items.get(i);
            if (wrapper.item instanceof Date) {
                if (i == 0) {
                    indicesToDelete.add(i);
                } else {
                    if (items.get(i - 1).item instanceof Date) {
                        indicesToDelete.add(i);
                    }
                }
            }
        }

        Collections.reverse(indicesToDelete);
        for (int i : indicesToDelete) {
            items.remove(i);
            notifyItemRemoved(i);
        }
    }

    private void generateDateHeaders(List<MESSAGE> messages) {
        for (int i = 0; i < messages.size(); i++) {
            MESSAGE message = messages.get(i);
            Log.d(TAG, "message before time: " + message.getText());
            if(message.getText() != null && message.getText().startsWith(MessageParser.PHOTO_MESSAGE_PREFIX)){
                String url = Util.getInstance()
                        .constructImageUrl(MessageParser.extractImageUrl(message.getText()).trim());
                message.setImage(new CustomMessage.Image(url));
            }
            this.items.add(new Wrapper<>(message));
            if (messages.size() > i + 1) {
                MESSAGE nextMessage = messages.get(i + 1);
                Log.d(TAG, "time: " + nextMessage.getText());
                if (isPreviousSameMinute(nextMessage.getCreatedAt(), message.getCreatedAt())
                        && nextMessage.getUser().getId().equals(message.getUser().getId())
                        && message.getImageUrl() == null) {
                    Log.d(TAG, "not displaying time for message: " + nextMessage.getText());
                    nextMessage.setToDisplayTime(false);
                }
                if (!DateFormatter.isSameDay(message.getCreatedAt(), nextMessage.getCreatedAt())) {
                    this.items.add(new Wrapper<>(message.getCreatedAt()));
                }
            } else {
                this.items.add(new Wrapper<>(message.getCreatedAt()));
            }
        }
    }

    @SuppressWarnings("unchecked")
    private int getMessagePositionById(String id) {
        for (int i = 0; i < items.size(); i++) {
            Wrapper wrapper = items.get(i);
            if (wrapper.item instanceof IMessage) {
                MESSAGE message = (MESSAGE) wrapper.item;
                if (message.getId().contentEquals(id)) {
                    return i;
                }
            }
        }
        return -1;
    }

    @SuppressWarnings("unchecked")
    private boolean isPreviousSameDate(int position, Date dateToCompare) {
        if (items.size() <= position) return false;
        if (items.get(position).item instanceof IMessage) {
            Date previousPositionDate = ((MESSAGE) items.get(position).item).getCreatedAt();
            return DateFormatter.isSameDay(dateToCompare, previousPositionDate);
        } else return false;
    }

    @SuppressWarnings("unchecked") //TODO
    private boolean isPreviousSameMinute(int position, Date dateToCompare) {
        if (items.size() <= position) return false;
        if (items.get(position).item instanceof IMessage) {
            String dateFormat = "yyyy-MM-dd HH:mm";
            SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.CHINA);
            return sdf.format(((MESSAGE) items.get(position).item).getCreatedAt()).equals(sdf.format(dateToCompare));
        } else return false;
    }

    @SuppressWarnings("unchecked") //TODO
    private boolean isPreviousSameMinute(Date privDate, Date dateToCompare) {
        String dateFormat = "yyyy-MM-dd HH:mm";
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.CHINA);
        return sdf.format(privDate).equals(sdf.format(dateToCompare));
    }

    @SuppressWarnings("unchecked")
    private boolean isPreviousSameAuthor(int position, String id) {
        return items.size() > position &&
                !(items.get(position).item instanceof Date) &&
                items.get(position).item instanceof IMessage
                && ((MESSAGE) items.get(position).item).getUser().getId().equals(id);
    }

    private void incrementSelectedItemsCount() {
        selectedItemsCount++;
        notifySelectionChanged();
    }

    private void decrementSelectedItemsCount() {
        selectedItemsCount--;
        isSelectionModeEnabled = selectedItemsCount > 0;

        notifySelectionChanged();
    }

    private void notifySelectionChanged() {
        if (selectionListener != null) {
            selectionListener.onSelectionChanged(selectedItemsCount);
        }
    }

    private void notifyMessageClicked(MESSAGE message) {
        if (onMessageClickListener != null) {
            onMessageClickListener.onMessageClick(message);
        }
    }

    private void notifyMessageViewClicked(View view, MESSAGE message) {
        if (onMessageViewClickListener != null) {
            view.setTransitionName(message.getId());
            onMessageViewClickListener.onMessageViewClick(view, message);
        }
    }

    private void notifyMessageLongClicked(MESSAGE message) {
        if (onMessageLongClickListener != null) {
            onMessageLongClickListener.onMessageLongClick(message);
        }
    }

    private void notifyMessageViewLongClicked(View view, MESSAGE message) {
        if (onMessageViewLongClickListener != null) {
            onMessageViewLongClickListener.onMessageViewLongClick(view, message);
        }
    }

    private View.OnClickListener getMessageClickListener(final Wrapper<MESSAGE> wrapper) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectionListener != null && isSelectionModeEnabled) {
                    wrapper.isSelected = !wrapper.isSelected;

                    if (wrapper.isSelected) incrementSelectedItemsCount();
                    else decrementSelectedItemsCount();

                    MESSAGE message = (wrapper.item);
                    notifyItemChanged(getMessagePositionById(message.getId()));
                } else {
                    notifyMessageClicked(wrapper.item);
                    notifyMessageViewClicked(view, wrapper.item);
                }
            }
        };
    }

    private View.OnLongClickListener getMessageLongClickListener(final Wrapper<MESSAGE> wrapper) {
        return new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (selectionListener == null) {
                    notifyMessageLongClicked(wrapper.item);
                    notifyMessageViewLongClicked(view, wrapper.item);
                    return true;
                } else {
                    isSelectionModeEnabled = true;
                    view.performClick();
                    return true;
                }
            }
        };
    }

    private String getSelectedText(Formatter<MESSAGE> formatter, boolean reverse) {
        StringBuilder builder = new StringBuilder();

        ArrayList<MESSAGE> selectedMessages = getSelectedMessages();
        if (reverse) Collections.reverse(selectedMessages);

        for (MESSAGE message : selectedMessages) {
            builder.append(formatter == null
                    ? message.toString()
                    : formatter.format(message));
            builder.append("\n\n");
        }
        if(builder.length() - 2 >=0)
            builder.replace(builder.length() - 2, builder.length(), "");

        return builder.toString();
    }

    private void copyToClipboard(Context context, String copiedText) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText(copiedText, copiedText);
        if (clipboard != null) {
            clipboard.setPrimaryClip(clip);
        }
    }

    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    void setStyle(MessagesListStyle style) {
        this.messagesListStyle = style;
    }

    /*
    * WRAPPER
    * */
    private class Wrapper<DATA> {
        protected DATA item;
        protected boolean isSelected;

        Wrapper(DATA item) {
            this.item = item;
        }
    }

    /*
    * LISTENERS
    * */

    /**
     * Interface definition for a callback to be invoked when next part of messages need to be loaded.
     */
    public interface OnLoadMoreListener {

        /**
         * Fires when user scrolled to the end of list.
         *
         * @param page            next page to download.
         * @param totalItemsCount current items count.
         */
        void onLoadMore(int page, int totalItemsCount);
    }

    /**
     * Interface definition for a callback to be invoked when selected messages count is changed.
     */
    public interface SelectionListener {

        /**
         * Fires when selected items count is changed.
         *
         * @param count count of selected items.
         */
        void onSelectionChanged(int count);
    }

    /**
     * Interface definition for a callback to be invoked when message item is clicked.
     */
    public interface OnMessageClickListener<MESSAGE extends IMessage> {

        /**
         * Fires when message is clicked.
         *
         * @param message clicked message.
         */
        void onMessageClick(MESSAGE message);
    }

    /**
     * Interface definition for a callback to be invoked when message view is clicked.
     */
    public interface OnMessageViewClickListener<MESSAGE extends IMessage> {

        /**
         * Fires when message view is clicked.
         *
         * @param message clicked message.
         */
        void onMessageViewClick(View view, MESSAGE message);
    }

    /**
     * Interface definition for a callback to be invoked when message item is long clicked.
     */
    public interface OnMessageLongClickListener<MESSAGE extends IMessage> {

        /**
         * Fires when message is long clicked.
         *
         * @param message clicked message.
         */
        void onMessageLongClick(MESSAGE message);
    }

    /**
     * Interface definition for a callback to be invoked when message view is long clicked.
     */
    public interface OnMessageViewLongClickListener<MESSAGE extends IMessage> {

        /**
         * Fires when message view is long clicked.
         *
         * @param message clicked message.
         */
        void onMessageViewLongClick(View view, MESSAGE message);
    }

    /**
     * Interface used to format your message model when copying.
     */
    public interface Formatter<MESSAGE> {

        /**
         * Formats an string representation of the message object.
         *
         * @param message The object that should be formatted.
         * @return Formatted text.
         */
        String format(MESSAGE message);
    }
}
