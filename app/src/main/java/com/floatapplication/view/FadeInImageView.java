package com.floatapplication.view;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * Created by william on 2018-06-07.
 *
 */

public class FadeInImageView extends AppCompatImageView {
    private static final String TAG = FadeInImageView.class.getSimpleName();
    private static final int FADE_IN_TIME_MS = 125;

    public FadeInImageView(Context context) {
        super(context);
    }

    public FadeInImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        if (drawable == null) {
            return;
        }
        TransitionDrawable td = new TransitionDrawable(new Drawable[]{
                new ColorDrawable(getResources().getColor(android.R.color.transparent)),
                drawable
        });
        super.setImageDrawable(td);
        td.startTransition(FADE_IN_TIME_MS);
    }
}
