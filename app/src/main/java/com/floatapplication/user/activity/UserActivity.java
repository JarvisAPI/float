package com.floatapplication.user.activity;

import android.Manifest;
import android.Manifest.permission;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.image.activity.ImageUploadPreviewActivity;
import com.floatapplication.map.activity.MapMainActivity;
import com.floatapplication.messages.activity.DialogsActivity;
import com.floatapplication.messages.activity.MessagesActivity;
import com.floatapplication.shop.activity.ShopEditActivity;
import com.floatapplication.shop.activity.ShopViewActivity;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.shop.util.ShopHandler;
import com.floatapplication.shop.util.ShopHandler.OnShopConstructedListener;
import com.floatapplication.user.fragment.UserFragment;
import com.floatapplication.user.fragment.ZoomInPictureFragment;
import com.floatapplication.user.models.User.NamePair;
import com.floatapplication.user.util.UserProfilePresenter.OnUserProfileClickListener;
import com.floatapplication.user.util.UserProfileShopItemPresenter.UserShopItemListener;
import com.floatapplication.util.GlobalHandler;
import com.floatapplication.util.TransitionHandler;
import com.simplexorg.mapfragment.model.GeoPoint;

/**
 * Created by william on 2018-05-06.
 * Activity for users to modify and look at their own profile.
 */
public class UserActivity extends AppCompatActivity
        implements
        UserShopItemListener,
        OnUserProfileClickListener,
        OnShopConstructedListener {
    private static final String TAG = UserActivity.class.getSimpleName();
    private static final int REQUEST_READ_EXTERNAL_PERMISSION_COVER = 12;
    private static final int REQUEST_READ_EXTERNAL_PERMISSION_PROFILE = 13;
    public static final int REQUEST_PROFILE_IMAGE_CHANGE = 11;
    public static final int REQUEST_COVER_IMAGE_CHANGE = 10;
    public static final int REQUEST_PROFILE_IMAGE_UPLOADED = 100;
    public static final int REQUEST_COVER_IMAGE_UPLOADED = 101;
    public static final int REQUEST_SHOP_EDIT = 102;

    private UserFragment mUserFragment;
    private TransitionHandler mTransitionHandler;
    private ShopHandler mShopHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        mTransitionHandler = new TransitionHandler();
        mShopHandler = new ShopHandler();
        mUserFragment = new UserFragment();
        Bundle extras = getIntent().getExtras();
        mUserFragment.setArguments(extras);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragment, mUserFragment)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTransitionHandler.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mTransitionHandler.onActivityResult();
        if (requestCode == REQUEST_PROFILE_IMAGE_CHANGE) {
            if (resultCode == RESULT_OK) {
                requestImageChange(REQUEST_PROFILE_IMAGE_UPLOADED, REQUEST_PROFILE_IMAGE_CHANGE, data);
            }
        } else if (requestCode == REQUEST_COVER_IMAGE_CHANGE) {
            if (resultCode == RESULT_OK) {
                requestImageChange(REQUEST_COVER_IMAGE_UPLOADED, REQUEST_COVER_IMAGE_CHANGE, data);
            }
        } else if (requestCode == REQUEST_PROFILE_IMAGE_UPLOADED) {
            if (resultCode == RESULT_OK) {
                mUserFragment.getPresenter().reloadCoverImage();
            }
        } else if (requestCode == REQUEST_COVER_IMAGE_UPLOADED) {
            if (resultCode == RESULT_OK) {
                mUserFragment.getPresenter().reloadProfileImage();
            }
        } else if (requestCode == REQUEST_SHOP_EDIT) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    if(data.getStringExtra(ShopEditActivity.EDIT_TYPE).equals(ShopEditActivity.Type.delete.toString())){
                        mUserFragment.getPresenter().removeShop(data.getStringExtra(Shop.ID));
                    } else if(data.getStringExtra(ShopEditActivity.EDIT_TYPE).equals(ShopEditActivity.Type.change.toString())){
                        mUserFragment.getPresenter().refreshShop(data.getStringExtra(Shop.ID));
                    }
                }
            }
        }
    }

    private void requestImageChange(int requestCode, int operationCode, Intent data) {
        Uri selectedImage = data.getData();
        String path = GlobalHandler.getInstance().getAbsolutePathFromURI(this, selectedImage);
        if (GlobalHandler.getInstance().fileIsImage(path)) {
            Intent intent = new Intent();
            intent.putExtra("image_path", path);
            intent.putExtra("operation", operationCode);
            mTransitionHandler.transitionForResult(this, ImageUploadPreviewActivity.class, intent,
                    requestCode);
        } else {
            Toast.makeText(this, getString(R.string.invalid_image), Toast.LENGTH_SHORT).show();
        }
    }

    private void requestPermission(int requestCode) {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_EXTERNAL_PERMISSION_PROFILE || requestCode == REQUEST_READ_EXTERNAL_PERMISSION_COVER) {
            if (permissions.length == 1 &&
                    permissions[0].equals(permission.READ_EXTERNAL_STORAGE) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (requestCode == REQUEST_READ_EXTERNAL_PERMISSION_COVER) {
                    mTransitionHandler.transitionForResult(this, Intent.ACTION_PICK,
                            MediaStore.Images.Media.INTERNAL_CONTENT_URI, REQUEST_COVER_IMAGE_CHANGE);
                } else {
                    mTransitionHandler.transitionForResult(this, Intent.ACTION_PICK,
                            MediaStore.Images.Media.INTERNAL_CONTENT_URI, REQUEST_PROFILE_IMAGE_CHANGE);
                }
            }
        }
    }

    @Override
    public void onProfileImageClicked(String url) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        ZoomInPictureFragment zoomInPictureFragment = new ZoomInPictureFragment();
        Bundle args = new Bundle();
        args.putString("url", url);
        zoomInPictureFragment.setArguments(args);
        fragmentTransaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        fragmentTransaction.add(R.id.fragment, zoomInPictureFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onProfileBackgroundImageClicked(String url) {
        onProfileImageClicked(url);
    }

    @Override
    public void onMessageButtonClicked(NamePair namePair) {
        if(FloatApplication.getUserSessionInterface().isLoggedIn()) {
            Intent data = new Intent();
            data.putExtra(getString(R.string.contact), namePair.username);
            data.putExtra(getString(R.string.chat_username), FloatApplication.getUserSessionInterface().getUsername());
            mTransitionHandler.transition(this, MessagesActivity.class, data);
        } else{
            mTransitionHandler.transition(this, LoginActivity.class);
        }
    }

    @Override
    public void onNavigationBackClicked() {
        mTransitionHandler.transition(this, MapMainActivity.class);
    }

    @Override
    public void onChangeProfileImageClicked() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(REQUEST_READ_EXTERNAL_PERMISSION_PROFILE);
        } else {
            mTransitionHandler.transitionForResult(this, Intent.ACTION_PICK,
                    MediaStore.Images.Media.INTERNAL_CONTENT_URI, REQUEST_PROFILE_IMAGE_CHANGE);
        }
    }

    @Override
    public void onChangeProfileBackgroundClicked() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermission(REQUEST_READ_EXTERNAL_PERMISSION_COVER);
        } else {
            mTransitionHandler.transitionForResult(this, Intent.ACTION_PICK,
                    MediaStore.Images.Media.INTERNAL_CONTENT_URI, REQUEST_COVER_IMAGE_CHANGE);
        }
    }

    @Override
    public void onGoToShopClicked(String shopId) {
        Log.d(TAG, "GoToShopId: " + shopId);
        mShopHandler.getShop(shopId, this);
    }

    @Override
    public void onGoToShopLocationClicked(GeoPoint location) {
        Intent data = new Intent();
        data.setAction(MapMainActivity.ACTION_GO_TO_LOCATION);
        data.putExtra("location", location);
        mTransitionHandler.transition(this, MapMainActivity.class, data);
    }

    @Override
    public void onShopEditClicked(String shopId, String shopName) {
        Intent data = new Intent();
        Shop shop = Shop.createShop(shopId, shopName, mUserFragment.getPresenter().getUserNamePair());
        data.putExtra("shop", shop);
        mTransitionHandler.transitionForResult(this, ShopEditActivity.class, data, REQUEST_SHOP_EDIT);
    }

    @Override
    public void onGoToShopInboxClicked(String shopId){
        Intent data = new Intent();
        data.putExtra("username",shopId);
        mTransitionHandler.transition(this, DialogsActivity.class, data);
    }

    @Override
    public void onShopConstructed(Shop shop) {
        Intent data = new Intent();
        data.putExtra("shop", shop);
        mTransitionHandler.transition(this, ShopViewActivity.class, data);
    }
}
