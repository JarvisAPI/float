package com.floatapplication.product.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.GridLayout.LayoutParams;

import com.floatapplication.R;
import com.floatapplication.product.models.Product;
import com.pchmn.materialchips.ChipView;
import com.pchmn.materialchips.ChipView.OnChipClickListener;

/**
 * Created by william on 2018-05-29.
 * Presents the category item.
 */

public class ProductCreationCategoryItemPresenter
        implements
        OnChipClickListener {

    private static final String TAG = ProductCreationCategoryItemPresenter.class.getSimpleName();
    private ChipView mCurrentChip;
    private Context mContext;
    private GridLayout mLayout;
    private String[] mCategories;

    public ProductCreationCategoryItemPresenter(@NonNull Context context) {
        mContext = context;
    }

    public void bind(@NonNull GridLayout layout, @NonNull String[] categories) {
        mLayout = layout;
        mCategories = categories;
    }

    /**
     * Must call bind before calling this method.
     */
    public void present() {
        for (String category : mCategories) {
            LayoutParams params = new LayoutParams();
            params.setMarginEnd(8);
            mLayout.addView(makeChip(category), params);
        }
    }

    void selectCategory(@NonNull String category) {
        for(int i = 0; i < mLayout.getChildCount(); i++) {
            ChipView chipView = (ChipView) mLayout.getChildAt(i);
            if (category.toLowerCase().equals(chipView.getLabel().toLowerCase())) {
                onChipClick(chipView);
            }
        }
    }

    private ChipView makeChip(String category) {
        ChipView chipView = new ChipView(mContext);
        chipView.setLabel(category);
        chipView.setChipBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimaryLight));
        chipView.setLabelColor(ContextCompat.getColor(mContext, R.color.white));
        chipView.setOnChipClicked(this);
        return chipView;
    }

    public String getSelectedCategory() {
        if (mCurrentChip != null) {
            return mCurrentChip.getLabel();
        }
        return Product.NONE_CATEGORY;
    }

    @Override
    public void onChipClick(ChipView chipView) {
        if (mCurrentChip != null) {
            mCurrentChip.setChipBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimaryLight));
        }
        if (mCurrentChip == chipView) {
            mCurrentChip = null;
            return;
        }
        chipView.setChipBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        mCurrentChip = chipView;
    }
}
