package com.floatapplication.search;


import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.util.Extractor;
import com.floatapplication.util.IFactory;
import com.floatapplication.util.Util;
import com.simplexorg.mapfragment.model.GeoPoint;
import com.simplexorg.searchfragment.model.Suggestion;
import com.simplexorg.searchfragment.search.SearchData;
import com.simplexorg.searchfragment.search.SearchSuggestionSupplier.OnSuggestionObtainedListener;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import java.util.List;
import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class SearchHandlerUnitTest {
    private SearchHandler mSearchHandler;
    @Mock private IFactory mIFactory;
    @Mock private Util mUtil;
    @Mock private Extractor mExtractor;
    @Mock private Map queries;
    @Mock private HypermediaInterface mHypermedia;

    @SuppressWarnings("unchecked")
    @Before
    public void setup() {
        initMocks(this);

        when(mIFactory.createMap()).thenReturn(queries);
        IFactory.setIFactory(mIFactory);
        Util.setUtil(mUtil);
        Extractor.setExtractor(mExtractor);
        FloatApplication.setHypermediaInterface(mHypermedia);

        mSearchHandler = new SearchHandler();
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_getSuggestion_setup() {
        SearchData searchData = mock(SearchData.class);
        String query = "query";
        GeoPoint location = mock(GeoPoint.class);
        when(location.toGeoJSON()).thenReturn("[1,0]");

        when(searchData.get(SearchQuery.QUERY)).thenReturn(query);
        when(searchData.get(SearchQuery.LOCATION)).thenReturn(location);

        mSearchHandler.getSuggestion(searchData, mock(OnSuggestionObtainedListener.class));

        verify(queries).put(SearchQuery.QUERY, query);
        verify(queries).put(SearchQuery.LOCATION, location.toGeoJSON());
        verify(mUtil).buildQuery(queries);
        verify(mHypermedia).follow(eq(HypermediaInterface.GET), eq(HypermediaInterface.SEARCH_PRODUCTS), anyMap(), anyMap(), eq(queries),
                any(ServerCallback.class), any(ServerErrorCallback.class));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_getSuggestion_callback() {
        String response = "response";
        List<Suggestion> suggestions = mock(List.class);
        SearchData searchData = mock(SearchData.class);
        String query = "query";
        GeoPoint location = mock(GeoPoint.class);
        when(searchData.get(SearchQuery.QUERY)).thenReturn(query);
        when(searchData.get(SearchQuery.LOCATION)).thenReturn(location);

        OnSuggestionObtainedListener listener = mock(OnSuggestionObtainedListener.class);
        when(mExtractor.extractSuggestions(response)).thenReturn(suggestions);

        mSearchHandler.getSuggestion(searchData, listener);

        ArgumentCaptor<ServerCallback> argumentCaptor = ArgumentCaptor.forClass(ServerCallback.class);
        verify(mHypermedia).follow(eq(HypermediaInterface.GET), eq(HypermediaInterface.SEARCH_PRODUCTS), anyMap(), anyMap(), eq(queries),
                argumentCaptor.capture(), any(ServerErrorCallback.class));

        argumentCaptor.getValue().onSuccessResponse(response);
        verify(mExtractor).extractSuggestions(response);
        verify(listener).onSuggestionObtained(suggestions);
    }
}
