package com.floatapplication.shop.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.R;
import com.floatapplication.product.adapter.ProductAdapter.OnBottomReachedListener;
import com.floatapplication.product.models.Product;
import com.floatapplication.shop.adapter.ShopViewAdapter.ProductViewHolder;
import com.floatapplication.shop.util.ShopViewProductPresenter;
import com.floatapplication.util.GlideApp;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by william on 2018-05-09.
 * Recycler view adapter for the shop.
 */

public class ShopViewAdapter extends RecyclerView.Adapter<ProductViewHolder> {
    private static final String TAG = ShopViewAdapter.class.getSimpleName();
    private List<Product> mShopItems;
    private Context mContext;

    private OnBottomReachedListener mBottomReachedListener;
    private ShopViewProductPresenter mPresenter;

    public ShopViewAdapter(Context context) {
        mContext = context;
        mPresenter = new ShopViewProductPresenter()
                .setGlide(GlideApp.with(mContext));
        mShopItems = new ArrayList<>();
    }

    public ShopViewProductPresenter getPresenter() {
        return mPresenter;
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.list_shop_item, parent, false);
        return new ProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        mPresenter.present(holder.itemView, holder.mProductImage, holder.mProductName,
                holder.mProductPrice, mShopItems.get(position));
        if (position == mShopItems.size() - 1) {
            if (mBottomReachedListener != null) {
                mBottomReachedListener.onBottomReached();
            }
        }
    }

    @Override
    public int getItemCount() {
        return mShopItems.size();
    }

    public void setOnBottomReachedListener(OnBottomReachedListener listener) {
        mBottomReachedListener = listener;
    }

    public void addShopItems(List<Product> shopItems) {
        int positionStart = mShopItems.size();
        mShopItems.addAll(shopItems);
        notifyItemRangeInserted(positionStart, shopItems.size());
    }

    static class ProductViewHolder extends RecyclerView.ViewHolder {
        private ImageView mProductImage;
        private TextView mProductName;
        private TextView mProductPrice;

        private ProductViewHolder(View view) {
            super(view);
            mProductName = view.findViewById(R.id.product_name);
            mProductPrice = view.findViewById(R.id.product_price);
            mProductImage = view.findViewById(R.id.product_image);
        }
    }
}
