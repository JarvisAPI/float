package com.floatapplication.messages.server_connection;

import android.content.Context;
import android.util.Log;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.messages.activity.activity_helpers.MessageLoader;
import com.floatapplication.messages.message_database.InboxDatabaseHelper;
import com.floatapplication.messages.message_database.MessageDatabaseHelper;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.models.Dialog;
import com.floatapplication.messages.models.User;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *  Class that handles messages syncing between devices and archives
 */
public class MessageSyncManager {
    public interface onMessageSyncCompleteListener{
        void onMessageSyncComplete();
    }
    private String TAG = "MessageSyncManager";
    private Context mContext;
    private ChatConnection connection;
    private ArrayList<CustomMessage> lastMessages;
    private MessageDatabaseHelper mMessageDatabase;
    private InboxDatabaseHelper mInboxDatabase;
    private ArchiveMessageManager mArchiveMessageManager;
    private ArrayList<Dialog> mDialogs;
    private boolean isDialogReady = false;

    public MessageSyncManager(ArrayList<CustomMessage> lm, Context context, ChatConnection connection){
        this.connection = connection;
        this.lastMessages = lm;
        this.mContext = context;
        this.mMessageDatabase = FloatApplication.getFactoryInterface().getMessageDatabase(connection.getUsername());
        this.mInboxDatabase = FloatApplication.getFactoryInterface().getInboxDatabase(connection.getUsername());
    }

    void setArchiveMessageManager(ArchiveMessageManager amm){
        this.mArchiveMessageManager = amm;
    }

    public void fetchServerDialogs(){
        Map<String, String> queries = new HashMap<>();
        if(!FloatApplication.getUserSessionInterface().getUsername().equals(connection.getUsername())){
            queries.put("shop_id", connection.getUsername());
        }
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.GET,
                mContext, HypermediaInterface.GET_CHAT_DIALOGS, queries, new ServerCallback() {
                    @Override
                    public void onSuccessResponse(String response) {
                        Log.d(TAG, "chat dialogs retrieved");
                        try {
                            mDialogs = new ArrayList<>();
                            JSONArray serverDialogs = (new JSONObject(response)).getJSONArray(ChatConnectionService.JSON_DIALOG_LIST_KEY);
                            for (int i = 0; i < serverDialogs.length(); i++) {
                                JSONObject jsonDialog = serverDialogs.getJSONObject(i);
                                String jid = User.formatJid(jsonDialog.getString("bare_peer"));
                                Dialog dialog= createDialog(jid, jsonDialog);
                                mInboxDatabase.addData(dialog);
                                mDialogs.add(createDialog(jid, jsonDialog));
                            }
                            ArrayList<String> usernames = new ArrayList<>();
                            for(Dialog d: mDialogs){
                                usernames.add(d.getUsers().get(0).getId());
                            }
                            FloatApplication.getFactoryInterface().getChatUserCache().updateCachedUsers(usernames);
                            isDialogReady = true;
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new ServerErrorCallback() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        connection.onMessageSyncComplete();
                    }
                });
    }

    void syncDialogs() throws JSONException {
        if(isDialogReady) {
            for (Dialog d: mDialogs) {
                handleSync(d);
            }
            connection.onMessageSyncComplete();
        }
    }

    private void handleSync(Dialog dialog) throws JSONException {
        String jid = dialog.getId();
        CustomMessage msg = findMessage(jid);
        if (msg != null) {
            handleMessage(dialog, msg, jid);
        }
        else {
            dialogDoesNotExist(dialog);
        }
    }

    private void handleMessage(Dialog dialog, CustomMessage msg, String jid) throws JSONException {
        String latestMessageId = dialog.getLastMessage().getId();
        Log.d(TAG,latestMessageId);
        Log.d(TAG,msg.getId());
        if(!latestMessageId.equals(msg.getId())) {
            ArrayList<CustomMessage> archives = mArchiveMessageManager.getArchiveMessagesAfter(msg, MessageLoader.PAGE_SIZE);
            if(!archives.isEmpty() && latestMessageId.equals(archives.get(archives.size()-1).getId())){
                // Most recent message matches in this case the device only needs to load unread message count
                mInboxDatabase.addData(createDialog(archives.get(archives.size() - 1) , dialog.getUnreadCount()));
            }
            else {
                handleLatestMessageMissMatch(dialog, jid);
            }
        }
        else {
            // The purpose of this line is to add the unread count to the local device
            mInboxDatabase.addData(dialog);
        }
    }

    private CustomMessage findMessage(String jid) {
        for(CustomMessage msg: lastMessages){
            if(jid.equals(msg.getUser().getId())){
                return msg;
            }
        }
        return null;
    }

    private void handleLatestMessageMissMatch(Dialog dialog, String jid) throws JSONException {
        mMessageDatabase.flushConversation(jid);
        mInboxDatabase.addData(dialog);
        mMessageDatabase.addData(dialog.getLastMessage());
    }

    private void dialogDoesNotExist(Dialog dialog) throws JSONException {
        mInboxDatabase.addData(dialog);
        mMessageDatabase.addData(dialog.getLastMessage());
    }

    private Dialog createDialog(CustomMessage msg, int unreadCount){
        String jid = msg.getUser().getId();
        ArrayList<User> users = new ArrayList<>();
        users.add(msg.getUser());
        return new Dialog(jid, users,msg, unreadCount);
    }

    private Dialog createDialog(String jid, JSONObject jsonDialog) throws JSONException {
        // The archive id is the timestamp however the server uses bigint which is 10 ^ 18 causing an overflow
        String archiveId = jsonDialog.getString("timestamp");
        String timeStamp = archiveId;
        int javaUnixTimeStampLength = 13;
        if(timeStamp.length() > javaUnixTimeStampLength) {
            timeStamp = timeStamp.substring(0, javaUnixTimeStampLength);// prevent overflow
        }
        String xmlMessage = jsonDialog.getString("xml");

        User user = new User(jid);
        ArrayList<User> users = new ArrayList<>();
        users.add(user);

        String from = getFrom(xmlMessage);
        ChatConnectionService.MessageType type = from.equals(connection.getUsername()) ? ChatConnectionService.MessageType.SEND : ChatConnectionService.MessageType.RECEIVE;
        CustomMessage lastMsg = new CustomMessage(getMessageId(xmlMessage), user, Long.parseLong(timeStamp), jsonDialog.getString("txt"), archiveId, type);
        return new Dialog(jid,  users, lastMsg,jsonDialog.getInt("unread_count"));
    }

    private String getMessageId(String xmlMessage){
        String regex = "id='";
        String msgId = xmlMessage.substring(xmlMessage.indexOf(regex)+regex.length());
        return msgId.substring(0, msgId.indexOf("'"));
    }

    private String getFrom(String xmlMessage){
        String regex = "from='";
        String msgId = xmlMessage.substring(xmlMessage.indexOf(regex)+regex.length());
        String bareFrom = msgId.substring(0, msgId.indexOf("'"));
        return User.formatJid(bareFrom);
    }
}