package com.floatapplication.shop.edit;

import android.content.Context;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.config.ConfigStore;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.util.IFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Model for editing shop info.
 */
public class ShopAboutEditModel implements ShopAboutEditContract.Model {
    private Shop mShop;
    private Context mContext;
    private Observer mObserver0;
    private Observer mObserver1;

    // The new images to upload.
    private List<String> mImagePaths;

    private int mUpdateCounter;

    ShopAboutEditModel() {
        mImagePaths = new ArrayList<>();
        mUpdateCounter = 0;
    }

    public void init(Context context, Shop shop) {
        mContext = context;
        mShop = shop;
    }

    @Override
    public void retrieveShopInfo() {
        Map<String, String> vMap = IFactory.getInstance().createMap();
        vMap.put(Shop.ID, mShop.getShopId());
        FloatApplication.getHypermediaInterface().follow(Hypermedia.GET, HypermediaInterface.SHOP_INFO,
                vMap, null,
                (String response) -> {
                    mShop.augmentShopInfo(response);
                    notifyObservers(Observer.INFO_RECEIVED);
                },
                (VolleyError error) -> {
                    error.printStackTrace();
                    notifyObservers(Observer.ERROR_NO_INFO);
                });
    }

    @Override
    public void submitEditUpdate(String shopDescription) {
        mUpdateCounter++;
        Map<String, String> vMap = IFactory.getInstance().createMap();
        vMap.put(Shop.ID, mShop.getShopId());
        String body = getUpdateShopBody(shopDescription);
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.PUT, mContext, Hypermedia.EDIT_SHOP,
                vMap, body,
                (String response) -> completeSingleUpdate(),
                (VolleyError error) -> {
                    error.printStackTrace();
                    notifyObservers(Observer.ERROR);
                });

    }

    @Override
    public boolean submitUploadImages() {
        mUpdateCounter += mImagePaths.size();

        for (String imagePath : mImagePaths) {
            Map<String, String> vMap = IFactory.getInstance().createMap();
            vMap.put(Shop.ID, mShop.getShopId());
            String uploadPath = FloatApplication.getHypermediaInterface().getPath(Hypermedia.UPLOAD_SHOP_IMAGE, vMap);
            FloatApplication.getHypermediaInterface().uploadImage(mContext,
                    uploadPath,
                    imagePath,
                    (String response) -> completeSingleUpdate(),
                    (VolleyError error) -> {
                        error.printStackTrace();
                        notifyObservers(Observer.ERROR);
                    });
        }
        return mImagePaths.size() > 0;
    }

    private void completeSingleUpdate() {
        if (--mUpdateCounter == 0) {
            notifyObservers(Observer.INFO_UPDATED);
        }
    }

    private String getUpdateShopBody(String description) {
        try {
            JSONObject shopEditJson = IFactory.getInstance().create(JSONObject.class)
                    .put(Shop.DESCRIPTION, description);
            return IFactory.getInstance().create(JSONObject.class)
                    .put("shop_edit", shopEditJson)
                    .toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "{}";
    }

    @Override
    public void deleteShop() {
        Map<String, String> vMap = IFactory.getInstance().createMap();
        vMap.put(Shop.ID, mShop.getShopId());
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.DELETE, mContext,
                HypermediaInterface.DELETE_SHOP,
                vMap, null,
                (String response) -> notifyObservers(Observer.SHOP_DELETED),
                (VolleyError error) -> {
                    error.printStackTrace();
                    notifyObservers(Observer.SHOP_DELETE_FAILED);
                });
    }

    @Override
    public void addObserver(Observer observer) {
        if (mObserver0 == null) {
            mObserver0 = observer;
            return;
        }
        if (mObserver1 == null) {
            mObserver1 = observer;
        }
    }

    @Override
    public Shop getShop() {
        return mShop;
    }

    @Override
    public void addUploadImage(String imagePath) {
        mImagePaths.add(imagePath);
    }

    @Override
    public void removeUploadImages(List<String> imagePaths) {
        mImagePaths.removeAll(imagePaths);
    }

    @Override
    public void removeUploadImage(String imagePath) {
        mImagePaths.remove(imagePath);
    }

    @Override
    public boolean containsUploadImage(String imagePath) {
        return mImagePaths.contains(imagePath);
    }

    @Override
    public List<String> getUploadImagePaths() {
        List<String> imagePaths = new ArrayList<>();
        imagePaths.addAll(mImagePaths);
        return imagePaths;
    }

    @Override
    public void submitNewMainImage(String imagePath) {
        mUpdateCounter++;
        Map<String, String> vMap = IFactory.getInstance().createMap();
        vMap.put(Shop.ID, mShop.getShopId());
        String uploadPath = FloatApplication.getHypermediaInterface().getPath(Hypermedia.UPLOAD_SHOP_MAIN_IMAGE, vMap);
        FloatApplication.getHypermediaInterface().uploadImage(mContext,
                uploadPath,
                imagePath,
                (String response) -> completeSingleUpdate(),
                (VolleyError error) -> {
                    error.printStackTrace();
                    notifyObservers(Observer.ERROR);
                });
    }

    @Override
    public void deleteImages(List<String> imageFiles) {
        mUpdateCounter++;
        Map<String, String> vMap = IFactory.getInstance().createMap();
        vMap.put(Shop.ID, mShop.getShopId());
        vMap.put("image_files", new JSONArray(imageFiles).toString());
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.DELETE, mContext,
                Hypermedia.DELETE_SHOP_IMAGES, vMap, null,
                (String response) -> completeSingleUpdate(),
                (VolleyError error) -> {
                    error.printStackTrace();
                    notifyObservers(Observer.ERROR);
                });
    }

    @Override
    public void changeMainImage(String imageFile) {
        mUpdateCounter++;
        Map<String, String> vMap = IFactory.getInstance().createMap();
        vMap.put(Shop.ID, mShop.getShopId());

        String body;
        try {
            body = new JSONObject()
                    .put("image_file", imageFile)
                    .toString();
        } catch (JSONException e) {
            e.printStackTrace();
            body = "{}";
        }
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.PUT, mContext,
                Hypermedia.SWAP_SHOP_MAIN_IMAGE, vMap, body,
                (String response) -> completeSingleUpdate(),
                (VolleyError error) -> {
                    error.printStackTrace();
                    notifyObservers(Observer.ERROR);
                });
    }

    @Override
    public int getNumUploadImages() {
        return mImagePaths.size();
    }

    @Override
    public int getMaxShopImageUploads() {
        return ConfigStore.getInstance().getMaxShopImageUploads(mContext);
    }

    private void notifyObservers(int event) {
        if (mObserver0 != null) {
            mObserver0.update(event);
        }
        if (mObserver1 != null) {
            mObserver1.update(event);
        }
    }
}
