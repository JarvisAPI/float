package com.floatapplication.shop.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.floatapplication.R;
import com.floatapplication.map.models.Icon;
import com.floatapplication.map.models.Icon.IconPair;
import com.floatapplication.shop.adapter.ShopCreationIconsAdapter;
import com.floatapplication.util.Util;
import com.simplexorg.mapfragment.model.GeoPoint;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * fragment for shop creation.
 */

public class ShopCreationFragment extends Fragment implements FragmentCallback {
    private static final String TAG = ShopCreationFragment.class.getSimpleName();
    private static final String KEY_ICON_TYPE = "icon_type";
    private static final String KEY_SHOP_LOC = "shopLoc";
    public interface OnCompleteListener {
        void onComplete();
    }

    private OnClickListener mSetLocationListener;
    private OnCompleteListener mCompleteListener;
    private GeoPoint mShopLocation;

    private ShopCreationIconsAdapter mIconsAdapter;

    @BindView(R.id.address)
    TextView mAddressText;
    @BindView(R.id.set_location_button)
    Button mSetLocationButton;
    @BindView(R.id.input_shop_name)
    TextInputEditText mShopNameText;
    @BindView(R.id.input_shop_description)
    TextInputEditText mShopDescriptionText;

    @BindView(R.id.toolbar_progress_bar)
    ProgressBar mProgressBar;

    private Unbinder mUnbinder;

    private MenuItem mDoneButton;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shop_creation, container, false);
        mUnbinder = ButterKnife.bind(this, v);
        mIconsAdapter = new ShopCreationIconsAdapter();
        mIconsAdapter.setIconIds(Icon.getIconIds());
        
        if (savedInstanceState != null) {
            int notFound = 404;
            int type = savedInstanceState.getInt(KEY_ICON_TYPE, notFound);
            if (type != notFound) {
                mIconsAdapter.setSelectedIconType(type);
            }
            mShopLocation = savedInstanceState.getParcelable(KEY_SHOP_LOC);
            if (mShopLocation != null) {
                Util.getInstance().getAddress(getContext(), mShopLocation, mAddressText::setText);
            }
        }

        mSetLocationButton = v.findViewById(R.id.set_location_button);
        mSetLocationButton.setOnClickListener(mSetLocationListener);
        setupActionBar(v);
        setHasOptionsMenu(true);
        setupIconsRecyclerView(v);
        return v;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_ICON_TYPE, mIconsAdapter.getSelectedIconPair().type);
        outState.putParcelable(KEY_SHOP_LOC, mShopLocation);
    }

    public void setListeners(OnClickListener setLocationListener, OnCompleteListener completeListener) {
        mSetLocationListener = setLocationListener;
        mCompleteListener = completeListener;
    }

    public GeoPoint getShopLocation() {
        return mShopLocation;
    }

    private void setupActionBar(View v) {
        Toolbar toolbar = v.findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        if (activity != null) {
            activity.setSupportActionBar(toolbar);

            ActionBar actionBar = activity.getSupportActionBar();
            Log.d(TAG, "actionBar: " + actionBar);
            if (actionBar != null) {
                actionBar.setTitle(R.string.shop_creation_title);
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    private void setupIconsRecyclerView(View v) {
        Context context = v.getContext();
        RecyclerView recyclerView = v.findViewById(R.id.icons_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(context, LinearLayoutManager.HORIZONTAL));
        recyclerView.setAdapter(mIconsAdapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.confirm_menu, menu);
        mDoneButton = menu.findItem(R.id.action_done);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        // To avoid memory leaks
        mDoneButton = null;
        mIconsAdapter = null;
        mUnbinder.unbind();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                checkCompletion();
                return true;
            case android.R.id.home:
                Activity activity = getActivity();
                if (activity != null) {
                    activity.finish();
                }
                return true;
        }
        return false;
    }

    public void setShopLocation(GeoPoint shopLocation) {
        mShopLocation = shopLocation;
    }

    public IconPair getShopIconPair() {
        if (mIconsAdapter != null) {
            // Null check due to orientation change, mIconsAdapter could be null here.
            return mIconsAdapter.getSelectedIconPair();
        }
        return null;
    }

    @Override
    public void onResumeFragment(Bundle data) {
        if (mShopLocation != null) {
            Util.getInstance().getAddress(getContext(), mShopLocation, mAddressText::setText);
        }
    }

    private void checkCompletion() {
        if (verifyShopInfo()) {
            if (mDoneButton != null) {
                mDoneButton.setVisible(false);
            }
            mProgressBar.setVisibility(View.VISIBLE);
            mCompleteListener.onComplete();
        }
    }

    private boolean verifyShopInfo() {
        mShopNameText.setError(null);
        mShopDescriptionText.setError(null);

        boolean noError = true;
        if (mShopLocation == null) {
            Toast.makeText(getContext(), "Shop location required!", Toast.LENGTH_SHORT).show();
            noError = false;
        }
        if ("".equals(mShopNameText.getText().toString())) {
            mShopNameText.setError("Shop name is required");
            noError = false;
        }
        return noError;
    }

    public String obtainShopBody() {
        try {
            JSONObject shopJson = new JSONObject()
                    .put("shop_id", UUID.randomUUID().toString())
                    .put("shop_name", mShopNameText.getText().toString().trim())
                    .put("shop_description", mShopDescriptionText.getText().toString().trim())
                    .put("shop_icon_type", Icon.getIconType(mIconsAdapter.getSelectedIconPair().res))
                    .put("shop_icon_latitude", mShopLocation.latitude)
                    .put("shop_icon_longitude", mShopLocation.longitude);

            return new JSONObject()
                    .put("shop", shopJson)
                    .toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void onShopCreationError() {
        mDoneButton.setVisible(true);
        mProgressBar.setVisibility(View.GONE);
    }

    public void clearError() {
        mShopNameText.setError(null);
        mShopDescriptionText.setError(null);
    }
}
