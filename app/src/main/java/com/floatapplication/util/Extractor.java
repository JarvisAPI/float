package com.floatapplication.util;

import android.support.annotation.VisibleForTesting;

import com.floatapplication.product.models.Product;
import com.simplexorg.searchfragment.model.Suggestion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/*
 * Extracts information obtained from the server and convert it into useful objects.
 */
public class Extractor {
    private static Extractor mExtractor;
    private ProductExtractor mProductExtractor;
    private SuggestionExtractor mSuggestionExtractor;

    private Extractor() {

    }

    private abstract class BaseExtractorTemplate<T> {
        List<T> extractProducts(String response) {
            List<T> items = new ArrayList<>();
            try {
                JSONArray productsJsonArr = new JSONObject(response).getJSONArray(Product.LIST);
                for (int i = 0; i < productsJsonArr.length(); i++) {
                    JSONObject productJson = productsJsonArr.getJSONObject(i);
                    Product product = Product.createBasicProduct(productJson);
                    items.add(doExtractFromProduct(product));
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            return items;
        }

        protected abstract T doExtractFromProduct(Product product);
    }

    private class ProductExtractor extends BaseExtractorTemplate<Product> {
        @Override
        protected Product doExtractFromProduct(Product product) {
            return product;
        }
    }

    private class SuggestionExtractor extends BaseExtractorTemplate<Suggestion> {
        @Override
        protected Suggestion doExtractFromProduct(Product product) {
            return new Suggestion(product.getProductName(), product, Product.class.getName());
        }
    }

    public List<Product> extractProducts(String response) {
        if (mProductExtractor == null) {
            mProductExtractor = new ProductExtractor();
        }
        return mProductExtractor.extractProducts(response);
    }


    public List<Suggestion> extractSuggestions(String response) {
        if (mSuggestionExtractor == null) {
            mSuggestionExtractor = new SuggestionExtractor();
        }
        return mSuggestionExtractor.extractProducts(response);
    }

    public static Extractor getInstance() {
        if (mExtractor == null) {
            mExtractor = new Extractor();
        }
        return mExtractor;
    }

    @VisibleForTesting
    public static void setExtractor(Extractor extractor) {
        mExtractor = extractor;
    }
}
