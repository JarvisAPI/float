package com.floatapplication.hypermedia;

import android.content.Context;
import android.support.annotation.NonNull;

import com.android.volley.Request.Method;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;

import java.util.Map;

/**
 * Created by william on 2017/12/26.
 * An interface used to store, update and manipulate the state of the hypermedia.
 */
public interface HypermediaInterface {
    String SELF = "self";
    String DEFAULT_CONFIG = "default_config";

    String LOGIN = "login";
    String SIGN_UP = "sign_up";
    String MAP_ICONS = "map_icons";
    String SHOP_INFO = "shop_info";
    String IMAGE = "image";
    String MAKE_SHOP = "make_shop";
    String MAKE_PRODUCT = "make_product";
    String GET_CHAT_DIALOGS = "get_chat_dialogs";
    String SET_CONVERSATION_MESSAGE_COUNT = "set_conversation_message_count";
    String UPDATE_CHAT_NOTIFICATION_TOKEN = "update_chat_notification_token";
    String GET_CHAT_PROFILES = "get_chat_profiles";
    String EDIT_PRODUCT = "edit_product";
    String USER_PROFILE = "user_profile";
    String SEND_IMAGE_CHAT = "send_image_chat";
    String EDIT_SHOP = "edit_shop";
    String DELETE_SHOP = "delete_shop";
    String OAUTH = "oauth";

    String ADD_USER_PROFILE_IMAGE = "add_user_profile_image";
    String ADD_USER_BACKGROUND_IMAGE = "add_user_background_image";
    String NEARBY_SHOPS = "nearby_shops";

    String UPLOAD_PRODUCT_MAIN_IMAGE = "upload_product_main_image";
    String UPLOAD_PRODUCT_IMAGE = "upload_product_image";
    String SHOP_PRODUCTS = "shop_products";
    String USER_SHOPS = "user_shops";
    String DELETE_PRODUCT = "delete_product";
    String PRODUCT_DETAILS = "product_details";
    String GET_CHAT_TOKEN = "get_chat_token";
    String SEARCH_PRODUCTS = "search_products";
    String UPLOAD_SHOP_IMAGE = "upload_shop_image";
    String UPLOAD_SHOP_MAIN_IMAGE = "upload_shop_main_image";
    String SWAP_SHOP_MAIN_IMAGE = "swap_shop_main_image";
    String DELETE_SHOP_IMAGES = "delete_shop_images";

    String SWAP_PRODUCT_MAIN_IMAGE = "swap_product_main_image";
    String DELETE_PRODUCT_MAIN_IMAGE = "delete_product_main_image";
    String DELETE_PRODUCT_IMAGES = "delete_product_images";

    int GET = Method.GET;
    int POST = Method.POST;
    int DELETE = Method.DELETE;
    int PUT = Method.PUT;

    boolean hasKey(String key);

    String getPath(String key, Map<String, String> vMap);

    /**
     * Initializes the hypermedia.
     */
    void init(Map<String, String> headers,
              @NonNull ServerCallback callback,
              @NonNull ServerErrorCallback errorCallback);

    void follow(int method,
                String hypermediaKey,
                Map<String, String> vMap,
                Map<String, String> headers,
                @NonNull ServerCallback callback,
                @NonNull ServerErrorCallback errorCallback);

    void follow(int method,
                String hypermediaKey,
                Map<String, String> vMap,
                Map<String, String> headers,
                String jsonBody,
                @NonNull ServerCallback callback,
                @NonNull ServerErrorCallback errorCallback);

    void follow(int method,
                String hypermediaKey,
                Map<String, String> vMap,
                Map<String, String> headers,
                Map<String, String> queries,
                @NonNull ServerCallback callback,
                @NonNull ServerErrorCallback errorCallback);

    void follow(int method,
                String hypermediaKey,
                Map<String, String> vMap,
                Map<String, String> headers,
                Map<String, String> queries,
                String jsonBody,
                @NonNull ServerCallback callback,
                @NonNull ServerErrorCallback errorCallback);

    void authFollow(int method,
                    Context context,
                    String hypermediaKey,
                    @NonNull ServerCallback callback,
                    @NonNull ServerErrorCallback errorCallback);

    void authFollow(int method,
                    Context context,
                    String hypermediaKey,
                    Map<String, String> queries,
                    @NonNull ServerCallback callback,
                    @NonNull ServerErrorCallback errorCallback);

    void authFollow(int method,
                    Context context,
                    String hypermediaKey,
                    String body,
                    @NonNull ServerCallback callback,
                    @NonNull ServerErrorCallback errorCallback);

    void authFollow(int method,
                    Context context,
                    String hypermediaKey,
                    Map<String, String> vMap,
                    String body,
                    @NonNull ServerCallback callback,
                    @NonNull ServerErrorCallback errorCallback);

    void authFollow(int method,
                    Context context,
                    String hypermediaKey,
                    Map<String, String> vMap,
                    Map<String, String> queries,
                    String body,
                    @NonNull ServerCallback callback,
                    @NonNull ServerErrorCallback errorCallback);

    /**
     * Uploads an image, requires authentication to upload.
     * @param context the application context
     * @param uploadPath the api path on the server
     * @param imagePath the image path on android
     * @param callback the callback on success
     * @param errorCallback the callback on failure
     */
    void uploadImage(Context context,
                     String uploadPath,
                     String imagePath,
                     @NonNull ServerCallback callback,
                     @NonNull ServerErrorCallback errorCallback);

}
