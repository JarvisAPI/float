package com.floatapplication.shop.edit;

import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.TextView;

import com.floatapplication.shop.adapter.ShopEditImagePagerAdapter;
import com.simplexorg.customviews.views.SpinnerButton;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ShopAboutEditViewUnitTest {
    private ShopAboutEditView mView;
    @Mock private ViewPager mViewPager;
    @Mock private TextView mOwnerName;
    @Mock private EditText mShopDescription;
    @Mock private View mRootView;
    @Mock private SpinnerButton mShopDeleteBtn;
    @Mock private ShopEditImagePagerAdapter mShopImageAdapter;
    @Mock private Fragment mFragment;
    @Mock private ShopAboutEditContract.Presenter mPresenter;

    @Before
    public void setup() {
        initMocks(this);
        mView = new ShopAboutEditView();
        mView.mViewPager = mViewPager;
        mView.mOwnerName = mOwnerName;
        mView.mShopDescription = mShopDescription;
        mView.mRootView = mRootView;
        mView.mShopDeleteBtn = mShopDeleteBtn;
        mView.mShopImageAdapter = mShopImageAdapter;
        mView.mFragment = mFragment;
        mView.setPresenter(mPresenter);
    }

    @Test
    public void test_init() {
        mView.init();
        verify(mViewPager).setAdapter(mShopImageAdapter);
        ArgumentCaptor<OnClickListener> onClickCaptor = ArgumentCaptor.forClass(OnClickListener.class);

        verify(mShopImageAdapter).setOnImageClickListener(onClickCaptor.capture());
        onClickCaptor.getValue().onClick(null);
        verify(mPresenter).onShopImageClick(new Object());

        verify(mShopDescription).setOnClickListener(onClickCaptor.capture());
        onClickCaptor.getValue().onClick(null);
        verify(mPresenter).onShopDescriptionClick();

        ArgumentCaptor<OnFocusChangeListener> onFocusCaptor = ArgumentCaptor.forClass(OnFocusChangeListener.class);

        verify(mShopDescription).setOnFocusChangeListener(onFocusCaptor.capture());
        onFocusCaptor.getValue().onFocusChange(null, true);
        verify(mPresenter).onShopDescriptionFocusChange(true);
        onFocusCaptor.getValue().onFocusChange(null, false);
        verify(mPresenter).onShopDescriptionFocusChange(false);

        verify(mRootView).setOnClickListener(onClickCaptor.capture());
        onClickCaptor.getValue().onClick(null);
        verify(mPresenter).onRootClick();

        verify(mShopDeleteBtn).setOnClickListener(onClickCaptor.capture());
        onClickCaptor.getValue().onClick(null);
        verify(mPresenter).onShopDeleteButtonClick();
    }

    @Test
    public void test_setOwnerName() {
        String name = "test";
        mView.setOwnerName(name);
        verify(mOwnerName).setText(name);
    }

    @Test
    public void test_setShopDeleteButtonText() {
        int resId = 0xffff;
        String resStr = "res";
        Resources resources = mock(Resources.class);
        when(resources.getString(resId)).thenReturn(resStr);
        when(mRootView.getResources()).thenReturn(resources);

        mView.setShopDeleteButtonText(resId);

        verify(mShopDeleteBtn).setText(resStr);
    }

    @Test
    public void test_setShopDesTextFocus() {
        mView.setShopDesTextFocus();

        verify(mShopDescription).setCursorVisible(true);
    }

    @Test
    public void test_setShopDescriptionText() {
        String desc = "desc";
        mView.setShopDescriptionText(desc);

        verify(mShopDescription).setText(desc);
    }

    @Test
    public void test_setShopDeleteButtonEnable() {
        mView.setShopDeleteButtonEnable(true);
        verify(mShopDeleteBtn).setEnabled(true);

        mView.setShopDeleteButtonEnable(false);
        verify(mShopDeleteBtn).setEnabled(false);
    }

    @Test
    public void test_setShopDeleteButtonSpin() {
        mView.setShopDeleteButtonSpin(true);
        verify(mShopDeleteBtn).spin(true);

        mView.setShopDeleteButtonSpin(false);
        verify(mShopDeleteBtn).spin(false);
    }
}
