package com.floatapplication.user.util;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.Toolbar.OnMenuItemClickListener;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.user.adapter.UserProfileAdapter;
import com.floatapplication.user.models.User;
import com.floatapplication.user.models.User.NamePair;
import com.floatapplication.user.models.UserShopItem;
import com.floatapplication.util.GlideRequests;
import com.floatapplication.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by william on 2018-06-06.
 * Presents the user profile in user fragment.
 */

public class UserProfilePresenter
        implements
        OnClickListener,
        ServerErrorCallback,
        OnMenuItemClickListener,
        PopupMenu.OnMenuItemClickListener {
    public interface OnUserProfileClickListener {
        void onProfileImageClicked(String url);

        void onProfileBackgroundImageClicked(String url);

        void onMessageButtonClicked(NamePair namePair);

        void onNavigationBackClicked();

        void onChangeProfileImageClicked();

        void onChangeProfileBackgroundClicked();
    }

    private static final String TAG = UserProfilePresenter.class.getSimpleName();
    private NamePair mUserNamePair;
    private OnUserProfileClickListener mOnUserProfileClickListener;
    private UserProfileAdapter mUserProfileAdapter;
    private GlideRequests mGlide;

    private RecyclerView mRecyclerView;
    private ImageView mShopsWallpaperView;

    private ImageView mBackgroundImageView;
    private String mBackgroundImageUrl;
    private CircleImageView mProfileImageView;
    private String mProfileImageUrl;
    private Toolbar mToolbar;

    public UserProfilePresenter(GlideRequests glideRequests) {
        mGlide = glideRequests;
    }

    public void present(ImageView shopsWallpaperView, RecyclerView recyclerView,
                        UserProfileAdapter adapter,
                        ImageView backgroundImage, CircleImageView profileImage,
                        FloatingActionButton messageButton,
                        Toolbar toolbar,
                        TextView nameText, @NonNull String username,
                        @NonNull String name) {
        mShopsWallpaperView = shopsWallpaperView;
        mRecyclerView = recyclerView;

        mUserNamePair = new NamePair(username, name);
        setupUserProfileAdapter(adapter);
        setupRecyclerView(recyclerView);
        nameText.setText(StringUtils.capitalize(name));
        mBackgroundImageView = backgroundImage;
        mProfileImageView = profileImage;
        retrieveUserProfile();
        retrieveShops();
        messageButton.setOnClickListener(this);
        mToolbar = toolbar;
        setupToolbar();
    }

    private void setupUserProfileAdapter(UserProfileAdapter adapter) {
        mUserProfileAdapter = adapter;
        if (mUserNamePair.username.equals(FloatApplication.getUserSessionInterface().getUsername())) {
            adapter.getPresenter().setIsProfileOwner();
        }
    }

    private void setupToolbar() {
        mToolbar.setNavigationIcon(R.drawable.round_arrow_back_white_24);
        mToolbar.setNavigationOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnUserProfileClickListener.onNavigationBackClicked();
            }
        });
        if (mUserNamePair.username.equals(FloatApplication.getUserSessionInterface().getUsername())) {
            mToolbar.inflateMenu(R.menu.fragment_user_profile);
            mToolbar.setOnMenuItemClickListener(this);
        }
    }

    public void setOnUserProfileClickListener(OnUserProfileClickListener listener) {
        mOnUserProfileClickListener = listener;
    }

    private void retrieveUserProfile() {
        Map<String, String> vMap = new HashMap<>();
        vMap.put(User.USERNAME, mUserNamePair.username);
        FloatApplication.getHypermediaInterface()
                .follow(Hypermedia.GET, HypermediaInterface.USER_PROFILE, vMap, null,
                        new ServerCallback() {
                            @Override
                            public void onSuccessResponse(String response) {
                                try {
                                    JSONObject userData = new JSONObject(response);
                                    setProfileImage(userData);
                                    setBackgroundImage(userData);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        this);
    }

    private void setProfileImage(JSONObject userData) throws JSONException {
        if (userData.has(User.PROFILE_IMAGES)) {
            JSONArray profileImage = userData.getJSONArray(User.PROFILE_IMAGES);
            if (profileImage.length() != 0) {
                mProfileImageUrl = setImageToView(profileImage.getJSONObject(0)
                        .getString("url"), mProfileImageView);
                return;
            }
        }
        mProfileImageView.setImageResource(R.drawable.default_user_profile_image);
    }

    private void setBackgroundImage(JSONObject userData) throws JSONException {
        if (userData.has(User.BACKGROUND_IMAGES)) {
            JSONArray backgroundImage = userData.getJSONArray(User.BACKGROUND_IMAGES);
            if (backgroundImage.length() != 0) {
                mBackgroundImageUrl = setImageToView(backgroundImage.getJSONObject(0)
                        .getString("url"), mBackgroundImageView);
                return;
            }
        }
        mBackgroundImageView.setImageResource(R.drawable.default_user_profile_background);
    }

    /**
     * @param imageFile image file obtained from server.
     * @param imageView view to load image into.
     * @return the url that is constructed from the image file.
     */
    private String setImageToView(String imageFile, final ImageView imageView) {
        String url = Util.getInstance().constructImageUrl(imageFile);
        mGlide
                .load(url)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        imageView.setImageDrawable(resource);
                    }
                });
        imageView.setOnClickListener(this);
        return url;
    }

    private void retrieveShops() {
        Map<String, String> vMap = new HashMap<>();
        vMap.put(User.USERNAME, mUserNamePair.username);
        FloatApplication.getHypermediaInterface()
                .follow(Hypermedia.GET, HypermediaInterface.USER_SHOPS, vMap, null,
                        new ServerCallback() {
                            @Override
                            public void onSuccessResponse(String response) {
                                processShopData(response);
                            }
                        },
                        (VolleyError error) -> {
                            mShopsWallpaperView.setVisibility(View.VISIBLE);
                            mRecyclerView.setVisibility(View.GONE);
                            error.printStackTrace();
                        });
    }

    private void processShopData(String response) {
        List<UserShopItem> userItems = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray shops = jsonObject.getJSONArray("shops");
            for (int i = 0; i < shops.length(); i++) {
                try {
                    userItems.add(UserShopItem.create(mBackgroundImageView.getContext(), shops.getJSONObject(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mUserProfileAdapter.setUserItems(userItems);

        if (userItems.isEmpty()) {
            mShopsWallpaperView.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    private void setupRecyclerView(RecyclerView recyclerView) {
        Log.d(TAG, "Setting up user profile adapter");
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(recyclerView.getContext(),
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mUserProfileAdapter);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.user_profile_background:
                mOnUserProfileClickListener.onProfileBackgroundImageClicked(mBackgroundImageUrl);
                break;
            case R.id.user_profile_image:
                mOnUserProfileClickListener.onProfileImageClicked(mProfileImageUrl);
                break;
            case R.id.message_fab:
                mOnUserProfileClickListener.onMessageButtonClicked(mUserNamePair);
                break;
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_edit:
                showEditMenu();
                return true;
            case R.id.user_cover_change:
                mOnUserProfileClickListener.onChangeProfileBackgroundClicked();
                return true;
            case R.id.user_prof_change:
                mOnUserProfileClickListener.onChangeProfileImageClicked();
                return true;
        }
        return false;
    }

    private void showEditMenu() {
        View editItem = mToolbar.findViewById(R.id.action_edit);
        PopupMenu popupMenu = new PopupMenu(mToolbar.getContext(), editItem);
        popupMenu.inflate(R.menu.user_profile_owner_menu);
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(this);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        error.printStackTrace();
    }

    public void reloadCoverImage() {
        retrieveUserProfile();
    }

    public void reloadProfileImage() {
        retrieveUserProfile();
    }

    public NamePair getUserNamePair() {
        return mUserNamePair;
    }

    public void removeShop(String shopId) {
        mUserProfileAdapter.removeShop(shopId);
    }

    public void refreshShop(String shopId) {mUserProfileAdapter.refreshShop(shopId);}
}
