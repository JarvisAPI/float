package com.floatapplication.search;

import android.os.Parcel;

import com.simplexorg.mapfragment.model.GeoPoint;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class SearchQueryUnitTest {
    private SearchQuery mSearchQuery;

    @Before
    public void setup() {
        mSearchQuery = new SearchQuery("query", new GeoPoint(1, 2));
    }

    @Test
    public void test_writeToParcel() {
        Parcel parcel = mock(Parcel.class);
        mSearchQuery.writeToParcel(parcel, 0);

        verify(parcel).writeString(mSearchQuery.query);
        verify(parcel).writeParcelable(mSearchQuery.location, 0);
        verify(parcel).writeInt(mSearchQuery.distance);
    }

    @Test
    public void test_createFromParcel() {
        Parcel parcel = mock(Parcel.class);
        SearchQuery.CREATOR.createFromParcel(parcel);

        InOrder inOrder = Mockito.inOrder(parcel);
        inOrder.verify(parcel).readString();
        inOrder.verify(parcel).readParcelable(GeoPoint.class.getClassLoader());
        inOrder.verify(parcel).readInt();
    }
}
