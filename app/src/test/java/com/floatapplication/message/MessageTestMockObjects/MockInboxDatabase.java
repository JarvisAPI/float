package com.floatapplication.message.MessageTestMockObjects;

import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.models.Dialog;

import junit.framework.Assert;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by jamescho on 2018-06-15.
 */

public class MockInboxDatabase {
    private HashMap<String, Dialog> db;

    public MockInboxDatabase() {
        this.db = new HashMap<>();
    }

    public void insertData(Dialog dialog) {
        if (dialog == null) {
            Assert.fail("a inserting dialog should not be null");
        }
        db.put(dialog.getId(), dialog);
    }

    public Set<CustomMessage> getLastMessages(){
        Set<String> keys = db.keySet();
        Set<CustomMessage> lm = new HashSet<>();
        for(String key: keys){
            lm.add(db.get(key).getLastMessage());
        }
        return lm;
    }

    public boolean dialogExists(String jid){
        return db.containsKey(jid);
    }
}
