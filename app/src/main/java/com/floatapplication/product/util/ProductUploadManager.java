package com.floatapplication.product.util;

import android.content.Context;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.floatapplication.R;
import com.floatapplication.product.models.ProductCreator;
import com.floatapplication.product.util.ProductCreationHandler.ProgressListener;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by william on 2018-05-15.
 * Manages uploading of products as well as displaying upload progress to the user.
 */

public class ProductUploadManager {
    public interface OnProductUploadedListener {
        void onProductUploaded();
    }
    private static final String TAG = ProductUploadManager.class.getSimpleName();
    private Queue<ProductCreator> mProductUploadQueue;
    private String mShopId;

    private Context mContext;
    private View mView;
    private Snackbar mSnackbar;
    private ProgressBar mSnackProgressBar;
    private TextView mProgressText;
    private OnProductUploadedListener mOnProductUploadedListener;

    private boolean mCurrentlyWorking;

    public ProductUploadManager(Context context, View view, String shopId) {
        mContext = context;
        mShopId = shopId;
        mCurrentlyWorking = false;
        mView = view;
        mProductUploadQueue = new LinkedList<>();
    }

    public void uploadProduct(ProductCreator productCreator) {
        if (!mCurrentlyWorking) {
            mCurrentlyWorking = true;
            createShopProduct(productCreator);
        } else {
            mProductUploadQueue.add(productCreator);
        }
    }

    private void createShopProduct(ProductCreator productCreator) {
        createShopProductEditSnackbar();
        ProductCreationHandler productCreationHandler = new ProductCreationHandler(mContext, productCreator, mShopId);
        productCreationHandler.setListener(new ProgressListener() {
            @Override
            public void onProgressUpdate(int progress) {
                if (progress > 0) {
                    if (mSnackbar != null) {
                        mProgressText.setText(String.format("%s%%", String.valueOf(progress)));
                        if (VERSION.SDK_INT >= VERSION_CODES.N) {
                            mSnackProgressBar.setProgress(progress, true);
                        } else {
                            mSnackProgressBar.setProgress(progress);
                        }
                        if (progress == 100) {
                            Log.d(TAG, "mSnackbar: " + mSnackbar);
                            mSnackbar.setText("Product Uploaded!");
                            mSnackbar.setDuration(Snackbar.LENGTH_SHORT);
                            mSnackbar.show();
                            if (mOnProductUploadedListener != null) {
                                mOnProductUploadedListener.onProductUploaded();
                            }
                            checkUploadQueue();
                        }
                    }
                } else {
                    if (progress < 0) {
                        if (mSnackbar != null) {
                            mSnackbar.dismiss();
                        }
                    }
                    checkUploadQueue();
                }
            }
        });
        productCreationHandler.makeSendProductRequest();
    }

    public void setOnProductUploadedListener(OnProductUploadedListener listener) {
        mOnProductUploadedListener = listener;
    }

    private void checkUploadQueue() {
        ProductCreator productCreator = mProductUploadQueue.poll();
        if (productCreator == null) {
            mCurrentlyWorking = false;
        } else {
            mCurrentlyWorking = true;
            createShopProduct(productCreator);
        }
    }

    private void createShopProductEditSnackbar() {
        if (mSnackbar != null && mSnackbar.isShown()) {
            mSnackbar.dismiss();
        }
        mSnackbar = Snackbar.make(mView, "Uploading Product Info", Snackbar.LENGTH_INDEFINITE);
        ViewGroup contentLayout = (ViewGroup) mSnackbar.getView().findViewById(android.support.design.R.id.snackbar_text).getParent();

        LinearLayout layout = new LinearLayout(mContext);
        layout.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        layout.setOrientation(LinearLayout.VERTICAL);
        mSnackProgressBar = new ProgressBar(mContext, null, android.R.attr.progressBarStyleHorizontal);
        mProgressText = new TextView(mContext);
        mProgressText.setTextColor(ContextCompat.getColor(mContext, R.color.white));
        mProgressText.setText("0%");
        layout.addView(mSnackProgressBar);
        layout.addView(mProgressText);
        contentLayout.addView(layout);
        mSnackbar.show();
    }
}
