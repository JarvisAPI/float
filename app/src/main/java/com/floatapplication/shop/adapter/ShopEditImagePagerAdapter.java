package com.floatapplication.shop.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.floatapplication.R;
import com.floatapplication.util.GlobalHandler;
import com.floatapplication.util.IFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class ShopEditImagePagerAdapter extends ShopImagePagerAdapterBase {
    //private static final String TAG = ShopEditImagePagerAdapter.class.getSimpleName();

    public interface OnMainImageIndicatorSetListener {
        void onMainImageIndicatorSet();
    }

    private static class ViewState {
        boolean toBeDeleted;
        View itemView;
    }

    private boolean mInitialMainItemSet;

    private String mInitialMainImageUrl;
    private String mCurrentMainImageUrl;

    private OnMainImageIndicatorSetListener mMainImageIndicatorSetListener;
    private Context mContext;
    private Toast mLastToast;

    private Map<String, ViewState> mViewStateMap;

    public ShopEditImagePagerAdapter(Context context) {
        mContext = context;
        mInitialMainImageUrl = "";
        mInitialMainItemSet = false;
        mViewStateMap = IFactory.getInstance().createMap();
    }

    @Override
    protected void afterItemInflate(View itemView, int position) {
        if (mImages.isEmpty()) {
            return;
        }

        ViewState viewState = mViewStateMap.get(mImages.get(position));
        if (viewState == null) {
            viewState = new ViewState();
            viewState.toBeDeleted = false;
            viewState.itemView = itemView;
        }
        String imageUrl = mImages.get(position);

        itemView.findViewById(R.id.delete).setVisibility(View.VISIBLE);
        itemView.setTag(R.id.tag0, imageUrl);

        ImageView mainImageIcon = itemView.findViewById(R.id.main_image_icon);
        mainImageIcon.setOnClickListener(this::onMainImageIconClick);

        ImageView deleteImageIcon = itemView.findViewById(R.id.delete);
        deleteImageIcon.setTag(R.id.tag0, imageUrl);
        deleteImageIcon.setOnClickListener(this::onDeleteImageIconClick);

        if (viewState.toBeDeleted) {
            deleteImageIcon.setImageResource(R.drawable.baseline_restore_from_trash_black_24);
        }

        if (!mInitialMainItemSet && imageUrl.equals(mInitialMainImageUrl)) {
            mCurrentMainImageUrl = imageUrl;

            if (mMainImageIndicatorSetListener != null) {
                mMainImageIndicatorSetListener.onMainImageIndicatorSet();
            }
            mainImageIcon.setVisibility(View.VISIBLE);
            mInitialMainItemSet = true;
        } else if (imageUrl.equals(mCurrentMainImageUrl)) {
            viewState.itemView = itemView;
            mainImageIcon.setVisibility(View.VISIBLE);
        }

        ImageView imageView = itemView.findViewById(R.id.image);
        imageView.setTag(R.id.tag0, itemView);

        imageView.setOnLongClickListener(this::onImageLongClick);

        mViewStateMap.put(imageUrl, viewState);
    }

    @Override
    protected void afterSetImages(int numImages) {
        if (!mImages.isEmpty()) {
            mInitialMainImageUrl = mImages.get(0);
        }
    }

    private void onMainImageIconClick(@NonNull View view) {
        new SimpleTooltip.Builder(view.getContext())
                .anchorView(view)
                .text(R.string.shop_edit_main_image_icon_tooltip)
                .textColor(Color.WHITE)
                .gravity(Gravity.END)
                .animated(true)
                .transparentOverlay(false)
                .build()
                .show();
    }

    private boolean onImageLongClick(@NonNull View view) {
        View curItemView = (View) view.getTag(R.id.tag0);

        if (curItemView == null) {
            return false;
        }

        String mainImageUrl = (String) curItemView.getTag(R.id.tag0);
        String curMainImageUrl = mCurrentMainImageUrl;

        if (curMainImageUrl == null || !curMainImageUrl.equals(mainImageUrl)) {
            if (mCurrentMainImageUrl != null) {
                View itemView = mViewStateMap.get(mCurrentMainImageUrl).itemView;
                itemView.findViewById(R.id.main_image_icon).setVisibility(View.INVISIBLE);
            }
            mCurrentMainImageUrl = mainImageUrl;
            curItemView.findViewById(R.id.main_image_icon).setVisibility(View.VISIBLE);

            if (mMainImageIndicatorSetListener != null) {
                mMainImageIndicatorSetListener.onMainImageIndicatorSet();
            }
        }

        return true;
    }

    public void clearMainImageIndicator() {
        if (mCurrentMainImageUrl != null) {
            View itemView = mViewStateMap.get(mCurrentMainImageUrl).itemView;
            itemView.findViewById(R.id.main_image_icon).setVisibility(View.INVISIBLE);
            mCurrentMainImageUrl = null;
        }
    }

    public void setOnMainImageIndicatorSetListener(OnMainImageIndicatorSetListener listener) {
        mMainImageIndicatorSetListener = listener;
    }

    public boolean hasMainImageIndicator() {
        return mCurrentMainImageUrl != null;
    }

    public List<String> getImagesMarkedForDeletion() {
        List<String> markedImages = new ArrayList<>();
        for (String imageUrl : mViewStateMap.keySet()) {
            ViewState viewState = mViewStateMap.get(imageUrl);
            if (viewState.toBeDeleted) {
                markedImages.add(GlobalHandler.getInstance().constructImageFile(imageUrl));
            }
        }
        return markedImages;
    }

    public String getChangedMainImageFile() {
        if (mCurrentMainImageUrl != null) {
            if (!mInitialMainImageUrl.equals(mCurrentMainImageUrl)) {
                return GlobalHandler.getInstance().constructImageFile(mCurrentMainImageUrl);
            }
        }
        return null;
    }

    private void onDeleteImageIconClick(View view) {
        String imageUrl = (String) view.getTag(R.id.tag0);
        ImageView deleteButton = (ImageView) view;
        if (imageUrl != null) {
            if (mLastToast != null) {
                mLastToast.cancel();
            }
            ViewState viewState = mViewStateMap.get(imageUrl);

            if (!viewState.toBeDeleted) {
                viewState.toBeDeleted = true;

                deleteButton.setImageResource(R.drawable.baseline_restore_from_trash_black_24);
                mLastToast = Toast.makeText(mContext, R.string.item_marked_for_deletion, Toast.LENGTH_SHORT);
                mLastToast.show();
                return;
            }
            viewState.toBeDeleted = false;
            deleteButton.setImageResource(R.drawable.baseline_delete_black_24);

            mLastToast = Toast.makeText(mContext, R.string.item_unmarked_for_deletion, Toast.LENGTH_SHORT);
            mLastToast.show();
        }
    }
}
