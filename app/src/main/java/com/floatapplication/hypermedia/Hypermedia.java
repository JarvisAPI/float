package com.floatapplication.hypermedia;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.Request.Method;
import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.user.util.UserSessionInterface.TokenCallback;
import com.floatapplication.util.Util;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.web.util.UriTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by william on 2017/12/24.
 * Keeps track of hypermedia state.
 */
public final class Hypermedia implements HypermediaInterface {
    private static final String TAG = Hypermedia.class.getSimpleName();
    private static final String LINKS = "_links";
    private static final String HREF = "href";
    private static final String TEMPLATED = "templated";

    private Map<String, String> mServiceDocument;

    public Hypermedia() {
        mServiceDocument = new HashMap<>();
    }

    @Override
    public boolean hasKey(String key) {
        return mServiceDocument.containsKey(key);
    }

    @Override
    public void init(Map<String, String> headers,
                     @NonNull ServerCallback callback,
                     @NonNull ServerErrorCallback errorCallback) {
        FloatApplication.getServerInterface().asyncReq(Method.GET, "/", headers,
                (String response) -> {
                    callback.onSuccessResponse(processResponse(response));
                }, errorCallback);
    }

    @Override
    public String getPath(String key, Map<String, String> variableMap) {
        try {
            JSONObject linkObj = new JSONObject(mServiceDocument.get(key));
            String uri = linkObj.getString(Hypermedia.HREF);
            if (linkObj.has(Hypermedia.TEMPLATED) && linkObj.getBoolean(Hypermedia.TEMPLATED)) {
                // expand uri.
                UriTemplate uriTemplate = new UriTemplate(uri);
                uri = uriTemplate.expand(variableMap).toString();
            }
            return uri;
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            Log.i(TAG, "getting hyper link for: " + key + ", resulted in a null link");
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "Passed in null variable map for templated link");
        }
        return "";
    }

    @Override
    public void follow(int method,
                       String key,
                       Map<String, String> vMap,
                       Map<String, String> headers,
                       @NonNull ServerCallback callback,
                       @NonNull ServerErrorCallback errorCallback) {
        String path = getPath(key, vMap);
        Log.d(TAG, "Path: " + path);
        FloatApplication.getServerInterface().asyncReq(method, path, headers,
                (String response) -> {
                    Log.i(TAG, "response: " + response);
                    callback.onSuccessResponse(processResponse(response));
                }, errorCallback);
    }

    @Override
    public void follow(int method,
                       String key,
                       Map<String, String> vMap,
                       Map<String, String> headers,
                       Map<String, String> queries,
                       @NonNull ServerCallback callback,
                       @NonNull ServerErrorCallback errorCallback) {
        String path = getPath(key, vMap);
        path = path + Util.getInstance().buildQuery(queries);
        Log.d(TAG, "Path: " + path);
        FloatApplication.getServerInterface().asyncReq(method, path, headers,
                (String response) -> {
                    Log.i(TAG, "response: " + response);
                    callback.onSuccessResponse(processResponse(response));
                }, errorCallback);
    }


    @Override
    public void follow(int method,
                       String key,
                       Map<String, String> vMap,
                       Map<String, String> headers,
                       String jsonBody,
                       @NonNull ServerCallback callback,
                       @NonNull ServerErrorCallback errorCallback) {
        String path = getPath(key, vMap);
        FloatApplication.getServerInterface().asyncReq(method, path, headers, jsonBody,
                (String response) -> {
                    Log.i(TAG, "response: " + response);
                    callback.onSuccessResponse(processResponse(response));
                }, errorCallback);
    }

    public void follow(int method,
                       String key,
                       Map<String, String> vMap,
                       Map<String, String> headers,
                       Map<String, String> queries,
                       String jsonBody,
                       @NonNull ServerCallback callback,
                       @NonNull ServerErrorCallback errorCallback) {
        String path = getPath(key, vMap);
        path = path + Util.getInstance().buildQuery(queries);
        FloatApplication.getServerInterface().asyncReq(method, path, headers, jsonBody,
                (String response) -> {
                    Log.i(TAG, "response: " + response);
                    callback.onSuccessResponse(processResponse(response));
                }, errorCallback);
    }

    private String processResponse(String response) {
        try {
            JSONObject jsonBody = new JSONObject(response);
            if (jsonBody.has(LINKS)) {
                JSONObject _links = jsonBody.getJSONObject(LINKS);
                Iterator<String> it = _links.keys();
                while (it.hasNext()) {
                    String key = it.next();
                    mServiceDocument.put(key, _links.getString(key));
                }
                jsonBody.remove(LINKS);
                return jsonBody.toString();
            }
            return response;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    public void authFollow(int method,
                           Context context,
                           String hypermediaKey,
                           @NonNull ServerCallback callback,
                           @NonNull ServerErrorCallback errorCallback) {
        FloatApplication.getUserSessionInterface()
                .getUserAccessToken(context, new TokenCallback() {
                    @Override
                    public void onTokenObtained(String token) {
                        follow(method, hypermediaKey, null,
                                getAuthHeaders(token), callback, errorCallback);
                    }

                    @Override
                    public void onTokenError(String errorMsg) {
                        VolleyError error = new VolleyError(errorMsg);
                        errorCallback.onErrorResponse(error);
                    }
                });
    }

    @Override
    public void authFollow(int method,
                           Context context,
                           String hypermediaKey,
                           Map<String, String> queries,
                           @NonNull ServerCallback callback,
                           @NonNull ServerErrorCallback errorCallback) {
        FloatApplication.getUserSessionInterface()
                .getUserAccessToken(context, new TokenCallback() {
                    @Override
                    public void onTokenObtained(String token) {
                        follow(method, hypermediaKey, null,
                                getAuthHeaders(token), queries, callback, errorCallback);
                    }

                    @Override
                    public void onTokenError(String errorMsg) {
                        VolleyError error = new VolleyError(errorMsg);
                        errorCallback.onErrorResponse(error);
                    }
                });
    }

    @Override
    public void authFollow(int method,
                           Context context,
                           String hypermediaKey,
                           String body,
                           @NonNull ServerCallback callback,
                           @NonNull ServerErrorCallback errorCallback) {
        FloatApplication.getUserSessionInterface()
                .getUserAccessToken(context, new TokenCallback() {
                    @Override
                    public void onTokenObtained(String token) {
                        follow(method, hypermediaKey, null, getAuthHeaders(token),
                                body, callback, errorCallback);
                    }

                    @Override
                    public void onTokenError(String errorMsg) {
                        VolleyError error = new VolleyError(errorMsg);
                        errorCallback.onErrorResponse(error);
                    }
                });
    }

    @Override
    public void authFollow(int method,
                           Context context,
                           String hypermediaKey,
                           Map<String, String> vMap,
                           String body,
                           @NonNull ServerCallback callback,
                           @NonNull ServerErrorCallback errorCallback) {
        FloatApplication.getUserSessionInterface()
                .getUserAccessToken(context, new TokenCallback() {
                    @Override
                    public void onTokenObtained(String token) {
                        follow(method, hypermediaKey, vMap, getAuthHeaders(token),
                                body, callback, errorCallback);
                    }

                    @Override
                    public void onTokenError(String errorMsg) {
                        VolleyError error = new VolleyError(errorMsg);
                        errorCallback.onErrorResponse(error);
                    }
                });
    }

    @Override
    public void authFollow(int method,
                           Context context,
                           String hypermediaKey,
                           Map<String, String> vMap,
                           Map<String, String> queries,
                           String body,
                           @NonNull ServerCallback callback,
                           @NonNull ServerErrorCallback errorCallback) {
        FloatApplication.getUserSessionInterface()
                .getUserAccessToken(context, new TokenCallback() {
                    @Override
                    public void onTokenObtained(String token) {
                        follow(method, hypermediaKey, vMap, getAuthHeaders(token), queries,
                                body, callback, errorCallback);
                    }

                    @Override
                    public void onTokenError(String errorMsg) {
                        VolleyError error = new VolleyError(errorMsg);
                        errorCallback.onErrorResponse(error);
                    }
                });
    }

    @Override
    public void uploadImage(Context context,
                            String uploadPath,
                            String imagePath,
                            @NonNull ServerCallback callback,
                            @NonNull ServerErrorCallback errorCallback) {
        FloatApplication.getUserSessionInterface()
                .getUserAccessToken(context, new TokenCallback() {
                    @Override
                    public void onTokenObtained(String token) {
                        List<String> imagePaths = new ArrayList<>();
                        imagePaths.add(imagePath);
                        FloatApplication.getFactoryInterface().createUploadImageTask(uploadPath, "image",
                                imagePaths, getAuthHeaders(token), callback, errorCallback)
                                .execute();
                    }

                    @Override
                    public void onTokenError(String errorMsg) {
                        VolleyError error = new VolleyError(errorMsg);
                        errorCallback.onErrorResponse(error);
                    }
                });
    }

    private Map<String, String> getAuthHeaders(String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + token);
        return headers;
    }
}
