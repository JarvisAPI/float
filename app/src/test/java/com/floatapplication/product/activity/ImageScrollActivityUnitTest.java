package com.floatapplication.product.activity;

import android.content.Intent;
import android.view.MenuItem;

import com.floatapplication.R;
import com.floatapplication.TestApplication;
import com.floatapplication.image.activity.ImageScrollActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.fakes.RoboMenuItem;
import org.robolectric.shadows.ShadowActivity;

import java.util.ArrayList;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by william on 2018-05-23.
 * Tests the product scrolling functionality.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ImageScrollActivityUnitTest {
    private ImageScrollActivity activity;


    @Test
    public void passInOneImage_thenDeleteIt_shouldFinishWithResult() {
        ArrayList<String> imagePaths = new ArrayList<>();
        imagePaths.add("0");
        Intent intent = new Intent();
        intent.putExtra(ImageScrollActivity.SELECTED_IMAGE_PATH, "0");
        intent.putStringArrayListExtra(ImageScrollActivity.IMAGE_PATHS, imagePaths);
        activity = Robolectric.buildActivity(ImageScrollActivity.class, intent)
                .create()
                .start()
                .resume()
                .get();
        MenuItem item = new RoboMenuItem(R.id.image_scroll_delete);
        activity.onMenuItemClick(item);
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent result = shadowActivity.getResultIntent();
        assertNotNull(result);
        ArrayList<String> resultList = result.getStringArrayListExtra(ImageScrollActivity.IMAGE_PATHS);
        assertEquals(imagePaths.get(0), resultList.get(0));
        assertTrue(activity.isFinishing());
    }
}
