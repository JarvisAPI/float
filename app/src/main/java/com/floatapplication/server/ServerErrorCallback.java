package com.floatapplication.server;

import com.android.volley.VolleyError;

/**
 * Created by william on 2017/12/21.
 *
 */

public interface ServerErrorCallback {
    void onErrorResponse(VolleyError error);
}
