package com.floatapplication.messages.message_database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class MessageUserDatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "MessageDatabaseHelper";
    private static final String TABLE_NAME = "chat_users";
    private static final String COL_JID = "jid";
    private static final String COL_CHAT_TOKEN = "chat_token";
    private static final String COL_MESSAGE_EXISTS = "message_exists";
    private static final int COL_JID_ID_NUM = 0;
    private static final int COL_CHAT_TOKEN_NUM = 1;
    private static final int COL_MESSAGE_EXISTS_NUM = 2;

    public MessageUserDatabaseHelper(Context context) {
        super(context, TABLE_NAME, null, 1);
        onCreate(getWritableDatabase());
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG,"onCreate() message Database");
        String createTable = "CREATE TABLE IF NOT EXISTS "
                + TABLE_NAME
                + "("
                + COL_JID + " TEXT,"
                + COL_CHAT_TOKEN + " TEXT,"
                + COL_MESSAGE_EXISTS + " INTEGER DEFAULT 0,"
                + "PRIMARY KEY (" + COL_JID+")"
                + ")";// TODO add cascade
        db.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        Log.d(TAG,"onUpgrade()");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public void addUser(String jid, String chatToken){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_JID, jid);
        contentValues.put(COL_CHAT_TOKEN, chatToken);
        db.insertWithOnConflict(TABLE_NAME,null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public ArrayList<String> getUsers() {
        Cursor curs = null;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String query = "SELECT * FROM " + TABLE_NAME;
            curs = db.rawQuery(query, null);
            ArrayList<String> jids = new ArrayList<>();
            while (curs.moveToNext()) {
                jids.add(curs.getString(COL_JID_ID_NUM));
            }
            return jids;
        } finally {
            if (curs != null) {
                curs.close();
                this.close();
            }
        }
    }

    public String getChatToken(String jid){
        Cursor curs = null;
        Log.d(TAG,"getting chat token from database for jid: " + jid);
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String query = "SELECT * FROM "
                    + TABLE_NAME + " WHERE " + COL_JID + "=" + getSQLString(jid);
            curs = db.rawQuery(query, null);
            if (curs.getCount() != 0) {
                curs.moveToFirst();
                Log.d(TAG,"chat token is: " + curs.getString(COL_CHAT_TOKEN_NUM));
                return curs.getString(COL_CHAT_TOKEN_NUM);
            }
            else{
                return null;
            }
        }
        finally {
            if (curs != null) {
                curs.close();
                this.close();
            }
        }
    }

    private String getSQLString(String str){
        return "'" + str.toLowerCase() +"'";
    }

    public void flushData(){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
        db.close();
    }
}
