package com.floatapplication.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.VisibleForTesting;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.config.ConfigStore;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.image.ImageLoaderListener;
import com.floatapplication.map.models.Icon;
import com.floatapplication.map.models.Icon.IconPair;
import com.floatapplication.server.ServerInterface;
import com.simplexorg.customviews.fragment.ZoomViewPagerFragment.ImageLoader;
import com.simplexorg.mapfragment.model.GeoPoint;
import com.simplexorg.mapfragment.model.SelectableIconModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.util.StringUtils;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

public class Util {
    private static final String TAG = Util.class.getSimpleName();

    public static final String EXTRA_PRODUCT = "product";
    public static final String EXTRA_SEARCH_DATA = "searchData";
    public static final String EXTRA_TRANSITION = "transition";
    public static final String PLACEHOLDER_SHOP = "placeholderShop";
    public static final String PLACEHOLDER_PRODUCT = "placeholderProduct";

    public interface OnAddressObtainedListener {
        void onAddressObtained(String addressLine);
    }

    private static Util mUtil;

    private Util() {

    }

    private static class AddressTask extends AsyncTask<Void, Void, String> {
        private GeoPoint mGeoPoint;
        private WeakReference<Context> mContext;
        private OnAddressObtainedListener mListener;

        private AddressTask(Context context, GeoPoint geoPoint,
                            @NonNull OnAddressObtainedListener listener) {
            mContext = new WeakReference<>(context);
            mGeoPoint = geoPoint;
            mListener = listener;
        }

        @Override
        protected String doInBackground(Void... voids) {
            String addressLine = "Unknown";
            Context context = mContext.get();
            if (context != null) {
                try {
                    Geocoder geocoder = new Geocoder(context, Locale.getDefault());
                    List<Address> addresses = geocoder.getFromLocation(mGeoPoint.latitude,
                            mGeoPoint.longitude, 1);
                    addressLine = addresses.get(0).getAddressLine(0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return addressLine;
        }

        @Override
        protected void onPostExecute(String result) {
            mListener.onAddressObtained(result);
        }
    }

    public void getAddress(Context context, GeoPoint position, @NonNull OnAddressObtainedListener listener) {
        new AddressTask(context, position, listener).execute();
    }

    public List<SelectableIconModel> createIconModels(String response) {
        List<SelectableIconModel> iconModels = new ArrayList<>();
        try {
            JSONArray jsonIconMarkerArray = new JSONObject(response).getJSONArray("icons");
            for (int i = 0; i < jsonIconMarkerArray.length(); i++) {
                JSONObject marker = jsonIconMarkerArray.getJSONObject(i);
                iconModels.add(genIconModelFromJson(marker));
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return iconModels;
    }

    private SelectableIconModel genIconModelFromJson(JSONObject jsonIconMarker) throws JSONException {
        String markerId = jsonIconMarker.getString(Icon.ID);
        double markerLat = jsonIconMarker.getDouble(Icon.LATITUDE);
        double markerLng = jsonIconMarker.getDouble(Icon.LONGITUDE);
        int markerType = jsonIconMarker.getInt(Icon.TYPE);
        IconPair iconPair = Icon.getIconResourcePair(markerType);
        return new SelectableIconModel.Builder()
                .id(markerId)
                .position(new GeoPoint(markerLat, markerLng))
                .normalResId(iconPair.res)
                .selectedResId(iconPair.selectedRes)
                .build();
    }

    public String buildQuery(Map<String, String> queries) {
        if (queries == null) {
            return "";
        }
        Set<String> queryKeys = queries.keySet();
        StringBuilder query = new StringBuilder("?");
        for (String key : queryKeys) {
            if (queries.get(key) == null) {
                continue;
            }
            query = query.append(key);
            query = query.append("=");
            query = query.append(queries.get(key));
            query = query.append("&");
        }
        return query.substring(0, query.length() - 1);
    }

    public GlideRequests glide(Context context) {
        return GlideApp.with(context);
    }

    @Nullable
    public String constructImageUrl(String imageFile) {
        if (FloatApplication.getHypermediaInterface() != null &&
                FloatApplication.getHypermediaInterface().hasKey(HypermediaInterface.IMAGE)) {
            Map<String, String> vMap = new HashMap<>();
            vMap.put("image_file", imageFile);
            String path = FloatApplication.getHypermediaInterface().getPath("image", vMap);
            return ServerInterface.URL + path;
        }
        return null;
    }

    public void loadImage(@NonNull ImageView imageView, String url, int placeHolderRes) {
        try {
            FloatApplication.getFactoryInterface()
                    .getGlideRequest(imageView.getContext())
                    .load(url)
                    .error(placeHolderRes)
                    .into(imageView);
        } catch (IllegalArgumentException e) {
            // When activity is destroyed then load will throw an exception.
            e.printStackTrace();
        }
    }

    public String capitalize(String str) {
        return StringUtils.capitalize(str);
    }

    /**
     * Checks to see if image url is a null link, that is a link that won't return an image.
     *
     * @param imageUrl the image url to test.
     * @return true if the image url is a null link, false otherwise.
     */
    public boolean isImageUrlNullLink(String imageUrl) {
        int nullLength = 4;
        return imageUrl.startsWith("null", imageUrl.length() - nullLength);
    }

    public ImageLoader getImageLoader(@Nullable ImageLoaderListener listener) {
        Log.d(TAG, "Making image loader");
        return (String uri, ImageView view) -> {
            Log.d(TAG, "Loading image into glide");
            int res = 0;
            switch (uri) {
                case PLACEHOLDER_SHOP:
                    res = R.drawable.default_store_image;
                    break;
                case PLACEHOLDER_PRODUCT:
                    res = R.drawable.default_no_image;
                    break;
            }

            RequestListener<Drawable> requestListener = new RequestListener<Drawable>() {
                @Override
                public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                            Target<Drawable> target, boolean isFirstResource) {
                    if (listener != null) {
                        listener.onImageLoadingError();
                    }
                    return false;
                }

                @Override
                public boolean onResourceReady(Drawable resource, Object model,
                                               Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                    if (listener != null) {
                        listener.onImageLoaded();
                    }
                    return false;
                }
            };

            if (res != 0) {
                Log.d(TAG, "first");
                GlideApp.with(view.getContext())
                        .load(res)
                        .listener(requestListener)
                        .into(view);
            } else {
                Log.d(TAG, "second");
                GlideApp.with(view.getContext())
                        .load(uri)
                        .listener(requestListener)
                        .error(R.drawable.default_no_image)
                        .into(view);
            }
        };
    }

    /**
     * Animates to hide the action bar.
     *
     * @param ab       the action bar to hide.
     * @param toolbar  the toolbar set as the action bar.
     * @param duration duration of the animation.
     */
    public void hideActionBar(@Nullable ActionBar ab, @Nullable Toolbar toolbar, long duration) {
        if (ab != null && ab.isShowing()) {
            if (toolbar != null) {
                toolbar.animate().translationY(-112).setDuration(duration)
                        .withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                ab.hide();
                            }
                        }).start();
            } else {
                ab.hide();
            }
        }
    }

    /**
     * Animates to show the action bar.
     *
     * @param ab       the action bar to hide.
     * @param toolbar  the toolbar set as the action bar.
     * @param duration duration of the animation.
     */
    public void showActionBar(@Nullable ActionBar ab, @Nullable Toolbar toolbar, long duration) {
        if (ab != null && !ab.isShowing()) {
            ab.show();
            if (toolbar != null) {
                toolbar.animate().translationY(0).setDuration(duration).start();
            }
        }
    }

    /**
     * Reorder items in a staggered grid layout while showing a user friendly message.
     * @param context the context.
     * @param layoutManager the layout manager used for the items.
     */
    public void reorderItems(Context context, StaggeredGridLayoutManager layoutManager) {
        if (context != null) {
            int spanCount = ConfigStore.getInstance().getProductColumnSpanCount(context);
            switch (spanCount) {
                case 1:
                    spanCount = 3;
                    break;
                case 2:
                    spanCount = 1;
                    break;
                case 3:
                    spanCount = 2;
            }
            ConfigStore.getInstance().setProductColumnSpanCount(context, spanCount);
            layoutManager.setSpanCount(spanCount);
            Toast.makeText(context, "Reordered items", Toast.LENGTH_SHORT).show();
        }
    }

    public static Util getInstance() {
        if (mUtil == null) {
            mUtil = new Util();
        }
        return mUtil;
    }

    @VisibleForTesting
    public static void setUtil(Util util) {
        mUtil = util;
    }
}
