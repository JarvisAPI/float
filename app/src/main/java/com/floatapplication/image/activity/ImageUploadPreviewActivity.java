package com.floatapplication.image.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.async_tasks.BitmapDecodeTask;
import com.floatapplication.async_tasks.UploadImageTask;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.server.ServerErrorCode;
import com.floatapplication.user.activity.UserActivity;
import com.floatapplication.user.util.UserSessionInterface;
import com.floatapplication.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jamescho on 2018-05-25.
 * Preview an image before allowing the user to confirm the upload.
 */

public class ImageUploadPreviewActivity extends AppCompatActivity {
    private static final String TAG = ImageUploadPreviewActivity.class.getSimpleName();

    private String mImagePath;
    private Snackbar mSnackbar;
    private int mOperation;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_upload_preview);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mOperation = bundle.getInt("operation");
            mImagePath = bundle.getString("image_path");
        } else {
            Log.e(TAG, "Bundle is null");
            finish();
            return;
        }
        ImageView imageView = findViewById(R.id.preview_image);
        imageView.setOnClickListener((View view) -> {
            final long duration = getResources().getInteger(R.integer.actionbar_show_hide_duration);
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                if (actionBar.isShowing()) {
                    Util.getInstance().hideActionBar(actionBar, mToolbar, duration);
                } else {
                    Util.getInstance().showActionBar(actionBar, mToolbar, duration);
                }
            }
        });
        BitmapDecodeTask task = new BitmapDecodeTask(imageView);
        task.execute(mImagePath);
        setupActionBar();
    }

    private void setupActionBar() {
        mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.image_preview_title);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.confirm_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                onComplete();
                return true;
            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }

    private void onComplete() {
        startUpload();
    }

    private void startUpload() {
        showConnectingSnackbar();
        FloatApplication.getUserSessionInterface()
                .getUserAccessToken(getApplicationContext(), new UserSessionInterface.TokenCallback() {
                    @Override
                    public void onTokenObtained(String token) {
                        uploadImage(token);
                    }

                    @Override
                    public void onTokenError(String errorMsg) {
                        Toast.makeText(getApplication(), "Error: try again later", Toast.LENGTH_LONG).show();
                        Log.e(TAG, errorMsg);
                        setResult(Activity.RESULT_CANCELED);
                        finish();
                    }
                });
    }

    private void uploadImage(String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + token);
        List<String> path = new ArrayList<>();
        path.add(mImagePath);
        String uploadPath;
        switch (mOperation) {
            case (UserActivity.REQUEST_PROFILE_IMAGE_CHANGE): {
                uploadPath = FloatApplication.getHypermediaInterface().getPath(HypermediaInterface.ADD_USER_PROFILE_IMAGE, null);
                break;
            }
            case (UserActivity.REQUEST_COVER_IMAGE_CHANGE): {
                uploadPath = FloatApplication.getHypermediaInterface().getPath(HypermediaInterface.ADD_USER_BACKGROUND_IMAGE, null);
                break;
            }
            default:
                uploadPath = "";
        }
        String uploadDataType = "image";
        UploadImageTask uploadImageTask =
                new UploadImageTask(uploadPath, uploadDataType, path, headers,
                        (String response) -> {
                            if (mSnackbar != null && mSnackbar.isShown()) {
                                mSnackbar.dismiss();
                            }
                            setResult(RESULT_OK);
                            finish();
                        },
                        this::onError);
        uploadImageTask.execute();
    }

    private void onError(VolleyError error) {
        if (error.networkResponse != null && error.networkResponse.statusCode == 400) {
            if (error.networkResponse.data != null) {
                try {
                    JSONObject jsonObject = new JSONObject(new String(error.networkResponse.data));
                    String errCode = jsonObject.getString(ServerErrorCode.FIELD_TYPE);
                    switch (errCode) {
                        case ServerErrorCode.ERR_FILE_TOO_LARGE:
                            runOnUiThread(() -> {
                                Toast.makeText(getApplicationContext(),
                                        getString(R.string.exceed_upload_size), Toast.LENGTH_SHORT).show();
                                setResult(Activity.RESULT_CANCELED);
                                finish();
                            });
                            return;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            runOnUiThread(() -> {
                Toast.makeText(getApplicationContext(), "Bad input", Toast.LENGTH_SHORT).show();
                setResult(Activity.RESULT_CANCELED);
                finish();
            });
        } else {
            Toast.makeText(getApplicationContext(), "Error please try again later", Toast.LENGTH_SHORT).show();
            error.printStackTrace();
            setResult(Activity.RESULT_CANCELED);
            finish();
        }
    }

    public void showConnectingSnackbar() {
        RelativeLayout relativeLayout = findViewById(R.id.image_upload_preview);
        mSnackbar = Snackbar.make(relativeLayout, "Uploading image", Snackbar.LENGTH_INDEFINITE);
        ViewGroup contentLay = (ViewGroup) mSnackbar.getView().findViewById(android.support.design.R.id.snackbar_text).getParent();
        ProgressBar item = new ProgressBar(getApplicationContext());
        contentLay.addView(item, 0);
        mSnackbar.show();
    }
}
