package com.floatapplication.shop.fragment;

import android.Manifest.permission;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.shop.util.ShopHandler;
import com.floatapplication.util.Callback;
import com.floatapplication.util.Util;
import com.simplexorg.mapfragment.map.BaseOnMapReadyCallback;
import com.simplexorg.mapfragment.map.SimpleMapFragment;
import com.simplexorg.mapfragment.marker.BaseMarker;
import com.simplexorg.mapfragment.marker.BaseMarkerOptions;
import com.simplexorg.mapfragment.model.BaseMarkerModel;
import com.simplexorg.mapfragment.model.BaseModelDataRetriever;
import com.simplexorg.mapfragment.model.GeoPoint;
import com.simplexorg.mapfragment.model.SelectableIconModel;
import com.simplexorg.mapfragment.model.SelectableIconModel.Builder;
import com.simplexorg.mapfragment.util.MapFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by william on 2018-05-10.
 * Fragment that allows user to set shop location on map.
 */

public class ShopSetLocationFragment extends Fragment implements FragmentCallback, BaseOnMapReadyCallback {
    private static final String TAG = ShopSetLocationFragment.class.getSimpleName();
    private static final String KEY_IS_RECREATING = "recreating";
    private static final String KEY_SHOP_LOCATION = "shopLoc";
    private static final String KEY_ADD_MARKER_LOC = "addMarkerLoc";
    private static final String KEY_SHOP_ICON_RES = "shopIconRes";

    private boolean mMapIsReady = false;
    private boolean mIsRecreating = false;

    private SimpleMapFragment mMapFragment;
    private BaseMarker mSetMarker;

    private GeoPoint mSavedAddMarkerLoc;
    private BaseMarker mAddMarker;
    private ShopHandler mShopHandler;

    private int mIconRes;

    private GeoPoint mShopLocation;
    private Callback mCallback;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mShopHandler = new ShopHandler();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shop_set_location, container, false);
        setupButtons(v);

        if (savedInstanceState != null) {
            mIsRecreating = savedInstanceState.getBoolean(KEY_IS_RECREATING, false);
            mShopLocation = savedInstanceState.getParcelable(KEY_SHOP_LOCATION);
            mSavedAddMarkerLoc = savedInstanceState.getParcelable(KEY_ADD_MARKER_LOC);
            mIconRes = savedInstanceState.getInt(KEY_SHOP_ICON_RES);
        }

        mMapFragment = (SimpleMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mMapFragment.setOnMapReadyCallback(this);
        return v;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_IS_RECREATING, true);
        if (mShopLocation != null) {
            outState.putParcelable(KEY_SHOP_LOCATION, mShopLocation);
        }
        if (mAddMarker != null) {
            outState.putParcelable(KEY_ADD_MARKER_LOC, mAddMarker.getPosition());
        }
        outState.putInt(KEY_SHOP_ICON_RES, mIconRes);
    }

    private BaseMarker createAddMarker(GeoPoint pos) {
        return mMapFragment.addMarker(new BaseMarkerOptions()
                .title("Placement Marker")
                .snippet("Long press to drag")
                .position(pos)
                .draggable(true));
    }

    private BaseMarker createSetMarker(GeoPoint pos) {
        return mMapFragment.addMarker(new BaseMarkerOptions()
                .title("My Shop Location")
                .position(pos));
    }

    @Override
    public void onMapReady() {
        try {
            mMapIsReady = true;
            enableLocationServices();
            if (!mIsRecreating) {
                animateToShopArea();
            } else {
                if (mSavedAddMarkerLoc != null) {
                    mAddMarker = createAddMarker(mSavedAddMarkerLoc);
                    mSavedAddMarkerLoc = null;
                }
                if (mShopLocation != null) {
                    mSetMarker = createSetMarker(mShopLocation);
                    mSetMarker.setIcon(mIconRes);
                }
                mIsRecreating = false;
            }
            setMapDataRetrievers();
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
    }

    private void enableLocationServices() {
        Context context = getContext();
        if (context != null && ContextCompat.checkSelfPermission(context, permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMapFragment.setMyLocationEnabled(true);
        }
    }

    /**
     * animate the map to the area where the user should make the shop.
     */
    private void animateToShopArea() {
        Activity activity = getActivity();
        if (activity != null) {
            Intent data = activity.getIntent();
            GeoPoint loc = data.getParcelableExtra("location");
            if (loc != null) {
                mMapFragment.animateCamera(loc, 17.0f);
            }
        }
    }

    private void setMapDataRetrievers() {
        mMapFragment.setDataRetriever(new BaseModelDataRetriever<SelectableIconModel>() {
            @Override
            public void getModels(OnModelsRetrievedListener<SelectableIconModel> listener,
                                  GeoPoint geoPoint) {
                Map<String, String> variableMap = new HashMap<>();
                variableMap.put("latitude", Double.toString(geoPoint.latitude));
                variableMap.put("longitude", Double.toString(geoPoint.longitude));
                FloatApplication.getHypermediaInterface()
                        .follow(Hypermedia.GET, HypermediaInterface.MAP_ICONS, variableMap, null,
                                (String response) -> {
                                    List<SelectableIconModel> iconModels = Util.getInstance().createIconModels(response);
                                    listener.onModelsRetrieved(iconModels);
                                }, (VolleyError error) -> {
                                    Log.e(TAG, "Unable to get map icons");
                                    Log.e(TAG, "Volley error msg: " + error.getMessage());
                                });
            }

            @Override
            public void getModelDetails(OnModelDetailsRetrievedListener<SelectableIconModel> listener,
                                        BaseMarkerModel markerModel) {
                mShopHandler.getShop(markerModel.getId(),
                        (Shop shop) -> {
                            if (markerModel instanceof SelectableIconModel) {
                                Builder builder = new Builder((SelectableIconModel) markerModel);
                                listener.onModelDetailsRetrieved(builder
                                        .title(shop.getShopName())
                                        .description(shop.getShopDescription())
                                        .build());
                            }
                        });
            }
        });
    }

    private void dropAddIconMarker(Point start, Point end) {
        GeoPoint target = mMapFragment.projectFromScreenLocation(end);
        GeoPoint startLatLng = mMapFragment.projectFromScreenLocation(start);
        Log.d(TAG, "target: " + target + ", startLatLng: " + startLatLng);
        mAddMarker = createAddMarker(startLatLng);
        MapFactory.getInstance()
                .createDropAnimator(1500L, mAddMarker, new Handler(), target, startLatLng)
                .start();
    }

    private void showAddMarker() {
        Point center = new Point();
        center.x = getResources().getDisplayMetrics().widthPixels / 2;
        center.y = getResources().getDisplayMetrics().heightPixels / 2;
        Point startPoint = new Point(center);
        startPoint.y = 0;
        dropAddIconMarker(startPoint, center);
    }

    private void setupButtons(View v) {
        FloatingActionButton doneButton = v.findViewById(R.id.fab_done);
        doneButton.setOnClickListener((View view) -> {
            if (mAddMarker != null) {
                verifyShopPlacement();
            } else {
                Toast.makeText(getContext(), "No marker set", Toast.LENGTH_SHORT).show();
            }
        });
        FloatingActionButton dropMarkerButton = v.findViewById(R.id.fab_drop);
        dropMarkerButton.setOnClickListener((View view) -> {
            if (mAddMarker != null) {
                mAddMarker.remove();
            }
            showAddMarker();
        });
    }

    public void setConfirmCallback(Callback callback) {
        mCallback = callback;
    }

    public GeoPoint getSetShopLocation() {
        return mShopLocation;
    }

    /**
     * Verify that the shop is placed on a valid location on the map.
     * i.e that its not too close to any other markers already on the map.
     */
    private void verifyShopPlacement() {
        double range = 3; // in meters
        if (!mMapFragment.hasMakerWithinDistance(mAddMarker.getPosition(), range)) {
            // No marker within range so we can attempt to return it.
            setShopIconMarker();
            if (mCallback != null) {
                mCallback.callback();
            }
        } else {
            Toast.makeText(getContext(), getString(R.string.shop_too_close_error), Toast.LENGTH_SHORT).show();
        }
    }


    private void setShopIconMarker() {
        if (mAddMarker == null) {
            return;
        }
        mShopLocation = mAddMarker.getPosition();
        Log.d(TAG, "Shop Location: " + mShopLocation);
        if (mSetMarker != null) {
            mSetMarker.remove();
        }
        mSetMarker = createSetMarker(mShopLocation);
        if (mIconRes != 0) {
            mSetMarker.setIcon(mIconRes);
        }
    }

    @Override
    public void onResumeFragment(Bundle data) {
        if (mMapIsReady && mAddMarker == null) {
            showAddMarker();
        }
        if (data != null) {
            int iconRes = data.getInt("icon_res");
            if (iconRes != 0 && mIconRes != iconRes) {
                Log.d(TAG, "iconRes: " + iconRes);
                mIconRes = iconRes;
                if (mSetMarker != null) {
                    mSetMarker.setIcon(mIconRes);
                }
            }
        }
    }
}
