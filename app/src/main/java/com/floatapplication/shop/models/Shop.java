package com.floatapplication.shop.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.floatapplication.user.models.User;
import com.floatapplication.user.models.User.NamePair;
import com.floatapplication.util.Util;
import com.simplexorg.mapfragment.model.GeoPoint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by william on 2017/12/25.
 *
 */
public class Shop implements Parcelable {
    public static final String ID = "shop_id";
    public static final String NAME = "shop_name";
    public static final String DESCRIPTION = "shop_description";
    public static final String OWNERS = "usernames";
    public static final String IMAGE_FILES = "images";
    public static final String LOCATION = "location";

    private String mShopId;
    private String mShopName;
    private String mShopDescription;
    private List<User.NamePair> mShopUsernames;
    private List<String> mShopImages;
    private GeoPoint mShopLocation;

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<Shop>() {
        @Override
        public Shop createFromParcel(Parcel parcel) {
            return new Shop(parcel);
        }
        @Override
        public Shop[] newArray(int size) {
            return new Shop[size];
        }
    };

    private Shop(Parcel parcel) {
        mShopId = parcel.readString();
        mShopName = parcel.readString();
        mShopDescription = parcel.readString();
        mShopUsernames = new ArrayList<>();
        parcel.readList(mShopUsernames, NamePair.class.getClassLoader());
        mShopImages = new ArrayList<>();
        parcel.readStringList(mShopImages);
        mShopLocation = parcel.readParcelable(GeoPoint.class.getClassLoader());
    }

    private Shop() {
        mShopUsernames = new ArrayList<>();
        mShopImages = new ArrayList<>();
    }

    public static Shop createShop(String shopId, String shopName, NamePair namePair) {
        Shop shop = new Shop();
        shop.mShopId = shopId;
        shop.mShopName = shopName;
        shop.mShopUsernames.add(namePair);
        return shop;
    }

    public void augmentShopInfo(String info) {
        try {
            JSONObject shopJson = new JSONObject(info).getJSONObject("shop");
            mShopDescription = shopJson.getString(DESCRIPTION);

            if (shopJson.has(IMAGE_FILES)) {
                JSONArray shopImageJsonArray = shopJson.getJSONArray(IMAGE_FILES);
                for (int i = 0; i < shopImageJsonArray.length(); i++) {
                    mShopImages.add(shopImageJsonArray.getString(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static Shop createShop(JSONObject shopInfo) {
        Shop shop = new Shop();
        try {
            JSONObject shopJson = shopInfo.getJSONObject("shop");
            shop.mShopId = shopJson.getString(ID);
            shop.mShopName = shopJson.getString(NAME);
            shop.mShopDescription = shopJson.getString(DESCRIPTION);

            JSONArray shopOwners = shopInfo.getJSONArray(OWNERS);
            for (int i = 0; i < shopOwners.length(); i ++) {
                JSONObject namePairJson = shopOwners.getJSONObject(i);
                shop.mShopUsernames.add(
                        new User.NamePair(namePairJson.getString(User.USERNAME), namePairJson.getString(User.NAME)));
            }

            shop.mShopLocation = getLocation(shopJson);

            if (shopJson.has(IMAGE_FILES)) {
                JSONArray shopImageJsonArray = shopJson.getJSONArray(IMAGE_FILES);
                for (int i = 0; i < shopImageJsonArray.length(); i++) {
                    shop.mShopImages.add(shopImageJsonArray.getString(i));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return shop;
    }

    private static GeoPoint getLocation(JSONObject shopJson) throws JSONException {
        JSONObject locationJSON = shopJson.getJSONObject(LOCATION);
        JSONArray coords = locationJSON.getJSONArray("coordinates");
        return new GeoPoint(coords.getDouble(1), coords.getDouble(0));
    }

    public String getShopName() {
        return mShopName;
    }

    public String getShopDescription() {
        return mShopDescription;
    }

    public String getShopId() {
        return mShopId;
    }

    public List<NamePair> getShopUsernames() {
        return mShopUsernames;
    }

    public NamePair getOwnerNamePair() {
        if (mShopUsernames.isEmpty()) {
            return null;
        }
        return mShopUsernames.get(0);
    }

    public List<String> getShopImages() {
        return mShopImages;
    }

    public List<String> getShopImageLinks() {
        List<String> arr = new ArrayList<>();
        for (String imageFile : mShopImages) {
            arr.add(Util.getInstance().constructImageUrl(imageFile));
        }
        return arr;
    }

    public GeoPoint getShopLocation() {
        return mShopLocation;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeString(mShopId);
        dest.writeString(mShopName);
        dest.writeString(mShopDescription);
        dest.writeList(mShopUsernames);
        dest.writeStringList(mShopImages);
        dest.writeParcelable(mShopLocation, 0);
    }
}
