package com.floatapplication.shop.util;

import android.support.v7.widget.RecyclerView;

import com.floatapplication.product.models.Product;
import com.floatapplication.shop.adapter.ShopViewAdapter;
import com.floatapplication.TestApplication;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by william on 2018-06-06.
 *
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ShopProductsPresenterUnitTest {
    private ShopProductsPresenter presenter;
    private ShopViewAdapter adapter;
    private RecyclerView recyclerView;
    private ShopProductLoader loader;

    @Before
    public void setup() {
        presenter = new ShopProductsPresenter();
        adapter = mock(ShopViewAdapter.class);
        recyclerView = mock(RecyclerView.class);
        loader = mock(ShopProductLoader.class);
        when(adapter.getPresenter()).thenReturn(mock(ShopViewProductPresenter.class));
        presenter.present(recyclerView, adapter, loader);
    }

    @Test
    public void loadProducts_initially() {
        verify(loader).getShopProducts();
    }

    @Test
    public void onBottomReached_thenGetProducts() {
        reset(loader);
        presenter.onBottomReached();
        verify(loader).getShopProducts();
    }

    @Test
    public void onProductLoaded_shouldAddShopItems() {
        List<Product> products = new ArrayList<>();
        products.add(mock(Product.class));
        presenter.onProductLoaded(products);
        verify(adapter).addShopItems(products);
    }
}
