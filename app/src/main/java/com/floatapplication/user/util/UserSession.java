package com.floatapplication.user.util;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Log;

import com.android.volley.VolleyError;
import com.floatapplication.R;
import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

/**
 * Created by william on 2018/1/2.
 * Keeps track of any session state of the app, for instance user logged in state, etc.
 */
public class UserSession
        implements
        UserSessionInterface {
    private static final String TAG = UserSession.class.getSimpleName();
    private String mAccessToken;
    private String mUsername;
    private String mRefreshToken;
    private boolean mLoggedInStatus;
    private Date mExpiration;

    public UserSession() {
        Log.d(TAG, "Creating user session!, reference: " + this);
    }

    @Override
    public boolean isLoggedIn() {
        return mLoggedInStatus;
    }

    public void logout() {
        Log.d(TAG, "logout");
        mLoggedInStatus = false;
        mAccessToken = null;
        mRefreshToken = null;
        mUsername = null;
    }

    @Override
    public void getUserAccessToken(@NonNull Context context, @NonNull TokenCallback callback) {
        long expirationMargin = 60000; // Require token expiration to be more than this margin in milliseconds.
        if (new Date(System.currentTimeMillis() + expirationMargin).before(mExpiration)) {
            callback.onTokenObtained(mAccessToken);
            return;
        }
        Log.d(TAG, "Refreshing token");
        refreshToken(context, callback);
    }

    private void refreshToken(final Context context, final TokenCallback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Bearer " + mAccessToken);
        try {
            String jsonBody = new JSONObject()
                    .put("refresh_token", mRefreshToken)
                    .toString();
            FloatApplication.getHypermediaInterface()
                    .follow(Hypermedia.POST, HypermediaInterface.LOGIN, null, headers, jsonBody,
                            new ServerCallback() {
                                @Override
                                public void onSuccessResponse(String response) {
                                    UserHandler.refreshToken(context, response, UserSession.this);
                                    callback.onTokenObtained(mAccessToken);
                                }
                            },
                            new ServerErrorCallback() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    callback.onTokenError("Error: " + error.getMessage());
                                }
                            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getUserRefreshToken() {
        return mRefreshToken;
    }

    @Override
    public String getUsername() {
        return mUsername;
    }

    @Override
    public void setRefreshToken(@NonNull String refreshToken) {
        Log.d(TAG, "Set user tokens, refresh_token: " + refreshToken);
        mRefreshToken = refreshToken;
    }

    @Override
    public void updateAccessToken(Context context, String accessToken) {
        Log.d(TAG, "updateAccessToken");
        try {
            Claims claims = Jwts.parser().setSigningKey(getPublicKey(context))
                    .parseClaimsJws(accessToken).getBody();
            mUsername = claims.getSubject();
            mExpiration = claims.getExpiration();
            // If current data is before expiration then we assume that we are logged in.
            mLoggedInStatus = new Date(System.currentTimeMillis()).before(mExpiration);
        } catch (Exception e) {
            mLoggedInStatus = false;
            e.printStackTrace();
        }
        mAccessToken = accessToken;
    }

    private PublicKey getPublicKey(Context context) throws Exception {
        InputStream inputStream = context.getResources().openRawResource(R.raw.jws_key);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
        List<String> lines = new ArrayList<>();

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            lines.add(line);
        }

        // removes the first and last lines of the file (comments)
        if (lines.size() > 1 && lines.get(0).startsWith("-----") && lines.get(lines.size() - 1).startsWith("-----")) {
            lines.remove(0);
            lines.remove(lines.size() - 1);
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (String keyLine : lines) {
            stringBuilder.append(keyLine);
        }
        String keyStr = stringBuilder.toString();
        // converts the String to a PublicKey instance
        byte[] keyBytes = Base64.decode(keyStr.getBytes("utf-8"), Base64.DEFAULT);
        X509EncodedKeySpec spec = new X509EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(spec);
    }
}
