package com.floatapplication.shop.util;

import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.R;
import com.floatapplication.product.models.Product;
import com.floatapplication.util.GlideRequests;
import com.floatapplication.util.Util;

/**
 * Created by william on 2018-05-25.
 * MVP pattern for displaying products in shop view.
 */
public class ShopViewProductPresenter {
    public interface OnProductClickListener {
        void onProductClick(Product product, View itemView);
    }
    private GlideRequests mGlide;
    private OnProductClickListener mOnProductClickListener;

    public ShopViewProductPresenter setGlide(GlideRequests glide) {
        mGlide = glide;
        return this;
    }

    public void present(final View itemView, ImageView productImage, TextView productName,
                         TextView productPrice, final Product product) {
        productName.setText(product.getProductName());
        productPrice.setText(product.getProductPrice());
        if ("null".equals(product.getProductMainImageFile()) || product.getProductMainImageFile() == null) {
            mGlide
                    .load(R.drawable.default_no_image)
                    .into(productImage);
        } else {
            String url = Util.getInstance().constructImageUrl(product.getProductMainImageFile());
            mGlide
                    .load(url)
                    .placeholder(R.drawable.default_no_image)
                    .into(productImage);
        }
        itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnProductClickListener != null) {
                    mOnProductClickListener.onProductClick(product, itemView);
                }
            }
        });
        ViewCompat.setTransitionName(itemView, product.getProductId());
    }

    void setOnProductClickListener(OnProductClickListener listener) {
        mOnProductClickListener = listener;
    }
}
