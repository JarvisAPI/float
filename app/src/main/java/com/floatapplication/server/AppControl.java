package com.floatapplication.server;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageCache;
import com.android.volley.toolbox.Volley;
import com.floatapplication.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by William Ou.
 * This class follows a singleton pattern, it is used to send messages to the server.
 */
public class AppControl implements AppControlInterface {
    /**
     * Log or request TAG.
     */
    private static final String TAG = "VolleyRequest";

    // Global request queue for Volley.
    private final RequestQueue mRequestQueue;

    private ImageLoader mImageLoader;

    /**
     * Private constructor so we follow the singleton design pattern.
     */
    public AppControl(Context context) {
        SSLSocketFactory sslSocketFactory = getSSLSocketFactory(context);
        HurlStack hurlStack = getHurlStack(sslSocketFactory);
        Cache cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024); // 1MB cap
        mRequestQueue = new RequestQueue(cache, new BasicNetwork(hurlStack));
        mRequestQueue.start();
        mImageLoader = new ImageLoader(mRequestQueue, new ImageCache() {
            private final LruCache<String, Bitmap> mCache = new LruCache<>(10);
            @Override
            public Bitmap getBitmap(String url) {
                return mCache.get(url);
            }
            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                mCache.put(url, bitmap);
            }
        });
    }

    public static RequestQueue getSSLRequestQueue(Context context) {
        SSLSocketFactory sslSocketFactory = getSSLSocketFactory(context);
        HurlStack hurlStack = getHurlStack(sslSocketFactory);
        return Volley.newRequestQueue(context, hurlStack);
    }

    public ImageLoader getImageLoader() {
        return mImageLoader;
    }

    private static HurlStack getHurlStack(final SSLSocketFactory sslSocketFactory) {
        if (sslSocketFactory == null) {
            Log.e(TAG, "Unable to use https");
            return null;
        }
        return new CustomHurlStack(sslSocketFactory);
    }

    private static SSLSocketFactory getSSLSocketFactory(Context context) {
        try(InputStream caInput =
                    context.getResources().openRawResource(R.raw.mapapp_server_cert)) {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            Certificate ca = cf.generateCertificate(caInput);

            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, tmf.getTrustManagers(), null);
            return sslContext.getSocketFactory();
        } catch (IOException | CertificateException | KeyStoreException |
                NoSuchAlgorithmException | KeyManagementException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static HostnameVerifier getHostnameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession sslSession) {
                return ServerContact.HOST.equals(hostname);
            }
        };
    }

    /**
     * Adds the specific request to the global queue, if tag is specified then it's
     * used, otherwise default tag is used
     *
     * @param req the request to add to the request queue
     * @param tag the tag to associate the request with
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

        VolleyLog.d("Adding request to queue: %s", req.getUrl());

        mRequestQueue.add(req);
    }

    /**
     * Adds the specified request to the global queue using the default TAG
     *
     * @param req the request to add to the request queue
     */
    public <T> void addToRequestQueue(Request<T> req) {
        // set the default tag if tag is empty
        req.setTag(TAG);

        mRequestQueue.add(req);
    }

    /**
     * Cancels all pending request made without specifying an explicit tag.
     */
    public void cancelPendingRequest() {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(TAG);
        }
    }

    /**
     * Cancels all pending request by the specified TAG, it is important
     * to specify a TAG so that the pending/ongoing requests can be cancelled
     *
     * @param tag the tag associated with the requests that is to be cancelled
     */
    public void cancelPendingRequest(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }

    private static class CustomHurlStack extends HurlStack {
        private SSLSocketFactory mSSLSocketFactory;

        private CustomHurlStack(SSLSocketFactory sslSocketFactory) {
            mSSLSocketFactory = sslSocketFactory;
        }

        @Override
        protected HttpURLConnection createConnection(URL url) throws IOException {
            HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
            try {
                httpsURLConnection.setSSLSocketFactory(mSSLSocketFactory);
                httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return httpsURLConnection;
        }
    }
}