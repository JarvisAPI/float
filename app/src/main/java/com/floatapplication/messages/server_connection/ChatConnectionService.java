package com.floatapplication.messages.server_connection;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.messages.models.CustomMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Chat connection service that is used to wrap around the XMPP connection thread.
 */
public class ChatConnectionService extends Service {
    private static final String TAG = "ChatConnectionService";

    public interface OnMessageSentListener {
        /**
         * Callback is invoked on main thread.
         *
         * @param message custom message.
         */
        void onMessageSent(CustomMessage message);
    }

    public interface OnArchiveMessageReceivedListener {
        /**
         * Callback is invoked on main thread.
         *
         * @param archiveMessages archive messages.
         */
        void onArchiveMessagesReceived(List<CustomMessage> archiveMessages);

        /**
         * Callback is invoked on main thread.
         *
         * @param archiveMessages archive messages.
         */
        void onMostRecentMessagesReceived(List<CustomMessage> archiveMessages);
    }

    public enum MessageType {
        SEND("send"),
        RECEIVE("receive");
        private String messageType;

        MessageType(String type) {
            this.messageType = type;
        }

        public String get() {
            return messageType;
        }
    }

    public static final String DIALOG_LOAD_STATE_CHAGE = "dialog_load_state_change";
    public static final String CONNECTION_STATE_CHANGE = "connection_state_change";
    public static final String INCOMING_MESSAGE = "incoming_message";

    public static final String MARK_DIALOG_REQUEST_KEY_BARE_PEER = "barePeer";
    public static final String MARK_DIALOG_REQUEST_KEY_COUNT = "count";
    public static final String JSON_DIALOG_LIST_KEY = "dialog_list";
    public static final String JSON_NOTIFICATION_TOKEN_KEY = "notification_token";

    private Map<String,XMPPThread> mChatConnections = new HashMap<>();
    private OnMessageSentListener mOnMessageSentListener;
    private OnArchiveMessageReceivedListener mOnArchiveMessageReceivedListener;
    private final Binder mBinder = new ChatConnectionServiceBinder();

    public class ChatConnectionServiceBinder extends Binder {
        public ChatConnectionService getService() {
            return ChatConnectionService.this;
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate()");
    }

    /**
     * Starts server connection thread
     */
    public void reconnect(String jid) {
        Log.d(TAG," reconnect()");
        if(mChatConnections.get(jid) == null) {
            XMPPThread connectionThread = new XMPPThread(new ChatConnection(getApplicationContext()), jid);
            connectionThread.start();
            mChatConnections.put(jid, connectionThread);
        } else if(!mChatConnections.get(jid).isAlive()){
            Log.d(TAG, "mXMPPThread is dead");
            mChatConnections.get(jid).dispose();
            XMPPThread connectionThread = new XMPPThread(new ChatConnection(getApplicationContext()), jid);
            connectionThread.start();
            mChatConnections.put(jid, connectionThread);
        } else {
            Log.d(TAG,"mXMPPThread is alive");
            Message message = new Message();
            message.what = XMPPThread.MSG_RECONNECT;
            message.obj = jid;
            sendHandlerMessage(jid, message);
        }
    }



    private void sendHandlerMessage(String jid, Message message) {
        if (mChatConnections.get(jid) != null && mChatConnections.get(jid).getHandler() != null) {
            Log.d(TAG,"mXMPPThread.getHandler() is not null");
            mChatConnections.get(jid).getHandler().sendMessage(message);
        } else if(mChatConnections.get(jid) != null && mChatConnections.get(jid).getConnection().getState().equals(ChatConnection.ConnectionState.AUTHENTICATED)){
            Log.d(TAG,"mXMPPThread.getHandler() is null");
            mChatConnections.get(jid).addToMessageQueue(message);
        }
    }

    public void setOnMessageSentListener(@NonNull OnMessageSentListener onMessageSentListener) {
        mOnMessageSentListener = onMessageSentListener;
    }

    public void setOnArchiveMessageReceivedListener(@NonNull OnArchiveMessageReceivedListener onArchiveMessageReceivedListener) {
        mOnArchiveMessageReceivedListener = onArchiveMessageReceivedListener;
    }

    /**
     * @param textMessage text to send.
     * @param contactJid person to send to.
     * @return true if message is able to be sent, false otherwise.
     */
    public boolean sendMessage(String jid, String textMessage, String contactJid) {
        Log.d(TAG, "sendMessage()");
        if (mChatConnections.get(jid).getConnection().getState().equals(ChatConnection.ConnectionState.AUTHENTICATED)) {
            Message message = new Message();
            message.what = XMPPThread.MSG_SEND_TEXT;
            Bundle bundle = new Bundle();
            bundle.putString("text", textMessage);
            bundle.putString("jid", contactJid);
            message.setData(bundle);
            message.obj = mOnMessageSentListener;
            sendHandlerMessage(jid, message);
            return true;
        }
        return false;
    }

    public void fetchArchiveMessages(String jid, CustomMessage tailMessage) {
        Log.d(TAG, "fetchArchiveMessages");
        if (mChatConnections.get(jid).getConnection().getState().equals(ChatConnection.ConnectionState.AUTHENTICATED)) {
            Log.d(TAG, "AUTHENTICATED fetching messages for " + tailMessage.getUser().getId());
            Message message = new Message();
            message.what = XMPPThread.MSG_RET_ARCHIVE;
            Bundle bundle = new Bundle();
            bundle.putParcelable("msg", tailMessage);
            message.setData(bundle);
            message.obj = mOnArchiveMessageReceivedListener;
            sendHandlerMessage(jid, message);
        }
        else{
            Log.d(TAG, "not AUTHENTICATED returning an empty list");
            mOnArchiveMessageReceivedListener.onArchiveMessagesReceived(new ArrayList<>());
        }
    }

    public void getMostRecentMessages(String jid, CustomMessage mostRecentMessage) {
        Log.d(TAG, "getMostRecentMessages()");
        if (mChatConnections.get(jid).getConnection().getState().equals(ChatConnection.ConnectionState.AUTHENTICATED)) {
            Message message = new Message();
            message.what = XMPPThread.MSG_GET_MOST_RECENT;
            Bundle bundle = new Bundle();
            bundle.putParcelable("msg", mostRecentMessage);
            message.setData(bundle);
            message.obj = mOnArchiveMessageReceivedListener;
            sendHandlerMessage(jid, message);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand()");
        ArrayList<String> jids = FloatApplication.getFactoryInterface().getMessageUserDatabase().getUsers();
        for(String jid: jids){
            createInitialXMPPThread(jid);
        }
        return Service.START_NOT_STICKY;
    }

    private void createInitialXMPPThread(String jid){
        XMPPThread mainUserThread = new XMPPThread(new ChatConnection(getApplicationContext()), jid);
        mainUserThread.start();
        mChatConnections.put(jid, mainUserThread);
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy()");
        Iterator it = mChatConnections.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            Message message = new Message();
            message.what = XMPPThread.MSG_DISCONNECT;
            sendHandlerMessage(pair.getKey().toString(),message);
            it.remove();
        }
        super.onDestroy();
    }

    public ChatConnection.ConnectionState getState(String jid) {
        if(mChatConnections.get(jid) == null){
            return ChatConnection.ConnectionState.DISCONNECTED;
        }
        else if(!mChatConnections.get(jid).isAlive()) {
            return ChatConnection.ConnectionState.DISCONNECTED;
        }
        else{
            return  mChatConnections.get(jid).getConnection().getState();
        }
    }

    public ChatConnection.DialogLoadState getLoadState(String jid){
        return mChatConnections.get(jid).getConnection().getsDialogLoadState();
    }

    public void setIsBindedWithDialogActivity(String jid, boolean isBinded) {
        mChatConnections.get(jid).getConnection().setIsBindedWithDialogActivity(isBinded);
    }

    public void setCurrentContactView(String jid, String currentView) {
        mChatConnections.get(jid).getConnection().setCurrentContactView(currentView);
    }

    public boolean connectionReady(String jid){
        return mChatConnections.get(jid) != null
                && getState(jid).equals(ChatConnection.ConnectionState.AUTHENTICATED);
    }
}
