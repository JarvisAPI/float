package com.floatapplication.user.util;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.user.models.UserShopItem;
import com.floatapplication.util.GlobalHandler;
import com.floatapplication.util.IFactory;
import com.floatapplication.util.Util;
import com.simplexorg.mapfragment.model.GeoPoint;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * Created by william on 2018-06-07.
 * Presents items in the user profile list fo example shops.
 */

public class UserProfileShopItemPresenter
        implements
        OnClickListener {
    public interface UserShopItemListener {
        void onGoToShopClicked(String shopId);

        void onGoToShopLocationClicked(GeoPoint geoPoint);

        void onShopEditClicked(String shopId, String shopName);

        void onGoToShopInboxClicked(String shopId);
    }

    private UserShopItemListener mUserShopItemListener;
    private boolean mIsProfileOwner;

    public void present(TextView shopNameText, TextView shopLocationText, ImageView shopImage,
                        ImageView goToShopButton, ImageView goToShopLocationButton,
                        ImageView goToShopEditButton, ImageView goToShopInboxButton, UserShopItem shopItem) {
        goToShopButton.setTag(R.id.tag0, shopItem);
        goToShopEditButton.setTag(R.id.tag0, shopItem);
        goToShopLocationButton.setTag(R.id.tag0, shopItem);
        goToShopInboxButton.setTag(R.id.tag0, shopItem);
        shopNameText.setText(shopItem.getShopName());
        Util.getInstance().getAddress(shopLocationText.getContext(),
                shopItem.getShopLocation(), shopLocationText::setText);
        setupShopEditButton(goToShopEditButton);
        setupGoToShopInboxButton(goToShopInboxButton);
        goToShopButton.setOnClickListener(this);
        goToShopLocationButton.setOnClickListener(this);
        loadShopImage(shopItem, shopImage);
    }

    // TODO: make loading image more efficient
    private void loadShopImage(UserShopItem shopItem, ImageView shopImage) {
        Map<String, String> vMap = IFactory.getInstance().createMap();
        vMap.put(Shop.ID, shopItem.getShopId());
        FloatApplication.getHypermediaInterface()
                .follow(Hypermedia.GET, Hypermedia.SHOP_INFO, vMap, null,
                        (String response) -> {
                            try {
                                JSONObject shopJson = new JSONObject(response);
                                Shop shop = Shop.createShop(shopJson);
                                List<String> shopImages = shop.getShopImages();
                                if (!shopImages.isEmpty()) {
                                    GlobalHandler.getInstance().loadImage(shopImage,
                                            Util.getInstance().constructImageUrl(shopImages.get(0)),
                                            R.drawable.default_store_image);
                                } else {
                                    GlobalHandler.getInstance().loadImage(shopImage,
                                            "",
                                            R.drawable.default_store_image);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        },
                        (VolleyError error) -> {
                            GlobalHandler.getInstance().loadImage(shopImage,
                                    "",
                                    R.drawable.default_store_image);
                            error.printStackTrace();
                        });
    }

    private void setupGoToShopInboxButton(ImageView goToShopInboxButton) {
        if (mIsProfileOwner) {
            goToShopInboxButton.setVisibility(View.VISIBLE);
            goToShopInboxButton.setOnClickListener(this);
        } else {
            goToShopInboxButton.setVisibility(View.INVISIBLE);
        }
    }

    private void setupShopEditButton(ImageView goToShopEditButton) {
        if (mIsProfileOwner) {
            goToShopEditButton.setVisibility(View.VISIBLE);
            goToShopEditButton.setOnClickListener(this);
        } else {
            goToShopEditButton.setVisibility(View.INVISIBLE);
        }
    }

    public void setUserShopItemListener(UserShopItemListener listener) {
        mUserShopItemListener = listener;
    }

    void setIsProfileOwner() {
        mIsProfileOwner = true;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        UserShopItem shopItem = (UserShopItem) view.getTag(R.id.tag0);
        switch (id) {
            case R.id.go_to_shop_image_view:
                mUserShopItemListener.onGoToShopClicked(shopItem.getShopId());
                break;
            case R.id.shop_location_icon:
                mUserShopItemListener.onGoToShopLocationClicked(shopItem.getShopLocation());
                break;
            case R.id.shop_edit_icon:
                mUserShopItemListener.onShopEditClicked(shopItem.getShopId(), shopItem.getShopName());
                break;
            case R.id.shop_messages_icon:
                mUserShopItemListener.onGoToShopInboxClicked(shopItem.getShopId());
                break;
        }
    }


}
