package com.floatapplication.product.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by william on 2018/1/1.
 * Base product that contains information on fields on a product. This class
 * is used to hold information on a product before creating the product via
 * communication with the server.
 */
public class ProductCreator extends ProductBase {
    private String mProductMainImagePath;
    private List<String> mProductImagePaths;

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<ProductCreator>() {
        @Override
        public ProductCreator createFromParcel(Parcel parcel) {
            return new ProductCreator(parcel);
        }
        @Override
        public ProductCreator[] newArray(int size) {
            return new ProductCreator[size];
        }
    };

    private ProductCreator(Parcel parcel) {
        super(parcel);
        mProductMainImagePath = parcel.readString();
        mProductImagePaths = new ArrayList<>();
        parcel.readStringList(mProductImagePaths);
    }

    public ProductCreator() {
        mProductImagePaths = new ArrayList<>();
    }

    public void setProductName(String productName) {
        mProductName = productName;
    }

    public void setProductPrice(String productPrice) {
        mProductPrice = productPrice;
    }

    public void setProductCategory(String productCategory) {
        mProductCategory = productCategory.toLowerCase();
    }

    public void setProductDescription(String productDescription) {
        mProductDescription = productDescription;
    }

    public void setMainImagePath(String imagePath) {
        mProductMainImagePath = imagePath;
    }

    public void setImagePaths(List<String> imagePaths) {
        mProductImagePaths.clear();
        mProductImagePaths.addAll(imagePaths);
    }

    public void genProductId() {
        mProductId = UUID.randomUUID().toString();
    }

    public String getProductMainImagePath() {
        return mProductMainImagePath;
    }

    public List<String> getProductImagePaths() {
        return mProductImagePaths;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(mProductMainImagePath);
        dest.writeStringList(mProductImagePaths);
    }
}