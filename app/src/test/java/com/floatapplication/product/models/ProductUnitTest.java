package com.floatapplication.product.models;

import android.os.Parcel;

import com.floatapplication.TestApplication;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;

/**
 * Created by william on 2018-06-14.
 * Test parcelable done correctly.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ProductUnitTest extends ProductBaseChecker {
    private Product product;


    private JSONObject genProductJson() throws Exception {
        return new JSONObject()
                .put(ProductBase.ID, productId)
                .put(ProductBase.NAME, productName)
                .put(ProductBase.PRICE, productPrice)
                .put(ProductBase.MAIN_IMAGE, productMainImageFile)
                .put(ProductBase.SHOP_ID, productShopId);
    }

    private JSONObject genProductInfoJson() throws Exception {
        return new JSONObject()
                .put(ProductBase.IMAGES, new JSONArray(productImageFiles))
                .put(ProductBase.DESCRIPTION, productDescription)
                .put(ProductBase.CATEGORY, productCategory);
    }

    @Before
    public void setup() throws Exception {
        productId = "id";
        productName = "name";
        productPrice = "10";
        productCategory = "food";
        productDescription = "desc";
        productMainImageFile = "file";
        productShopId = "shop_id";
        productImageFiles.add("image-file");
        product = Product.createBasicProduct(genProductJson());
        product.augmentProductInfo(genProductInfoJson());
    }

    @Test
    public void writeToParcel_andReconstruct() {
        Parcel parcel = Parcel.obtain();
        product.writeToParcel(parcel, 0);
        parcel.setDataPosition(0);
        Product result = (Product) Product.CREATOR.createFromParcel(parcel);
        check(result);
    }

    @Test
    public void augmentProductInfo_twiceDoesNotResultInDuplicateImageFiles() throws Exception {
        int numImageFiles = product.getProductImageFiles().size();
        product.augmentProductInfo(genProductInfoJson());
        assertEquals(numImageFiles, product.getProductImageFiles().size());
    }
}
