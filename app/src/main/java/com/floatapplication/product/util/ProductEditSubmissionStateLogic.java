package com.floatapplication.product.util;

/**
 * Created by william on 2018-06-18.
 * logic to execute inside the states of product edit submission state machine.
 */

public interface ProductEditSubmissionStateLogic {
    void uploadProductDetails();
    void uploadNewImages();
    void uploadNewMainImage();
    void onSubmitFailed();
    void onSubmitSuccess();
    void deleteMainImage();
    void swapMainImageAutoSelected();
    void swapMainImageUserSelected();
    void deleteImages();
}
