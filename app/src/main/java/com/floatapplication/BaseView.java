package com.floatapplication;


public interface BaseView<T> {
    void setPresenter(T presenter);
}
