package com.floatapplication.shop.fragment;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.R;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.shop.util.ShopAboutPresenter;
import com.floatapplication.shop.util.ShopAboutPresenter.OnShopAboutButtonClickListener;
import com.simplexorg.customviews.fragment.ZoomViewPagerFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by william on 2018-06-06.
 *
 */

public class ShopAboutFragment extends Fragment {
    @BindView(R.id.owner_icon)
    ImageView mOwnerIcon;
    @BindView(R.id.owner_name)
    TextView mOwnerName;
    @BindView(R.id.go_to_location_icon)
    ImageView mGotoLocationIcon;
    @BindView(R.id.message_icon)
    ImageView mMessageIcon;
    @BindView(R.id.shop_description)
    TextView mShopDescription;

    private ShopAboutPresenter mPresenter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mPresenter = new ShopAboutPresenter();
        Activity activity = getActivity();
        if (activity instanceof OnShopAboutButtonClickListener) {
            mPresenter.setOnShopAboutButtonClickListener((OnShopAboutButtonClickListener) activity);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_shop_about, container, false);
        ButterKnife.bind(this, v);
        ZoomViewPagerFragment zoomFragment = new ZoomViewPagerFragment();
        getChildFragmentManager().beginTransaction()
                .replace(R.id.zoom_view_pager_fragment, zoomFragment)
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null)
                .commit();
        Bundle args = getArguments();
        if (args != null) {
            mPresenter.present(zoomFragment, mOwnerIcon,
                    mOwnerName, mGotoLocationIcon, mMessageIcon, mShopDescription,
                    (Shop) args.getParcelable("shop"));
        }
        return v;
    }
}
