package com.floatapplication.shop.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.server.ServerErrorCode;
import com.floatapplication.shop.fragment.FragmentCallback;
import com.floatapplication.shop.fragment.ShopCreationFragment;
import com.floatapplication.shop.fragment.ShopSetLocationFragment;
import com.simplexorg.mapfragment.model.GeoPoint;

import org.json.JSONObject;

import java.util.List;

/**
 * Activity that displays shop creation fragment and shop set location fragment.
 */

public class ShopCreationActivity extends AppCompatActivity implements ShopCreationFragment.OnCompleteListener {
    private static final String TAG = ShopCreationActivity.class.getSimpleName();
    private static final int NUM_ITEMS = 2;

    private static final int PAGE_CREATION = 0;
    private static final int PAGE_SET_LOCATION = 1;

    @VisibleForTesting
    ShopCreationFragment mShopCreationFragment;
    @VisibleForTesting
    ShopSetLocationFragment mShopSetLocationFragment;

    private ShopCreationFragmentAdapter mAdapter;
    private ViewPager mPager;


    private class ShopCreationFragmentAdapter extends FragmentPagerAdapter {
        ShopCreationFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            if (position == 0) {
                return mShopCreationFragment;
            }
            return mShopSetLocationFragment;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_creation);
        mAdapter = new ShopCreationFragmentAdapter(getSupportFragmentManager());

        if (savedInstanceState == null) {
            mShopCreationFragment = new ShopCreationFragment();
            mShopSetLocationFragment = new ShopSetLocationFragment();
        } else {
            List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
            Log.d(TAG, "number of fragments: " + fragmentList.size());
            for (Fragment fragment : fragmentList) {
                if (fragment instanceof ShopCreationFragment) {
                    mShopCreationFragment = (ShopCreationFragment) fragment;
                }
                if (fragment instanceof ShopSetLocationFragment) {
                    mShopSetLocationFragment = (ShopSetLocationFragment) fragment;
                }
            }
        }
        setupFragments();

        mPager = findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);
        mPager.addOnPageChangeListener(new OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "page selected: " + position);
                FragmentCallback fragmentCallback = (FragmentCallback) mAdapter.getItem(position);
                if (position == PAGE_SET_LOCATION) {
                    hideKeyboard();
                    if (mShopCreationFragment.getShopIconPair() != null) {
                        Bundle bundle = new Bundle();
                        bundle.putInt("icon_res", mShopCreationFragment.getShopIconPair().res);
                        fragmentCallback.onResumeFragment(bundle);
                    }
                } else {
                    fragmentCallback.onResumeFragment(null);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void setupFragments() {
        mShopCreationFragment.setListeners((View view) -> {
            mShopCreationFragment.clearError();
            mPager.setCurrentItem(PAGE_SET_LOCATION);
        }, this);
        mShopSetLocationFragment.setConfirmCallback(() -> {
            GeoPoint shopLocation = mShopSetLocationFragment.getSetShopLocation();
            mShopCreationFragment.setShopLocation(shopLocation);
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPager.clearOnPageChangeListeners();
    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == PAGE_SET_LOCATION) {
            mPager.setCurrentItem(PAGE_CREATION);
        } else {
            finish();
        }
    }

    /**
     * Once shop completion is pressed and info is verified this function is invoked.
     */
    @Override
    public void onComplete() {
        final GeoPoint shopLocation = mShopCreationFragment.getShopLocation();
        String shopBody = mShopCreationFragment.obtainShopBody();
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.POST, this, Hypermedia.MAKE_SHOP, shopBody,
                (String response) -> {
                    Toast.makeText(getApplicationContext(), "Shop Created!", Toast.LENGTH_SHORT).show();
                    Intent data = new Intent();
                    //FloatApplication.getFactoryInterface().getMessageUserDatabase().addUser();
                    data.putExtra("shop_location", shopLocation);
                    setResult(RESULT_OK, data);
                    FloatApplication.getMessageFeatureHandlerInterface().syncChatUserDatabase();
                    finish();
                },
                this::onServerError);
    }

    private void onServerError(VolleyError error) {
        Log.d(TAG, "Error: " + error.getMessage());
        error.printStackTrace();
        boolean showedMessage = false;
        if (error.networkResponse != null) {
            try {
                String message = new String(error.networkResponse.data);
                Log.d(TAG, "Server error message: " + message);
                JSONObject jsonMessage = new JSONObject(message);
                String errCode = jsonMessage.getString(ServerErrorCode.FIELD_TYPE);
                switch (errCode) {
                    case ServerErrorCode.ERR_SHOP_TOO_CLOSE:
                        Toast.makeText(getApplicationContext(),
                                getString(R.string.shop_too_close_error), Toast.LENGTH_SHORT).show();
                        showedMessage = true;
                        break;
                    case ServerErrorCode.ERR_SHOP_LIMIT_REACHED:
                        Toast.makeText(getApplicationContext(), jsonMessage.getString(ServerErrorCode.FIELD_MESSAGE),
                                Toast.LENGTH_SHORT).show();
                        showedMessage = true;
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!showedMessage) {
            Toast.makeText(getApplicationContext(), "Unable to create shop", Toast.LENGTH_SHORT).show();
        }
        mShopCreationFragment.onShopCreationError();
    }

    public void hideKeyboard() {
            InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (in != null) {
                View focusedView = getCurrentFocus();
                if (focusedView != null) {
                    in.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
                }
            }
    }
}
