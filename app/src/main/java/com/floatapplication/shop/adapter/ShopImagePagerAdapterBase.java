package com.floatapplication.shop.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.floatapplication.R;
import com.floatapplication.util.Util;
import com.simplexorg.customviews.model.ImageDataHolder;
import com.simplexorg.customviews.util.VUtil;

import java.util.ArrayList;
import java.util.List;

public abstract class ShopImagePagerAdapterBase extends PagerAdapter {
    private static final String TAG = ShopImagePagerAdapterBase.class.getSimpleName();

    private OnClickListener mOnClickListener;
    protected List<String> mImages;
    protected Object mDefault;
    protected List<String> mTransitionNames;

    public ShopImagePagerAdapterBase() {
        mImages = new ArrayList<>();
        mTransitionNames = new ArrayList<>();
    }

    /**
     * Called after inflating view from call to instantiateItem.
     *
     * @param itemView the view that was inflated.
     * @param position the position of the view.
     */
    protected abstract void afterItemInflate(View itemView, int position);

    /**
     * Called after setting images.
     *
     * @param numImages the number of images set.
     */
    protected abstract void afterSetImages(int numImages);

    public void setOnImageClickListener(OnClickListener listener) {
        mOnClickListener = listener;
    }

    /**
     * @param imageLinks the image links, if null then all links are cleared.
     */
    public void setImages(@Nullable List<String> imageLinks) {
        int randomStringLen = 8;
        if (imageLinks == null || imageLinks.isEmpty()) {
            mImages.clear();
            mImages.add(Util.PLACEHOLDER_SHOP);
            mTransitionNames.clear();
            mTransitionNames.add(VUtil.getInstance().genRandomString(randomStringLen));
        } else {
            mImages.clear();
            mImages.addAll(imageLinks);

            mTransitionNames.clear();
            for (int i = 0; i < imageLinks.size(); i++) {
                mTransitionNames.add(VUtil.getInstance().genRandomString(randomStringLen));
            }

            afterSetImages(imageLinks.size());
        }

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mImages.size();
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        Log.d(TAG, "instantiateItem, position: " + position);
        View itemView = LayoutInflater.from(container.getContext()).inflate(R.layout.item_image, container, false);

        ImageView imageView = itemView.findViewById(R.id.image);
        imageView.setScaleType(ScaleType.CENTER);

        if (!Util.PLACEHOLDER_SHOP.equals(mImages.get(position))) {
            afterItemInflate(itemView, position);
        } else {
            mDefault = itemView;
            Util.getInstance().loadImage(imageView, "", R.drawable.default_store_image);
        }

        imageView.setTransitionName(mTransitionNames.get(position));
        Util.getInstance().loadImage(imageView, mImages.get(position), R.drawable.default_store_image);
        imageView.setOnClickListener(mOnClickListener);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        if (mDefault == object && !mImages.isEmpty()) {
            mDefault = null;
            return POSITION_NONE;
        }
        return super.getItemPosition(object);
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    public String getTransitionName(int position) {
        return mTransitionNames.get(position);
    }

    public ArrayList<ImageDataHolder> getImageDataList() {
        ArrayList<ImageDataHolder> imageDataHolders = new ArrayList<>();
        for (int i = 0; i < mImages.size(); i++) {
            imageDataHolders.add(new ImageDataHolder(mImages.get(i), mTransitionNames.get(i)));
        }
        return imageDataHolders;
    }
}
