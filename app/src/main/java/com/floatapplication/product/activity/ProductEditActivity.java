package com.floatapplication.product.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.floatapplication.R;
import com.floatapplication.product.adapter.ProductEditImagePagerAdapter;
import com.floatapplication.product.models.Product;
import com.floatapplication.product.util.ProductCreationCategoryItemPresenter;
import com.floatapplication.product.util.ProductEditPresenter;
import com.floatapplication.product.util.ProductEditSubmissionLogic;
import com.floatapplication.product.util.ProductEditSubmissionStateMachine;
import com.floatapplication.util.PhotoHandler;
import com.floatapplication.util.UploadImageHandler;
import com.simplexorg.customviews.adapter.SimpleImageItemAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by william on 2018-06-13.
 * This activity allows a user to edit an existing product.
 */
public class ProductEditActivity extends AppCompatActivity {
    private static final String TAG = ProductEditActivity.class.getSimpleName();
    private ProductEditPresenter mPresenter;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_edit);
        ButterKnife.bind(this);

        Product product = getIntent().getParcelableExtra("product");
        mPresenter = new ProductEditPresenter(product);
        ButterKnife.bind(mPresenter, this);
        mPresenter.bind(this,
                new PhotoHandler(),
                new ProductEditImagePagerAdapter(),
                new ProductCreationCategoryItemPresenter(this),
                SimpleImageItemAdapter.newTrackerInstance(this, mPresenter),
                new UploadImageHandler());
        mPresenter.present();
        setupActionBar();
    }

    private void setupActionBar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(R.string.product_edit_title);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.done:
                submitChanges(item);
        }
        return true;
    }

    private void submitChanges(MenuItem doneButton) {
        ProductEditSubmissionLogic logic = new ProductEditSubmissionLogic(this, new ProductEditSubmissionStateMachine());
        mPresenter.submit(doneButton, logic);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_product_edit, menu);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mPresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mPresenter.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
