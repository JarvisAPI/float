package com.floatapplication.map.activity;

import android.Manifest.permission;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.map.fragment.ShopItemCardFragment;
import com.floatapplication.map.fragment.ShopItemCardFragment.MapActivityNavigation;
import com.floatapplication.map.util.MapMainPresenter;
import com.floatapplication.messages.activity.DialogsActivity;
import com.floatapplication.product.activity.ProductActivity;
import com.floatapplication.product.activity.ProductInfiniteScrollActivity;
import com.floatapplication.product.models.Product;
import com.floatapplication.search.SearchActivity;
import com.floatapplication.search.SearchHandler;
import com.floatapplication.search.SearchQuery;
import com.floatapplication.shop.activity.ShopCreationActivity;
import com.floatapplication.shop.activity.ShopViewActivity;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.shop.util.ShopHandler;
import com.floatapplication.user.activity.LoginActivity;
import com.floatapplication.user.activity.UserActivity;
import com.floatapplication.user.models.User;
import com.floatapplication.user.util.UserHandler;
import com.floatapplication.util.TransitionHandler;
import com.floatapplication.util.Util;
import com.floatapplication.util.WebViewActivity;
import com.simplexorg.customviews.util.VUtil;
import com.simplexorg.mapfragment.map.BaseCancelableCallback;
import com.simplexorg.mapfragment.map.BaseOnMapReadyCallback;
import com.simplexorg.mapfragment.map.SimpleMapFragment;
import com.simplexorg.mapfragment.marker.BaseMarker;
import com.simplexorg.mapfragment.model.BaseMarkerModel;
import com.simplexorg.mapfragment.model.BaseModelDataRetriever;
import com.simplexorg.mapfragment.model.GeoPoint;
import com.simplexorg.mapfragment.model.SelectableIconModel;
import com.simplexorg.mapfragment.model.SelectableIconModel.Builder;
import com.simplexorg.searchfragment.model.Suggestion;
import com.simplexorg.searchfragment.search.SearchData;
import com.simplexorg.searchfragment.search.SearchFragment;
import com.simplexorg.searchfragment.view.BaseTextWatcher;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.floatapplication.search.SearchQuery.LOCATION;
import static com.floatapplication.search.SearchQuery.QUERY;

public class MapMainActivity extends AppCompatActivity
        implements
        ActivityCompat.OnRequestPermissionsResultCallback,
        NavigationView.OnNavigationItemSelectedListener,
        MapActivityNavigation,
        BaseOnMapReadyCallback {

    private static final String TAG = MapMainActivity.class.getSimpleName();
    private static final int REQUEST_LOCATION = 1;
    public static final int REQUEST_SHOP_CREATION = 2;
    public static final int REQUEST_SEARCH = 10;
    public static final String ACTION_GO_TO_LOCATION = "action_go_to_location";
    public static final String ACTION_LOGIN = "action_login";

    private ShopHandler mShopHandler;
    private TransitionHandler mTransitionHandler;
    private SearchData mSearchData;

    private ShopItemCardFragment mShopItemCardFragment;
    private SearchFragment mSearchFragment;
    private SimpleMapFragment mMapFragment;
    private MapMainPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_main);

        mTransitionHandler = new TransitionHandler();
        mShopHandler = new ShopHandler();
        setupSearchFragment();
        setupShopItemCardFragment();
        mMapFragment = (SimpleMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mMapFragment.setOnMapReadyCallback(this);
        setupPresenter();
        mSearchData = new SearchData();
    }

    private void setupPresenter() {
        NavigationView navigationView = findViewById(R.id.map_main_nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        DrawerLayout drawerLayout = findViewById(R.id.map_main_drawer_layout);
        mPresenter = new MapMainPresenter(FloatApplication.getFactoryInterface().getGlideRequest(this),
                FloatApplication.getUserSessionInterface(),
                FloatApplication.getHypermediaInterface());
        mPresenter.present(navigationView, drawerLayout);

        mSearchFragment.setOnMenuClickListener(mPresenter);
    }

    private void setupSearchFragment() {
        mSearchFragment = (SearchFragment) getSupportFragmentManager().findFragmentById(R.id.search);
        mSearchFragment.addIcons(new int[]{R.drawable.ic_shop_black_24dp, R.drawable.baseline_my_location_black_24});
        mSearchFragment.attachOnClickListener(0, (View view) ->
            mTransitionHandler.transition(getApplicationContext(),
                    ProductInfiniteScrollActivity.class,
                    new Intent().putExtra("location", mMapFragment.getCameraLocationCenter()))
        );
        mSearchFragment.setOnSearchClickListener(this::startSearchActivity);
        mSearchFragment.setOnSuggestionClickListener((Suggestion suggestion) -> {
            Log.d(TAG, "Suggestion: " + suggestion.suggestion + " clicked!");
            Product product = (Product) suggestion.data;
            Intent data = new Intent();
            data.putExtra(Util.EXTRA_TRANSITION, VUtil.getInstance().genRandomString(8));
            data.putExtra(Util.EXTRA_PRODUCT, product);
            SearchQuery localSearch = new SearchQuery(
                    (String) mSearchData.get(QUERY),
                    (GeoPoint) mSearchData.get(LOCATION));
            data.putExtra(Util.EXTRA_SEARCH_DATA, localSearch);
            mTransitionHandler.transition(getApplicationContext(), ProductActivity.class, data);
        });
        mSearchFragment.setSearchSuggestionSupplier(new SearchHandler());
        mSearchFragment.addTextChangedListener(new BaseTextWatcher() {
            @Override
            public void afterTextChanged(Editable editable) {
                int MIN_GRAMS = 2;
                if (editable.length() >= MIN_GRAMS) {
                    mSearchData.put(QUERY, editable.toString());
                    mSearchData.put(LOCATION, mMapFragment.getCameraLocationCenter());
                    mSearchFragment.updateSuggestion(mSearchData);
                } else {
                    mSearchFragment.clearSuggestions();
                }
            }

        });
    }

    private void setupShopItemCardFragment() {
        mShopItemCardFragment = (ShopItemCardFragment) getSupportFragmentManager().findFragmentById(R.id.card);
        getSupportFragmentManager().beginTransaction()
                .hide(mShopItemCardFragment)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        String username = FloatApplication.getUserSessionInterface().getUsername();
        if(mPresenter != null && username != null){
            mPresenter.setNavigationHeaderImages(username);
        }
        mTransitionHandler.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        GeoPoint geoPoint = mMapFragment.getCameraLocationCenter();
        if (geoPoint != null) {
            SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
            String saveLocation = String.format(Locale.CHINA, "%f:%f", geoPoint.latitude, geoPoint.longitude);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.saved_starting_location), saveLocation);
            editor.apply();
        }
    }

    @Override
    public void onMapReady() {
        try {
            enableLocationServices();
            enableCustomMarkers();
            animateMapToSavedLocation();
        } catch (Resources.NotFoundException e) {
            Log.e(TAG, "Can't find style. Error: ", e);
        }
    }

    private void enableCustomMarkers() {
        mMapFragment.setDataRetriever(new BaseModelDataRetriever<SelectableIconModel>() {
            @Override
            public void getModels(OnModelsRetrievedListener<SelectableIconModel> listener,
                                  GeoPoint geoPoint) {
                Map<String, String> variableMap = new HashMap<>();
                variableMap.put("latitude", Double.toString(geoPoint.latitude));
                variableMap.put("longitude", Double.toString(geoPoint.longitude));
                FloatApplication.getHypermediaInterface()
                        .follow(Hypermedia.GET, HypermediaInterface.MAP_ICONS, variableMap, null,
                                (String response) -> {
                                    List<SelectableIconModel> iconModels = Util.getInstance().createIconModels(response);
                                    listener.onModelsRetrieved(iconModels);
                                }, (VolleyError error) -> {
                                    Log.e(TAG, "Unable to get map icons");
                                    Log.e(TAG, "Volley error msg: " + error.getMessage());
                                });
            }

            @Override
            public void getModelDetails(OnModelDetailsRetrievedListener<SelectableIconModel> listener,
                                        BaseMarkerModel markerModel) {
                mShopHandler.getShop(markerModel.getId(),
                        (Shop shop) -> {
                            if (markerModel instanceof SelectableIconModel) {
                                Builder builder = new Builder((SelectableIconModel) markerModel);
                                listener.onModelDetailsRetrieved(builder
                                        .title(shop.getShopName())
                                        .description(shop.getShopDescription())
                                        .build());
                            }
                            popupShopItemCard(shop);
                        });
            }
        });

        mMapFragment.setOnInfoWindowClickListener((BaseMarker baseMarker) ->
                mShopHandler.getShop(((BaseMarkerModel) baseMarker.getTag()).getId(),
                this::startShopViewActivity));

        mMapFragment.setOnMapClickListener((GeoPoint geoPoint) -> {
            hideShopItemCard();
            mSearchFragment.clearFocus();
        });

        mMapFragment.setOnMarkerClickListener((BaseMarker baseMarker) -> {
            Object tag = baseMarker.getTag();
            if (tag instanceof SelectableIconModel) {
                SelectableIconModel iconModel = (SelectableIconModel) tag;
                if (iconModel.getState() == SelectableIconModel.NORMAL) {
                    hideShopItemCard();
                }
            }
        });
    }

    private void popupShopItemCard(Shop shop) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        mShopItemCardFragment.setShop(shop);
        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .show(mShopItemCardFragment)
                .commit();
    }

    private void hideShopItemCard() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .hide(mShopItemCardFragment)
                .commit();
    }

    private void enableLocationServices() {
        if (ContextCompat.checkSelfPermission(this, permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mSearchFragment.attachOnClickListener(1, mMapFragment.getMyLocationClickListener());
        } else {
            ActivityCompat.requestPermissions(this, new String[]{permission.ACCESS_FINE_LOCATION},
                    REQUEST_LOCATION);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if (permissions.length == 1 &&
                    permissions[0].equals(permission.ACCESS_FINE_LOCATION) &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mSearchFragment.attachOnClickListener(1, mMapFragment.getMyLocationClickListener());
            } else {
                // Permission was denied. Display a toast to notify user.
                Toast.makeText(getApplicationContext(), "Location Services Disabled", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Log.d(TAG, "Clicked navigation item");
        int id = item.getItemId();
        if (id == R.id.create_shop) {
            goToShopCreationActivity();
        }
        if (id == R.id.inbox) {
            goToMessageActivity();
        }
        if (id == R.id.user_profile) {
            goToUserActivity();
        }
        if (id == R.id.login) {
            goToLoginActivity();
        }
        if (id == R.id.logout) {
            // Sanity check that user is indeed logged in.
            if (FloatApplication.getUserSessionInterface().isLoggedIn()) {
                logout();
            }
        }
        if (id == R.id.privacy_policies){
            Intent data = new Intent();
            data.putExtra("url", getString(R.string.privacy_polices_url));
            mTransitionHandler.transition(this, WebViewActivity.class, data);
        }
        return true;
    }


    private void logout() {
        UserHandler.logout(this);
        mPresenter.onLogout();
    }

    @Override
    public void onNewIntent(Intent intent) {
        String action = intent.getAction();
        if (ACTION_GO_TO_LOCATION.equals(action)) {
            mPresenter.closeDrawer();
            animateToShopLocation(intent);
        } else if (ACTION_LOGIN.equals(action)) {
            mPresenter.onLogin();
        }
    }

    private void goToLoginActivity() {
        mTransitionHandler.transition(this, LoginActivity.class);
    }

    private void goToUserActivity() {
        if (mPresenter.getName() != null) {
            Intent data = new Intent();
            data.putExtra(User.USERNAME, FloatApplication.getUserSessionInterface().getUsername());
            data.putExtra(User.NAME, mPresenter.getName());
            mTransitionHandler.transition(this, UserActivity.class, data);
        }
    }

    private void goToShopCreationActivity() {
        Intent data = new Intent();
        GeoPoint loc = mMapFragment.getCameraLocationCenter();
        data.putExtra("location", loc);
        mTransitionHandler.transitionForResult(this, ShopCreationActivity.class, data, REQUEST_SHOP_CREATION);
    }

    /**
     * Animates the map to the location that was last visited by the user.
     */
    private void animateMapToSavedLocation() {
        Log.d(TAG, "Animating to saved location");
        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.preference_file_key), MODE_PRIVATE);
        String savedLocation = sharedPref.getString(getString(R.string.saved_starting_location), null);
        if (savedLocation != null) {
            String[] latLngArr = savedLocation.split(":");
            double lat = Double.parseDouble(latLngArr[0]);
            double lng = Double.parseDouble(latLngArr[1]);
            GeoPoint latLng = new GeoPoint(lat, lng);
            float zoomLevel = 17.5f;
            mMapFragment.animateCamera(latLng, zoomLevel);
        }
    }

    private void goToMessageActivity() {
        Intent data = new Intent();
        data.putExtra("username", FloatApplication.getUserSessionInterface().getUsername());
        mTransitionHandler.transition(this, DialogsActivity.class, data);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SHOP_CREATION) {
            if (resultCode == RESULT_OK) {
                mPresenter.closeDrawer();
                goToCreatedShopLocation(data);
            }
        } else if (requestCode == REQUEST_SEARCH) {
            mSearchFragment.clearFocus();
        }
    }

    private void animateToShopLocation(Intent data) {
        GeoPoint location = data.getParcelableExtra("location");
        if (location != null) {
            mMapFragment.animateCamera(location, 17.0f);
        }
    }

    private void goToCreatedShopLocation(Intent data) {
        final GeoPoint shopLocation = data.getParcelableExtra("shop_location");
        if (shopLocation != null) {
            mMapFragment.animateCamera(shopLocation, 17.0f, new BaseCancelableCallback() {
                @Override
                public void onFinish() {
                    mMapFragment.refreshMarkers();
                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    public void startSearchActivity(String query) {
        Intent data = new Intent();
        int kilometers = 50;
        data.putExtra(Util.EXTRA_SEARCH_DATA, new SearchQuery(query,
                mMapFragment.getCameraLocationCenter(),
                kilometers));
        View sharedView = mSearchFragment.getView();
        if (VERSION.SDK_INT >= VERSION_CODES.LOLLIPOP && sharedView != null) {
            ViewCompat.setTransitionName(sharedView, SearchActivity.TRANSITION_NAME);
            mTransitionHandler.sharedElementTransitionForResult(this, SearchActivity.class,
                    data, REQUEST_SEARCH, sharedView);
        } else {
            mTransitionHandler.transitionForResult(this, SearchActivity.class, data, REQUEST_SEARCH);
        }
    }

    @Override
    public void startShopViewActivity(Shop shop) {
        Intent data = new Intent();
        data.putExtra("shop", shop);
        mTransitionHandler.transition(this, ShopViewActivity.class, data);
    }
}
