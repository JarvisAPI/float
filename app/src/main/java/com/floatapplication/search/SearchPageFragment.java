package com.floatapplication.search;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.floatapplication.R;
import com.floatapplication.config.ConfigStore;
import com.floatapplication.product.adapter.ProductAdapter;
import com.floatapplication.product.adapter.ProductAdapter.OnBottomReachedListener;
import com.floatapplication.product.models.Product;
import com.floatapplication.shop.util.ShopViewProductPresenter.OnProductClickListener;
import com.floatapplication.util.Util;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SearchPageFragment extends Fragment {
    private static final String TAG = SearchPageFragment.class.getSimpleName();

    @BindView(R.id.recycler_view)
    RecyclerView mRecyclerView;
    private ProductAdapter mAdapter;
    private StaggeredGridLayoutManager mLayoutManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_page,
                container, false);
        ButterKnife.bind(this, view);
        mAdapter = new ProductAdapter();
        int spanCount = ConfigStore.getInstance().getProductColumnSpanCount(getContext());
        mLayoutManager = new StaggeredGridLayoutManager(spanCount,
                StaggeredGridLayoutManager.VERTICAL);
        mLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }

    public void displaySearchResults(List<Product> items) {
        mAdapter.addProducts(items);
    }

    public void clearSearchResults() {
        mAdapter.clear();
    }

    public void setOnProductClickListener(OnProductClickListener listener) {
        mAdapter.setOnProductClickListener(listener);
    }

    public void setOnBottomReachedListener(OnBottomReachedListener listener) {
        mAdapter.setOnBottomReachedListener(listener);
    }

    void onReorderItemsClicked() {
        Util.getInstance().reorderItems(getContext(), mLayoutManager);
    }
}
