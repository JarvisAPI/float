package com.floatapplication.product.models;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;

/**
 * Created by william on 2018-06-14.
 * Checks fields match in product base.
 */
@SuppressWarnings("WeakerAccess")
public class ProductBaseChecker {
    protected String productId;
    protected String productName;
    protected String productPrice;
    protected String productCategory;
    protected String productDescription;
    protected String productMainImageFile;
    protected List<String> productImageFiles = new ArrayList<>();
    protected String productShopId;

    protected void check(ProductBase result) {
        assertEquals(productId, result.getProductId());
        assertEquals(productName, result.getProductName());
        assertEquals(productPrice, result.getProductPrice());
        assertEquals(productCategory, result.getProductCategory());
        assertEquals(productDescription, result.getProductDescription());
        assertEquals(productMainImageFile, result.getProductMainImageFile());
        assertEquals(productImageFiles, result.getProductImageFiles());
        assertEquals(productShopId, result.getProductShopId());
    }
}
