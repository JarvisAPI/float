package com.floatapplication.product.util;

import com.floatapplication.product.util.ProductInfiniteScrollStateMachine.Event;
import com.floatapplication.TestApplication;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

/**
 * Created by william on 2018-05-28.
 * Tests state transitions are correct.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ProductInfiniteScrollStateMachineUnitTest {
    private ProductInfiniteScrollStateMachine stateMachine;
    private ProductInfiniteScrollStateLogic stubLogic;

    @Before
    public void setup() {
        stateMachine = new ProductInfiniteScrollStateMachine();
        stubLogic = mock(ProductInfiniteScrollStateLogic.class);
        stateMachine.setLogicImplementer(stubLogic);
        stateMachine.start();
    }

    @Test
    public void starting_stateMachine_getsShops() {
        verify(stubLogic).getShops();
    }

    @Test
    public void goto_loadProductState_afterObtainedShops() {
        reset(stubLogic);
        stateMachine.run(Event.OBTAINED_SHOPS);
        verify(stubLogic).loadProductFromShops();
    }

    @Test
    public void loadMoreProducts_onceUserHitBottom_andShopListNotEmpty() {
        stateMachine.run(Event.OBTAINED_SHOPS);
        reset(stubLogic);
        stateMachine.run(Event.HIT_BOTTOM_SHOP_LIST_NOT_EMPTY);
        verify(stubLogic).loadProductFromShops();
    }

    @Test
    public void getMoreShops_onceUserHitBottom_andShopListEmpty() {
        stateMachine.run(Event.OBTAINED_SHOPS);
        reset(stubLogic);
        stateMachine.run(Event.HIT_BOTTOM_SHOP_LIST_EMPTY);
        verify(stubLogic).getShops();
    }
}
