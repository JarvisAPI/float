package com.floatapplication.shop.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.floatapplication.R;
import com.floatapplication.product.activity.ProductCreationActivity;
import com.floatapplication.product.activity.ProductEditActivity;
import com.floatapplication.product.models.Product;
import com.floatapplication.product.models.ProductCreator;
import com.floatapplication.product.util.ProductUploadManager;
import com.floatapplication.product.util.ProductUploadManager.OnProductUploadedListener;
import com.floatapplication.shop.adapter.ShopViewFragmentPagerAdapter;
import com.floatapplication.shop.edit.ShopAboutEditContract;
import com.floatapplication.shop.edit.ShopAboutEditFragment;
import com.floatapplication.shop.fragment.ShopProductsEditFragment;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.shop.util.ShopProductsEditPresenter.OnShopProductsEditClickListener;
import com.floatapplication.util.TransitionHandler;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 *
 * Allows the shop owner to edit the shop.
 *
 * Sets RESULT_OK along with intent containing data on the shop deleted, if shop is deleted.
 */
public class ShopEditActivity extends AppCompatActivity
        implements
        OnPageChangeListener,
        OnShopProductsEditClickListener,
        OnProductUploadedListener,
        ShopAboutEditContract.Model.Observer {
    private static final String TAG = ShopEditActivity.class.getSimpleName();
    private static final int REQUEST_PRODUCT_CREATION = 10;
    private static final int REQUEST_PRODUCT_EDIT = 11;
    public static final String EDIT_TYPE = "edit_type";

    public enum Type{
        change,delete
    }

    private Shop mShopEdit;
    private TransitionHandler mTransitionHandler;
    private ShopAboutEditFragment mShopAboutEditFragment;
    private ShopProductsEditFragment mShopProductsEditFragment;
    private ProductUploadManager mProductUploadManger;

    private int mCurrentPos;
    private final int SHOP_ABOUT = 0;
    private final int SHOP_PRODUCTS = 1;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.toolbar_progress_bar)
    ProgressBar mProgressbar;
    private MenuItem mDoneButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop_edit);
        ButterKnife.bind(this);
        mShopEdit = getIntent().getParcelableExtra("shop");
        mProductUploadManger = new ProductUploadManager(this, findViewById(android.R.id.content), mShopEdit.getShopId());
        mProductUploadManger.setOnProductUploadedListener(this);
        setupToolbar();
        mTransitionHandler = new TransitionHandler();
        setupViewPager();
        mCurrentPos = SHOP_ABOUT;
    }

    @Override
    public void onResume() {
        super.onResume();
        mTransitionHandler.onResume();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(mShopEdit.getShopName());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.product_delete:
                mShopProductsEditFragment.getPresenter().onProductDeleteClicked();
                break;
            case R.id.done:
                mDoneButton = item;
                submitChanges();
                break;
            case R.id.action_reorder:
                mShopProductsEditFragment.onReorderProductsClicked();
                break;
        }
        return true;
    }

    private void submitChanges() {
        boolean submitted = mShopAboutEditFragment.submit();
        if (submitted) {
            mDoneButton.setVisible(false);
            mProgressbar.setVisibility(View.VISIBLE);
        } else {
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        ActionBar actionBar = getSupportActionBar();
        switch (mCurrentPos) {
            case SHOP_ABOUT:
                if (actionBar != null) {
                    actionBar.setDisplayHomeAsUpEnabled(true);
                }
                getMenuInflater().inflate(R.menu.activity_shop_edit_done, menu);
                break;
            case SHOP_PRODUCTS:
                if (actionBar != null) {
                    actionBar.setDisplayHomeAsUpEnabled(false);
                }
                getMenuInflater().inflate(R.menu.activity_shop_edit_products, menu);
        }
        return true;
    }

    private void setupViewPager() {
        ViewPager viewPager = findViewById(R.id.view_pager);
        ShopViewFragmentPagerAdapter adapter = new ShopViewFragmentPagerAdapter(getFragments(), getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(this);
        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
    }

    private List<Fragment> getFragments() {
        List<Fragment> fragments = new ArrayList<>();
        mShopAboutEditFragment = makeShopAboutEditFragment();
        mShopAboutEditFragment.addModelObserver(this);
        fragments.add(mShopAboutEditFragment);
        mShopProductsEditFragment = makeShopProductsEditFragment();
        fragments.add(mShopProductsEditFragment);
        return fragments;
    }

    private ShopProductsEditFragment makeShopProductsEditFragment() {
        ShopProductsEditFragment shopProductsEditFragment = new ShopProductsEditFragment();
        Bundle args = new Bundle();
        args.putString("title", getString(R.string.fragment_shop_products_title));
        args.putParcelable("shop", mShopEdit);
        shopProductsEditFragment.setArguments(args);
        return shopProductsEditFragment;
    }

    private ShopAboutEditFragment makeShopAboutEditFragment() {
        ShopAboutEditFragment shopAboutEditFragment = new ShopAboutEditFragment();
        Bundle args = new Bundle();
        args.putString("title", getString(R.string.fragment_shop_about_title));
        args.putParcelable("shop", mShopEdit);
        shopAboutEditFragment.setArguments(args);
        return shopAboutEditFragment;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        mCurrentPos = position;
        invalidateOptionsMenu();
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onAddProductClicked() {
        mTransitionHandler.transitionForResult(this, ProductCreationActivity.class, REQUEST_PRODUCT_CREATION);
    }

    @Override
    public void onProductClicked(Product product) {
        Intent data = new Intent();
        data.putExtra("product", product);
        mTransitionHandler.transitionForResult(this, ProductEditActivity.class, data, REQUEST_PRODUCT_EDIT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == REQUEST_PRODUCT_CREATION) {
            ProductCreator productCreator = data.getParcelableExtra("data");
            mProductUploadManger.uploadProduct(productCreator);
        } else if (requestCode == REQUEST_PRODUCT_EDIT) {
            String productId = data.getStringExtra("product_id");
            mShopProductsEditFragment.getPresenter().refreshProduct(productId);
        }
    }

    @Override
    public void onProductUploaded() {
        mShopProductsEditFragment.getPresenter().onProductUploaded();
    }

    @Override
    public void update(int event) {
        switch (event) {
            case INFO_UPDATED:
                Toast.makeText(this, "Shop modified", Toast.LENGTH_SHORT).show();
                Intent i = new Intent();
                i.putExtra(Shop.ID, mShopEdit.getShopId());
                i.putExtra(EDIT_TYPE, Type.change.toString());
                setResult(RESULT_OK, i);
                finish();
                break;
            case SHOP_DELETED: {
                Intent data = new Intent();
                data.putExtra(Shop.ID, mShopEdit.getShopId());
                data.putExtra(EDIT_TYPE, Type.delete.toString());
                setResult(RESULT_OK, data);
                finish();
                break;
            }
            case ERROR: {
                Toast.makeText(this, "Server Error", Toast.LENGTH_SHORT).show();
                mDoneButton.setVisible(true);
                mProgressbar.setVisibility(View.GONE);
                break;
            }
        }
    }
}
