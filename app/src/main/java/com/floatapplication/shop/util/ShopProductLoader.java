package com.floatapplication.shop.util;

import android.util.Log;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.product.models.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Handles loading of shop products.
 */

public class ShopProductLoader {
    public interface OnProductLoadedListener {
        void onProductLoaded(List<Product> products);
    }

    private static final String TAG = ShopProductLoader.class.getSimpleName();

    private final String mShopId;
    private OnProductLoadedListener mListener;
    private int mOffset, mLimit;
    private boolean mRequesting;

    public ShopProductLoader(String shopId) {
        mShopId = shopId;
        mOffset = 0;
        mLimit = 10;
        mRequesting = false;
    }

    public void setListener(OnProductLoadedListener listener) {
        mListener = listener;
    }

    void getShopProducts() {
        if (!mRequesting) {
            mRequesting = true;
            Map<String, String> vMap = new HashMap<>();
            vMap.put("shop_id", mShopId);
            vMap.put("offset", String.valueOf(mOffset));
            vMap.put("limit", String.valueOf(mLimit));
            FloatApplication.getHypermediaInterface()
                    .follow(Hypermedia.GET, HypermediaInterface.SHOP_PRODUCTS, vMap, null,
                            (String response) -> {
                                obtainProducts(response);
                                mRequesting = false;
                            },
                            (VolleyError error) -> {
                                Log.e(TAG, "volley error: " + error.getMessage());
                                error.printStackTrace();
                            });
        }
    }

    private void obtainProducts(String productListData) {
        List<Product> productList = new ArrayList<>();
        try {
            JSONArray products = new JSONObject(productListData).getJSONArray("product_list");
            for (int i = 0; i < products.length(); i++) {
                productList.add(Product.createBasicProduct(products.getJSONObject(i)));
            }
            mOffset += products.length();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if (mListener != null) {
            mListener.onProductLoaded(productList);
        }
    }
}
