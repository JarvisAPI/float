package com.floatapplication.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;

import com.floatapplication.R;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by william on 2018-12-19.
 * Used to store meta data of the app.
 */

public class ConfigStore {
    private static ConfigStore mConfigStore;

    private final String MAX_SHOP_IMAGE_UPLOADS = "max_shop_image_uploads";
    private final String MAX_PRODUCT_IMAGE_UPLOADS = "max_product_image_uploads";

    private int mMaxShopImageUploads = -1;
    private int mMaxProductImageUploads = -1;
    private int mShopProductColumnSpanCount = -1;
    private static final int MAX_SPAN_COUNT = 3;

    private ConfigStore() {

    }

    /**
     * Get the maximum number of shop image uploads.
     * @return the maximum number of shop images that user can upload.
     */
    public int getMaxShopImageUploads(@NonNull Context context) {
        if (mMaxShopImageUploads == -1) {
            SharedPreferences sharedPreferences =
                    context.getSharedPreferences(context.getString(R.string.preference_file_key), MODE_PRIVATE);
            int defaultMaxNum = 10;

            mMaxShopImageUploads = sharedPreferences.getInt(
                    context.getString(R.string.max_shop_image_uploads), defaultMaxNum);
        }
        return mMaxShopImageUploads;
    }

    public int getMaxProductImageUploads(@NonNull Context context) {
        if (mMaxProductImageUploads == -1) {
            SharedPreferences sharedPreferences =
                    context.getSharedPreferences(context.getString(R.string.preference_file_key), MODE_PRIVATE);
            int defaultMaxNum = 10;

            mMaxProductImageUploads = sharedPreferences.getInt(
                    context.getString(R.string.max_product_image_uploads), defaultMaxNum);
        }
        return mMaxProductImageUploads;
    }

    public void setConfigurations(Context context, String jsonConfig) {
        try {
            JSONObject jsonObject = new JSONObject(jsonConfig);
            Editor editor = context.getSharedPreferences(
                    context.getString(R.string.preference_file_key), MODE_PRIVATE).edit();

            mMaxShopImageUploads = jsonObject.getInt(MAX_SHOP_IMAGE_UPLOADS);
            editor.putInt(context.getString(R.string.max_shop_image_uploads), mMaxShopImageUploads);

            mMaxProductImageUploads = jsonObject.getInt(MAX_PRODUCT_IMAGE_UPLOADS);
            editor.putInt(context.getString(R.string.max_product_image_uploads), mMaxProductImageUploads);

            editor.apply();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getProductColumnSpanCount(Context context) {
        if (mShopProductColumnSpanCount == -1) {
            SharedPreferences sharedPreferences =
                    context.getSharedPreferences(context.getString(R.string.preference_file_key), MODE_PRIVATE);

            final int defaultSpanCount = 3;
            mShopProductColumnSpanCount = sharedPreferences.getInt(
                    context.getString(R.string.product_column_span_count), defaultSpanCount);
        }
        return mShopProductColumnSpanCount;
    }

    public void setProductColumnSpanCount(Context context, int spanCount) {
        if (mShopProductColumnSpanCount != spanCount) {
            if (spanCount > MAX_SPAN_COUNT) {
                spanCount = MAX_SPAN_COUNT;
            }
            SharedPreferences sharedPreferences =
                    context.getSharedPreferences(context.getString(R.string.preference_file_key), MODE_PRIVATE);
            Editor editor = sharedPreferences.edit();
            editor.putInt(context.getString(R.string.product_column_span_count), spanCount);
            editor.apply();

            mShopProductColumnSpanCount = spanCount;
        }
    }

    public static ConfigStore getInstance() {
        if (mConfigStore == null) {
            mConfigStore = new ConfigStore();
        }
        return mConfigStore;
    }
}
