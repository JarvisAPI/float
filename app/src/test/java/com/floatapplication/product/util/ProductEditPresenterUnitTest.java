package com.floatapplication.product.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.LayoutManager;
import android.text.Editable;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.TestApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.product.adapter.ProductEditImagePagerAdapter;
import com.floatapplication.product.models.Product;
import com.floatapplication.product.models.ProductCreator;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.util.GlobalHandler;
import com.floatapplication.util.PhotoHandler;
import com.floatapplication.util.UploadImageHandler;
import com.floatapplication.util.Util;
import com.simplexorg.customviews.adapter.SimpleImageItemAdapter;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowToast;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static android.app.Activity.RESULT_OK;
import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by william on 2018-06-19.
 * Tests presenting all of product edit.
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class ProductEditPresenterUnitTest {
    private ProductEditPresenter presenter;
    private Context context;
    @Mock private Product product;
    @Mock private Activity activity;
    @Mock private PhotoHandler photoHandler;
    @Mock private ProductEditImagePagerAdapter productEditImagePagerAdapter;
    @Mock private ProductCreationCategoryItemPresenter productCategoriesPresenter;
    @Mock private SimpleImageItemAdapter productImageItemAdapter;
    @Mock private ViewPager viewPager;
    @Mock private TabLayout tabDots;
    @Mock private EditText productName;
    @Mock private EditText productPrice;
    @Mock private EditText productDescription;
    @Mock private GridLayout productCategories;
    @Mock private View rootView;
    @Mock private Button takePhotoButton;
    @Mock private Button uploadImageButton;
    @Mock private RecyclerView productImageItemsRecyclerView;
    @Mock private ProgressBar progressBar;
    @Mock private HypermediaInterface hypermediaInterface;
    @Mock private GlobalHandler globalHandler;
    @Mock private PackageManager packageManager;
    @Mock private ProductEditImagePagerPresenter productEditImagePagerPresenter;
    @Mock private UploadImageHandler mUploadImageHandler;

    private String productNameInput = "name";
    private String productPriceInput = "10";
    private String productCategoryInput = "food";
    private String productDescriptionInput = "desc";

    @Before
    public void setup() {
        initMocks(this);
        presenter = new ProductEditPresenter(product);
        when(activity.getPackageManager()).thenReturn(packageManager);
        when(productImageItemsRecyclerView.getContext()).thenReturn(RuntimeEnvironment.application.getApplicationContext());
        when(productEditImagePagerAdapter.getPresenter()).thenReturn(productEditImagePagerPresenter);
        context = RuntimeEnvironment.application.getApplicationContext();
        when(activity.getApplicationContext()).thenReturn(context);
        presenter.bind(activity, photoHandler, productEditImagePagerAdapter, productCategoriesPresenter,
                productImageItemAdapter, mUploadImageHandler);
        bindPresenterViews();
        mockProduct();
        FloatApplication.setHypermediaInterface(hypermediaInterface);
        GlobalHandler.setGlobalHandler(globalHandler);
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    private void mockProduct() {
        when(product.getProductMainImageFile()).thenReturn("file");
        when(product.getProductName()).thenReturn("name");
        when(product.getProductPrice()).thenReturn("10");
        when(product.getProductDescription()).thenReturn("desc");
        when(product.getProductCategory()).thenReturn("food");
    }

    private void bindPresenterViews() {
        presenter.mViewPager = viewPager;
        presenter.mTabDots = tabDots;
        presenter.mProductName = productName;
        presenter.mProductPrice = productPrice;
        presenter.mProductDescription = productDescription;
        presenter.mProductCategories = productCategories;
        presenter.mRootView = rootView;
        presenter.mTakePhotoButton = takePhotoButton;
        presenter.mUploadImageButton = uploadImageButton;
        presenter.mProductImageItemsRecyclerView = productImageItemsRecyclerView;
        presenter.mProgressBar = progressBar;
    }

    private void verifyEditTextSetup(EditText editText) {
        verify(editText).setCursorVisible(false);
        verify(editText).setOnClickListener(presenter);
        verify(editText).setOnFocusChangeListener(presenter);
    }

    @Test
    public void verifyPresent_executesCorrectSequence() {
        when(packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)).thenReturn(true);
        presenter.present();
        verifyEditTextSetup(productName);
        verifyEditTextSetup(productPrice);
        verifyEditTextSetup(productDescription);

        verify(productEditImagePagerAdapter).setOnClickListener(presenter);
        verify(viewPager).setAdapter(productEditImagePagerAdapter);
        verify(tabDots).setupWithViewPager(viewPager);

        verify(productEditImagePagerAdapter).addImageUrl(Util.getInstance().constructImageUrl(product.getProductMainImageFile()));
        verify(productName).setText(product.getProductName());
        verify(productPrice).setText(product.getProductPrice());

        verify(hypermediaInterface).follow(eq(Hypermedia.GET), eq(HypermediaInterface.PRODUCT_DETAILS), anyMapOf(String.class, String.class),
                anyMapOf(String.class, String.class), any(ServerCallback.class), any(ServerErrorCallback.class));

        verify(takePhotoButton).setTag(R.id.tag0, packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA));
        verify(takePhotoButton).setOnClickListener(presenter);
        verify(uploadImageButton).setOnClickListener(presenter);

        verify(productImageItemsRecyclerView).setLayoutManager(any(LayoutManager.class));
        verify(productImageItemsRecyclerView).setAdapter(productImageItemAdapter);

        verify(productEditImagePagerPresenter).setStatusMediator(presenter);
    }

    @Test
    public void receiveProductInfo_shouldUpdateProductProperly() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                ServerCallback callback = invocation.getArgumentAt(3, ServerCallback.class);
                callback.onSuccessResponse(new JSONObject().put("product", new JSONObject()).toString());
                return null;
            }
        }).when(hypermediaInterface).follow(eq(Hypermedia.GET), eq(HypermediaInterface.PRODUCT_DETAILS), anyMapOf(String.class, String.class),
                anyMapOf(String.class, String.class), any(ServerCallback.class), any(ServerErrorCallback.class));
        when(productCategories.getResources()).thenReturn(mock(Resources.class));

        presenter.present();
        verify(product).augmentProductInfo(any(JSONObject.class));
        verify(productEditImagePagerAdapter).addImageUrls(anyListOf(String.class));
        verify(productDescription).setText(product.getProductDescription());
        verify(productCategoriesPresenter).bind(eq(productCategories), any(String[].class));
        verify(productCategoriesPresenter).present();
        verify(productCategoriesPresenter).selectCategory(product.getProductCategory());
    }

    private void verifyEditTextClick(EditText editText, int id) {
        when(editText.getId()).thenReturn(id);
        presenter.onClick(editText);
        verify(editText).setCursorVisible(true);
    }

    @Test
    public void clickingEditText_setsCursorVisible() {
        verifyEditTextClick(productName, R.id.product_name);
        verifyEditTextClick(productPrice, R.id.product_price);
        verifyEditTextClick(productDescription, R.id.product_description);
    }

    private void setupClearEditTextFocusMocks() {
        Context context = mock(Context.class);
        InputMethodManager inputMethodManager = mock(InputMethodManager.class);
        when(productName.getContext()).thenReturn(context);
        when(context.getSystemService(Context.INPUT_METHOD_SERVICE)).thenReturn(inputMethodManager);
    }

    @Test
    public void takePhotoButtonPressed_shouldDispatchTakePhotoIntent() {
        setupClearEditTextFocusMocks();
        when(takePhotoButton.getId()).thenReturn(R.id.photo_button);
        when(takePhotoButton.getTag(R.id.tag0)).thenReturn(true); // Has camera.
        presenter.onClick(takePhotoButton);
        verify(photoHandler).dispatchTakePhotoIntent(activity);

        when(takePhotoButton.getTag(R.id.tag0)).thenReturn(false); // Has no camera.
        presenter.onClick(takePhotoButton);
        assertEquals(context.getResources().getString(R.string.camera_not_available),
                ShadowToast.getTextOfLatestToast());
    }

    @Test
    public void uploadImageButtonPressed_withPermission_shouldRequestImageGallery() {
        setupClearEditTextFocusMocks();
        when(uploadImageButton.getId()).thenReturn(R.id.image_button);
        presenter.onClick(uploadImageButton);
        ArgumentCaptor<Intent> captor = ArgumentCaptor.forClass(Intent.class);
        verify(activity).startActivityForResult(captor.capture(), eq(UploadImageHandler.REQUEST_IMAGE_GALLERY));
        assertEquals(Intent.ACTION_PICK, captor.getValue().getAction());
    }

    @Test
    public void obtainPhoto_onActivityResult_shouldAddToAdapter() {
        when(photoHandler.getCurrentPhotoPath()).thenReturn("photo-path");
        presenter.onActivityResult(PhotoHandler.REQUEST_IMAGE_CAPTURE, RESULT_OK, null);
        verify(productImageItemAdapter).addImagePath(photoHandler.getCurrentPhotoPath());
    }

    @Test
    public void obtainUploadFile_shouldAddFileToAdapter() {
        Uri selectedImage = mock(Uri.class);
        Intent data = new Intent();
        data.setData(selectedImage);
        String path = "upload-path";
        when(globalHandler.getAbsolutePathFromURI(any(Context.class), any(Uri.class))).thenReturn(path);
        presenter.onActivityResult(UploadImageHandler.REQUEST_IMAGE_GALLERY, RESULT_OK, data);
        verify(productImageItemAdapter).addImagePath(path);
    }

    private void setupProductEditableField(EditText editText, String val) {
        Editable text = mock(Editable.class);
        when(text.toString()).thenReturn(val);
        when(editText.getText()).thenReturn(text);
    }

    private void setupProductInputFieldsBeforeSubmit() {
        setupProductEditableField(productName, productNameInput);
        setupProductEditableField(productPrice, productPriceInput);
        setupProductEditableField(productDescription, productDescriptionInput);
        when(productCategoriesPresenter.getSelectedCategory()).thenReturn(productCategoryInput);
    }

    @Test
    public void submitExecutes_correctSequence() {
        setupProductInputFieldsBeforeSubmit();

        Set<String> imageDeleteSet = new HashSet<>();
        String mainImageUrl = "mainImageUrl";
        List<String> imagePaths = new ArrayList<>();
        String mainImagePath = "mainImagePath";
        imagePaths.add(mainImagePath);

        when(productEditImagePagerPresenter.getImageDeleteSet()).thenReturn(imageDeleteSet);
        when(productEditImagePagerPresenter.getMainImageUrl()).thenReturn(mainImageUrl);
        when(productImageItemAdapter.getImagePaths()).thenReturn(imagePaths);
        when(productImageItemAdapter.getMainImagePath()).thenReturn(mainImagePath);

        ProductEditSubmissionLogic logic = mock(ProductEditSubmissionLogic.class);
        MenuItem doneButton = mock(MenuItem.class);

        presenter.submit(doneButton, logic);
        imagePaths.remove(mainImagePath);

        verify(doneButton).setVisible(false);
        verify(progressBar).setVisibility(View.VISIBLE);

        verify(logic).setSubmissionListener(presenter);

        ArgumentCaptor<ProductCreator> captor = ArgumentCaptor.forClass(ProductCreator.class);
        verify(logic).submit(eq(product), captor.capture(), eq(imageDeleteSet), eq(mainImageUrl),
                eq(imagePaths), eq(mainImagePath));
        ProductCreator productCreator = captor.getValue();
        assertEquals(productNameInput, productCreator.getProductName());
        assertEquals(productPriceInput, productCreator.getProductPrice());
        assertEquals(productCategoryInput, productCreator.getProductCategory());
        assertEquals(productDescriptionInput, productCreator.getProductDescription());
    }

    @Test
    public void onSubmitSuccess_setsResultAndFinish() {
        setupProductInputFieldsBeforeSubmit();

        ProductEditSubmissionLogic logic = mock(ProductEditSubmissionLogic.class);
        MenuItem doneButton = mock(MenuItem.class);
        presenter.submit(doneButton, logic);

        String productId = "product-id";
        when(product.getProductId()).thenReturn(productId);
        ArgumentCaptor<Intent> captor = ArgumentCaptor.forClass(Intent.class);
        presenter.onSubmitSuccess();
        verify(activity).setResult(eq(RESULT_OK), captor.capture());
        Intent data = captor.getValue();
        assertEquals(productId, data.getStringExtra("product_id"));
        verify(activity).finish();
    }

    @Test
    public void onSubmitFailure_hideProgressBar_andShowToast() {
        setupProductInputFieldsBeforeSubmit();

        ProductEditSubmissionLogic logic = mock(ProductEditSubmissionLogic.class);
        MenuItem doneButton = mock(MenuItem.class);
        presenter.submit(doneButton, logic);

        presenter.onSubmitFailure();
        verify(progressBar).setVisibility(View.GONE);
        verify(doneButton).setVisible(true);
        assertEquals(context.getString(R.string.server_error), ShadowToast.getTextOfLatestToast());
    }
}
