package com.floatapplication.product.adapter;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.request.RequestListener;
import com.floatapplication.R;
import com.floatapplication.util.GlideApp;
import com.floatapplication.util.Util;
import com.simplexorg.customviews.model.ImageDataHolder;
import com.simplexorg.customviews.util.VUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by william on 2018-05-18.
 * View Pager adapter for displaying product images.
 */

public class ProductImagePagerAdapter extends PagerAdapter {
    /*
     * First view in this class is never destroyed.
     */
    private static final String TAG = ProductImagePagerAdapter.class.getSimpleName();
    private static final int RANDOM_STRING_LENGTH = 8;
    private List<String> mTransitionNames;
    private List<String> mImageUrls;
    private RequestListener<Drawable> mListener;
    private OnClickListener mImageClickListener;

    public ProductImagePagerAdapter() {
        mTransitionNames = new ArrayList<>();
        mTransitionNames.add("");
        mImageUrls = new ArrayList<>();
    }

    public void addImageUrl(String imageUrl) {
        mImageUrls.add(imageUrl);
        mTransitionNames.add(VUtil.getInstance().genRandomString(RANDOM_STRING_LENGTH));
        notifyDataSetChanged();
    }

    public String getTransitionName(int position) {
        return mTransitionNames.get(position);
    }

    public void addImageUrls(List<String> imageUrls) {
        mImageUrls.addAll(imageUrls);
        for (int i = 0; i < imageUrls.size(); i++) {
            mTransitionNames.add(VUtil.getInstance().genRandomString(RANDOM_STRING_LENGTH));
        }
        notifyDataSetChanged();
    }

    public ArrayList<ImageDataHolder> getImageDataList() {
        ArrayList<ImageDataHolder> imageDataList = new ArrayList<>();
        for (int i = 0; i < mImageUrls.size(); i++) {
            imageDataList.add(new ImageDataHolder(mImageUrls.get(i), mTransitionNames.get(i)));
        }
        return imageDataList;
    }

    /**
     * Set transition name of the first item.
     *
     * @param transitionName the transition name.
     */
    public void setTransitionName(String transitionName) {
        mTransitionNames.set(0, transitionName);
    }

    public void setListener(RequestListener<Drawable> listener) {
        mListener = listener;
    }

    public void setOnImageClickListener(OnClickListener listener) {
        mImageClickListener = listener;
    }

    @Override
    public int getCount() {
        return mImageUrls.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        if (position == 0) {
            View view = container.getChildAt(0);
            if (view != null) {
                return view;
            }
        }
        View itemView = LayoutInflater.from(container.getContext()).inflate(R.layout.item_image, container, false);
        ImageView imageView = itemView.findViewById(R.id.image);
        imageView.setOnClickListener(mImageClickListener);

        ViewCompat.setTransitionName(imageView, mTransitionNames.get(position));

        if (Util.getInstance().isImageUrlNullLink(mImageUrls.get(position))) {
            GlideApp.with(container.getContext())
                    .load(R.drawable.default_no_image)
                    .listener(mListener)
                    .into(imageView);
        } else {
            GlideApp.with(container.getContext())
                    .load(mImageUrls.get(position))
                    .listener(mListener)
                    .into(imageView);
        }

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        if (position != 0) {
            container.removeView((View) object);
        }
    }
}
