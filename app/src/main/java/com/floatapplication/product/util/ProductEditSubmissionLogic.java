package com.floatapplication.product.util;

import android.content.Context;
import android.util.Log;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.product.models.Product;
import com.floatapplication.product.models.ProductCreator;
import com.floatapplication.product.util.ProductEditSubmissionStateMachine.Event;
import com.floatapplication.server.ServerCallback;
import com.floatapplication.server.ServerErrorCallback;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.util.GlobalHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * Handles submitting product edit request.
 */

public class ProductEditSubmissionLogic
        implements
        ProductEditSubmissionStateLogic,
        ProductEditSubmissionStateTransitionLogic {
    private static final String TAG = ProductEditSubmissionLogic.class.getSimpleName();

    public interface ProductEditSubmissionListener {
        void onSubmitSuccess();

        void onSubmitFailure();
    }

    private ProductEditSubmissionListener mSubmissionListener;
    private Context mContext;
    private Product mProduct;
    private ProductEditSubmissionStateMachine mStateMachine;
    private Map<String, String> mVMap;
    private Set<String> mImageFilesDeleteSet;
    private String mSwapMainImageFile;
    private List<String> mNewImagePaths;
    private String mNewMainImagePath;
    private List<String> mLeftOverProductSubImageFiles;
    private String mProductEditJsonBody;

    private ServerCallback mGeneralCallback;
    private ServerErrorCallback mGeneralErrorCallback;

    public ProductEditSubmissionLogic(Context context, ProductEditSubmissionStateMachine stateMachine) {
        mContext = context;
        mStateMachine = stateMachine;
        mStateMachine.bind(this, this);
    }

    /**
     * Submit product edit request to the server.
     *
     * @param product           the original product.
     * @param productCreator    holds the possible changes in the product.
     * @param imageDeleteUrlSet the set of image urls to delete.
     * @param swapMainImageUrl  the main image url that user selected.
     * @param newImagePaths     the new image paths that user selected to be uploaded.
     * @param newMainImagePath  the new main image that user wants to be uploaded to the server.
     */
    void submit(Product product, ProductCreator productCreator,
                Set<String> imageDeleteUrlSet, String swapMainImageUrl,
                List<String> newImagePaths, String newMainImagePath) {
        Log.d(TAG, "submit");
        mProduct = product;
        mImageFilesDeleteSet = new HashSet<>();
        for (String imageUrl : imageDeleteUrlSet) {
            String imageFile = GlobalHandler.getInstance().constructImageFile(imageUrl);
            mImageFilesDeleteSet.add(imageFile);
        }
        if (swapMainImageUrl != null) {
            mSwapMainImageFile = GlobalHandler.getInstance().constructImageFile(swapMainImageUrl);
        }
        mNewImagePaths = newImagePaths;
        mNewMainImagePath = newMainImagePath;

        mVMap = new HashMap<>();
        mVMap.put(Shop.ID, product.getProductShopId());
        mVMap.put(Product.ID, product.getProductId());

        mLeftOverProductSubImageFiles = new ArrayList<>(product.getProductImageFiles());
        mLeftOverProductSubImageFiles.removeAll(mImageFilesDeleteSet);

        mGeneralCallback = new ServerCallback() {
            @Override
            public void onSuccessResponse(String response) {
                mStateMachine.run(Event.SUCCESS);
            }
        };

        mGeneralErrorCallback = new ServerErrorCallback() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                mStateMachine.run(Event.FAILURE);
            }
        };

        mProductEditJsonBody = constructProductEditJson(mProduct, productCreator);

        Log.d(TAG, "mImageFilesDeleteSet: " + mImageFilesDeleteSet);
        Log.d(TAG, "mSwapMainImageFile: " + mSwapMainImageFile);
        Log.d(TAG, "mNewImagePaths: " + mNewImagePaths);
        Log.d(TAG, "mNewMainImagePath: " + mNewMainImagePath);
        Log.d(TAG, "mLeftOverProductSubImageFiles: " + mLeftOverProductSubImageFiles);

        mStateMachine.start();
    }


    private String constructProductEditJson(Product product, ProductCreator productCreator) {
        try {
            String productName;
            String productPrice;
            String productCategory;
            String productDescription;
            boolean modified = false;
            JSONObject productEditJson = new JSONObject();
            if (!product.getProductName().equals(productCreator.getProductName())) {
                productName = productCreator.getProductName();
                productEditJson.put(Product.NAME, productName);
                modified = true;
            }
            if (!product.getProductPrice().equals(productCreator.getProductPrice())) {
                productPrice = productCreator.getProductPrice();
                productEditJson.put(Product.PRICE, productPrice);
                modified = true;
            }
            if (!product.getProductCategory().equals(productCreator.getProductCategory())) {
                productCategory = productCreator.getProductCategory();
                productEditJson.put(Product.CATEGORY, productCategory);
                modified = true;
            }
            if (!product.getProductDescription().equals(productCreator.getProductDescription())) {
                productDescription = productCreator.getProductDescription();
                productEditJson.put(Product.DESCRIPTION, productDescription);
                modified = true;
            }
            if (modified) {
                return new JSONObject()
                        .put("product_edit", productEditJson)
                        .toString();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    void setSubmissionListener(ProductEditSubmissionListener listener) {
        mSubmissionListener = listener;
    }


    @Override
    public void uploadProductDetails() {
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.PUT,
                mContext, HypermediaInterface.EDIT_PRODUCT, mVMap, mProductEditJsonBody,
                mGeneralCallback,
                mGeneralErrorCallback);
    }

    @Override
    public void uploadNewImages() {
        Log.d(TAG, "uploadNewImages");
        final int numImages = mNewImagePaths.size();
        ServerCallback callback = new ServerCallback() {
            int counter = 0;

            @Override
            public void onSuccessResponse(String response) {
                counter++;
                if (counter >= numImages) {
                    mStateMachine.run(Event.SUCCESS);
                }
            }
        };
        for (String imagePath : mNewImagePaths) {
            String uploadPath = FloatApplication.getHypermediaInterface().getPath(HypermediaInterface.UPLOAD_PRODUCT_IMAGE, mVMap);
            FloatApplication.getHypermediaInterface().uploadImage(mContext, uploadPath, imagePath, callback, mGeneralErrorCallback);
        }
    }

    @Override
    public void uploadNewMainImage() {
        String uploadPath = FloatApplication.getHypermediaInterface().getPath(HypermediaInterface.UPLOAD_PRODUCT_MAIN_IMAGE, mVMap);
        FloatApplication.getHypermediaInterface().uploadImage(mContext, uploadPath, mNewMainImagePath, mGeneralCallback, mGeneralErrorCallback);
    }

    @Override
    public void onSubmitFailed() {
        mSubmissionListener.onSubmitFailure();
    }

    @Override
    public void onSubmitSuccess() {
        mSubmissionListener.onSubmitSuccess();
    }

    @Override
    public void deleteMainImage() {
        Log.d(TAG, "deleteMainImage");
        FloatApplication.getHypermediaInterface().authFollow(Hypermedia.DELETE, mContext,
                HypermediaInterface.DELETE_PRODUCT_MAIN_IMAGE, mVMap, null,
                mGeneralCallback,
                mGeneralErrorCallback);
    }

    private void sendSwapMainImageRequest(String old_main_image_file, String new_main_image_file,
                                          final Map<String, String> vMap) {
        try {
            String body = new JSONObject()
                    .put("new_main_image_file", new_main_image_file)
                    .put("old_main_image_file", old_main_image_file)
                    .toString();
            FloatApplication.getHypermediaInterface().authFollow(Hypermedia.PUT, mContext,
                    HypermediaInterface.SWAP_PRODUCT_MAIN_IMAGE, vMap, body,
                    mGeneralCallback,
                    mGeneralErrorCallback);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void swapMainImageAutoSelected() {
        Log.d(TAG, "swapMainImageAutoSelected");
        String replacementProductSubImageFile = mLeftOverProductSubImageFiles.get(0);
        sendSwapMainImageRequest(mProduct.getProductMainImageFile(), replacementProductSubImageFile, mVMap);
    }

    @Override
    public void swapMainImageUserSelected() {
        Log.d(TAG, "swapMainImageUserSelected");
        sendSwapMainImageRequest(mProduct.getProductMainImageFile(), mSwapMainImageFile, mVMap);
    }

    @Override
    public void deleteImages() {
        Log.d(TAG, "deleteImages");
        try {
            String body = new JSONObject()
                    .put("image_files", new JSONArray(mImageFilesDeleteSet))
                    .toString();
            Log.d(TAG, "body: " + body);
            FloatApplication.getHypermediaInterface().authFollow(Hypermedia.PUT, mContext,
                    HypermediaInterface.DELETE_PRODUCT_IMAGES, mVMap, body,
                    mGeneralCallback,
                    mGeneralErrorCallback);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean shouldUploadProductDetails() {
        return mProductEditJsonBody != null;
    }

    @Override
    public boolean shouldUploadNewImages() {
        return !mNewImagePaths.isEmpty();
    }

    @Override
    public boolean shouldChangeMainImageWithNewImage() {
        return mNewMainImagePath != null;
    }

    @Override
    public boolean shouldDeleteMainImage() {
        return mImageFilesDeleteSet.contains(mProduct.getProductMainImageFile());
    }

    @Override
    public boolean shouldSwapMainImage() {
        return !"null".equals(mProduct.getProductMainImageFile()) &&
                !mProduct.getProductMainImageFile().equals(mSwapMainImageFile);
    }

    @Override
    public boolean shouldDeleteImages() {
        return !mImageFilesDeleteSet.isEmpty();
    }

    @Override
    public boolean noImagesRemain() {
        return mLeftOverProductSubImageFiles.isEmpty();
    }
}
