package com.floatapplication.shop.edit;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.StringRes;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.floatapplication.R;
import com.floatapplication.image.activity.ImageScrollActivity;
import com.floatapplication.image.activity.CustomZoomViewPagerActivity;
import com.floatapplication.shop.adapter.ShopEditImagePagerAdapter;
import com.floatapplication.shop.edit.ShopAboutEditContract.Presenter;
import com.floatapplication.util.PhotoHandler;
import com.floatapplication.util.UploadImageHandler;
import com.simplexorg.customviews.adapter.SimpleImageItemAdapter;
import com.simplexorg.customviews.adapter.SimpleImageItemAdapter.ImageItemsListener;
import com.simplexorg.customviews.adapter.StatusMediator;
import com.simplexorg.customviews.model.ImageDataHolder;
import com.simplexorg.customviews.util.VUtil;
import com.simplexorg.customviews.views.SpinnerButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * View for editing shop info.
 */
public class ShopAboutEditView implements ShopAboutEditContract.View, ImageItemsListener {

    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    @BindView(R.id.tab_dots)
    TabLayout mTabDots;
    @BindView(R.id.owner_name)
    TextView mOwnerName;
    @BindView(R.id.shop_description)
    EditText mShopDescription;
    @BindView(R.id.root)
    View mRootView;
    @BindView(R.id.shop_delete)
    SpinnerButton mShopDeleteBtn;
    @BindView(R.id.photo_button)
    Button mPhotoButton;
    @BindView(R.id.image_button)
    Button mImageButton;
    @BindView(R.id.upload_images)
    RecyclerView mUploadImagesView;

    ShopEditImagePagerAdapter mShopImageAdapter;
    SimpleImageItemAdapter mUploadImagesAdapter;
    PhotoHandler mPhotoHandler;
    UploadImageHandler mUploadImageHandler;
    Fragment mFragment;

    private ShopAboutEditContract.Presenter mPresenter;

    @Override
    public void setPresenter(Presenter presenter) {
        mPresenter = presenter;
    }

    @Override
    public void init() {
        mViewPager.setAdapter(mShopImageAdapter);
        mTabDots.setupWithViewPager(mViewPager);
        LinearLayoutManager llm = new LinearLayoutManager(mUploadImagesView.getContext());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        mUploadImagesView.setLayoutManager(llm);
        mUploadImagesView.setAdapter(mUploadImagesAdapter);
        setupListeners();
    }

    private void setupListeners() {
        mShopImageAdapter.setOnImageClickListener((View view) -> mPresenter.onShopImageClick(view));
        mShopImageAdapter.setOnMainImageIndicatorSetListener(() -> mPresenter.onMainImageIndicatorSet());
        mShopDescription.setOnClickListener((View view) -> mPresenter.onShopDescriptionClick());
        mShopDescription.setOnFocusChangeListener((View view, boolean focus) ->
                mPresenter.onShopDescriptionFocusChange(focus));
        mRootView.setOnClickListener((View view) -> mPresenter.onRootClick());
        mShopDeleteBtn.setOnClickListener((View view) -> mPresenter.onShopDeleteButtonClick());
        mPhotoButton.setOnClickListener((View view) -> mPresenter.onPhotoButtonClick());
        mImageButton.setOnClickListener((View view) -> mPresenter.onImageButtonClick());
        mUploadImagesAdapter.setImageItemsListener(this);
    }

    @Override
    public void setShopImages(List<String> imageLinks) {
        Log.d("ShopAboutEditView", imageLinks.toString());
        mShopImageAdapter.setImages(imageLinks);
    }

    @Override
    public void clearMainImageIndicator() {
        mShopImageAdapter.clearMainImageIndicator();
    }

    @Override
    public void clearUploadMainImageIndicator() {
        mUploadImagesAdapter.getStatusObserver().update(StatusMediator.Observer.CLEAR_STATUS);
    }

    @Override
    public boolean hasTopMainImageIndicator() {
        return mShopImageAdapter.hasMainImageIndicator();
    }

    @Override
    public String getUploadMainImagePath() {
        return mUploadImagesAdapter.getMainImagePath();
    }

    @Override
    public List<String> getImagesToBeDeleted() {
        return mShopImageAdapter.getImagesMarkedForDeletion();
    }

    @Override
    public String getChangedMainImageFile() {
        return mShopImageAdapter.getChangedMainImageFile();
    }

    @Override
    public void startZoomInImageActivity(Object data) {
        if (mFragment.getActivity() != null) {
            Intent intent = new Intent(mFragment.getContext(), CustomZoomViewPagerActivity.class);
            ArrayList<ImageDataHolder> imageDataHolderList = mShopImageAdapter.getImageDataList();
            intent.putExtra(VUtil.EXTRA_IMAGE_DATA, imageDataHolderList);
            intent.putExtra(VUtil.EXTRA_TRANSITION_NAME, mShopImageAdapter.getTransitionName(mViewPager.getCurrentItem()));

            View view = (View) data;
            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat
                    .makeSceneTransitionAnimation(mFragment.getActivity(), view, view.getTransitionName());
            ActivityCompat.startActivity(mFragment.getActivity(), intent, optionsCompat.toBundle());
        }
    }

    @Override
    public void setOwnerName(String ownerName) {
        mOwnerName.setText(ownerName);
    }

    @Override
    public void setShopDeleteButtonText(@StringRes int resId) {
        mShopDeleteBtn.setText(mRootView.getResources().getString(resId));
    }

    @Override
    public void clearShopDesTextFocus() {
        mShopDescription.clearFocus();
        InputMethodManager imm = (InputMethodManager) mShopDescription.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(mShopDescription.getWindowToken(), 0);
            mShopDescription.setCursorVisible(false);
        }
    }

    @Override
    public void setShopDesTextFocus() {
        mShopDescription.setCursorVisible(true);
    }

    @Override
    public void setShopDescriptionText(String description) {
        mShopDescription.setText(description);
    }

    @Override
    public void setShopDeleteButtonEnable(boolean enable) {
        mShopDeleteBtn.setEnabled(enable);
    }

    @Override
    public void setShopDeleteButtonSpin(boolean spin) {
        mShopDeleteBtn.spin(spin);
    }

    @Override
    public void displayToast(@StringRes int resId) {
        Toast.makeText(mRootView.getContext(), mRootView.getContext().getString(resId),
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public String getShopDescription() {
        return mShopDescription.getText().toString().trim();
    }

    @Override
    public void displayUploadImage(String imagePath) {
        mUploadImagesAdapter.addImagePath(imagePath);
    }

    @Override
    public void takePhoto() {
        mPhotoHandler.dispatchTakePhotoIntent(mFragment);
    }

    @Override
    public void pickUploadImage() {
        mUploadImageHandler.dispatchUploadImageIntent(mFragment);
    }

    @Override
    public void startImageScroll(List<String> imagePaths, String imagePath) {
        Intent intent = new Intent(mFragment.getContext(), ImageScrollActivity.class);
        intent.putStringArrayListExtra(ImageScrollActivity.IMAGE_PATHS, new ArrayList<>(imagePaths));
        intent.putExtra(ImageScrollActivity.SELECTED_IMAGE_PATH, imagePath);
        mFragment.startActivityForResult(intent, ShopAboutEditFragment.REQUEST_IMAGE_SCROLL);
    }

    @Override
    public void removeUploadImage(String imagePath) {
        mUploadImagesAdapter.removeImagePath(imagePath);
    }

    @Override
    public void displayDialog(DialogFragment dialogFragment) {
        if (mFragment.getFragmentManager() != null) {
            dialogFragment.show(mFragment.getFragmentManager(), "ShopAboutEditDialog");
        }
    }

    @Override
    public void onImageCountChange(int itemCount) {

    }

    @Override
    public void onImageClick(String imagePath) {
        mPresenter.onUploadImageClick(imagePath);
    }
}
