package com.floatapplication.message.activity.activity_helpers;

import android.support.v7.widget.RecyclerView;

import com.floatapplication.FloatApplication;
import com.floatapplication.TestApplication;
import com.floatapplication.factory.FactoryInterface;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.messages.activity.activity_helpers.MessageLoader;
import com.floatapplication.messages.chatkit.messages.MessagesListAdapter;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.models.User;
import com.floatapplication.messages.server_connection.ChatConnection;
import com.floatapplication.messages.server_connection.ChatConnectionService;
import com.floatapplication.user.util.UserSessionInterface;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Created by jamescho on 2018-06-11.
 *
 */
@RunWith(RobolectricTestRunner.class)
@Config(application = TestApplication.class)
public class MessageLoaderTest {
    private MessageLoader messageLoader;
    @Mock private ChatConnectionService chatConnectionService;
    @Mock private MessagesListAdapter<CustomMessage> mockedAdapter;
    @Mock private RecyclerView.LayoutManager layoutManager;
    @Mock  private HypermediaInterface hypermediaInterface;
    @Mock private UserSessionInterface userSessionInterface;
    @Mock private FactoryInterface factoryInterface;
    private int mostRecentNewMessageLoadCount;

    @SuppressWarnings("unchecked")
    @Before
    public void setup() {
        initMocks(this);
        FloatApplication.setUserSessionInterface(userSessionInterface);
        FloatApplication.setHypermediaInterface(hypermediaInterface);
        FloatApplication.setFactoryInterface(factoryInterface);
        setMockedChatConnection();
        setAdapterMock();
        setFetchArchiveMessageMock();
        setGetMostRecentMessagesMock();
    }

    @After
    public void cleanup() {
        FloatApplication.clear();
    }

    public void setAdapterMock(){
        doAnswer(new Answer() {
            @Override
            public CustomMessage answer(InvocationOnMock invocation) throws Throwable {
                return mock(CustomMessage.class);
            }
        }).when(mockedAdapter).getLastMessage();

        doAnswer(new Answer() {
            @Override
            public CustomMessage answer(InvocationOnMock invocation) throws Throwable {
                return mock(CustomMessage.class);
            }
        }).when(mockedAdapter).getMostRecentMessage();

    }

    private void setMockedChatConnection() {
        doAnswer(new Answer() {
            @Override
            public ChatConnection.ConnectionState answer(InvocationOnMock invocation){
                return ChatConnection.ConnectionState.AUTHENTICATED;
            }
        }).when(chatConnectionService).getState();

        doAnswer(new Answer() {
            @Override
            public Boolean answer(InvocationOnMock invocation) throws Throwable {
                return true;
            }
        }).when(chatConnectionService).connectionReady();
    }

    private CustomMessage getRandomMessage(){
        Random random = new Random();
        User user = new User("test");
        long timeStamp = 1525799580;
        String text = "abc";
        String archiveId = "15235295";
        ChatConnectionService.MessageType msgType = ChatConnectionService.MessageType.SEND;
        return new CustomMessage(Integer.toString(random.nextInt()), user, timeStamp, text, archiveId, msgType);
    }

    private List<CustomMessage> getRandomNumberOfMessages(int count) {
        if (count <= 0) {
            throw new IllegalArgumentException("illegal negative number argument");
        }
        List<CustomMessage> messages = new ArrayList<>();
        for (int n = 0; n < count ; n ++) {
            messages.add(getRandomMessage());
        }
        return messages;
    }

    private void setFetchArchiveMessageMock(){
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                List<CustomMessage> fetchArchiveMessagesMock = new ArrayList<>();
                for(int n = 0 ; n < MessageLoader.PAGE_SIZE ; n++){
                    fetchArchiveMessagesMock.add(getRandomMessage());
                }
                messageLoader.onArchiveMessagesReceived(fetchArchiveMessagesMock);
                return null;
            }
        }).when(chatConnectionService).fetchArchiveMessages(any(CustomMessage.class));
    }

    private void setGetMostRecentMessagesMock(){
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Random r = new Random();
                List<CustomMessage> mostRecentMessagesMock = new ArrayList<>();
                for(int n = 0 ; n < MessageLoader.PAGE_SIZE / (r.nextInt(MessageLoader.PAGE_SIZE * 2) + 1) ; n++){
                    mostRecentMessagesMock.add(getRandomMessage());
                }
                mostRecentNewMessageLoadCount = mostRecentMessagesMock.size();
                messageLoader.onMostRecentMessagesReceived(mostRecentMessagesMock);
                return null;
            }
        }).when(chatConnectionService).getMostRecentMessages(any(CustomMessage.class));
    }

    @Test
    public void basicMessageLoadTest(){
        List<CustomMessage> messages = new ArrayList<>();
        int numOfRandomMessages = 37;
        messages.addAll(getRandomNumberOfMessages(numOfRandomMessages));
        messageLoader = new MessageLoader(messages,mockedAdapter);
        messageLoader.setConnectionService(chatConnectionService);

        messageLoader.loadMessages();
        verify(mockedAdapter, times(1)).addToEnd(anyListOf(CustomMessage.class), anyBoolean());
        verify(mockedAdapter, times(0)).addToStart(any(CustomMessage.class), anyBoolean());
        verify(mockedAdapter, times(0)).getLastMessage();
        verify(chatConnectionService, times(0)).getMostRecentMessages(any(CustomMessage.class));
        verify(chatConnectionService, times(0)).fetchArchiveMessages(any(CustomMessage.class));
        assertEquals(false, messageLoader.getIsEndOfConversation());

        messageLoader.loadMessages();
        verify(mockedAdapter, times(1 + 1)).addToEnd(anyListOf(CustomMessage.class), anyBoolean());
        verify(mockedAdapter, times(0)).addToStart(any(CustomMessage.class), anyBoolean());
        verify(mockedAdapter, times(0)).getLastMessage();
        verify(chatConnectionService, times(0)).getMostRecentMessages(any(CustomMessage.class));
        verify(chatConnectionService, times(0)).fetchArchiveMessages(any(CustomMessage.class));
        assertEquals(false , messageLoader.getIsEndOfConversation());

        messageLoader.loadMessages();
        verify(mockedAdapter, times(1 + 1 + 1)).addToEnd(anyListOf(CustomMessage.class), anyBoolean());
        verify(mockedAdapter, times(0)).addToStart(any(CustomMessage.class), anyBoolean());
        verify(mockedAdapter, times(2)).getLastMessage();
        verify(chatConnectionService, times(0)).getMostRecentMessages(any(CustomMessage.class));
        verify(chatConnectionService, times(1)).fetchArchiveMessages(any(CustomMessage.class));
        assertEquals(false, messageLoader.getIsEndOfConversation());

        messageLoader.loadNewMessages();
        verify(mockedAdapter, times(1 + 1 + 1)).addToEnd(anyListOf(CustomMessage.class), anyBoolean());
        verify(mockedAdapter, times(mostRecentNewMessageLoadCount)).addToStart(any(CustomMessage.class), anyBoolean());
        verify(mockedAdapter, times(2)).getLastMessage();
        verify(chatConnectionService, times(1)).getMostRecentMessages(any(CustomMessage.class));
        verify(chatConnectionService, times(1)).fetchArchiveMessages(any(CustomMessage.class));
        assertEquals(false , messageLoader.getIsEndOfConversation());

        setArchiveMessageLoadEnd(MessageLoader.PAGE_SIZE/2); // random number smaller than page size
        messageLoader.loadMessages();
        verify(mockedAdapter, times(1 + 1 + 1 + 1)).addToEnd(anyListOf(CustomMessage.class), anyBoolean());
        verify(mockedAdapter, times(mostRecentNewMessageLoadCount)).addToStart(any(CustomMessage.class), anyBoolean());
        verify(mockedAdapter, times(2 + 2)).getLastMessage();
        verify(chatConnectionService, times(1)).getMostRecentMessages(any(CustomMessage.class));
        verify(chatConnectionService, times(2)).fetchArchiveMessages(any(CustomMessage.class));
        assertEquals(true, messageLoader.getIsEndOfConversation());
    }

    private void setArchiveMessageLoadEnd(final int numberOfArchives){
        if (numberOfArchives <= 0 || numberOfArchives >= MessageLoader.PAGE_SIZE) {
            throw new IllegalArgumentException("argument range must be between 0 and "
                    + Integer.toString(MessageLoader.PAGE_SIZE - 1));
        }
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                List<CustomMessage> fetchArchiveMessagesMock = new ArrayList<>();
                for(int n = 0 ; n < numberOfArchives ; n++){
                    fetchArchiveMessagesMock.add(getRandomMessage());
                }
                messageLoader.onArchiveMessagesReceived(fetchArchiveMessagesMock);
                return null;
            }
        }).when(chatConnectionService).fetchArchiveMessages(any(CustomMessage.class));
    }
}
