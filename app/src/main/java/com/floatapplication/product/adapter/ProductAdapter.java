package com.floatapplication.product.adapter;

import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.R;
import com.floatapplication.product.adapter.ProductAdapter.ProductViewHolder;
import com.floatapplication.product.models.Product;
import com.floatapplication.shop.util.ShopViewProductPresenter.OnProductClickListener;
import com.floatapplication.util.Util;

import java.util.ArrayList;
import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter<ProductViewHolder> {
    private static final String TAG = ProductAdapter.class.getSimpleName();
    public interface OnBottomReachedListener {
        void onBottomReached();
    }

    private OnProductClickListener mOnProductClickListener;
    private OnBottomReachedListener mOnBottomReachedListener;
    private List<Product> mProductList;

    public ProductAdapter() {
        mProductList = new ArrayList<>();
    }

    public void addProducts(List<Product> products) {
        int start = mProductList.size();
        mProductList.addAll(products);
        notifyItemRangeChanged(start, products.size());
    }

    public void clear() {
        mProductList.clear();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_shop_item, parent, false);
        return new ProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        Product product = mProductList.get(position);
        holder.mProductName.setText(product.getProductName());
        holder.mProductPrice.setText(product.getProductPrice());

        String productMainImageFile = product.getProductMainImageFile();
        if ("null".equals(productMainImageFile) || productMainImageFile == null) {
            Util.getInstance().glide(holder.itemView.getContext())
                    .load(R.drawable.default_no_image)
                    .into(holder.mProductImage);
        } else {
            String url = Util.getInstance().constructImageUrl(productMainImageFile);
            Util.getInstance().loadImage(holder.mProductImage, url, R.drawable.default_no_image);
        }

        holder.itemView.setOnClickListener((View view) -> {
            if (mOnProductClickListener != null) {
                mOnProductClickListener.onProductClick(product, holder.itemView);
            }
        });

        ViewCompat.setTransitionName(holder.itemView, product.getProductId());

        if (position == mProductList.size() - 1) {
            if (mOnBottomReachedListener != null) {
                Log.d(TAG, "Bottom Reached!");
                mOnBottomReachedListener.onBottomReached();
            }
        }
    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }

    public void setOnBottomReachedListener(OnBottomReachedListener listener) {
        mOnBottomReachedListener = listener;
    }

    public void setOnProductClickListener(OnProductClickListener listener) {
        mOnProductClickListener = listener;
    }

    static class ProductViewHolder extends RecyclerView.ViewHolder {
        final ImageView mProductImage;
        final TextView mProductName;
        final TextView mProductPrice;

        ProductViewHolder(View view) {
            super(view);
            mProductName = view.findViewById(R.id.product_name);
            mProductPrice = view.findViewById(R.id.product_price);
            mProductImage = view.findViewById(R.id.product_image);
        }
    }
}
