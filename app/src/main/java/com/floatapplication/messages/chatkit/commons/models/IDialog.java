package com.floatapplication.messages.chatkit.commons.models;

/**
 * Created by jamescho on 2018-04-25.
 */

import java.util.List;

/**
 * For implementing by real dialog model
 */

public interface IDialog<MESSAGE extends IMessage> {

    String getId();

    List<? extends IUser> getUsers();

    MESSAGE getLastMessage();

    void setLastMessage(MESSAGE message);

    int getUnreadCount();
}
