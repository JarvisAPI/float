package com.floatapplication.messages.activity.activity_helpers;

import android.util.Log;
import android.widget.Toast;

import com.floatapplication.FloatApplication;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.messages.activity.MessagesActivity;
import com.floatapplication.messages.chatkit.messages.MessagesListAdapter;
import com.floatapplication.messages.chatkit.utils.MessageParser;
import com.floatapplication.messages.models.CustomMessage;
import com.floatapplication.messages.server_connection.ChatConnectionService;
import com.floatapplication.util.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class that is used to send messages in MessagesActivity
 */

public class MessageSender implements ChatConnectionService.OnMessageSentListener {
    private String mContactJid;
    private final String TAG = MessageSender.class.getSimpleName();
    private MessagesActivity mMessagesActivity;
    private MessagesListAdapter<CustomMessage> mMessagesAdapter;
    private ChatConnectionService mService;
    private String mUsername;

    public MessageSender(MessagesActivity activity, MessagesListAdapter<CustomMessage> adapter, String contact, String username) {
        mContactJid = contact;
        mMessagesActivity = activity;
        mMessagesAdapter = adapter;
        mUsername = username;
    }

    public void setConnectionService(ChatConnectionService service) {
        mService = service;
        mService.setOnMessageSentListener(this);
    }

    /**
     * @param input text to send.
     * @return false if message not sent, true otherwise.
     */
    public boolean sendTextMessage(CharSequence input) {
        return mService != null && mService.sendMessage(mUsername, input.toString(), mContactJid);
    }

    public void sendImageMessage(final List<String> imagesEncodedList) {
        for (String uri : imagesEncodedList) {
            uploadImage(uri);
        }
    }

    private void uploadImage(String uri) {
        Map<String, String> vMap = new HashMap<>();
        vMap.put("barePeer", mContactJid);
        String uploadPath = FloatApplication.getHypermediaInterface().getPath(HypermediaInterface.SEND_IMAGE_CHAT, vMap);
        Map<String, String> queries = new HashMap<>();
        if(!FloatApplication.getUserSessionInterface().getUsername().equals(mUsername)){
            queries.put("shop_id", mUsername);
            uploadPath = uploadPath.concat(Util.getInstance().buildQuery(queries));
        }
        FloatApplication.getHypermediaInterface().uploadImage(mMessagesActivity, uploadPath, uri, response -> {
            String imageUrl = null;
            try {
                JSONObject res = new JSONObject(response);
                imageUrl = res.getString("image_url");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (imageUrl != null) {
                sendTextMessage(MessageParser.buildImageMessage(imageUrl));
            }
        },
                error -> Toast.makeText(mMessagesActivity, "unstable internet connection while uploading", Toast.LENGTH_SHORT).show());
    }

    @Override
    public void onMessageSent(final CustomMessage msg) {
        if (msg == null) {
            return;
        }
        Log.d(TAG, "setting messages");
        mMessagesAdapter.addToStart(msg, true);
    }
}
