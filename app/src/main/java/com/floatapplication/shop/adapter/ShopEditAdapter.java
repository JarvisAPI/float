package com.floatapplication.shop.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.product.adapter.ProductAdapter.OnBottomReachedListener;
import com.floatapplication.product.models.Product;
import com.floatapplication.shop.adapter.ShopEditAdapter.ProductViewHolder;
import com.floatapplication.shop.util.ShopEditProductPresenter;
import com.floatapplication.util.GlideRequests;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by william on 2018-05-17.
 * Recycler view adapter for the shop edit.
 */

public class ShopEditAdapter extends RecyclerView.Adapter<ProductViewHolder> {
    public interface OnProductDeleteListener {
        void onProductDelete(Product product);
    }
    public interface OnProductClickListener {
        void onProductClick(Product product);
    }

    private static final String TAG = ShopEditAdapter.class.getSimpleName();
    private List<Product> mShopItems;

    private OnBottomReachedListener mBottomReachedListener;
    private ShopEditProductPresenter mPresenter;

    public ShopEditAdapter(Context context) {
        mShopItems = new ArrayList<>();
        setHasStableIds(true);
        GlideRequests glideRequests = FloatApplication.getFactoryInterface().getGlideRequest(context);
        mPresenter = new ShopEditProductPresenter()
                .setGlide(glideRequests)
                .setAdapter(this);
    }

    public void refreshProduct(Product product) {
        for (int i = 0; i < mShopItems.size(); i++) {
            Product currentProduct = mShopItems.get(i);
            if (product.getProductId().equals(currentProduct.getProductId())) {
                mShopItems.set(i, product);
                notifyItemChanged(i);
                return;
            }
        }
    }

    @NonNull
    @Override
    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_shop_item, parent, false);
        return new ProductViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductViewHolder holder, int position) {
        mPresenter.present(holder.mProductImage, holder.mProductName, holder.mProductPrice,
                holder.mProductDelete, mShopItems.get(position), position);
        if (position == mShopItems.size() - 1) {
            if (mBottomReachedListener != null) {
                mBottomReachedListener.onBottomReached();
            }
        }
    }

    @Override
    public int getItemCount() {
        return mShopItems.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setOnProductDeleteListener(OnProductDeleteListener listener) {
        mPresenter.setOnProductDeleteListener(listener);
    }

    public void setOnProductClickListener(OnProductClickListener listener) {
        mPresenter.setOnProductClickListener(listener);
    }

    /**
     * Remove product at a given position.
     *
     * @param position position of the product to delete.
     */
    public void removeProduct(int position) {
        mShopItems.remove(position);
        notifyItemRemoved(position);
    }

    public void addShopItems(List<Product> shopItems) {
        int positionStart = mShopItems.size();
        mShopItems.addAll(shopItems);
        notifyItemRangeInserted(positionStart, shopItems.size());
    }

    public int getShopItemsCount() {
        return mShopItems.size();
    }

    public void setDelete(boolean delete) {
        mPresenter.setDelete(delete);
    }

    public boolean getDeleteStatus() {
        return mPresenter.getDeleteStatus();
    }

    public void setOnBottomReachedListener(OnBottomReachedListener listener) {
        mBottomReachedListener = listener;
    }

    static class ProductViewHolder extends RecyclerView.ViewHolder {
        private ImageView mProductImage;
        private TextView mProductName;
        private TextView mProductPrice;
        private ImageView mProductDelete;

        private ProductViewHolder(View view) {
            super(view);
            mProductName = view.findViewById(R.id.product_name);
            mProductPrice = view.findViewById(R.id.product_price);
            mProductImage = view.findViewById(R.id.product_image);
            mProductDelete = view.findViewById(R.id.product_delete);
        }
    }
}
