package com.floatapplication.messages.chatkit.commons.models;

/**
 * Created by jamescho on 2018-04-25.
 */

/**
 * For implementing by real user model
 */
public interface IUser {

    /**
     * Returns the user's id
     *
     * @return the user's id
     * */
    String getId();
}
