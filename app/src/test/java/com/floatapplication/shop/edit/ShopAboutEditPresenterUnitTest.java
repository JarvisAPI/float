package com.floatapplication.shop.edit;

import com.floatapplication.R;
import com.floatapplication.shop.edit.ShopAboutEditContract.Model.Observer;
import com.floatapplication.shop.models.Shop;
import com.floatapplication.util.IFactory;
import com.floatapplication.util.Util;
import com.simplexorg.customviews.dialog.CriticalConfirmDialog;
import com.simplexorg.customviews.dialog.CriticalConfirmDialog.OnConfirmListener;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class ShopAboutEditPresenterUnitTest {
    private ShopAboutEditPresenter mPresenter;
    @Mock private ShopAboutEditContract.View mView;
    @Mock private ShopAboutEditContract.Model mModel;
    @Mock private Shop mShop;
    @Mock private Util mUtil;
    @Mock private IFactory mFactory;

    @Before
    public void setup() {
        initMocks(this);
        Util.setUtil(mUtil);
        IFactory.setIFactory(mFactory);
        when(mShop.getOwnerName()).thenReturn("owner");
        when(mModel.getShop()).thenReturn(mShop);

        mPresenter = new ShopAboutEditPresenter();
        mPresenter.attach(mView, mModel);
    }

    @After
    public void cleanup() {
        Util.setUtil(null);
        IFactory.setIFactory(null);
    }

    @Test
    public void test_subscribe() {
        String OWNER = "OWNER";
        when(mUtil.capitalize(mModel.getShop().getOwnerName())).thenReturn(OWNER);
        mPresenter.subscribe();

        verify(mView).setOwnerName(OWNER);
        verify(mView).setShopDeleteButtonText(R.string.shop_delete);
        verify(mView).setShopDeleteButtonEnable(false);
        verify(mModel).retrieveShopInfo();
    }

    @Test
    public void test_onShopImageClick() {
        mPresenter.onShopImageClick(new Object());

        verify(mView).clearShopDesTextFocus();
    }

    @Test
    public void test_onShopDescriptionFocusChange() {
        mPresenter.onShopDescriptionFocusChange(true);
        verify(mView).setShopDesTextFocus();

        mPresenter.onShopDescriptionFocusChange(false); // Doesn't call set focus
        verify(mView).setShopDesTextFocus(); // Still only one call recorded
    }

    @Test
    public void test_onShopDescriptionClick() {
        mPresenter.onShopDescriptionClick();
        verify(mView).setShopDesTextFocus();
    }

    @Test
    public void test_onShopDeleteButtonClick() {
        CriticalConfirmDialog confirmDialog = mock(CriticalConfirmDialog.class);
        when(mFactory.create(CriticalConfirmDialog.class)).thenReturn(confirmDialog);
        mPresenter.onShopDeleteButtonClick();

        verify(mView).displayDialog(confirmDialog);

        ArgumentCaptor<OnConfirmListener> argumentCaptor = ArgumentCaptor.forClass(OnConfirmListener.class);
        verify(confirmDialog).setOnConfirmListener(argumentCaptor.capture());
        argumentCaptor.getValue().onConfirm();

        verify(mView).setShopDeleteButtonSpin(true);
        verify(mModel).deleteShop();
    }

    @Test
    public void test_onSubmitEdit_sameShopDescription() {
        String shopDesc = "shopDesc";
        when(mView.getShopDescription()).thenReturn(shopDesc);
        when(mShop.getShopDescription()).thenReturn(shopDesc);

        assertFalse(mPresenter.onSubmitEdit());
        verify(mModel, times(0)).submitEditUpdate(anyString());
    }

    @Test
    public void test_onSubmitEdit_differentShopDescription() {
        String shopDesc = "shopDesc";
        when(mView.getShopDescription()).thenReturn(shopDesc);
        when(mShop.getShopDescription()).thenReturn("diffDesc");

        assertTrue(mPresenter.onSubmitEdit());
        verify(mModel).submitEditUpdate(shopDesc);
    }

    @Test
    public void test_update_INFO_RECEIVED() {
        String desc = "desc";
        when(mShop.getShopDescription()).thenReturn(desc);
        mPresenter.update(Observer.INFO_RECEIVED);

        verify(mView).setShopDescriptionText(desc);
        verify(mView).setShopDeleteButtonEnable(true);
    }

    @Test
    public void test_update_SHOP_DELETED_FAILED() {
        mPresenter.update(Observer.SHOP_DELETE_FAILED);

        verify(mView).setShopDeleteButtonSpin(false);
    }

    @Test
    public void test_update_ERROR() {
        mPresenter.update(Observer.ERROR);

        verify(mView).displayToast(anyInt());
    }
}
