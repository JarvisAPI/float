package com.floatapplication.messages.server_connection;


/**
 * Functions that must be implemented in MessageFeatureHandler
 *
 */

public interface MessageFeatureHandlerInterface {

    /**
     * Initiates ChatConnectionService that is used to connect with the xmpp server.
     * This method also syncs the devices notification token with the server.
     * This method only restarts the connection and sends the notification token when the service is not running
     */
    void initXMPPConnection();

    void syncChatUserDatabase();


    /**
     * Flushes notification token s.t the device will not receive message notifications.
     * This process runs on a different thread
     */
    void flushNotificationToken();

    void addChatUser(String jid, String chatToken);

}
