package com.floatapplication.image.activity;

import android.util.Log;
import android.widget.ImageView;

import com.floatapplication.image.ImageLoaderListener;
import com.floatapplication.util.Util;
import com.simplexorg.customviews.activity.ZoomViewPagerActivity;

/**
 * Created by william on 2018-12-26.
 * Custom zoom view pager activity.
 */

public class CustomZoomViewPagerActivity extends ZoomViewPagerActivity {

    @Override
    public void loadImage(String uri, ImageView view) {
        Log.d("TAAADSADAG", " called!!!!");
        Util.getInstance().getImageLoader(new ImageLoaderListener() {
            @Override
            public void onImageLoaded() {
                CustomZoomViewPagerActivity.this.onImageLoaded(view);
            }

            @Override
            public void onImageLoadingError() {
                CustomZoomViewPagerActivity.this.onImageLoaded(view);
            }
        }).loadImage(uri, view);
    }
}
