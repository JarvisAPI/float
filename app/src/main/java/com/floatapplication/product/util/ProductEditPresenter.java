package com.floatapplication.product.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.floatapplication.FloatApplication;
import com.floatapplication.R;
import com.floatapplication.config.ConfigStore;
import com.floatapplication.hypermedia.Hypermedia;
import com.floatapplication.hypermedia.HypermediaInterface;
import com.floatapplication.image.activity.ImageScrollActivity;
import com.floatapplication.product.adapter.ProductEditImagePagerAdapter;
import com.floatapplication.product.models.Product;
import com.floatapplication.product.models.ProductCreator;
import com.floatapplication.product.util.ProductEditSubmissionLogic.ProductEditSubmissionListener;
import com.floatapplication.image.activity.CustomZoomViewPagerActivity;
import com.floatapplication.util.GlobalHandler;
import com.floatapplication.util.PhotoHandler;
import com.floatapplication.util.UploadImageHandler;
import com.floatapplication.util.Util;
import com.simplexorg.customviews.adapter.SimpleImageItemAdapter;
import com.simplexorg.customviews.adapter.SimpleImageItemAdapter.ImageItemsListener;
import com.simplexorg.customviews.adapter.StatusMediator;
import com.simplexorg.customviews.util.VUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;

import static android.app.Activity.RESULT_OK;

/**
 * Created by william on 2018-06-13.
 * Presents the product editing to the user.
 */

public class ProductEditPresenter
        implements
        StatusMediator,
        OnClickListener,
        OnFocusChangeListener,
        ImageItemsListener,
        ProductEditSubmissionListener {

    private static final String TAG = ProductEditPresenter.class.getSimpleName();

    @BindView(R.id.view_pager)
    ViewPager mViewPager;
    @BindView(R.id.tab_dots)
    TabLayout mTabDots;
    @BindView(R.id.product_name)
    EditText mProductName;
    @BindView(R.id.product_price)
    EditText mProductPrice;
    @BindView(R.id.product_description)
    EditText mProductDescription;
    @BindView(R.id.product_categories_grid_layout)
    GridLayout mProductCategories;
    @BindView(R.id.root)
    View mRootView;
    @BindView(R.id.photo_button)
    Button mTakePhotoButton;
    @BindView(R.id.image_button)
    Button mUploadImageButton;
    @BindView(R.id.recycler_view)
    RecyclerView mProductImageItemsRecyclerView;
    @BindView(R.id.toolbar_progress_bar)
    ProgressBar mProgressBar;

    private final int REQUEST_IMAGE_SCROLL = 100;

    private MenuItem mDoneButton;

    private Product mProduct;
    private ProductEditImagePagerAdapter mProductEditImagePagerAdapter;
    private ProductCreationCategoryItemPresenter mProductCategoriesPresenter;
    private PhotoHandler mPhotoHandler;
    private UploadImageHandler mUploadImageHandler;
    private Activity mActivity;
    private SimpleImageItemAdapter mProductImageItemAdapter;

    public ProductEditPresenter(Product product) {
        mProduct = product;
    }

    public void bind(Activity activity, PhotoHandler photoHandler, ProductEditImagePagerAdapter adapter,
                     ProductCreationCategoryItemPresenter categoryItemPresenter,
                     SimpleImageItemAdapter productImageItemAdapter, UploadImageHandler uploadImageHandler) {
        mActivity = activity;
        mPhotoHandler = photoHandler;
        mUploadImageHandler = uploadImageHandler;
        mProductCategoriesPresenter = categoryItemPresenter;
        mProductEditImagePagerAdapter = adapter;
        mProductImageItemAdapter = productImageItemAdapter;
        mProductImageItemAdapter.setImageItemsListener(this);
    }

    /**
     * Should call bind and ButterKnife bind before calling this.
     */
    public void present() {
        mRootView.setOnClickListener(this);

        setupEditField(mProductName);
        setupEditField(mProductPrice);
        setupEditField(mProductDescription);

        setupViewPager();
        setupBasicProductInfo();
        getProductInfo();
        setupPhotoAndImageUploadListeners();
        setupProductImageItemsRecyclerView();

        mProductEditImagePagerAdapter.getPresenter().setStatusMediator(this);
    }

    @Override
    public boolean shouldTrackStatus(Observer observer) {
        if (observer == mProductEditImagePagerAdapter) {
            return mProductImageItemAdapter.getMainImagePath() == null;
        }
        return observer == mProductImageItemAdapter.getStatusObserver() &&
                !mProductEditImagePagerAdapter.getPresenter().hasMainImage();
    }

    @Override
    public void requestToTrackStatus(Observer observer) {
        if (observer == mProductEditImagePagerAdapter.getPresenter()) {
            Observer obs = mProductImageItemAdapter.getStatusObserver();
            if (obs != null) {
                obs.update(Observer.CLEAR_STATUS);
            }
        } else if (observer == mProductImageItemAdapter.getStatusObserver()) {
            mProductEditImagePagerAdapter.getPresenter().update(Observer.CLEAR_STATUS);
        }
    }

    private void setupProductImageItemsRecyclerView() {
        LinearLayoutManager llm = new LinearLayoutManager(mProductImageItemsRecyclerView.getContext());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        mProductImageItemsRecyclerView.setLayoutManager(llm);
        mProductImageItemsRecyclerView.setAdapter(mProductImageItemAdapter);
    }

    private void setupPhotoAndImageUploadListeners() {
        PackageManager pm = mActivity.getPackageManager();
        mTakePhotoButton.setTag(R.id.tag0, pm.hasSystemFeature(PackageManager.FEATURE_CAMERA));
        mTakePhotoButton.setOnClickListener(this);
        mUploadImageButton.setOnClickListener(this);
    }

    private void setupEditField(EditText editText) {
        editText.setCursorVisible(false);
        editText.setOnClickListener(this);
        editText.setOnFocusChangeListener(this);
    }

    private void setupViewPager() {
        mProductEditImagePagerAdapter.setOnClickListener(this);
        mViewPager.setAdapter(mProductEditImagePagerAdapter);
        mTabDots.setupWithViewPager(mViewPager);
    }

    private void setupBasicProductInfo() {
        Log.d(TAG, "setupBasicProductInfo");
        if (!"null".equals(mProduct.getProductMainImageFile())) {
            mProductEditImagePagerAdapter.addImageUrl(Util.getInstance().constructImageUrl(mProduct.getProductMainImageFile()));
        } else {
            mViewPager.setVisibility(View.GONE);
            mTabDots.setVisibility(View.GONE);
        }
        mProductName.setText(mProduct.getProductName());
        mProductPrice.setText(mProduct.getProductPrice());
    }

    private void getProductInfo() {
        Map<String, String> vMap = new HashMap<>();
        vMap.put("shop_id", mProduct.getProductShopId());
        vMap.put("product_id", mProduct.getProductId());
        FloatApplication.getHypermediaInterface()
                .follow(Hypermedia.GET, HypermediaInterface.PRODUCT_DETAILS, vMap, null,
                        this::onProductInfoReceived,
                        VolleyError::printStackTrace);
    }

    private void onProductInfoReceived(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            mProduct.augmentProductInfo(jsonObject.getJSONObject("product"));
            mProductEditImagePagerAdapter.addImageUrls(obtainProductImageUrlList());
            mProductDescription.setText(mProduct.getProductDescription());
            mProductCategoriesPresenter.bind(mProductCategories,
                    mProductCategories.getResources().getStringArray(R.array.product_categories_array));
            mProductCategoriesPresenter.present();
            mProductCategoriesPresenter.selectCategory(mProduct.getProductCategory());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private List<String> obtainProductImageUrlList() {
        List<String> urls = new ArrayList<>();
        for (String imageFile : mProduct.getProductImageFiles()) {
            urls.add(Util.getInstance().constructImageUrl(imageFile));
        }
        return urls;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.product_name:
                mProductName.setCursorVisible(true);
                break;
            case R.id.product_price:
                mProductPrice.setCursorVisible(true);
                break;
            case R.id.product_description:
                mProductDescription.setCursorVisible(true);
                break;
            case R.id.photo_button:
            case R.id.image_button: {
                if (mProductEditImagePagerAdapter.getCount() + mProductImageItemAdapter.getItemCount()
                        < ConfigStore.getInstance().getMaxProductImageUploads(mActivity)) {
                    if (id == R.id.photo_button) {
                        onTakePhotoButtonPressed(view);
                    } else {
                        mUploadImageHandler.dispatchUploadImageIntent(mActivity);
                    }
                } else {
                    Toast.makeText(mActivity,
                            mActivity.getString(R.string.image_limit_reached), Toast.LENGTH_SHORT).show();
                }
                break;
            }
            case R.id.image: {
                Intent intent = new Intent(mActivity, CustomZoomViewPagerActivity.class);
                intent.putExtra(VUtil.EXTRA_IMAGE_DATA, mProductEditImagePagerAdapter
                        .getPresenter().getImageDataList());
                intent.putExtra(VUtil.EXTRA_TRANSITION_NAME, mProductEditImagePagerAdapter
                        .getPresenter().getTransitionName(mViewPager.getCurrentItem()));

                ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(mActivity, view, view.getTransitionName());
                mActivity.startActivity(intent, optionsCompat.toBundle());
            }
        }
        if (!(view instanceof EditText)) {
            clearEditTextFocus();
        }
    }

    private void onTakePhotoButtonPressed(View view) {
        boolean hasCamera = (boolean) view.getTag(R.id.tag0);
        if (hasCamera) {
            mPhotoHandler.dispatchTakePhotoIntent(mActivity);
        } else {
            Context context = mActivity.getApplicationContext();
            Toast.makeText(context,
                    context.getString(R.string.camera_not_available), Toast.LENGTH_SHORT).show();
        }
    }

    private void clearEditTextFocus() {
        // One view must hold focus so set the root view to have focus.
        mRootView.requestFocus();
        mProductName.clearFocus();
        mProductPrice.clearFocus();
        mProductDescription.clearFocus();
        InputMethodManager imm = (InputMethodManager) mProductName.getContext()
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(mRootView.getWindowToken(), 0);
            mProductName.setCursorVisible(false);
            mProductPrice.setCursorVisible(false);
            mProductDescription.setCursorVisible(false);
        }
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (hasFocus && view instanceof EditText) {
            ((EditText) view).setCursorVisible(true);
        }
    }

    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case UploadImageHandler.REQUEST_READ_EXTERNAL_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mUploadImageHandler.dispatchUploadImageIntent(mActivity);
                }
            }
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PhotoHandler.REQUEST_IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                String currentPhotoPath = mPhotoHandler.getCurrentPhotoPath();
                onCapturedPhoto(currentPhotoPath);
            }
        } else if (requestCode == UploadImageHandler.REQUEST_IMAGE_GALLERY) {
            if (resultCode == RESULT_OK) {
                Uri selectedImage = data.getData();
                onObtainedUploadImage(selectedImage);
            }
        } else if (requestCode == REQUEST_IMAGE_SCROLL) {
            if (resultCode == RESULT_OK) {
                List<String> imagePaths = data.getStringArrayListExtra(ImageScrollActivity.IMAGE_PATHS);
                for (String imagePath : imagePaths) {
                    mProductImageItemAdapter.removeImagePath(imagePath);
                }
            }
        }
    }

    private void onCapturedPhoto(String photoPath) {
        if (photoPath == null) {
            return;
        }
        Log.d(TAG, "Photo path: " + photoPath);
        mProductImageItemAdapter.addImagePath(photoPath);
    }

    private void onObtainedUploadImage(Uri selectedImage) {
        if (selectedImage == null) {
            return;
        }
        String path = GlobalHandler.getInstance().getAbsolutePathFromURI(mActivity, selectedImage);
        Log.d(TAG, "Upload image path: " + path);
        if (mProductImageItemAdapter.containsImagePath(path)) {
            Toast.makeText(mActivity, R.string.image_already_added, Toast.LENGTH_SHORT).show();
        } else {
            mProductImageItemAdapter.addImagePath(path);
        }
    }

    public void submit(MenuItem doneButton, ProductEditSubmissionLogic logic) {
        Log.d(TAG, "submit");
        mDoneButton = doneButton;
        mDoneButton.setVisible(false);
        mProgressBar.setVisibility(View.VISIBLE);

        ProductCreator productCreator = new ProductCreator();
        productCreator.setProductName(mProductName.getText().toString().trim());
        productCreator.setProductPrice(mProductPrice.getText().toString().trim());
        productCreator.setProductCategory(mProductCategoriesPresenter.getSelectedCategory());
        productCreator.setProductDescription(mProductDescription.getText().toString().trim());

        Set<String> imageDeleteSet = mProductEditImagePagerAdapter.getPresenter().getImageDeleteSet();
        String mainImageUrl = mProductEditImagePagerAdapter.getPresenter().getMainImageUrl();
        List<String> imagePaths = mProductImageItemAdapter.getImagePaths();
        String mainImagePath = mProductImageItemAdapter.getMainImagePath();
        imagePaths.remove(mainImagePath);

        logic.setSubmissionListener(this);
        logic.submit(mProduct, productCreator, imageDeleteSet, mainImageUrl, imagePaths, mainImagePath);
    }

    @Override
    public void onSubmitSuccess() {
        mProgressBar.setVisibility(View.GONE);
        mDoneButton.setVisible(true);
        Context context = mActivity.getApplicationContext();
        Toast.makeText(context,
                context.getString(R.string.submitted), Toast.LENGTH_SHORT).show();
        Intent data = new Intent();
        data.putExtra("product_id", mProduct.getProductId());
        mActivity.setResult(RESULT_OK, data);
        mActivity.finish();
    }

    @Override
    public void onSubmitFailure() {
        mProgressBar.setVisibility(View.GONE);
        mDoneButton.setVisible(true);
        Context context = mActivity.getApplicationContext();
        Toast.makeText(context,
                context.getString(R.string.server_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onImageCountChange(int itemCount) {

    }

    @Override
    public void onImageClick(String imagePath) {
        Intent intent = new Intent(mActivity, ImageScrollActivity.class);
        intent.putStringArrayListExtra(ImageScrollActivity.IMAGE_PATHS,
                new ArrayList<>(mProductImageItemAdapter.getImagePaths()));
        intent.putExtra(ImageScrollActivity.SELECTED_IMAGE_PATH, imagePath);
        mActivity.startActivityForResult(intent, REQUEST_IMAGE_SCROLL);
    }
}
